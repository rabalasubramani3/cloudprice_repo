class CodeValue {
    constructor(code, value) {
        this.code = code;
        this.value = value;
    }

    toWeb() {
        let json = this.toJSON();
        return json;
    }
}
module.exports = CodeValue;