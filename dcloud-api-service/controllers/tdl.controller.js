const tdlmodel = require('../models').TDL;
const tdlviewmodel = require('../models').TDLView;
const reftypemodel = require('../models').ReferenceType;
const refvaluemodel = require('../models').ReferenceValue;
const tdlforecastmodel = require('../models').TDLPeriodForecastView;
const popmodel = require('../models').PoP;
const CodeValue = require('../beans/codevalue');
const models = require("../models");
// const Op = models.sequelize.Op;
const authService = require('../services/auth.service');
const {
    to,
    ReE,
    ReS
} = require('../services/util.service');

const get = async (req, res) => {
    [err, tdls] = await to(tdlmodel.findAll());

    if (err) return ReE(res, 'error retrieving all TDLs');

    let tdls_json = []
    for (let i in tdls) {
        let tdl = tdls[i];
        let company_info = tdl.toWeb();
        tdls_json.push(company_info);
    }

    return ReS(res, {
        tdls: tdls_json
    });
}
module.exports.get = get;

const getTDL = async (req, res) => {
    let tdl_id = req.params.tdl_id;

    [err, tdl] = await to(tdlmodel.findOne({
        where: {
            tdl_id: tdl_id
        }
    }));

    if (err) return ReE(res, 'error getting TDL Information');

    return ReS(res, {
        tdl: tdl.toWeb()
    });
}
module.exports.getTDL = getTDL;

const getTDLViewInfo = async (req, res) => {
    let tdl_id = req.params.tdl_id;

    [err, tdlview] = await to(tdlviewmodel.findOne({
        where: {
            tdl_id: tdl_id
        }
    }));

    if (err) return ReE(res, 'error getting TDLView Information');

    return ReS(res, {
        tdlview: tdlview.toWeb()
    });
}
module.exports.getTDLViewInfo = getTDLViewInfo;

const getTDLStatuses = async (req, res) => {
    [err, reftype] = await to(reftypemodel.findOne({
        where: {
            TP_REF_TYPE_NM: 'TDL_STATUS'
        }
    }));

    if (err) return ReE(res, 'error getting Reference Type');

    if (reftype) {
        [err, reftypes] = await to(refvaluemodel.findAll({
            where: {
                TP_REF_TYPE_ID: reftype.TP_REF_TYPE_ID
            }
        }));

        let codeValues = [];

        for (let i in reftypes) {
            let refCode = reftypes[i].TP_REF_CODE;
            let refValue = reftypes[i].TP_REF_VALUE;
            let codeValue = new CodeValue(refCode, refValue);
            codeValues.push(codeValue);
        }
        return ReS(res, {
            status: JSON.parse(JSON.stringify(codeValues))
        });
    }

    return ReE(res, 'error getting TDL Statuses');
}
module.exports.getTDLStatuses = getTDLStatuses;

const getTDLTypes = async (req, res) => {
    [err, reftype] = await to(reftypemodel.findOne({
        where: {
            TP_REF_TYPE_NM: 'TDL_TYPE'
        }
    }));

    if (err) return ReE(res, 'error getting Reference Type');

    if (reftype) {
        [err, reftypes] = await to(refvaluemodel.findAll({
            where: {
                TP_REF_TYPE_ID: reftype.TP_REF_TYPE_ID
            }
        }));

        let codeValues = [];

        for (let i in reftypes) {
            let refCode = reftypes[i].TP_REF_CODE;
            let refValue = reftypes[i].TP_REF_VALUE;
            let codeValue = new CodeValue(refCode, refValue);
            codeValues.push(codeValue);
        }
        return ReS(res, {
            status: JSON.parse(JSON.stringify(codeValues))
        });
    }

    return ReE(res, 'error getting TDL Types');
}
module.exports.getTDLTypes = getTDLTypes;

const getCLINs = async (req, res) => {
    [err, reftype] = await to(reftypemodel.findOne({
        where: {
            TP_REF_TYPE_NM: 'CLIN'
        }
    }));

    if (err) return ReE(res, 'error getting Reference Type');

    if (reftype) {
        [err, reftypes] = await to(refvaluemodel.findAll({
            where: {
                TP_REF_TYPE_ID: reftype.TP_REF_TYPE_ID
            }
        }));

        let codeValues = [];

        for (let i in reftypes) {
            let refCode = reftypes[i].TP_REF_CODE;
            let refValue = reftypes[i].TP_REF_VALUE;
            let codeValue = new CodeValue(refCode, refValue);
            codeValues.push(codeValue);
        }
        return ReS(res, {
            status: JSON.parse(JSON.stringify(codeValues))
        });
    }

    return ReE(res, 'error getting TDL CLINs');
}
module.exports.getCLINs = getCLINs;

const getTDLCategories = async (req, res) => {
    [err, reftype] = await to(reftypemodel.findOne({
        where: {
            TP_REF_TYPE_NM: 'TDL_CATEGORY'
        }
    }));

    if (err) return ReE(res, 'error getting Reference Type');

    if (reftype) {
        [err, reftypes] = await to(refvaluemodel.findAll({
            where: {
                TP_REF_TYPE_ID: reftype.TP_REF_TYPE_ID
            }
        }));

        let codeValues = [];

        for (let i in reftypes) {
            let refCode = reftypes[i].TP_REF_CODE;
            let refValue = reftypes[i].TP_REF_VALUE;
            let codeValue = new CodeValue(refCode, refValue);
            codeValues.push(codeValue);
        }
        return ReS(res, {
            status: JSON.parse(JSON.stringify(codeValues))
        });
    }

    return ReE(res, 'error getting TDL Categories');
}
module.exports.getTDLCategories = getTDLCategories;

const getPopList = async (req, res) => {
    [err, pops] = await to(popmodel.findAll({
        order: [
            ['name', 'DESC']
        ]
    }));

    if (err) return ReE(res, 'error retrieving all PoPs');

    let pops_json = []
    for (let i in pops) {
        let pop = pops[i];
        let pop_info = pop.toWeb();
        pops_json.push(pop_info);
    }

    return ReS(res, {
        pops: pops_json
    });
}
module.exports.getPopList = getPopList;


const getTDLForecastMatrixFromDate = async (req, res) => {
    const query = 'CALL sp_getTDLPlannedHoursFromWeekEnding(:wkending, :popCode)';

    [err, planhours] = await to(models.sequelize.query(query, {
            replacements: {
                wkending: '2018-10-27',
                popCode: 'T03'
            },
            type: models.sequelize.QueryTypes.SELECT,
            model: tdlforecastmodel
        })
        .then((response) => {
            return response[0];
        })
    );

    if (err) return ReE(res, 'error retrieving all TDL Forecast Matrix');

    let forecastmatrix_json = []
    for (let i in planhours) {
        forecastmatrix_json.push(planhours[i].toWeb());
    }

    return ReS(res, {
        forecast: forecastmatrix_json
    });
}
module.exports.getTDLForecastMatrixFromDate = getTDLForecastMatrixFromDate;

const getTDLOnSearchCriteria = async (req, res) => {
    // req.body.searchFilter
    // req.body.employeeID
    // req.body.tdlCategories
    // req.body.taskOrderFilter

    // findTDLByProjectManagerAndCategoryAndTaskOrder
    [err, tdls] = await to(tdlviewmodel.findAll({
        where: {
            [models.sequelize.Op.or]: [{
                pm_employee_id: req.body.employeeID
            }, {
                allpm: 'Y'
            }],
            category: {
                [models.sequelize.Op.in]: req.body.tdlCategories
            },
            POP_CODE: req.body.taskOrderFilter
        }
    }));

    // [err, tdls] = await to(tdlviewmodel.findTDLByProjectManagerAndCategoryAndTaskOrder(req.body.employeeID, req.body.tdlCategories, req.body.taskOrderFilter));

    if (err) return ReE(res, 'error retrieving all TDLs based on search criteria');

    let tdls_json = []
    for (let i in tdls) {
        tdls_json.push(tdls[i].toWeb());
    }

    return ReS(res, {
        results: tdls_json
    });
}
module.exports.getTDLOnSearchCriteria = getTDLOnSearchCriteria;