const prodgroupmodel = require('../models').ProductGroup;
const prodgroupviewmodel = require('../models').ProductGroupView;
const prodgroupitemmodel = require('../models').ProductGroupItem;
const { to, ReE, ReS } = require('../services/util.service');
const models = require("../models");
const R = require('ramda');
const _ = require('lodash');

async function updateOrCreate (model, where, newItem) {
    // First try to find the record
   const foundItem = await model.findOne({where});
   if (!foundItem) {
        // Item not found, create a new one
        const item = await model.create(newItem)
        return  {item, created: true};
    }
    // Found an item, update it
    const item = await model.update(newItem, {where});
    return {item, created: false};
}

const getAllProductGroups = async function(req, res){
    [err, prodgroups] = await to(prodgroupviewmodel.findAll());

    if (err) return ReE(res, 'error retrieving all Product Groups');

    let prodgroups_json = []
    for (let i in prodgroups) {
        let prodgroup = prodgroups[i];
        let prodgroup_info = prodgroup.toWeb();
        prodgroups_json.push(prodgroup_info);
    }

    return ReS(res, {
        prodgroups: prodgroups_json
    });
}
module.exports.getAllProductGroups = getAllProductGroups;

const getAllProductGroupItems = async function(req, res){
    let prodgroup_id = req.params.prodgroup_id;

    [err, prodgroupitems] = await to(prodgroupitemmodel.findAll
        ({
            where: {
                productGroupId: prodgroup_id
            }
        }));

    if (err) return ReE(res, 'error retrieving all Product Group Items');

    let prodgroupitems_json = []
    for (let i in prodgroupitems) {
        let prodgroupitem = prodgroupitems[i];
        let prodgroupitem_info = prodgroupitem.toWeb();
        prodgroupitems_json.push(prodgroupitem_info);
    }

    return ReS(res, {
        prodgroupitems: prodgroupitems_json
    });
}
module.exports.getAllProductGroupItems = getAllProductGroupItems;

const createProductGroup = async function(req, res){
    let err;

    let prodgroup_info = req.body;

    console.log(prodgroup_info);

    [err, productgroup] = await to(prodgroupmodel.create(prodgroup_info));
    if(err) return ReE(res, err, 422);

    console.log("Product Group Id = " + productgroup.productGroupId);

    let prodgroup_json = productgroup.toWeb();

    return ReS(res, {productgroup: prodgroup_json}, 201);
}
module.exports.createProductGroup = createProductGroup;


const updateProductGroup = async function(req, res){
    let err, productgroup;

    let prodgroup_info = req.body;

    console.log(req.body);
    console.log(req.params.prodgroup_id);

    [err, productgroup] = await to(prodgroupmodel.update(
                {
                    productGroupName: prodgroup_info.productGroupName,
                    masterProductId: prodgroup_info.masterProductId
                },
                { where: {productGroupId: req.params.prodgroup_id}}
            ));
    if(err) return ReE(res, err, 422);

    // console.log("Product Group Id = " + productgroup.productGroupId);
    [err, productgroup] = await to(prodgroupmodel.findOne({
        where: {
            productGroupId: req.params.prodgroup_id
        }
    }));

    let prodgroup_json = productgroup.toWeb();

    return ReS(res, {productgroup: prodgroup_json}, 201);
}
module.exports.updateProductGroup = updateProductGroup;


const createProductGroupItems = async function(req, res){
    let err, prodgroup_id;

    let prodgroupitems_info = req.body;

    console.log(prodgroupitems_info);

    _.forEach(prodgroupitems_info, function(productgroupitem){
        prodgroup_id = productgroupitem.productGroupId;
    });


    [err, delproductgrouptems] = await to(prodgroupitemmodel.destroy({
        where: {
            productGroupId: prodgroup_id
        }
    }));

    if(err) return ReE(res, 'error occured trying to delete the product group items');

    [err, pgitems] = await to(prodgroupitemmodel.bulkCreate(prodgroupitems_info));
    if(err) return ReE(res, err, 422);

    [err, pgitems] = await to(prodgroupitemmodel.findAll({
        where: {
            productGroupId: prodgroup_id
        }
    }));


    let prodgroupitems_json = []
    for (let i in pgitems) {
        prodgroupitems_json.push(pgitems[i].toWeb());
    }

    return ReS(res, {productgroupitems: prodgroupitems_json}, 201);
}
module.exports.createProductGroupItems = createProductGroupItems;




