const bomawsmodel = require('../models').BOMAWS;
const { to, ReE, ReS } = require('../services/util.service');
const models = require("../models");
const R = require('ramda');
const _ = require('lodash');

const getAll = async (req, res) => {
    [err, boms] = await to(bomawsmodel.findAll());

    if (err) return ReE(res, 'error getting Lookup Values');

    let boms_json = []
    for (let i in boms) {
        boms_json.push(boms[i].toWeb());
    }

    return ReS(res, {
        boms: boms_json
    });
}
module.exports.getAll = getAll;

const getAllAWSBOMItems = async (req, res) => {
    let bom_id = req.params.bom_id;

    [err, boms] = await to(bomawsmodel.findAll({
        where: {
            bomId: bom_id
        }
    }));

    let boms_json = []
    for (let i in boms) {
        boms_json.push(boms[i].toWeb());
    }

    return ReS(res, {awsbomitems: boms_json}, 201);;
}
module.exports.getAllAWSBOMItems = getAllAWSBOMItems;

const getBOM = async (req, res) => {
    let bom_aws_id = req.params.bom_aws_id;

    [err, bom] = await to(bomawsmodel.findOne({
        where: {
            bomAWSId: bom_aws_id
        }
    }));

    if (err) return ReE(res, 'error getting BOM');

    if (_.isNil(bom)) return ReE(res, 'error getting BOM');

    let boms_json = bom.toWeb();

    return ReS(res, {
        boms: boms_json
    });
}
module.exports.getBOM = getBOM;

const create = async function(req, res){
    let err, bom_id;

    let bom_aws_items = req.body;

    _.forEach(bom_aws_items, function(awsitem){
        bom_id = awsitem.bomId;
        console.log(awsitem);
    });

    [err, delawsitems] = await to(bomawsmodel.destroy({
        where: {
            bomId: bom_id
        }
    }));

    if(err) return ReE(res, 'error occured trying to delete the aws items');



    [err, bom] = await to(bomawsmodel.bulkCreate(bom_aws_items));
    if(err) return ReE(res, err, 422);

    [err, boms] = await to(bomawsmodel.findAll({
        where: {
            bomId: bom_id
        }
    }));

    let boms_json = []
    for (let i in boms) {
        boms_json.push(boms[i].toWeb());
    }

    return ReS(res, {boms: boms_json}, 201);
}
module.exports.create = create;


const update = async function(req, res){
    let err, bom;

    let bom_id = req.params.bom_aws_id;
    let bom_info = req.body;

    var condition = { where: { bomAWSId: bom_aws_id }};

    [err, lookup] = await to(bomawsmodel.update(bom_info, condition));
    if(err) return ReE(res, err, 422);

    [err, bom] = await to(bommodel.findOne({
        where: {
            bomId: bom_id
        }
    }));
    let bom_json = bom.toWeb();

    return ReS(res, {bom: bom_json}, 201);
}
module.exports.update = update;

// const removeLookupEntity = async function(req, res){
//     let lookup, err;
//     let lookup_id = req.params.lookup_id;

//     [err, lookup] = await to(refvaluemodel.findOne({
//         where: {
//             lookupId: lookup_id
//         }
//     }));

//     [err, lookup] = await to(lookup.destroy());
//     if(err) return ReE(res, 'error occured trying to delete the Lookup');

//     let lookup_json = lookup.toWeb();

//     return ReS(res, {lookup: lookup_json},   204);
// }
// module.exports.removeLookupEntity = removeLookupEntity;


