const bomcotsgroupmodel = require('../models').BOMCOTSGroup;
const bomcotsitemmodel = require('../models').BOMCOTSItem;
const { to, ReE, ReS } = require('../services/util.service');
const models = require("../models");
const R = require('ramda');
const _ = require('lodash');

const getAll = async (req, res) => {
    [err, boms] = await to(bomcotsitemmodel.findAll());

    if (err) return ReE(res, 'error getting BOM COTS Values');

    let boms_json = []
    for (let i in boms) {
        boms_json.push(boms[i].toWeb());
    }

    return ReS(res, {
        boms: boms_json
    });
}
module.exports.getAll = getAll;

const getAllCOTSBOMItems = async (req, res) => {
    let bom_id = req.params.bom_id;

    [err, boms] = await to(bomcotsitemmodel.findAll({
        where: {
            bomId: bom_id
        }
    }));

    let boms_json = []
    for (let i in boms) {
        boms_json.push(boms[i].toWeb());
    }

    return ReS(res, {cotsbomitems: boms_json}, 201);;
}
module.exports.getAllCOTSBOMItems = getAllCOTSBOMItems;


const getBOM = async (req, res) => {
    let bom_cots_id = req.params.bom_cots_id;

    [err, bom] = await to(bomcotsitemmodel.findOne({
        where: {
            bomCOTSId: bom_cots_id
        }
    }));

    if (err) return ReE(res, 'error getting BOM');

    if (R.isNil(bom)) return ReE(res, 'error getting BOM');

    let boms_json = bom.toWeb();

    return ReS(res, {
        boms: boms_json
    });
}
module.exports.getBOM = getBOM;

// const create = async function(req, res){
//     let err, bom;

//     let bom_info = req.body;

//     [err, bom] = await to(bomcotsitemmodel.create(bom_info));
//     if(err) return ReE(res, err, 422);

//     let bom_json = bom.toWeb();

//     return ReS(res, {lookup: bom_json}, 201);
// }
// module.exports.create = create;


const create = async function(req, res){
    let err, bom_id;

    let bom_cots_items = req.body;

    _.forEach(bom_cots_items, function(cotsitem){
        bom_id = cotsitem.bomId;
        console.log(cotsitem);
    });

    [err, delcotsitems] = await to(bomcotsitemmodel.destroy({
        where: {
            bomId: bom_id
        }
    }));

    if(err) return ReE(res, 'error occured trying to delete the cots items');


    [err, bom] = await to(bomcotsitemmodel.bulkCreate(bom_cots_items));
    if(err) return ReE(res, err, 422);

    [err, boms] = await to(bomcotsitemmodel.findAll({
        where: {
            bomId: bom_id
        }
    }));

    let boms_json = []
    for (let i in boms) {
        boms_json.push(boms[i].toWeb());
    }

    return ReS(res, {boms: boms_json}, 201);
}
module.exports.create = create;

const update = async function(req, res){
    let err, bom;

    let bom_cots_id = req.params.bom_cots_id;
    let bom_info = req.body;

    var condition = { where: { bomCOTSId: bom_cots_id }};

    [err, lookup] = await to(bomcotsitemmodel.update(bom_info, condition));
    if(err) return ReE(res, err, 422);

    [err, bom] = await to(bomcotsitemmodel.findOne({
        where: {
            bomCOTSId: bom_cots_id
        }
    }));
    let bom_json = bom.toWeb();

    return ReS(res, {bom: bom_json}, 201);
}
module.exports.update = update;

// const removeLookupEntity = async function(req, res){
//     let lookup, err;
//     let lookup_id = req.params.lookup_id;

//     [err, lookup] = await to(refvaluemodel.findOne({
//         where: {
//             lookupId: lookup_id
//         }
//     }));

//     [err, lookup] = await to(lookup.destroy());
//     if(err) return ReE(res, 'error occured trying to delete the Lookup');

//     let lookup_json = lookup.toWeb();

//     return ReS(res, {lookup: lookup_json},   204);
// }
// module.exports.removeLookupEntity = removeLookupEntity;


