const bommodel = require('../models').BOM;
const { to, ReE, ReS } = require('../services/util.service');
const models = require("../models");
const R = require('ramda');
const _ = require('lodash');

const getAll = async (req, res) => {
    [err, boms] = await to(bommodel.findAll());

    if (err) return ReE(res, 'error getting Lookup Values');

    let boms_json = []
    for (let i in boms) {
        boms_json.push(boms[i].toWeb());
    }

    return ReS(res, {
        boms: boms_json
    });
}
module.exports.getAll = getAll;

const getBOM = async (req, res) => {
    let bom_id = req.params.bom_id;

    [err, bom] = await to(bommodel.findOne({
        where: {
            bomId: bom_id
        }
    }));

    if (err) return ReE(res, 'error getting BOM');

    if (_.isNil(bom)) return ReE(res, 'error getting BOM');

    let boms_json = bom.toWeb();

    return ReS(res, {
        boms: boms_json
    });
}
module.exports.getBOM = getBOM;

// const getAllReferenceTypes = async (req, res) => {
//     [err, refTypes] = await to(reftypemodel.findAll({order: [['name', 'ASC']]}));

//     if (err) return ReE(res, 'error getting Reference Types');

//     let rt_json = []
//     for (let i in refTypes) {
//         let rt = refTypes[i];
//         let rt_info = rt.toWeb();
//         rt_json.push(rt_info);
//     }

//     return ReS(res, {
//         refTypes: rt_json
//     });
// }
// module.exports.getAllReferenceTypes = getAllReferenceTypes;

// const getAllByRefType = async (req, res) => {
//     let refTypeId = req.params.reftype_id;

//     [err, refValues] = await to(refvaluemodel.findAll({
//         where: {
//             refTypeId: refTypeId
//         },
//         order: [['refValue', 'ASC']]
//     }));

//     if (err) return ReE(res, 'error getting Reference Values For A Given Ref Type');

//     let rv_json = []
//     for (let i in refValues) {
//         rv_json.push(refValues[i].toWeb());
//     }
//     return ReS(res, {
//         refValues: rv_json
//     });
// }
// module.exports.getAllByRefType = getAllByRefType;

const create = async function(req, res){
    let err, bom;

    let bom_info = req.body;

    [err, bom] = await to(bommodel.create(bom_info));
    if(err) return ReE(res, err, 422);

    let bom_json = bom.toWeb();

    return ReS(res, {lookup: bom_json}, 201);
}
module.exports.create = create;


const update = async function(req, res){
    let err, bom;

    let bom_id = req.params.bom_id;
    let bom_info = req.body;

    var condition = { where: { bomId: bom_id }};

    [err, lookup] = await to(bommodel.update(bom_info, condition));
    if(err) return ReE(res, err, 422);

    [err, bom] = await to(bommodel.findOne({
        where: {
            bomId: bom_id
        }
    }));
    let bom_json = bom.toWeb();

    return ReS(res, {bom: bom_json}, 201);
}
module.exports.update = update;

const deleteBOM = async function(req, res){
    let bom, err;
    let bom_id = req.params.bom_id;

    [err, bom] = await to(bommodel.findOne(
        {
            where: {
                bomId: bom_id
            }
        }
    ));

    [err, bom] = await to(bommodel.destroy(
        {
            where: {
                bomId: bom_id
            }
        }        
    ));
    if(err) return ReE(res, 'error occured trying to delete the BOM');

    let bom_json = bom.toWeb();

    return ReS(res, {bom: bom_json},   204);
}
module.exports.deleteBOM = deleteBOM;


