const resourcemodel = require('../models').Resource;
const {
    to,
    ReE,
    ReS
} = require('../services/util.service');


const getResourceByEmployeeId = async (req, res) => {
    let employeeId = req.params.employee_id;

    [err, resource] = await to(resourcemodel.findOne({
        where: {
            employeeID: employeeId
        }
    }));

    if (err) return ReE(res, 'error getting Resource Info');

    return ReS(res, {
        resource: resource.toWeb()
    });
}
module.exports.getResourceByEmployeeId = getResourceByEmployeeId;