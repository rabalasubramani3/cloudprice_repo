const tdlmodel = require('../models').TDL;
const tdlviewmodel = require('../models').TDLView;
const reftypemodel = require('../models').ReferenceType;
const refvaluemodel = require('../models').ReferenceValue;
const tdlforecastmodel = require('../models').TDLPeriodForecastView;
const popmodel = require('../models').PoP;
const CodeValue = require('../beans/codevalue');
const wklresplanhoursmodel = require('../models').WeeklyResourcePlanHours;
const models = require("../models");
// const Op = models.sequelize.Op;
const authService = require('../services/auth.service');
const {
    to,
    ReE,
    ReS
} = require('../services/util.service');


const getWeeklyResourcePlanTDLHoursForWeekEnding = async (req, res) => {
    const query = 'CALL sp_getWeeklyResourcePlanTDLHours(:wkending, :resourceid)';

    [err, planhours] = await to(models.sequelize.query(query, {
            replacements: {
                wkending: req.body.weekEnding,
                resourceid: req.body.resourceID
            },
            type: models.sequelize.QueryTypes.SELECT,
            mapToModel: true,
            model: wklresplanhoursmodel
        })
        .then((response) => {
            return response[0];
        })
    );

    if (err) return ReE(res, 'error retrieving all TDL Forecast Matrix');

    let planhours_json = []
    for (let i in planhours) {
        planhours_json.push(planhours[i].toWeb());
    }

    return ReS(res, {
        forecast: planhours_json
    });
}
module.exports.getWeeklyResourcePlanTDLHoursForWeekEnding = getWeeklyResourcePlanTDLHoursForWeekEnding;