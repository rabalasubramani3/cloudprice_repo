const reftypemodel = require('../models').ReferenceType;
const refvaluemodel = require('../models').ReferenceValue;
const { to, ReE, ReS } = require('../services/util.service');
const models = require("../models");
const R = require('ramda');
const _ = require('lodash');

const getAllLookupValues = async (req, res) => {
    [err, lookupvalues] = await to(refvaluemodel.findAll());

    if (err) return ReE(res, 'error getting Lookup Values');

    let lvs_json = []
    for (let i in lookupvalues) {
        let lv = lookupvalues[i];
        let lv_info = lv.toWeb();
        lvs_json.push(lv_info);
    }

    return ReS(res, {
        lookupvalues: lvs_json
    });
}
module.exports.getAllLookupValues = getAllLookupValues;

const getAllReferenceTypes = async (req, res) => {
    [err, refTypes] = await to(reftypemodel.findAll({order: [['name', 'ASC']]}));

    if (err) return ReE(res, 'error getting Reference Types');

    let rt_json = []
    for (let i in refTypes) {
        let rt = refTypes[i];
        let rt_info = rt.toWeb();
        rt_json.push(rt_info);
    }

    return ReS(res, {
        refTypes: rt_json
    });
}
module.exports.getAllReferenceTypes = getAllReferenceTypes;

const getAllByRefTypeId = async (req, res) => {
    let refTypeId = req.params.reftype_id;

    [err, refValues] = await to(refvaluemodel.findAll({
        where: {
            refTypeId: refTypeId
        },
        order: [['refValue', 'ASC']]
    }));

    if (err) return ReE(res, 'error getting Reference Values For A Given Ref Type');

    let rv_json = []
    for (let i in refValues) {
        rv_json.push(refValues[i].toWeb());
    }
    return ReS(res, {
        refValues: rv_json
    });
}
module.exports.getAllByRefTypeId = getAllByRefTypeId;


const getAllByRefTypeName = async (req, res) => {
    let refTypeName = req.params.reftype_name;

    [err, reftype] = await to(reftypemodel.findOne({
        where: {
            name: refTypeName
        }
    }));

    if (err) {
        return ReE(res, 'error getting Reference Type For A Given Ref Name');
    }

    if ( _.isNil(reftype) ) {
        return ReE(res, 'error getting Reference Type Id For A Given Ref Name');
    }

    [err, refValues] = await to(refvaluemodel.findAll({
        where: {
            refTypeId: reftype.referenceTypeId
        },
        order: [['refValue', 'ASC']]
    }));

    if (err) return ReE(res, 'error getting Reference Values For A Given Ref Type');

    let rv_json = []
    for (let i in refValues) {
        rv_json.push(refValues[i].toWeb());
    }
    return ReS(res, {
        refValues: rv_json
    });
}
module.exports.getAllByRefTypeName = getAllByRefTypeName;

const createLookupEntity = async function(req, res){
    let err, lookup;

    let lookup_info = req.body;

    [err, lookup] = await to(refvaluemodel.create(lookup_info));
    if(err) return ReE(res, err, 422);

    let lookup_json = lookup.toWeb();

    return ReS(res, {lookup: lookup_json}, 201);
}
module.exports.createLookupEntity = createLookupEntity;


const updateLookupEntity = async function(req, res){
    let err, lookup;

    let lookup_id = req.params.lookup_id;
    let lookup_info = req.body;

    var condition = { where: { lookupId: lookup_id }};

    [err, lookup] = await to(refvaluemodel.update(lookup_info, condition));
    if(err) return ReE(res, err, 422);

    [err, lookup] = await to(refvaluemodel.findOne({
        where: {
            lookupId: lookup_id
        }
    }));
    let lookup_json = lookup.toWeb();

    return ReS(res, {lookup: lookup_json}, 201);
}
module.exports.updateLookupEntity = updateLookupEntity;

const removeLookupEntity = async function(req, res){
    let lookup, err;
    let lookup_id = req.params.lookup_id;

    [err, lookup] = await to(refvaluemodel.findOne({
        where: {
            lookupId: lookup_id
        }
    }));

    [err, lookup] = await to(lookup.destroy());
    if(err) return ReE(res, 'error occured trying to delete the Lookup');

    let lookup_json = lookup.toWeb();

    return ReS(res, {lookup: lookup_json},   204);
}
module.exports.removeLookupEntity = removeLookupEntity;


