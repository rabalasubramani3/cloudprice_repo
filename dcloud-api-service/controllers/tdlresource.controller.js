const tdlresourceplanmodel = require('../models').ResourceTDLPlanView;
const tdlresourceviewmodel = require('../models').TDLResourcesView;
const models = require("../models");
const {
    to,
    ReE,
    ReS
} = require('../services/util.service');


const getResourceTDLPlanViewForGivenWeekEnding = async (req, res) => {
    [err, restdls] = await to(tdlresourceplanmodel.findAll({
        where: {
            week_ending: req.body.weekEnding,
            resource_id: req.body.resID
        }
    }));

    if (err) return ReE(res, 'error retrieving all resource TDLs');

    let restdl_json = []
    for (let i in restdls) {
        restdl_json.push(restdls[i].toWeb());
    }

    return ReS(res, {
        restdls: restdl_json
    });
}
module.exports.getResourceTDLPlanViewForGivenWeekEnding = getResourceTDLPlanViewForGivenWeekEnding;

const getResourceTDLPlanViewForGivenWeekEndingRange = async (req, res) => {
    [err, restdls] = await to(tdlresourceplanmodel.findAll({
        where: {
            week_ending: {
                [models.sequelize.Op.between]: [req.body.startweekEnding, req.body.endweekEnding]
            },
            resource_id: req.body.resourceID
        }


    }));

    if (err) return ReE(res, 'error retrieving all resource TDLs');

    let restdl_json = []
    for (let i in restdls) {
        restdl_json.push(restdls[i].toWeb());
    }

    return ReS(res, {
        restdls: restdl_json
    });
}
module.exports.getResourceTDLPlanViewForGivenWeekEndingRange = getResourceTDLPlanViewForGivenWeekEndingRange;


const getResourceTDLs = async (req, res) => {
    let resourceId = req.params.resourceId;

    [err, restdls] = await to(tdlresourceviewmodel.findAll({
        where: {
            resource_id: resourceId
        }
    }));

    if (err) return ReE(res, 'error retrieving all resource TDLs');

    let restdl_json = []
    for (let i in restdls) {
        restdl_json.push(restdls[i].toWeb());
    }

    return ReS(res, {
        restdls: restdl_json
    });
}
module.exports.getResourceTDLs = getResourceTDLs;