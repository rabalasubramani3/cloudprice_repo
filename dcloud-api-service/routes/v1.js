const express = require('express');
const router = express.Router();

const UserController = require('../controllers/user.controller');
const HomeController = require('../controllers/home.controller');
const TDLController = require('../controllers/tdl.controller');
const TDLPlanHoursController = require('../controllers/tdlplanhours.controller');
const ResourceController = require('../controllers/resource.controller');
const TDLResourceController = require('../controllers/tdlresource.controller');
const LookupController = require('../controllers/lookup.controller');
const BOMController = require('../controllers/bom.controller');
const BOMAWSController = require('../controllers/bom-aws.controller');
const BOMCOTSController = require('../controllers/bom-cots.controller');
const ProductGroupController = require('../controllers/productgroup.controller');

const custom = require('./../middleware/custom');

const passport = require('passport');
const path = require('path');


require('./../middleware/passport')(passport)
/* GET home page. */
router.get('/', function (req, res, next) {
  res.json({
    status: "success",
    message: "DCloud API v1",
    data: {
      "version_number": "v1.0.0"
    }
  })
});

// Routes
// TDL
router.get('/tdl/:tdl_id', passport.authenticate('jwt', {
  session: false
}), TDLController.getTDL); // R
router.get('/tdl/view/:tdl_id', passport.authenticate('jwt', {
  session: false
}), TDLController.getTDLViewInfo); // R
router.post('/tdl/search', passport.authenticate('jwt', {
  session: false
}), TDLController.getTDLOnSearchCriteria);
router.get('/tdl/status/list', passport.authenticate('jwt', {
  session: false
}), TDLController.getTDLStatuses);
router.get('/tdl/type/list',TDLController.getTDLTypes);
router.get('/tdl/clin/list', passport.authenticate('jwt', {
  session: false
}), TDLController.getCLINs);
router.get('/tdl/cat/list', passport.authenticate('jwt', {
  session: false
}), TDLController.getTDLCategories);
router.get('/tdl/pop/list', passport.authenticate('jwt', {
  session: false
}), TDLController.getPopList);
router.post('/tdl/forecast', passport.authenticate('jwt', {
  session: false
}), TDLController.getTDLForecastMatrixFromDate);

// Plan Hours
router.post('/planhrs/week/wktdlres', TDLPlanHoursController.getWeeklyResourcePlanTDLHoursForWeekEnding);

router.post('/tdlresource/plan/tdl', TDLResourceController.getResourceTDLPlanViewForGivenWeekEnding);
router.post('/tdlresource/plan/tdl/range', TDLResourceController.getResourceTDLPlanViewForGivenWeekEndingRange);
router.get('/tdlresource/tdl/:resourceId', TDLResourceController.getResourceTDLs);

router.get('/v1/resource/employee/:employee_id', ResourceController.getResourceByEmployeeId);

router.post('/users', UserController.create); // C
router.get('/users', passport.authenticate('jwt', {
  session: false
}), UserController.get); // R
router.put('/users', passport.authenticate('jwt', {
  session: false
}), UserController.update); // U
router.delete('/users', passport.authenticate('jwt', {
  session: false
}), UserController.remove); // D
router.post('/users/login', UserController.login);

router.get('/dash', passport.authenticate('jwt', {
  session: false
}), HomeController.Dashboard)

router.get('/bom', BOMController.getAll)
router.get('/bom/:bom_id', BOMController.getBOM)
router.post('/bom', BOMController.create)
router.put('/bom/:bom_id', BOMController.update)
router.put('/bom/:bom_id', BOMController.deleteBOM)

router.post('/bom/aws', BOMAWSController.create)
router.get('/bom/aws/:bom_id', BOMAWSController.getAllAWSBOMItems)
router.put('/bom/aws/:bom_id', BOMAWSController.update)

router.post('/bom/cots', BOMCOTSController.create)
router.get('/bom/cots/:bom_id', BOMCOTSController.getAllCOTSBOMItems)
router.put('/bom/cots/:bom_id', BOMCOTSController.update)

router.get('/prodgroup/product', ProductGroupController.getAllProductGroups)
router.get('/prodgroup/productitems/:prodgroup_id', ProductGroupController.getAllProductGroupItems)
router.post('/prodgroup/product', ProductGroupController.createProductGroup)
router.put('/prodgroup/product/:prodgroup_id', ProductGroupController.updateProductGroup)
router.post('/prodgroup/productitems', ProductGroupController.createProductGroupItems)

router.get('/lookup', LookupController.getAllLookupValues)
router.get('/lookup/reftype', LookupController.getAllReferenceTypes)
router.get('/lookup/reftype/:reftype_id', LookupController.getAllByRefTypeId)
router.get('/lookup/reftypename/:reftype_name', LookupController.getAllByRefTypeName)

router.put('/lookup/:lookup_id', LookupController.updateLookupEntity)
router.post('/lookup', LookupController.createLookupEntity)
router.delete('/lookup/:lookup_id', LookupController.removeLookupEntity); // D

//********* API DOCUMENTATION **********
router.use('/docs/api.json', express.static(path.join(__dirname, '/../public/v1/documentation/api.json')));
router.use('/docs', express.static(path.join(__dirname, '/../public/v1/documentation/dist')));
module.exports = router;