/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tp_tdl_resource', {
    tdl_resource_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    tdl_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'tp_tdl',
        key: 'tdl_id'
      }
    },
    resource_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    lcat_cd: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    period1: {
      type: DataTypes.STRING(4),
      allowNull: true
    },
    hours: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    period2: {
      type: DataTypes.STRING(4),
      allowNull: true
    },
    hours2: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    skills: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    created_dt: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    modified_dt: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    modified_by: {
      type: DataTypes.STRING(45),
      allowNull: true
    }
  }, {
    tableName: 'tp_tdl_resource'
  });
};
