/* jshint indent: 2 */

module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define('ReferenceValue', {
    lookupId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: "TP_REF_TYPE_VALUE_ID"
    },
    refTypeId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: "TP_REF_TYPE_ID"
    },
    refCode: {
      type: DataTypes.STRING(4),
      allowNull: true,
      field: "TP_REF_CODE"
    },
    refValue: {
      type: DataTypes.STRING(255),
      allowNull: false,
      field: "TP_REF_VALUE"
    },
    abbr: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: "TP_REF_VALUE_ABBR"
    },
    bronze: {
      type: DataTypes.STRING(1),
      allowNull: true,
      field: "BRONZE"
    },
    silver: {
      type: DataTypes.STRING(1),
      allowNull: true,
      field: "SILVER"
    },
    gold: {
      type: DataTypes.STRING(1),
      allowNull: true,
      field: "GOLD"
    },
    platinum: {
      type: DataTypes.STRING(1),
      allowNull: true,
      field: "PLATINUM"
    },
    CREATED_BY: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    CREATED_DT: {
      type: DataTypes.DATE,
      allowNull: true
    },
    MODIFIED_BY: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    MODIFIED_DT: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    timestamps: false,
    tableName: 'dc_ref_value'
  });


  Model.prototype.toWeb = function (pw) {
    let json = this.toJSON();
    return json;
  };


  return Model;
};