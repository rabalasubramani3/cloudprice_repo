/* jshint indent: 2 */
const {
  TE,
  to
} = require('../services/util.service');
module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define('BOMCOTSGroup', {
    productGroupId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: "cots_group_id"
    },
    enivronment: {
      type: DataTypes.STRING(45),
      allowNull: false,
      field: "environment"
    },
    productGroupName: {
      type: DataTypes.STRING(128),
      allowNull: false,
      field: "product_group_name"
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    created_dt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    modified_by: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    modified_dt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    timestamps: false,
    tableName: 'dc_bom_cots_group'
  });

  Model.prototype.toWeb = function (pw) {
    let json = this.toJSON();
    return json;
  };

  return Model;
};