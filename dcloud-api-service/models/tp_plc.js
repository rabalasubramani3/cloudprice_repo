/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('tp_plc', {
    plc_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    plc_code: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    plc_description: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    base_rate: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    op1_rate: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    op2_rate: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    op3_rate: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    op4_rate: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    op5_rate: {
      type: DataTypes.DECIMAL,
      allowNull: true
    }
  }, {
    timestamps: false,
    tableName: 'tp_plc'
  });
};