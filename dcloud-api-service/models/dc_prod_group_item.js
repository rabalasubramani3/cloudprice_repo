/* jshint indent: 2 */
const {
  TE,
  to
} = require('../services/util.service');
module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define('ProductGroupItem', {
    productGroupItemId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: "prod_group_item_id"
    },
    productGroupId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: "product_group_id"
    },
    category: {
      type: DataTypes.STRING(45),
      allowNull: true,
      field: "category"
    },
    component: {
      type: DataTypes.STRING(128),
      allowNull: true,
      field: "component"
    },
    licensing: {
      type: DataTypes.STRING(45),
      allowNull: true,
      field: "licensing"
    },
    size: {
      type: DataTypes.STRING(128),
      allowNull: true,
      field: "size"
    },
    osdb: {
      type: DataTypes.STRING(45),
      allowNull: true,
      field: "osdb"
    },
    qty: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: "qty"
    },
    comments: {
      type: DataTypes.STRING(255),
      allowNull: true,
      field: "comments"
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    created_dt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    modified_by: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    modified_dt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    timestamps: false,
    tableName: 'dc_prod_group_item'
  });

  Model.prototype.toWeb = function (pw) {
    let json = this.toJSON();
    return json;
  };

  return Model;
};