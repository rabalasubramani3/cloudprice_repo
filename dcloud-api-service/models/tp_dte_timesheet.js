/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tp_dte_timesheet', {
    dte_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    employee_name: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    wbs_code: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    project_name: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    work_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    week_ending: {
      type: DataTypes.DATE,
      allowNull: true
    },
    hours: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    employee_id: {
      type: DataTypes.STRING(45),
      allowNull: true
    }
  }, {
    tableName: 'tp_dte_timesheet'
  });
};
