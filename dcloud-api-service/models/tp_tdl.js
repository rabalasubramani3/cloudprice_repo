/* jshint indent: 2 */
const {
  TE,
  to
} = require('../services/util.service');
module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define('TDL', {
    tdl_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    pm_resource_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    notes: {
      type: DataTypes.STRING(4000),
      allowNull: true
    },
    category: {
      type: DataTypes.STRING(4),
      allowNull: true
    },
    type: {
      type: DataTypes.STRING(4),
      allowNull: true
    },
    clin: {
      type: DataTypes.STRING(4),
      allowNull: true
    },
    wbs_code: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    plan_start: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    plan_end: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    act_start: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    act_end: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    budget_cost: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    budget_hrs: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    plan_cost: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    plan_hrs: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    act_cost: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    act_hrs: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ceiling_amount: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    status: {
      type: DataTypes.STRING(4),
      allowNull: true
    },
    approved: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    allpm: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    pop_code: {
      type: DataTypes.STRING(4),
      allowNull: true
    },
    created_dt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    modified_dt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    modified_by: {
      type: DataTypes.STRING(45),
      allowNull: true
    }
  }, {
    timestamps: false,
    tableName: 'tp_tdl'
  });

  Model.prototype.toWeb = function (pw) {
    let json = this.toJSON();
    return json;
  };

  return Model;
};