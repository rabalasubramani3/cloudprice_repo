/* jshint indent: 2 */
const {
    TE,
    to
  } = require('../services/util.service');

module.exports = (sequelize, DataTypes) => {
    var Model = sequelize.define('ProductGroupView', {
        productGroupId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            defaultValue: '0',
            field: "prod_group_id"
        },
        masterProductId: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            field: "master_prod_id"
        },
        masterProductName: {
            type: DataTypes.STRING(255),
            allowNull: true,
            field: "master_prod_name"
        },
        productGroupName: {
            type: DataTypes.STRING(128),
            allowNull: true,
            field: "prod_group_name"
        }
    }, {
        timestamps: false,
        tableName: 'vw_dc_prod_group'
    });

    Model.prototype.toWeb = function (pw) {
        let json = this.toJSON();
        return json;
    };

    return Model;
};
