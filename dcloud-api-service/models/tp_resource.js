/* jshint indent: 2 */

module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define('Resource', {
    resourceID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'resource_id'
    },
    name: {
      type: DataTypes.STRING(45),
      allowNull: false,
      field: 'name'
    },
    lookupName: {
      type: DataTypes.STRING(128),
      allowNull: true,
      field: 'lookup_name'
    },
    displayName: {
      type: DataTypes.STRING(128),
      allowNull: true,
      field: 'display_name'
    },
    email: {
      type: DataTypes.STRING(45),
      allowNull: false,
      unique: true,
      field: 'email'
    },
    phone: {
      type: DataTypes.STRING(45),
      allowNull: true,
      field: 'phone'
    },
    employeeID: {
      type: DataTypes.STRING(45),
      allowNull: false,
      unique: true,
      field: 'employee_id'
    },
    lcatCode: {
      type: DataTypes.STRING(6),
      allowNull: true,
      field: 'lcat_cd'
    },
    typeCode: {
      type: DataTypes.STRING(4),
      allowNull: false,
      field: 'type_cd'
    },
    teamCode: {
      type: DataTypes.STRING(4),
      allowNull: true,
      field: 'team_cd'
    },
    rate: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'rate'
    },
    active: {
      type: DataTypes.CHAR(1),
      allowNull: true,
      defaultValue: 'Y',
      field: 'active'
    },
    start_dt: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    maxHours: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      field: 'max_hrs'
    },
    created_dt: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    modified_dt: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    modified_by: {
      type: DataTypes.STRING(45),
      allowNull: true
    }
  }, {
    timestamps: false,
    tableName: 'tp_resource'
  });

  Model.prototype.toWeb = function (pw) {
    let json = this.toJSON();
    return json;
  };

  return Model;
};