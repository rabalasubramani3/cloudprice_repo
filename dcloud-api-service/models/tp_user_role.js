/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tp_user_role', {
    user_role_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    user_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    role_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'tp_user_role'
  });
};
