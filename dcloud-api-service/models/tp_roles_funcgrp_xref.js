/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tp_roles_funcgrp_xref', {
    ROLE_ID: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    FUNCGRP_ID: {
      type: DataTypes.DECIMAL,
      allowNull: false
    }
  }, {
    tableName: 'tp_roles_funcgrp_xref'
  });
};
