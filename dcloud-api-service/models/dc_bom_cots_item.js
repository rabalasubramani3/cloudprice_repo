/* jshint indent: 2 */
const {
  TE,
  to
} = require('../services/util.service');
module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define('BOMCOTSItem', {
    productItemId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: "bom_cots_item_id"
    },
    productGroupId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: "product_group_id"
    },
    productGroupName: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: "product_group_name"
    },
    bomId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: "bom_id"
    },
    environment: {
      type: DataTypes.STRING(45),
      allowNull: false,
      field: "environment"
    },
    category: {
      type: DataTypes.STRING(128),
      allowNull: false,
      field: "category"
    },
    product: {
      type: DataTypes.STRING(128),
      allowNull: false,
      field: "product"
    },
    licensing: {
      type: DataTypes.STRING(128),
      allowNull: false,
      field: "licensing"
    },
    size: {
      type: DataTypes.STRING(128),
      allowNull: false,
      field: "size"
    },
    osdb: {
      type: DataTypes.STRING(45),
      allowNull: true,
      field: "osdb"
    },
    qty: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: "qty"
    },
    comments: {
      type: DataTypes.STRING(255),
      allowNull: true,
      field: "comments"
    },
    listprice: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: "listprice"
    },
    discount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: "discount"
    },
    discountedPrice: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: "discounted_price"
    },
    utilpercent: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: "utilpercent"
    },
    terms: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: "terms"
    },
    annualcost: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: "annualcost"
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    created_dt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    modified_by: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    modified_dt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    timestamps: false,
    tableName: 'dc_bom_cots_item'
  });

  Model.prototype.toWeb = function (pw) {
    let json = this.toJSON();
    return json;
  };

  return Model;
};