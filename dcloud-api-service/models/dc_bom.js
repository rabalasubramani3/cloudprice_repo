/* jshint indent: 2 */
const {
  TE,
  to
} = require('../services/util.service');
module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define('BOM', {
    bomId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: "bom_id"
    },
    uuid: {
      type: DataTypes.STRING(128),
      allowNull: true,
      field: "uuid"
    },
    referenceNum: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: "reference_num"
    },
    step: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: "step"
    },
    status: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: "status"
    },
    project: {
      type: DataTypes.STRING(128),
      allowNull: true,
      field: "project"
    },
    client: {
      type: DataTypes.STRING(128),
      allowNull: true,
      field: "client"
    },
    sponsor: {
      type: DataTypes.STRING(128),
      allowNull: true,
      field: "sponsor"
    },
    wbsCode: {
      type: DataTypes.STRING(45),
      allowNull: true,
      field: "wbs_code"
    },
    package: {
      type: DataTypes.STRING(10),
      allowNull: true,
      field: "package"
    },
    pii: {
      type: DataTypes.STRING(1),
      allowNull: true,
      field: "pii"
    },
    phi: {
      type: DataTypes.STRING(1),
      allowNull: true,
      field: "phi"
    },
    hippa: {
      type: DataTypes.STRING(1),
      allowNull: true,
      field: "hippa"
    },
    irs: {
      type: DataTypes.STRING(1),
      allowNull: true,
      field: "irs"
    },
    fedramp: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: "fedramp"
    },
    notes: {
      type: DataTypes.STRING(4000),
      allowNull: true
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    created_dt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    modified_by: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    modified_dt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    timestamps: false,
    tableName: 'dc_bom'
  });

  Model.prototype.toWeb = function (pw) {
    let json = this.toJSON();
    return json;
  };

  return Model;
};