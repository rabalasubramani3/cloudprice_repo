/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tp_resource_privs_xref', {
    FUNCGRP_ID: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0',
      primaryKey: true
    },
    PRIVILEGE_ID: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    RESOURCE_ID: {
      type: DataTypes.DECIMAL,
      allowNull: true
    }
  }, {
    tableName: 'tp_resource_privs_xref'
  });
};
