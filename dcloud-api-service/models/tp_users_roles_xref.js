/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tp_users_roles_xref', {
    USER_ROLE_XREF_ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    USER_ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ROLE_ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    }
  }, {
    tableName: 'tp_users_roles_xref'
  });
};
