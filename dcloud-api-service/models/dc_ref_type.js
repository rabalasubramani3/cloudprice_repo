/* jshint indent: 2 */

module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define('ReferenceType', {
    referenceTypeId: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: '0',
      primaryKey: true,
      field:"TP_REF_TYPE_ID"
    },
    name: {
      type: DataTypes.STRING(80),
      allowNull: false,
      unique: true,
      field:"TP_REF_TYPE_NM"
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true,
      field:"TP_REF_TYPE_DESC"
    },
    CREATED_BY: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field:"CREATED_BY"
    },
    CREATED_DT: {
      type: DataTypes.DATE,
      allowNull: true,
      field:"CREATED_DT"
    },
    MODIFIED_BY: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field:"MODIFIED_BY"
    },
    MODIFIED_DT: {
      type: DataTypes.DATE,
      allowNull: true,
      field:"MODIFIED_DT"
    }
  }, {
    timestamps: false,
    tableName: 'dc_ref_type'
  });

  Model.prototype.toWeb = function (pw) {
    let json = this.toJSON();
    return json;
  };

  return Model;
};