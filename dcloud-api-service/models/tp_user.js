/* jshint indent: 2 */
const bcrypt = require('bcrypt');
const bcrypt_p = require('bcrypt-promise');
const jwt = require('jsonwebtoken');
const {
  TE,
  to
} = require('../services/util.service');
const CONFIG = require('../config/config');

module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define('User', {
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'USER_ID'
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: true,
      field: 'EMAIL'
    },
    enabled: {
      type: DataTypes.STRING(1),
      allowNull: false,
      field: 'ENABLED'
    },
    firstName: {
      type: DataTypes.STRING(40),
      allowNull: true,
      field: 'FIRST_NAME'
    },
    lastName: {
      type: DataTypes.STRING(40),
      allowNull: true,
      field: 'LAST_NAME'
    },
    employeeId: {
      type: DataTypes.STRING(10),
      allowNull: false,
      field: 'EMPLOYEE_ID'
    },
    password: {
      type: DataTypes.STRING(60),
      allowNull: true,
      field: 'PASSWORD'
    },
    lastLoginDt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'LAST_LOGIN_DT'
    },
    endDt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'END_DATE'
    },
    SECRET: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    ENABLED_MFA: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    MFA_REGISTRATION_REQ: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    CREATED_BY: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    CREATED_DT: {
      type: DataTypes.DATE,
      allowNull: true
    },
    MODIFIED_BY: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    MODIFIED_DT: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    timestamps: false,
    tableName: 'tp_user'
  });

  Model.beforeSave(async (user, options) => {
    let err;
    if (user.changed('password')) {
      let [err, salt] = await to(bcrypt.genSalt(10));
      if (err) TE(err.message, true);

      [err, hash] = await to(bcrypt.hash(user.password, salt));
      if (err) TE(err.message, true);

      user.password = hash;
    }
  });

  Model.prototype.comparePassword = async function (pw) {
    let err, pass
    if (!this.password) TE('password not set');

    if (pw === this.password) {
      pass = true;
    } else {
      pass = false;
    }
    // [err, pass] = await to(bcrypt_p.compare(pw, this.password));
    // if (err) TE(err);

    if (!pass) TE('invalid password');

    return this;
  }

  Model.prototype.getJWT = function () {
    let expiration_time = parseInt(CONFIG.jwt_expiration);
    return "Bearer " + jwt.sign({
      user_id: this.userId
    }, CONFIG.jwt_encryption, {
      expiresIn: expiration_time
    });
  };

  Model.prototype.toWeb = function (pw) {
    let json = this.toJSON();
    return json;
  };

  return Model;
};