/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tp_tdl_timesheet', {
    timesheet_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    tdl_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    resource_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    timesheet_hrs_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    created_dt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    created_by: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    modified_dt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    modified_by: {
      type: DataTypes.STRING(45),
      allowNull: true
    }
  }, {
    tableName: 'tp_tdl_timesheet'
  });
};
