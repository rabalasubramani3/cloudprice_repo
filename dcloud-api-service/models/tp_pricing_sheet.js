/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tp_pricing_sheet', {
    pricing_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    lcat_cd: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    taskarea_cd: {
      type: DataTypes.STRING(4),
      allowNull: true
    },
    proposed_rate: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    proposed_hours: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    fte: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    pop_cd: {
      type: DataTypes.STRING(4),
      allowNull: true
    }
  }, {
    tableName: 'tp_pricing_sheet'
  });
};
