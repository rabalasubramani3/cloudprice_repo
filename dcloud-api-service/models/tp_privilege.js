/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tp_privilege', {
    PRIVILEGE_ID: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      primaryKey: true
    },
    PRIVILEGE_NAME: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    CREATED_BY: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    CREATED_DT: {
      type: DataTypes.DATE,
      allowNull: true
    },
    MODIFIED_BY: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    MODIFIED_DT: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'tp_privilege'
  });
};
