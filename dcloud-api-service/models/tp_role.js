/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tp_role', {
    ROLE_ID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    ROLE_NAME: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    ROLE_DESCRIPTION: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    CREATED_BY: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    CREATED_DT: {
      type: DataTypes.DATE,
      allowNull: true
    },
    MODIFIED_BY: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    MODIFIED_DT: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'tp_role'
  });
};
