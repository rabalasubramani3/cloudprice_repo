import { LoginPage } from './login.po';
import { AppPage } from './app.po';
import { browser, protractor, element, ProtractorBrowser } from 'protractor';
import { SearchPage } from './search.po';

describe('TRLM Multi-User - Login page', () => {
  let page: LoginPage;
  let appPage: AppPage;
  let searchPage: SearchPage;
  let originalTimeout;

  const userCredentials = {
    username: 'raghubala',
    password: '********'
  };

  beforeEach(() => {
    browser.waitForAngularEnabled(false);
    page = new LoginPage();
    appPage = new AppPage();
    searchPage = new SearchPage();
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;
  });

  // it('when user trying to login with wrong credentials he should stay on “login” page', () => {
  //   page.navigateTo();
  //   page.fillCredentials(userCredentials);
  //   expect(page.getPageTitleText()).toEqual('Login');
  //   expect(page.getErrorMessage()).toEqual('Username or password is incorrect');
  // });

  it('Import csv file test', () => {
      page.navigateTo();
      page.fillCredentials(userCredentials);
      let EC = protractor.ExpectedConditions;
      browser.wait(EC.visibilityOf(searchPage.getAddProductListFromFile()), 5000, "Unable to get this page");
      searchPage.getAddProductListFromFile().click();
      browser.wait(EC.visibilityOf(searchPage.getSelectFileName()), 5000, "Unable to get this page");
      searchPage.enterFileName("C:/Users/rabalasubramani3/Desktop/trlm_sample_data_4.csv");
      searchPage.getImportFileButton().click();
      browser.sleep(10000);
  });

  afterEach (() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });
});
