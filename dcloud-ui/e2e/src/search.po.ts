import { browser, by, element } from 'protractor';

export class SearchPage {
  navigateTo() {
    return browser.get('/app/bom/search');
  }

  getTitleText() {
    return element(by.css('app-root h1')).getText();
  }

  getAddProductListFromFile() {
    return element(by.id('btn-imp-prod'));
  }

  getImportFileButton() {
    return element(by.id('btn-import-file'));
  }

  getSelectFileName(){
    return element(by.id('import-prod-csv')).element(by.id('fileImport'));
  }

  enterFileName(fileName: string){
    return element(by.css('input[type="file"]')).sendKeys(fileName);
  }

}
