import { browser, by, element, protractor } from 'protractor';

declare var jQuery;

export class LoginPage {
  private credentials = {
    username: 'test',
    password: 'test'
  };

  navigateTo() {
    browser.driver.manage().window().maximize();
    return browser.get('/');
  }

  fillCredentials(credentials: any = this.credentials) {
    element(by.css('[name="username"]')).sendKeys(credentials.username);
    element(by.css('[name="password"]')).sendKeys(credentials.password);
    return element(by.css('.btn-primary')).click();
  }

  getPageTitleText() {
    return element(by.css('app-root h2')).getText();
  }

  getModalTitleText() {
    return element(by.css('modal-container h4')).getText();
  }

  sleep(timeout: number){
    return browser.sleep(timeout);
  }

  getErrorMessage() {
    return element(by.css('.alert-danger')).getText();
  }

  getAgreeButton(){
// tslint:disable-next-line: max-line-length
    return element(by.xpath('(.//*[normalize-space(text()) and normalize-space(.)="ANYONE USING THIS SYSTEM EXPRESSLY CONSENTS TO SUCH MONITORING"])[1]/following::button[1]'));
  }

  getSplashModalWindow(){
    return element(by.id('splashModal'));
  }

  acknowledgeSplashscreen(){
    let EC = protractor.ExpectedConditions;
    // let iagreebutton = element(by.css('.modal-content')).element(by.id('btn-iagree'));
    // tslint:disable-next-line: max-line-length
    return browser.wait(EC.visibilityOf(element(by.xpath('(.//*[normalize-space(text()) and normalize-space(.)="ANYONE USING THIS SYSTEM EXPRESSLY CONSENTS TO SUCH MONITORING"])[1]/following::button[1]'))), 5000, 'Unabled to find the button to click');
    // browser.wait(EC.visibilityOf(iagreebutton),5000);
    // return iagreebutton.click();
  }
}
