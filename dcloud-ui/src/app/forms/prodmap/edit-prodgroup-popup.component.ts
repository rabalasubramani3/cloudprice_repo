/*
@author : Deloitte
this is Componentfor adding contact as a popup.
*/
import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewEncapsulation, OnChanges, Input} from '@angular/core';
import { Router } from '@angular/router';
import * as _ from "lodash";
import { isNil } from 'ramda';
import { forkJoin, Observable, Subscription } from 'rxjs';
import { LookupService } from '../../admin/services/lookup.service';
import { BOMCOTSProductItem} from '../models/bom/bom-sheet.model';
import { ProductGroup, ProductGroupItem } from '../models/bom/product-group.model';
import { ProductGroupService } from '../services/productgroup.service';
import { BsModalRef } from 'ngx-bootstrap';

declare var jQuery: any;

@Component({
  // tslint:disable-next-line: component-selector
  selector: "[edit-prodgroup-popup]",
  templateUrl: "./edit-prodgroup-popup.template.html",
  styleUrls: ["./edit-prodgroup-popup.styles.scss"],
  providers: [],
  encapsulation: ViewEncapsulation.None
})

/**
 *
 *
 * @export
 * @class EditAccountPopup
 * @implements {OnInit}
 */
export class EditProductGroupPopup implements OnInit, OnDestroy {
  @Input() model;
  @Output() reloadProductGroups = new EventEmitter<boolean>();

  public lookupitems: Map<string, any> = new Map<string, any>();
  public lookupcategories: any;
  public lookupMasterProducts: any;
  public lookupOperatingSystems: any;
  public lookupSizes: any;
  public lookupLicensing: any;

  public rowCount = 1;
  public gridApi;
  public gridColumnApi;

  public columnDefs;
  public rowData;
  public rowSelection;
  public defaultColDef;
  public editType;

  public router: Router;
  public showErrorFlag: boolean;
  public alerts: Array<Object>;
  public editprodgroup: ProductGroup = new ProductGroup();


  public busySaveProductGroup:Subscription;
  public busyLoadMasterProducts: Subscription;
  public busyLoadLookupCategoriesAndItems: Subscription;
  public busyLoadProductGroupItems: Subscription;
  public busyLoadOperatingSystems: Subscription;
  public busyLoadSizes: Subscription;
  public busyLoadLicensingTerms: Subscription;

  public categorymap = new Map<string, string>();


  constructor(
    router: Router,

    public bsModalRef: BsModalRef,
    private lookupService: LookupService,
    private prodGroupService: ProductGroupService

  ) {
    let that = this;
    this.router = router;

    this.lookupLicensing = [];
    this.lookupOperatingSystems = [];
    this.lookupSizes = [];

    this.alerts = [
      {
        type: "warning",
        msg:
          '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages'
      }
    ];

    this.showErrorFlag = false;

    this.categorymap.set("COMPUTE", "AWS_COMPUTE_COMPONENTS");
    this.categorymap.set("STORAGE", "AWS_STORAGE_COMPONENTS");
    this.categorymap.set("SOFTWARE", "COTS_SOFTWARE");
    this.categorymap.set("SOFTWARE-MAINT", "COTS_SOFTWARE_MAINTENANCE");
    this.categorymap.set("AWS-NATIVE", "AWS_NATIVE_COMPONENTS");

    this.columnDefs = [
      {
        headerName: "Category",
        field: "category",
        width: 150,
        cellEditor: "agRichSelectCellEditor",
        cellEditorParams: function(params) {
          return {
            values: that.lookupcategories
          };
      }
      },
      {
        headerName: "Components",
        field: "component",
        width: 150,
        cellEditor: "agRichSelectCellEditor",
        cellEditorParams: function(params) {
            return {
              values: that.lookupitems.get(params.data.category)
            };
        }
      },
      {
        headerName: "Licensing",
        field: "licensing",
        cellEditor: "agRichSelectCellEditor",
        cellEditorParams: function(params) {
          return {
            values: that.lookupLicensing
          };
        }
      },
      {
        headerName: "Size",
        field: "size",
        cellEditor: "agRichSelectCellEditor",
        cellEditorParams: function(params) {
          return {
            values: that.lookupSizes
          };
        }
      },
      {
        headerName: "Operating System/DB",
        field: "osdb",
        cellEditor: "agRichSelectCellEditor",
        cellEditorParams: function(params) {
          return {
            values: that.lookupOperatingSystems
          };
        }
      },
      {
        headerName: "Qty",
        field: "qty"
      },
      {
        headerName: "Comments",
        field: "comments",
        width: 200,
        cellEditor: "agLargeTextCellEditor"
      }
    ];

    this.editType = "fullRow";
    this.rowSelection = "multiple";
    this.defaultColDef = {
      editable: true,
      resizable: true
    };
  }


  ngOnInit(): void {
    this.showErrorFlag = false;
    this.loadMasterProducts();
    this.loadLookupCategoriesAndItems();
    this.loadOperatingSystems();
    this.loadLicensing();
    this.loadSizes();
    this.populateProductGroupItems();
  }

  ngOnDestroy() {
    if(this.busySaveProductGroup){
      this.busySaveProductGroup.unsubscribe();
    }
    if(this.busyLoadLookupCategoriesAndItems){
      this.busyLoadLookupCategoriesAndItems.unsubscribe();
    }
    if(this.busyLoadMasterProducts){
      this.busyLoadMasterProducts.unsubscribe();
    }
    if(this.busyLoadProductGroupItems){
      this.busyLoadProductGroupItems.unsubscribe();
    }
    if(this.busyLoadLicensingTerms){
      this.busyLoadLicensingTerms.unsubscribe();
    }
    if(this.busyLoadOperatingSystems){
      this.busyLoadOperatingSystems.unsubscribe();
    }
    if(this.busyLoadSizes){
      this.busyLoadSizes.unsubscribe();
    }
  }

  public loadMasterProducts(){
    let that = this;
    this.busyLoadMasterProducts = this.lookupService.getAllByRefTypeName("COTS_PRODUCTS").subscribe(data => {
      if (data) {
        that.lookupMasterProducts = data.refValues;
      }
    }, error => {
      that.showErrorFlag = true;
      that.alerts = [];
      that.alerts.push({ type: 'warning', msg: error });
    });
  }

  public loadOperatingSystems(){
    let that = this;
    this.busyLoadOperatingSystems = this.lookupService.getAllByRefTypeName("AWS_OPERATING_SYSTEMS").subscribe(data => {
      if (data) {
        that.lookupOperatingSystems = _.toArray(_.map(data.refValues, 'refValue'));
      }
    }, error => {
      that.showErrorFlag = true;
      that.alerts = [];
      that.alerts.push({ type: 'warning', msg: error });
    });
  }

  public loadSizes(){
    let that = this;
    this.busyLoadSizes = this.lookupService.getAllByRefTypeName("AWS_COMPONENT_SIZE").subscribe(data => {
      if (data) {
        that.lookupSizes = _.toArray(_.map(data.refValues, 'refValue'));
      }
    }, error => {
      that.showErrorFlag = true;
      that.alerts = [];
      that.alerts.push({ type: 'warning', msg: error });
    });
  }

  public loadLicensing(){
    let that = this;
    this.busyLoadLicensingTerms = this.lookupService.getAllByRefTypeName("COTS_LICENSING").subscribe(data => {
      if (data) {
        that.lookupLicensing = _.toArray(_.map(data.refValues, 'refValue'));
      }
    }, error => {
      that.showErrorFlag = true;
      that.alerts = [];
      that.alerts.push({ type: 'warning', msg: error });
    });
  }


  public loadLookupCategoriesAndItems(){
    let that = this;
    this.lookupcategories = [];
    this.busyLoadLookupCategoriesAndItems = this.lookupService.getAllByRefTypeName("INFRASTRUCTURE_CATEGORY").subscribe(data => {
      if (data) {
        that.lookupcategories = _.toArray(_.map(data.refValues, 'refValue'));
        let apiitems:Observable<any>[] = [];
        _.forEach(that.lookupcategories, function(category){
          apiitems.push(that.lookupService.getAllByRefTypeName(that.categorymap.get(category)));
        });
        forkJoin(apiitems).subscribe(results => {
          let kdx = 0;
          _.forEach(that.lookupcategories, function(category){
            if(!isNil(results[kdx])){
              if((results[kdx].success === false) && (!isNil(results[kdx].error))){
                console.log("error:" + category + ":" + results[kdx].error);
              }else{
                if(results[kdx].refValues.length > 0){
                  that.lookupitems.set(category, _.toArray(_.map(results[kdx].refValues, 'refValue')));
                }else{
                  that.lookupitems.set(category, []);
                }
              }
            }
            kdx++;
          });
        });

      }
    }, error => {
      that.showErrorFlag = true;
      that.alerts = [];
      that.alerts.push({ type: 'warning', msg: error });
    });
  }

  public populateProductGroupItems(){
    let that = this;
    this.busyLoadProductGroupItems = this.prodGroupService.getAllProductGroupItems(this.editprodgroup.productGroupId).subscribe(data => {
      if(!isNil(data)){
        let prodgroupitems = data.prodgroupitems;
        _.forEach(prodgroupitems, function(pgitem){
          let newData: ProductGroupItem = new ProductGroupItem();
          newData.productGroupItemId = pgitem.productGroupItemId;
          newData.productGroupId = pgitem.productGroupId;
          newData.category = pgitem.category;
          newData.component = pgitem.component;
          newData.licensing = pgitem.licensing;
          newData.osdb = pgitem.osdb;
          newData.size = pgitem.size;
          newData.qty = pgitem.qty;
          newData.comments = pgitem.comments;
          that.gridApi.updateRowData({ add: [newData] });
        });

      }
    }, error => {
      that.showErrorFlag = true;
      that.alerts = [];
      that.alerts.push({ type: 'warning', msg: error });
    });
  }

  getRowData() {
    let rowData = [];
    this.gridApi.forEachNode(function(node) {
      rowData.push(node.data);
    });
  }


  clearData() {
    if(!isNil(this.gridApi)){
      this.gridApi.setRowData([]);
    }
  }

  OnMasterProductChanged(event){

  }
  onCellValueChanged(params) {
    let cell:any = params.api.getFocusedCell();
    // if(!isNil(cell)){
    //   if(cell.column.colId === 'category'){
    //     if (params.oldValue !== params.newValue) {
    //       if (params.newValue==='AWS-NATIVE') {
    //         this.loadLookupValues(1);
    //       } else {
    //         this.loadLookupValues(4);
    //       }
    //     }
    //   }
    // }
  }

  onAddProduct() {
    let newItem = this.createNewProductItem();
    let res = this.gridApi.updateRowData({ add: [newItem] });
  }

  onRemoveSelected() {
    let selectedData = this.gridApi.getSelectedRows();
    let res = this.gridApi.updateRowData({ remove: selectedData });
  }

  onGridReady(params){
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  public createProductGroupItems(prodgroup: ProductGroup) {
    this.busySaveProductGroup = this.prodGroupService.saveProductGroupItems(this.editprodgroup).subscribe(data => {
      if (data) {
        this.reset(true);
        // this.reloadProductGroups.emit(true);
      }
    }, error => {
        this.showErrorFlag = true;
        this.alerts = [];
        this.alerts.push({ type: 'warning', msg: error });
    });
  }

  public updateProductGroup(prodgroup: ProductGroup) {
    jQuery('#editproductgroupform').parsley().validate();

    // Toggle to edit mode or navigate to the next screen if validation passes
    if (jQuery('#editproductgroupform').parsley().isValid()) {

      let that = this;

      this.editprodgroup.items = [];

      this.gridApi.forEachNode(function(node) {
        let rowItem: ProductGroupItem = new ProductGroupItem();
        rowItem.productGroupId = node.data.productGroupId;
        rowItem.category = node.data.category;
        rowItem.component = node.data.component;
        rowItem.licensing = node.data.licensing;
        rowItem.osdb = node.data.osdb;
        rowItem.size = node.data.size;
        rowItem.qty = node.data.qty;
        rowItem.comments = node.data.comments;
        that.editprodgroup.items.push(rowItem);
      });

      this.busySaveProductGroup = this.prodGroupService.updateProductGroup(this.editprodgroup).subscribe(data => {
        if (!isNil(data) && !isNil(data.productgroup)) {
          let productGroupId = data.productgroup.productGroupId;
          _.forEach(that.editprodgroup.items, function(item){
            item.productGroupId = productGroupId;
          });
          that.createProductGroupItems(that.editprodgroup);
        }
      }, error => {
          this.showErrorFlag = true;
          this.alerts = [];
          this.alerts.push({ type: 'warning', msg: error });
      });
    }
  }


  // ngOnChanges(changes: SimpleChanges) {
  //   for (let propName in changes) {
  //     if (propName === "model") {
  //       let chg = changes[propName];
  //       if (chg.currentValue) {
  //         this.editprodgroup = Object.assign({}, chg.currentValue);
  //       }
  //     }
  //   }
  // }

  public reset(flag) {
    // jQuery("#edit-productgroup").on("hidden").find("#name").val("");
    jQuery("#editproductgroupform").parsley().reset();
    this.bsModalRef.hide();
    this.showErrorFlag = false;
  }


  public createNewProductItem() {
    let newData: BOMCOTSProductItem = new BOMCOTSProductItem();
    newData.category = "SOFTWARE";
    newData.product = "";
    newData.qty = 1;
    newData.listprice = 35000 + this.rowCount * 17;
    newData.discount = 0.0;
    newData.comments = "US GovCloud (East) | 1 yr Upfront RI (priced per year)";
    this.rowCount++;
    return newData;
  }

  onFirstDataRendered(params){
    this.sizeToFit();
  }

  public sizeToFit() {
    if(!isNil(this.gridApi)){
      this.gridApi.sizeColumnsToFit();
    }
  }

  public  autoSizeAll() {
    let allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(function(column) {
      allColumnIds.push(column.colId);
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);
  }
}
