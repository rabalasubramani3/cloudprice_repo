import { AfterViewInit, Component, OnDestroy, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { GridOptions } from "ag-grid-community";
import { Subscription } from "rxjs";
import { ProductGroup } from '../models/bom/product-group.model';
import { ProductGroupService } from '../services/productgroup.service';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { EditProductGroupPopup } from './edit-prodgroup-popup.component';

declare var jQuery: any;

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'edit-cell',
  template: `<i class="fa fa-edit" (click)="invokeParentMethod()" ></i>`
})
export class EditProductGroupComponent implements ICellRendererAngularComp {
  public params: any;

  agInit(params: any): void {
      this.params = params;
  }

  public invokeParentMethod() {
      this.params.context.componentParent.selectEditProductGroup(this.params.data);
  }

  refresh(): boolean {
      return false;
  }
}

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'delete-cell',
  template: `<i class="fa fa-trash-o txt-gap" (click)="invokeParentMethod()" data-toggle="modal"  title="Delete Product Group"
  data-keyboard="false" data-backdrop="static" data-target="#confirm-popup"></i>`
})
export class DeleteProductGroupComponent implements ICellRendererAngularComp {
  public params: any;

  agInit(params: any): void {
      this.params = params;
  }

  public invokeParentMethod() {
      this.params.context.componentParent.selectDeleteAccount(this.params.data);
  }

  refresh(): boolean {
      return false;
  }
}

// con
@Component({
  // tslint:disable-next-line: component-selector
  selector: "prodgroup-dashboard",
  templateUrl: "./prodgroup-dashboard.template.html",
  styleUrls: ["./prodgroup-dashboard.styles.scss"],
  providers: [ProductGroupService, BsModalService, BsModalRef],
  encapsulation: ViewEncapsulation.None
})
export class ProductGroupDashboardPage implements OnInit, OnDestroy, AfterViewInit {
  public router: Router;
  public productgroups: any[];

  public busyLoadProductGroups: Subscription;
  public busyDeleteProductGroup: Subscription;

  public dropdownData = [];
  public showErrorFlag: boolean;
  public alerts: Array<Object>;
  public effectiveDate: Date;
  public selProductGroup: ProductGroup;

  public showCreateProductGroup: boolean;
  public showEditProductGroup: boolean;
  public showDeleteProductGroup: boolean;

  public delAccountTitle: string = "Delete Account";
  public delAccountMessage: string = "Are you sure you want to delete Account?";
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;

  public gridOptions: GridOptions;
  public bsEditProductsGroupModalRef: BsModalRef;
  public gridApi;
  public gridColumnApi;

  public defaultColDef = {
    sortable: true,
    filter: true,
    headerClass: 'fw-semi-bold'
  };

  public columnDefs = [
    {
      headerName: "",
      field: "value",
      cellRendererFramework: EditProductGroupComponent,
      colId: "params",
      width: 50,
      filter: false
    },
    {
      headerName: 'Master Product',
      field: 'masterProductName',
      cellClass: function(params) {
        return "fw-semi-bold ";
      }
    },
    {
      headerName: 'Product',
      field: 'productGroupName',
    },
    {
      headerName: "",
      field: "value",
      cellRendererFramework: DeleteProductGroupComponent,
      colId: "params",
      width: 50,
      filter: false
    }
];

  constructor(
    router: Router,
    private productGroupService: ProductGroupService,
    private modalService: BsModalService,
    ) {
    this.alerts = [
      {
        type: "success",
        msg:
          '<i class="fa fa-circle text-success"></i><span class="alert-text">N/A</span>'
      }
    ];

    this.router = router;

    this.showCreateProductGroup = false;
    this.showEditProductGroup = false;
    this.showDeleteProductGroup = false;

    this.gridOptions = <GridOptions>{
      context: {
          componentParent: this
      },
      resizable: true,
      rowHeight: 30
    };
  }

  ngOnInit() {
      this.loadAllProductGroups();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  ngOnDestroy() {
    if (this.busyLoadProductGroups) {
      this.busyLoadProductGroups.unsubscribe();
    }
    if (this.busyDeleteProductGroup) {
      this.busyDeleteProductGroup.unsubscribe();
    }
 }

  ngAfterViewInit() {
  }


  private loadAllProductGroups() {
    let that = this;
    this.busyLoadProductGroups = this.productGroupService.getAllProductGroups().subscribe(data => {
      if (data) {
        that.productgroups = data.prodgroups;
        that.gridApi.sizeColumnsToFit();
      }
    }, error => {
      this.showErrorFlag = true;
      this.alerts = [];
      this.alerts.push({ type: 'warning', msg: error });
    });
  }

  public reloadProductGroups(reload: boolean) {
    this.showCreateProductGroup = false;
    this.showEditProductGroup = false;
    if (reload) {
      this.loadAllProductGroups();
    } else {
      this.selProductGroup = new ProductGroup();
    }
  }

  public deleteAccount(id) {
  //   this.busyDeleteProductGroup = this.accountService.deleteAccountById(id).subscribe(data => {
  //     if (data) {
  //       this.reloadAccounts(true);
  //       this.showDeleteAccount = false;
  //       jQuery('#confirm-popup').modal('hide');
  //     }
  //   }, error => {
  //     this.showErrorFlag = true;
  //     this.alerts = [];
  //     this.alerts.push({ type: 'warning', msg: error });
  // });
  }

  public addProductGroup() {
    this.showCreateProductGroup = true;
  }

  public selectEditProductGroup(productgroup) {
    this.showEditProductGroup = true;
    this.selProductGroup = productgroup;
    const initialState = {
      editprodgroup: productgroup
    };
    this.bsEditProductsGroupModalRef = this.modalService.show(EditProductGroupPopup, { initialState, class: 'modal-xl modal-center', animated: true, keyboard: false, backdrop: true, ignoreBackdropClick: true  });
    this.bsEditProductsGroupModalRef.content.closeBtnName = 'Close';
    // let newSubscriber = this.modalService.onHide.subscribe(r=>{
    //    newSubscriber.unsubscribe();
    //    if(this.bsEditProductsGroupModalRef.content.addProductGroupFlag){
    //       let env = this.bsEditProductsGroupModalRef.content.selEnvironment;
    //       let prodgroup =  this.bsEditProductsGroupModalRef.content.selProductGroupInfo;
    //       this.createNewProductGroupItems(env, prodgroup);
    //     }else{
    //       console.log("Action Cancelled");
    //     }
    //  });

  }

  public selectDeleteProductGroup(productgroup){
    this.showDeleteProductGroup = true;
    this.selProductGroup = productgroup;
  }
  public onClickBackButton(){
		this.router.navigate(["/app/bom/search"]);
  }
}

