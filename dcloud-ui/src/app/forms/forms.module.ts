
import { CommonModule, TitleCasePipe } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ModalModule } from 'ngx-bootstrap';
import { LookupService } from '../admin/services/lookup.service';
import { ProfileUpdateComponent } from "../login/components/update/profile-update.component";
import { AuthGuard } from '../login/services/auth-guard.service';
import { SharedModule } from "../shared/shared.module";
import { AddCOTSProductGroupComponent } from './bom-forms/add-cots-prodgroup/add-cots-prodgroup.component';
import { AWSComponentSizesDropdownEditorComponent } from './bom-forms/aws-compsizes-dropdown.component';
import { AWSOperatingSystemsDropdownEditorComponent } from './bom-forms/aws-os-dropdown.component';
import { AWSServicesDropdownEditorComponent } from './bom-forms/aws-services-dropdown.component';
import { BOMApplicantDetailsTabPageComponent } from "./bom-forms/bom-app-details.component";
import { BOMAWSDetailsTabPageComponent, DeleteAWSRowFromGridComponent, EditAWSItemFromGridComponent } from './bom-forms/bom-aws-details.component';
import { BOMCOTSDetailsTabPageComponent } from './bom-forms/bom-cots-details.component';
import { BOMDocumentUploadPage } from './bom-forms/bom-doc-upload.component';
import { BOMManagedServicesTabPageComponent } from './bom-forms/bom-managed-svcs.component';
import { BOMMessageBox } from './bom-forms/bom-msgbox.component';
import { BOMProductDetailsTabPageComponent } from './bom-forms/bom-prod-details.component';
import { BOMReviewSubmitTabPage } from './bom-forms/bom-review-submit.component';
import { BOMServiceDetailsTabPageComponent } from './bom-forms/bom-service-details.component';
import { BOMWizardStepComponent } from "./bom-forms/bom-wizard-step.component";
import { BOMWizardComponent } from "./bom-forms/bom-wizard.component";
import { BulkProductUploadPage } from './bom-forms/bulk-prod-upload.component';
import { COTSProductsDropdownEditorComponent } from './bom-forms/cots-products-dropdown.component';
import { CreateBillOfMaterialsgPageComponent } from "./bom-forms/create-bom.component";
import { ViewBOMFormPageComponent } from './bom-forms/view-bomform.component';
import { CreateProductGroupPopup } from './prodmap/create-prodgroup-popup.component';
import { EditProductGroupPopup } from './prodmap/edit-prodgroup-popup.component';
import { DeleteProductGroupComponent, EditProductGroupComponent, ProductGroupDashboardPage } from './prodmap/prodgroup-dashboard.component';
import { ProductGroupService } from './services/productgroup.service';

export const routes = [
    { path: '', component: CreateBillOfMaterialsgPageComponent, pathMatch: 'full' },
    { path: 'profile', component: ProfileUpdateComponent},
    { path: 'create', component: CreateBillOfMaterialsgPageComponent},
    { path: 'edit/:id', component: CreateBillOfMaterialsgPageComponent},
    { path: 'view/:id', component: ViewBOMFormPageComponent},
    { path: 'prodgroup', component: ProductGroupDashboardPage }
  ];

  @NgModule({
      imports: [ CommonModule, SharedModule, RouterModule.forChild(routes), ModalModule, NgMultiSelectDropDownModule.forRoot() ],
      declarations: [ ProfileUpdateComponent,
        CreateBillOfMaterialsgPageComponent, BOMApplicantDetailsTabPageComponent, BOMProductDetailsTabPageComponent,
        BOMServiceDetailsTabPageComponent, BOMAWSDetailsTabPageComponent, BOMCOTSDetailsTabPageComponent, BulkProductUploadPage,
        BOMDocumentUploadPage, BOMReviewSubmitTabPage, BOMWizardComponent, BOMWizardStepComponent, BOMMessageBox,
        ViewBOMFormPageComponent, EditAWSItemFromGridComponent, DeleteAWSRowFromGridComponent,
        AWSServicesDropdownEditorComponent, COTSProductsDropdownEditorComponent, ProductGroupDashboardPage,
        AWSComponentSizesDropdownEditorComponent, AWSOperatingSystemsDropdownEditorComponent,
        CreateProductGroupPopup, EditProductGroupPopup, BOMManagedServicesTabPageComponent,
        EditProductGroupComponent, DeleteProductGroupComponent,
        AddCOTSProductGroupComponent
      ],
      exports: [],
      providers: [AuthGuard, TitleCasePipe, LookupService, ProductGroupService],
    entryComponents: [
      BOMMessageBox,
      AWSServicesDropdownEditorComponent,
      AWSComponentSizesDropdownEditorComponent,
      AWSOperatingSystemsDropdownEditorComponent,
      EditAWSItemFromGridComponent,
      DeleteAWSRowFromGridComponent,
      COTSProductsDropdownEditorComponent,
      EditProductGroupComponent,
      DeleteProductGroupComponent,
      AddCOTSProductGroupComponent,
      EditProductGroupPopup
    ]
  })

  export class BOMFormsModule {
    static routes = routes;
  }
