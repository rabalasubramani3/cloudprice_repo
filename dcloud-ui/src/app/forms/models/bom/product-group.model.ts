export class ProductGroupItem{
  productGroupItemId: number;
  productGroupId: number;
  category: string;
  component: string;
  licensing: string;
  size: string;
  osdb: string;
  qty: number;
  comments: string;
}

export class ProductGroup{
  productGroupId: number;
  masterProductId: number;
  productGroupName: string;
  items: ProductGroupItem[];

  constructor(){
    this.items = [];
  }
}
