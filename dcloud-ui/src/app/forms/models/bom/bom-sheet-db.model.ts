import { isNil } from 'ramda';
import { BOMSheet } from './bom-sheet.model';

export class TPLNote {
  createdDate: string;
  note: string;
}

export class BOMSheetDB {
  constructor(
  ) {
  }

  id: number;
  product_name: string;
  product_type: string;
  product_id: number;
  product_use: string;
  product_category: string;
  product_sub_category: String;
  product_other_category: string;
  product_open_close: string;
  product_flavor: string;
  product_other_flavor: string;
  product_launch_date: Date;
  product_estb_id: string;
  TP_STEP_FLAG: string;

  //*************************************************** */
  // STEP 1
  //*************************************************** */
  PROFILE_ID: string;

  TP_NOTES: any;

  public set(data){

  }

  public mapToDB(application: BOMSheet, isProcessor: boolean) {
    // if(isNil(application.uuid)){
    //   // this.TP_PRODUCT_UUID = application.uuid;
    // }

    // // this.TP_PRODUCT_STATUS = application.productStatus;

    // // PASSED ON STEPS 1 THROUGH STEP 5
    // this.TP_STEP_FLAG = null; // M5

    // this.TP_NOTES = []; // M6
    // this.PROFILE_ID = application.profileid; // M7

  }

  public mapFromDB(): BOMSheet {
    let app = new BOMSheet();

    return app;
  }

  public setStep(step: number) {
    this.TP_STEP_FLAG = "step" + step;
  }

  private nvn(val: number) {
    if (isNil(val)) {
      return 0.0;
    }
    return Number(val);
  }

  private nv(val: number) {
    if (isNil(val)) {
      return "-";
    }
    return val.toString();
  }

  private sv(val: string) {
    if (isNil(val)) {
      return "-";
    }
    return val;
  }

  private sve(val: string) {
    if (isNil(val)) {
      return "";
    }
    return val;
  }

  private stv(val: string) {
    let returnVal = "MD";
    if(!isNil(val)) {
      val = val.trim();
      if(val.length > 0) {
        returnVal = val;
      }
    }
    return returnVal;
  }

  public dblv(val: number) {
    if (isNil(val)) {
      return 0.0;
    }
    return Number(val);
  }

  private unformatPhone(val:string){
    if(!isNil(val)){
      val = val.replace(/[^0-9\.]+/g, '');
    }
    return val;
  }

}
