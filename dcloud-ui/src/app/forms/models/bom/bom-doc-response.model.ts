export class BOMDocumentResponse{
  filename: string;
  filesize: number;
  timestamp: string;
  etag: string;
}
