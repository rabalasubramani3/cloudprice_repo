import * as moment from 'moment';
import { isNil } from 'ramda';

export class BOMCOTSProductItem{
  productItemId: number;
  bomId: number;
  environment: string;
  productGroupId: number;
  productGroupName: string;
  category: string;
  product: string;
  licensing: string;
  size: string;
  osdb:string;
  qty: number;
  comments: string;
  listprice: number;
  discount: number;
  discountedPrice: number;
  utilpercent: number;
  terms: number;
  annualcost: number;
  created_by: string;
  created_dt: Date;
  modified_by: string;
  modified_dt: Date;
}

// export class BOMCOTSProductGroup{
//   productGroupId: number;
//   environment: string;
//   productGroupName: string;
//   created_by: string;
//   created_dt: Date;
//   modified_by: string;
//   modified_dt: Date;
// }

export class BOMAWS{
  bomAWSId: number;
  bomId: number;
  environment: string;
  category: string;
  component: string;
  licensing: string;
  size: string;
  osdb: string;
  qty: number;
  comments: string;
  listprice: number;
  discount: number;
  discountedPrice: number;
  utilpercent: number;
  terms: number;
  annualcost: number;
  created_by: string;
  created_dt: Date;
  modified_by: string;
  modified_dt: Date;
}
export class BOMSheet {
  bomId: number;
  uuid: string;   // unique id of the sheet
  referenceNum: string; // sheet reference number
  step: number; // step number
  status: string; // status of the sheet
  created_by: string;
  created_dt: Date;
  modified_by: string;
  modified_dt: Date;
  // Step 1 variables
  project: string; // name of the project
  client: string; // name of the client
  sponsor: string; // name of the sponsor i.e. PMD
  wbsCode: string; // WBS Code

  // Step 2 variables
  package: string; // Bronze, Silver, Gold, Platinum
  pii: string; // 'Y' or 'N'
  phi: string; // 'Y' or 'N'
  hippa: string; // 'Y' or 'N'
  irs: string; // 'Y' or 'N'
  fedramp: number; // 0 - N/A 1 - Moderate 2 - High
  awsitems: BOMAWS[];
  cotsitems: BOMCOTSProductItem[];

  constructor(){
      this.uuid = "";
      this.referenceNum = "";
      this.status = "Pending";
      this.step = 0;
      this.created_by = "";
      this.modified_by = "";

      // Step 1 Applicant Detail
      this.project = "";
      this.client = "";
      this.sponsor = "";
      this.wbsCode = "";

      // Step 2 Products
      this.package = "Bronze";
      this.pii = "N";
      this.phi = "N";
      this.hippa = "N";
      this.irs = "N";
      this.fedramp = 0;

      this.awsitems = [];
      this.cotsitems = [];
  }

  public dblv(val: number) {
    if (isNil(val)) {
      return 0.0;
    }
    return val;
  }

  fromDB(dbdata){
  }

  private nv(val: number) {
    if (isNil(val)) {
      return "-";
    }
    return val.toString();
  }

  private sv(val: string) {
    if (isNil(val)) {
      return "-";
    }
    return val.toString();
  }

  private sve(val: string) {
    if (isNil(val)) {
      return "";
    }
    return val.toString();
  }

  private stv(val: string) {
    let returnVal = "MD";
    if(!isNil(val)) {
      val = val.trim();
      if(val.length > 0) {
        returnVal = val;
      }
    }
    return returnVal;
  }

  private dv(val: string) {
    if (isNil(val)) {
      return "-";
    }
    return moment(val).format("MM/DD/YYYY HH:SS");
  }

  private dv2(val: Date) {
    if (isNil(val)) {
      return "-";
    }
    return moment(val).format("YYYY-MM-DD");
  }

}
