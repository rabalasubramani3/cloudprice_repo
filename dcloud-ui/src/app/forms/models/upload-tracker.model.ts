export class UploadTracker{
  id: number;
  name: string;
  size: number;
  modificationTime: Date;
  uploaded: boolean;
  progress: number;
  started: boolean;
  starttime: Date;
  uploadstarttime: Date;
  endtime: Date;
  uploadtotalduration: number;
  uploadstartduration: number;

}
