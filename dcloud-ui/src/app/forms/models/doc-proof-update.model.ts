export class DocumentProofUpdate{
    FileName: string;
    Description: string;
    UploadStatus: string;
    UUID: string;
}