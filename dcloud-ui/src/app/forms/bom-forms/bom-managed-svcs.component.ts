import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChange, ViewEncapsulation } from "@angular/core";
import "ag-grid-enterprise";
import * as _ from "lodash";
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { isNil } from 'ramda';
import { forkJoin, Observable, Subscription } from 'rxjs';
import { LookupService } from '../../admin/services/lookup.service';
import { AuthService } from '../../login/services/auth.service';
import { BOMCOTSProductItem, BOMSheet } from '../models/bom/bom-sheet.model';
import { ProductGroupService } from '../services/productgroup.service';
import { AddCOTSProductGroupComponent } from './add-cots-prodgroup/add-cots-prodgroup.component';

declare let jQuery: any;

@Component({
  // tslint:disable-next-line: component-selector
  selector: "bom-managed-svcs",
  styleUrls: ['./create-bom.style.scss'],
  templateUrl: "./bom-managed-svcs.template.html",
  encapsulation: ViewEncapsulation.None
})
export class BOMManagedServicesTabPageComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {
  @Input() formdata: BOMSheet;
  @Output() validate = new EventEmitter<boolean>();

  public lookupitems: Map<string, any> = new Map<string, any>();
  public lookupcategories: any;
  public lookupOperatingSystems: any;
  public lookupSizes: any;
  public lookupLicensing: any;

  public gridApi;
  public gridColumnApi;
  public gridOptions;

  public columnDefs;
  public rowData;
  public rowSelection;
  public defaultColDef;
  public autoGroupColumnDef;
  public editType;

  public busyLoadCOTSProductItems: Subscription;
  public busyLoadProductGroupItems: Subscription;

  public busyLoadOperatingSystems: Subscription;
  public busyLoadSizes: Subscription;
  public busyLoadLicensingTerms: Subscription;

  public bsAddProductsGroupModalRef: BsModalRef;
  public busyLoadLookupCategoriesAndItems: Subscription;

  public showAddProductGroup: boolean;
  showErrorFlag: boolean;
  alerts: Array<Object>;
  public categorymap = new Map<string, string>();

  constructor(
    private lookupService: LookupService,
    private auth: AuthService,
    private modalService: BsModalService,
    private productGroupService: ProductGroupService
  ){
    let that = this;
    this.showErrorFlag = false;
    this.alerts = [{ type: 'warning',  msg: '' }];

    this.lookupLicensing = [];
    this.lookupOperatingSystems = [];
    this.lookupSizes = [];

    this.showAddProductGroup = false;

    this.categorymap.set("COMPUTE", "AWS_COMPUTE_COMPONENTS");
    this.categorymap.set("STORAGE", "AWS_STORAGE_COMPONENTS");
    this.categorymap.set("SOFTWARE", "COTS_SOFTWARE");
    this.categorymap.set("SOFTWARE-MAINT", "COTS_SOFTWARE_MAINTENANCE");
    this.categorymap.set("AWS-NATIVE", "AWS_NATIVE_COMPONENTS");

    this.columnDefs = [
      {
        headerName: "Environment",
        field: "environment",
        editable: false,
        rowGroup: true,
        hide: true
      },
      {
        headerName: "Product Group",
        field: "productGroupName",
        editable: false,
        rowGroup: true,
        hide: true
      },
      {
        headerName: "Product Group Id",
        field: "productGroupId",
        editable: false,
        hide: true
      },
      {
        headerName: "Category",
        field: "category",
        width: 150,
        cellEditor: "agRichSelectCellEditor",
        cellEditorParams: function(params) {
          return {
            values: that.lookupcategories
          };
        }
      },
      {
        headerName: "Products",
        field: "product",
        width: 150,
        cellEditor: "agRichSelectCellEditor",
        cellEditorParams: function(params) {
            return {
              values: that.lookupitems.get(params.data.category)
            };
        }
      },
      {
        headerName: "Licensing",
        field: "licensing",
        cellEditor: "agRichSelectCellEditor",
        cellEditorParams: function(params) {
          return {
            values: that.lookupLicensing
          };
        }
      },
      {
        headerName: "Size",
        field: "size",
        cellEditor: "agRichSelectCellEditor",
        cellEditorParams: function(params) {
          return {
            values: that.lookupSizes
          };
        }
      },
      {
        headerName: "Operating System/DB",
        field: "osdb",
        cellEditor: "agRichSelectCellEditor",
        cellEditorParams: function(params) {
          return {
            values: that.lookupOperatingSystems
          };
        }
      },
      {
        headerName: "Qty",
        field: "qty"
      },
      {
        headerName: "Comments",
        field: "comments",
        width: 200,
        cellEditor: "agLargeTextCellEditor"
      },
      {
        headerName: "List Price",
        field: "listprice",
        valueFormatter: currencyFormatter,
        aggFunc: "sum",
        cellStyle: { textAlign: "right" },
        valueGetter: function (params) {
          if (!isNil(params) && !isNil(params.data) && !isNil(params.data.listprice)) {
            return (params.data.listprice);
          } else {
            return 0.0;
          }
        }
      },
      {
        headerName: "Discount",
        field: "discount"
      },
      {
        headerName: "Discounted Price",
        field: "discountprice",
        valueFormatter: currencyFormatter,
        aggFunc: "sum",
        cellStyle: { textAlign: "right" },
        valueGetter: function (params) {
          if (!isNil(params) && !isNil(params.data) && !isNil(params.data.listprice) && !isNil(params.data.discount)) {
            return (params.data.listprice - (params.data.listprice * (params.data.discount / 100.0)));
          } else {
            return 0.0;
          }
        }
      },
      {
        headerName: "Util %",
        field: "utilpercent"
      },
      {
        headerName: "Term",
        field: "term"
      },
      {
        headerName: "Yearly Cost",
        field: "annualcost",
        valueFormatter: currencyFormatter,
        cellStyle: { textAlign: "right" },
        aggFunc: "sum",
        valueGetter: function (params) {
          if (!isNil(params) && !isNil(params.data) && !isNil(params.data.listprice) && !isNil(params.data.discount)
                && !isNil(params.data.qty)) {
            return (params.data.listprice - (params.data.listprice * (params.data.discount / 100.0))) * params.data.qty;
          } else {
            return 0.0;
          }
        }
      }
    ];

    this.editType = "fullRow";
    this.rowSelection = "multiple";
    this.defaultColDef = {
      editable: true,
      resizable: true
    };

    // this.autoGroupColumnDef = {
    //   headerName: " CUSTOM! ",
    //   cellRendererParams: {
    //     suppressCount: true,
    //     checkbox: true
    //   },
    //   comparator: function(valueA, valueB) {
    //     if (valueA == null || valueB == null) return valueA - valueB;
    //     if (!valueA.substring || !valueB.substring) return valueA - valueB;
    //     if (valueA.length < 1 || valueB.length < 1) return valueA - valueB;
    //     return strcmp(valueA.substring(1, valueA.length), valueB.substring(1, valueB.length));
    //   }
    // };
  }

  getRowData() {
    let rowData = [];
    this.gridApi.forEachNode(function(node) {
      rowData.push(node.data);
    });
  }

  clearData() {
    if(!isNil(this.gridApi)){
      this.gridApi.setRowData([]);
    }
  }

  public  createNewProductGroupItems(env: string, selprogroup: any) {

    let that = this;
    let newItems: any[] = [];

    this.busyLoadProductGroupItems = this.productGroupService.getAllProductGroupItems(selprogroup.productGroupId).subscribe( data => {
      if(!this.showErrorFlag){
        if(!isNil(data)){
          let prodgroupitems = data.prodgroupitems;
          _.forEach(prodgroupitems, function(pgitem){
            let newData: BOMCOTSProductItem = new BOMCOTSProductItem();
            newData.environment = env;
            newData.productGroupId = selprogroup.productGroupId;
            newData.productGroupName = selprogroup.productGroupName;
            newData.category = pgitem.category;
            newData.product = pgitem.product;
            newData.qty = pgitem.qty;
            newData.listprice = 0.0;
            newData.discount = 0.0;
            newData.comments = pgitem.comments;
            that.gridApi.updateRowData({ add: [newData] });
          });
        } else {
          // that.onShowError("Error retrieving Product Group ITems for D: " + selprogroup.productGroupId);
        }
      }
    }, error => {
      // that.onShowError(error);
    });
  }

  public loadLookupCategoriesAndItems(){
    let that = this;
    this.lookupcategories = [];
    this.busyLoadLookupCategoriesAndItems = this.lookupService.getAllByRefTypeName("INFRASTRUCTURE_CATEGORY").subscribe(data => {
      if (data) {
        that.lookupcategories = _.toArray(_.map(data.refValues, 'refValue'));
        let apiitems:Observable<any>[] = [];
        _.forEach(that.lookupcategories, function(category){
          apiitems.push(that.lookupService.getAllByRefTypeName(that.categorymap.get(category)));
        });
        forkJoin(apiitems).subscribe(results => {
          let kdx = 0;
          _.forEach(that.lookupcategories, function(category){
            if(!isNil(results[kdx])){
              if((results[kdx].success === false) && (!isNil(results[kdx].error))){
                console.log("error:" + category + ":" + results[kdx].error);
              }else{
                if(results[kdx].refValues.length > 0){
                  that.lookupitems.set(category, _.toArray(_.map(results[kdx].refValues, 'refValue')));
                }else{
                  that.lookupitems.set(category, []);
                }
              }
            }
            kdx++;
          });
        });

      }
    }, error => {
      that.showErrorFlag = true;
      that.alerts = [];
      that.alerts.push({ type: 'warning', msg: error });
    });
  }
  onAddNewProductGroup() {

    this.showAddProductGroup = true;
    const initialState = {
      message: 'Message',
      title: 'Need Email Verification'
    };
    this.bsAddProductsGroupModalRef = this.modalService.show(AddCOTSProductGroupComponent, { initialState, class: 'modal-lg modal-center', animated: true, keyboard: false, backdrop: true, ignoreBackdropClick: true  });
    this.bsAddProductsGroupModalRef.content.closeBtnName = 'Close';
    let newSubscriber = this.modalService.onHide.subscribe(r=>{
       newSubscriber.unsubscribe();
       if(this.bsAddProductsGroupModalRef.content.addProductGroupFlag){
          let env = this.bsAddProductsGroupModalRef.content.selEnvironment;
          let prodgroup =  this.bsAddProductsGroupModalRef.content.selProductGroupInfo;
          this.createNewProductGroupItems(env, prodgroup);
        }else{
          console.log("Action Cancelled");
        }
     });
  }

  updateItems() {
    let itemsToUpdate = [];
    this.gridApi.forEachNodeAfterFilterAndSort(function(rowNode, index) {
      if (index >= 5) {
        return;
      }
      let data = rowNode.data;
      data.price = Math.floor(Math.random() * 20000 + 20000);
      itemsToUpdate.push(data);
    });
    let res = this.gridApi.updateRowData({ update: itemsToUpdate });
  }

  onRemoveSelected() {
    let selectedData = this.gridApi.getSelectedRows();
    let res = this.gridApi.updateRowData({ remove: selectedData });
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  ngOnInit(): void {
    this.loadOperatingSystems();
    this.loadLicensing();
    this.loadSizes();
    this.loadLookupCategoriesAndItems();
  }

  ngOnDestroy(): void {
    if(this.busyLoadCOTSProductItems){
      this.busyLoadCOTSProductItems.unsubscribe();
    }
    if(this.busyLoadProductGroupItems){
      this.busyLoadProductGroupItems.unsubscribe();
    }
    if(this.busyLoadOperatingSystems){
      this.busyLoadOperatingSystems.unsubscribe();
    }
    if(this.busyLoadSizes){
      this.busyLoadSizes.unsubscribe();
    }
    if(this.busyLoadLicensingTerms){
      this.busyLoadLicensingTerms.unsubscribe();
    }
    if(this.busyLoadLookupCategoriesAndItems){
      this.busyLoadLookupCategoriesAndItems.unsubscribe();
    }
  }

  ngAfterViewInit(): void {

  }

  public loadOperatingSystems(){
    let that = this;
    this.busyLoadOperatingSystems = this.lookupService.getAllByRefTypeName("AWS_OPERATING_SYSTEMS").subscribe(data => {
      if (data) {
        that.lookupOperatingSystems = _.toArray(_.map(data.refValues, 'refValue'));
      }
    }, error => {
      that.showErrorFlag = true;
      that.alerts = [];
      that.alerts.push({ type: 'warning', msg: error });
    });
  }

  public loadSizes(){
    let that = this;
    this.busyLoadSizes = this.lookupService.getAllByRefTypeName("AWS_COMPONENT_SIZE").subscribe(data => {
      if (data) {
        that.lookupSizes = _.toArray(_.map(data.refValues, 'refValue'));
      }
    }, error => {
      that.showErrorFlag = true;
      that.alerts = [];
      that.alerts.push({ type: 'warning', msg: error });
    });
  }

  public loadLicensing(){
    let that = this;
    this.busyLoadLicensingTerms = this.lookupService.getAllByRefTypeName("COTS_LICENSING").subscribe(data => {
      if (data) {
        that.lookupLicensing = _.toArray(_.map(data.refValues, 'refValue'));
      }
    }, error => {
      that.showErrorFlag = true;
      that.alerts = [];
      that.alerts.push({ type: 'warning', msg: error });
    });
  }

  public loadSavedCOTSProductItems(){
    this.rowData = [];

    let that = this;
    _.forEach(this.formdata.cotsitems, function(cotsitem){
      let rowItem: BOMCOTSProductItem = new BOMCOTSProductItem();
      rowItem.productGroupId = cotsitem.productGroupId;
      rowItem.productGroupName = cotsitem.productGroupName;
      rowItem.environment = cotsitem.environment;
      rowItem.category = cotsitem.category;
      rowItem.product = cotsitem.product;
      rowItem.licensing = cotsitem.licensing;
      rowItem.size = cotsitem.size;
      rowItem.osdb = cotsitem.osdb;
      rowItem.qty = cotsitem.qty;
      rowItem.listprice = cotsitem.listprice;
      rowItem.discount = cotsitem.discount;
      rowItem.discountedPrice = cotsitem.discountedPrice;
      rowItem.comments = cotsitem.comments;
      that.rowData.push(rowItem);
    });

    if(!isNil(this.gridApi)){
      this.gridApi.setRowData(this.rowData);
      this.sizeToFit();
    }
  }

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    for (let propName in changes) {
      if (propName === "formdata") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.formdata = chg.currentValue;
          this.clearData();
          this.loadSavedCOTSProductItems();
        }
      }
    }
  }
  onFirstDataRendered(params){
    this.sizeToFit();
  }

  public sizeToFit() {
    if(!isNil(this.gridApi)){
      this.gridApi.sizeColumnsToFit();
    }
  }

  public  autoSizeAll() {
    let allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(function(column) {
      allColumnIds.push(column.colId);
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);
  }

  protected expandAllNodes(value:boolean) {
    this.gridApi.forEachNode((node) =>{
        if (node.group){
          node.setExpanded(value);
        }
    });
  }

  public expandAll(){
    // this.expandAllNodes(true);
  }

  public collapseAll(){
    // this.expandAllNodes(false);
  }
  onCellValueChanged(params) {
    console.log('cv changed');
    const cell = params.api.getFocusedCell();
    if (params.oldValue !== params.newValue) {
      cell.column.colDef.cellStyle = {color: 'red',  fontWeight: '550'};
    }
    params.api.refreshCells(cell);
  }

  onRowValueChanged(params){
    console.log('rv changed');
  }
  public skipOnlyRequiredValidation(){
    jQuery('#frm-cots-details .rq').each(function() {
      jQuery(this).removeAttr('required');
    });
  }

  /**
   *
   * @param isnextstep : false - Save and exit - skip validation
   */
  public validateForm(isnextstep): boolean{
    let isValidForm: boolean = true;
    if(!isnextstep){
      this.skipOnlyRequiredValidation();
    }
    // jQuery('#frm-cots-details').parsley().validate();
    // isValidForm = jQuery('#frm-cots-details').parsley().isValid();
    this.formdata.cotsitems = [];
    let curEnvironment = "";
    let curProdGroupName = "";
    let that = this;
    let rowItem: BOMCOTSProductItem = new BOMCOTSProductItem();
    this.gridApi.forEachNode(function(node) {
      if(node.group === true){
        if(node.field === 'environment'){
          curEnvironment = node.key;
        }
        if(node.field === 'productGroupName'){
          curProdGroupName = node.key;
        }
      } else {
        rowItem.bomId = that.formdata.bomId;
        rowItem.environment = curEnvironment;
        rowItem.productGroupName =  curProdGroupName;
        rowItem.productGroupId = node.data.productGroupId;
        rowItem.category = node.data.category;
        rowItem.product = node.data.product;
        rowItem.licensing = node.data.licensing;
        rowItem.size = node.data.size;
        rowItem.osdb = node.data.osdb;
        rowItem.qty = node.data.qty;
        rowItem.comments = node.data.comments;
        rowItem.listprice = node.data.listprice;
        rowItem.discount = node.data.discount;
        rowItem.discountedPrice = node.data.discountedPrice;
        that.formdata.cotsitems.push(rowItem);

        // Reset
        rowItem = new BOMCOTSProductItem();
        rowItem.environment = curEnvironment;
        rowItem.productGroupName =  curProdGroupName;

      }
    });
    return isValidForm;
  }

  public isProcessorMode(){
    return this.auth.isProcessorMode();
  }

  public clearFields(){
  }

  public dblv(val: number) {
    if (isNil(val)) {
      return 0.0;
    }
    return Number(val);
  }



}

function currencyFormatter(params) {
  let usdFormate = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2
  });
  if(!isNil(params.value)){
    return usdFormate.format(params.value);
  }
  return usdFormate.format(0.0);

}

function formatNumber(number) {
  return Math.floor(number)
    .toString()
    .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}
