import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { LocalStorageService } from 'ngx-webstorage';
import { isNil } from 'ramda';
import { Subscription } from 'rxjs';
import { environment } from '../../../environments/environment';
import { ApplicantService } from '../../bom/services/applicant.service';
import { OnComponentErrorHandler } from '../../core/coreerror.handler';
import { AuthService } from '../../login/services/auth.service';
import { ProfileService } from '../../login/services/profile.service';
import { BOMSheet } from '../models/bom/bom-sheet.model';
import { BOMFormsService } from '../services/bom-forms.service';
import { BOMApplicantDetailsTabPageComponent } from "./bom-app-details.component";
import { BOMAWSDetailsTabPageComponent } from "./bom-aws-details.component";
import { BOMCOTSDetailsTabPageComponent } from './bom-cots-details.component';
import { BOMDocumentUploadPage } from './bom-doc-upload.component';
import { BOMMessageBox } from './bom-msgbox.component';
import { BOMProductDetailsTabPageComponent } from "./bom-prod-details.component";
import { BOMReviewSubmitTabPage } from "./bom-review-submit.component";
import { BOMServiceDetailsTabPageComponent } from './bom-service-details.component';
import { BOMWizardComponent } from './bom-wizard.component';
import { BOMManagedServicesTabPageComponent } from './bom-managed-svcs.component';

declare var jQuery: any;

@Component({
  selector: 'app-create-bom',
  templateUrl: './create-bom.template.html',
  styleUrls: ['./create-bom.style.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [BOMFormsService, BsModalService, ApplicantService]
})

export class CreateBillOfMaterialsgPageComponent implements OnInit, OnDestroy, OnComponentErrorHandler {

  @ViewChild(BOMWizardComponent, {static: true}) formWizard: BOMWizardComponent;
  @ViewChild(BOMApplicantDetailsTabPageComponent, {static: true}) appDetailsPage: BOMApplicantDetailsTabPageComponent;

  @ViewChild(BOMProductDetailsTabPageComponent, {static: true}) prodDetailsPage: BOMProductDetailsTabPageComponent;

  @ViewChild(BOMServiceDetailsTabPageComponent, {static: true}) serviceDetailsPage: BOMServiceDetailsTabPageComponent;
  @ViewChild(BOMManagedServicesTabPageComponent, {static: true}) managedServicesDetailsPage: BOMManagedServicesTabPageComponent;
  @ViewChild(BOMAWSDetailsTabPageComponent, {static: true}) awsDetailsPage: BOMAWSDetailsTabPageComponent;
  @ViewChild(BOMCOTSDetailsTabPageComponent, {static: true}) cotsDetailsPage: BOMCOTSDetailsTabPageComponent;
  @ViewChild(BOMDocumentUploadPage, {static: true}) certificationPage: BOMDocumentUploadPage;
  @ViewChild(BOMReviewSubmitTabPage, {static: true}) reviewSubmitPage: BOMReviewSubmitTabPage;
  @ViewChild('agreetoSubmitModal', {static: true}) agreeModal: ModalDirective;

  router: Router;
  showErrorFlag: boolean;
  alerts: Array<Object>;

  busyLoadApplication: Subscription;
  busyCreateStep1: Subscription;
  busyCreateStep2: Subscription;
  busyCreateStep3: Subscription;
  busyCreateStep4: Subscription;
  busyCreateStep5: Subscription;
  busyCreateStep6: Subscription;
  busyCreateStep7: Subscription;
  busySubmitApplication: Subscription;
  busyLoadUserProfileData: Subscription;
  busyLoadURL: any;

  model: BOMSheet;
  errorMessage: string;
  isCompleted: boolean;
  currentTabInfo: string;
  bsModalRef: BsModalRef;

  constructor(router: Router,
    private formsService: BOMFormsService,
    private route: ActivatedRoute,
    private auth:AuthService,
    private profileService:ProfileService,
    private applicantService: ApplicantService,
    private storage: LocalStorageService,
    private modalService: BsModalService,
    private authService: AuthService
  ) {
    this.router = router;

    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span>'
      }
    ];

    this.showErrorFlag = false;
    this.currentTabInfo = "Applicant";

  }

  public isProcessorMode(){
    return this.auth.isProcessorMode();
  }

  public getAWSBOMItems(bomId: number){
    let that = this;
    this.busyLoadApplication = this.applicantService.getAllAWSBOMItems(bomId)
    .subscribe(
      data => {
        if(isNil(data)){
          // this.onShowError("Unable to retrieve tpl application details for uuid: " + applicationId);
        }
        if (data) {
          that.onShowBodyError(data);
          that.model.awsitems = data.awsbomitems;
          that.model = Object.assign({}, that.model);
        }
      },
      error => {
        this.onShowError(error);
      }
    );
  }

  public getCOTSBOMItems(bomId: number){
    let that = this;
    this.busyLoadApplication = this.applicantService.getAllCOTSBOMItems(bomId)
    .subscribe(
      data => {
        if(isNil(data)){
          // this.onShowError("Unable to retrieve tpl application details for uuid: " + applicationId);
        }
        if (data) {
          that.onShowBodyError(data);
          that.model.cotsitems = data.cotsbomitems;
          that.model = Object.assign({}, that.model);
        }
      },
      error => {
        this.onShowError(error);
      }
    );
  }

  public loadSavedApplication() {

    let that = this;
    this.onResetError();

    this.busyLoadURL = this.route.params.subscribe(params => {
      if (params["id"]) {
          that.model.bomId = params["id"];
          this.busyLoadApplication = this.applicantService.getBOM(that.model.bomId)
            .subscribe(
              data => {
                if(isNil(data)){
                  // this.onShowError("Unable to retrieve tpl application details for uuid: " + applicationId);
                }
                if (data) {
                  that.onShowBodyError(data);
                  that.model = data.boms;
                  that.getCOTSBOMItems(that.model.bomId);
                  that.getAWSBOMItems(that.model.bomId);
                }
              },
              error => {
                this.onShowError(error);
              }
            );
        }
    });
  }


  ngOnInit(): void {

    // Initialize model
    this.model = new BOMSheet();

    // load saved application
    this.loadSavedApplication();

    this.showErrorFlag = false;
  }

  public unsubscribe(busy:Subscription){
    if(busy){
      busy.unsubscribe();
    }
  }

  public ngOnDestroy(): void {
    this.unsubscribe(this.busyLoadURL);
    this.unsubscribe(this.busyLoadApplication);
    this.unsubscribe(this.busyCreateStep1);
    this.unsubscribe(this.busyCreateStep2);
    this.unsubscribe(this.busyCreateStep3);
    this.unsubscribe(this.busyCreateStep4);
    this.unsubscribe(this.busyCreateStep5);
    this.unsubscribe(this.busyCreateStep6);
    this.unsubscribe(this.busyCreateStep7);
    this.unsubscribe(this.busySubmitApplication);
    this.unsubscribe(this.busyLoadUserProfileData);
    this.unsubscribe(this.busyLoadURL);
  }

  /**
   * Step 1: Save applicant details
   * @param step - step number
   * @param isnextstep : false - Save and Exit Mode
   */
  public onStep1Next(step, isnextstep) {
    this.onResetError();

    step.isValidated = true;
     if (this.appDetailsPage.validateForm(isnextstep)) {
      step.isValidated = true;
    } else {
      step.isValidated = false;
    }
    if (step.isValidated) {
      this.busyCreateStep1 = this.formsService.createStep(this.model, 1).subscribe(data => {
        let errorFound = this.onShowBodyError(data);
        if(!isNil(data)){
          console.dir(JSON.stringify(data));
          if (!isnextstep) {
              this.router.navigate(['app/bom/search']);
          }else{
            this.formWizard.proceednext();
            this.currentTabInfo = "Enter Product Details";
          }
        }else{
          if(!errorFound){
            this.onShowError("Unable to save Applicant Details");
          }
        }
      }, error => {
        this.onShowError(error);
      });
    }
  }

    /**
   * Step 2: Save Product details
   * @param step - step number
   * @param isnextstep : false - Save and Exit Mode
   */
  public onStep2Next(step, isnextstep) {
    this.onResetError();

    step.isValidated = true;
    if (this.prodDetailsPage.validateForm(isnextstep)) {
      step.isValidated = true;
    } else {
      step.isValidated = false;
    }
    if (step.isValidated) {

      this.busyCreateStep2 = this.formsService.createStep(this.model, 2)
        .subscribe(data => {
          if(!isNil(data)){
            this.onShowBodyError(data);
            console.log(data);
            if (!isnextstep) {
                this.router.navigate(['app/bom/search']);
            }else{
              this.formWizard.proceednext();
              this.currentTabInfo = "Enter Service Details";
            }
          }else{
            this.onShowError("Unable to save Product Details");
          }
        }, error => {
          this.onShowError(error);
        });
    }
  }

    /**
   * Step 3: Save Services details
   * @param step - step number
   * @param isnextstep : false - Save and Exit Mode
   */
  public onStep3Next(step, isnextstep) {
    this.onResetError();

    step.isValidated = true;
    if (this.serviceDetailsPage.validateForm(isnextstep)) {
      step.isValidated = true;
    } else {
      step.isValidated = false;
    }
    if (step.isValidated) {
      this.busyCreateStep3 = this.formsService.createStep(this.model, 3).subscribe(data => {
        this.onShowBodyError(data);
        if(!isNil(data)){
          console.log(data);
          if (!isnextstep) {
            this.router.navigate(['app/bom/search']);
          }else{
            this.formWizard.proceednext();
            this.cotsDetailsPage.sizeToFit();
            this.currentTabInfo = "Enter COTS Details";
          }
        }else {
          this.onShowError("Unable to save Spouse & Household Details");
        }
      }, error => {
        this.onShowError(error);
      });
    }
  }

    /**
   * Step 4: Save Managed Services Details PAge
   * @param step - step number
   * @param isnextstep : false - Save and Exit Mode
   */
   public onStep4Next(step, isnextstep) {
    let that = this;
    this.onResetError();

    step.isValidated = true;
    if (this.managedServicesDetailsPage.validateForm(isnextstep)) {
      step.isValidated = true;
    } else {
      step.isValidated = false;
    }
    if (step.isValidated) {
      this.busyCreateStep4 = this.formsService.createCOTSStep(this.model, 4).subscribe(data => {
        this.onShowBodyError(data);
        if(!isNil(data)){
          console.log(data);
          if (!isnextstep) {
            that.router.navigate(['app/bom/search']);
          }else{
            that.formWizard.proceednext();
            this.awsDetailsPage.sizeToFit();
            this.currentTabInfo = "Enter AWS Details";
          }
        }else{
          that.onShowError("Unable to save COTS Details");
        }
      }, error => {
        this.onShowError(error);
      });
    }
  }

   /**
   * Step 5: Save COTS Products details
   * @param step - step number
   * @param isnextstep : false - Save and Exit Mode
   */
  public onStep5Next(step, isnextstep) {
    let that = this;
    this.onResetError();

    step.isValidated = true;
    if (this.cotsDetailsPage.validateForm(isnextstep)) {
      step.isValidated = true;
    } else {
      step.isValidated = false;
    }
    if (step.isValidated) {
      this.busyCreateStep5 = this.formsService.createCOTSStep(this.model, 5).subscribe(data => {
        this.onShowBodyError(data);
        if(!isNil(data)){
          console.log(data);
          if (!isnextstep) {
            that.router.navigate(['app/bom/search']);
          }else{
            that.formWizard.proceednext();
            this.awsDetailsPage.sizeToFit();
            this.currentTabInfo = "Enter AWS Details";
          }
        }else{
          that.onShowError("Unable to save COTS Details");
        }
      }, error => {
        this.onShowError(error);
      });
    }
  }

   /**
   * Step 6: Save AWS Items details
   * @param step - step number
   * @param isnextstep : false - Save and Exit Mode
   */
  public onStep6Next(step, isnextstep) {
    let that = this;
    this.onResetError();

    step.isValidated = true;
    if (this.awsDetailsPage.validateForm(isnextstep)) {
      step.isValidated = true;
    } else {
      step.isValidated = false;
    }
    if (step.isValidated) {
      this.busyCreateStep6 = this.formsService.createAWSStep(this.model, 6).subscribe(data => {
        this.onShowBodyError(data);
        if(!isNil(data)){
          console.log(data);
          if (!isnextstep) {
            that.router.navigate(['app/bom/search']);
          }else{
            that.formWizard.proceednext();
            // this.currentTabInfo = "Certification";
          }
        }else{
          that.onShowError("Unable to save AWS Details");
        }
      }, error => {
        this.onShowError(error);
      });
    }
  }

  public onStep7Next(step, isnextstep) {
    this.onResetError();

    step.isValidated = true;
    // if (this.certificationPage.validateForm(isnextstep)) {
    //   step.isValidated = true;
    // } else {
    //   step.isValidated = false;
    // }

    this.formWizard.proceednext();
  }

  onStep8Exit(_step) {
    this.onResetError();

    if(this.isProcessorMode){
      this.router.navigate(['app/bom/search']);
    } else {
      this.router.navigate(['app/bom/search']);
    }
  }

  public sendSubmissionCompletionEmail(application:BOMSheet){
    // let handler = this;
    // this.formsService.sendSubmitEmail(application.tplformid, application.email, application.creditYear).subscribe(data => {
    //   if (!isNil(data) && !isNil(data.errorMessage)) {
    //     handler.onShowBodyError(data);
    //   }
    //   handler.displayConfirmationMessage(application.tplformid);
    // }, error => {
    //   this.onShowError(error);
    // });
  }

  public onSubmitAgree(step){
    this.onResetError();
    step.isValidated = true;
    this.displaySubmissionAgreement();
  }

  public onSubmitValidate(step) {
    const initialState = {
      message: "You cannot submit because your email address has not been validated. " +
        "Please check your email and enter the confirmation code when you next log in to the application.",
      title: "Validate Email Address"
    };
    this.bsModalRef = this.modalService.show(BOMMessageBox, { initialState });
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  public onComplete(step) {
    this.onResetError();
    step.isValidated = true;
    let handler = this;
    if (step.isValidated && this.authService.isAuthenticated()) {
      // this.dbmodelSubmit= new BOMSheetDB();
      // if(this.model.productStatus === 'T'){
      //   this.model.productStatus = "S";
      // }
      // this.dbmodelSubmit.mapToDB(this.model, this.isProcessorMode());
      // this.dbmodelSubmit.setStep(6);
      this.busySubmitApplication = this.formsService.createStep(this.model, 6).subscribe(data => {
        handler.onShowBodyError(data);
        if(!this.showErrorFlag){
          if(!isNil(data)){
            // handler.model.tpnumber = data["Appl Ref"];
            handler.sendSubmissionCompletionEmail(handler.model);
          } else {
            console.log(handler.model);
            handler.onShowError("Submission Failed: Failed to return Reference ID for Application ID: " + handler.model.uuid);
          }
        }
      }, error => {
        handler.onShowError(error);
      });
    }
  }

  public onClearStep1Form(step) {
    this.appDetailsPage.clearFields();
  }

  public onClearStep2Form(step) {
    this.prodDetailsPage.clearFields();
  }

  public onClearStep3Form(step) {
    this.serviceDetailsPage.clearFields();
  }

  public onClearStep4Form(step) {
    this.managedServicesDetailsPage.clearFields();
  }

  public onClearStep5Form(step) {
    this.awsDetailsPage.clearFields();
  }

  public onClearStep6Form(step) {
    this.cotsDetailsPage.clearFields();
  }

  public displayConfirmationMessage(referenceId) {
    const initialState = {
      message: 'Submission Completed. Reference ID: '  + referenceId
        + ". Note: Please check your SPAM folder for confirmation email and add to safe sender's list. ",
      title: 'Submission Confirmation'
    };
    this.bsModalRef = this.modalService.show(BOMMessageBox, { initialState });
    this.bsModalRef.content.closeBtnName = 'Close';
    this.isCompleted = true;
  }

  public displayErrorMessagePopup(title, message) {
    const initialState = {
      message: message,
      title: title,
    };
    this.bsModalRef = this.modalService.show(BOMMessageBox, { initialState });
    this.bsModalRef.content.closeBtnName = 'Close';
    this.isCompleted = true;
  }

  onResetError(): void {
    this.showErrorFlag = false;
    this.alerts = [];
  }

  onShowError(error: any): void {
    this.showErrorFlag = true;
    this.alerts = [];
    this.alerts.push({ type: "warning", msg: "Error: " + error });
    // jQuery.scrollTo(jQuery("#pg-tbar"), 1000);
  }

  onShowBodyError(body: any): boolean {
    if (!isNil(body) && !isNil(body.errorMessage)) {
      let err: string = body.errorMessage;
      if(err.toUpperCase().includes("ALREADY EXISTS")){
        this.displayErrorMessagePopup("Application Exists", body.errorMessage);
        this.showErrorFlag = true;
      }else{
        this.showErrorFlag = true;
        this.alerts = [];
        this.alerts.push({ type: "warning", msg: "Error: " + body.errorMessage });
      }
      return true;
    }
    return false;
  }

  isDebugAllowed(){
    return(!environment.production);
  }

  public displaySubmissionAgreement() {
    this.agreeModal.show();
  }

  public cancelSubmit(){
    this.agreeModal.hide();
  }

  public agreetoSubmit(){
    this.agreeModal.hide();
    this.formWizard.activeStep.isAgreed = true;
    this.formWizard.proceedComplete();
  }

}
