import { TitleCasePipe } from '@angular/common';
import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChange, ViewEncapsulation } from "@angular/core";
import { LocalStorageService } from 'ngx-webstorage';
import { isNil } from 'ramda';
import { ApplicantService } from '../../bom/services/applicant.service';
import { ProfileUser } from '../../login/model/profile-user.model';
import { AuthService } from '../../login/services/auth.service';
import { BOMSheet } from "../models/bom/bom-sheet.model";

declare var jQuery: any;

@Component({
  selector: "bom-service-details",
  styleUrls: ['./create-bom.style.scss'],
  templateUrl: "./bom-service-details.template.html",
  encapsulation: ViewEncapsulation.None
})
export class BOMServiceDetailsTabPageComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {
  @Input() formdata: BOMSheet;
  @Output() validate = new EventEmitter<boolean>();

  showErrorFlag: boolean;
  alerts: Array<Object>;

  validDate="1";
  maxDate = new Date();

  showTaxYear: boolean = false;

  pastyearallowed="5";

  yearMask = {
    mask: [/[1-2]/, /[0-9]/, /[0-9]/, /[0-9]/]
  };



  zipCodeMask = {
    mask: [
      /[0-9]/,
      /[0-9]/,
      /[0-9]/,
      /[0-9]/,
      /[0-9]/
    ]
  };

  zipOptionCodeMask = {
    mask: [
      /[0-9]/,
      /[0-9]/,
      /[0-9]/,
      /[0-9]/
    ]
  };

  phoneMask = {
    mask: [
      /[0-9]/,
      /[0-9]/,
      /[0-9]/,
      "-",
      /[0-9]/,
      /[0-9]/,
      /[0-9]/,
      "-",
      /[0-9]/,
      /[0-9]/,
      /[0-9]/,
      /[0-9]/
    ]
  };

  dateMask = {
    mask: [/[0-1]/, /[0-9]/, "/", /[0-3]/, /[0-9]/, "/", /[1-2]/, /[0-9]/, /[0-9]/, /[0-9]/]
  };


  constructor(
    private storage: LocalStorageService,
    private applicantService: ApplicantService,
    private auth: AuthService,
    private titlecasePipe:TitleCasePipe
  ){
    this.showErrorFlag = false;
    this.alerts = [{ type: 'warning',  msg: '' }];
  }

  ngOnInit(): void {
    // this.formdata.profileid = this.storage.retrieve("LOGGEDUSERID");
    // if(!this.isProcessorMode()) {
    //   this.applicantService.getUserProfileData(this.formdata.profileid).subscribe(
    //     data => {
    //       if (!isNil(data)) {
    //         let user: ProfileUser = new ProfileUser();
    //         user.fromDB(data);
    //       }
    //     });
    // }
  }

  ngOnDestroy(): void {}

  ngAfterViewInit(): void {
    jQuery('#frm-app-details').parsley();
  }

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    for (let propName in changes) {
      if (propName === "formdata") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.formdata = chg.currentValue;
        }
      }
    }
  }

  public skipOnlyRequiredValidation(){
    jQuery('#frm-app-details .rq').each(function() {
      jQuery(this).removeAttr('required');
    });
  }

  /**
   *
   * @param isnextstep : false - Save and exit - skip validation
   */
  public validateForm(isnextstep): boolean{
    let isValidForm: boolean = true;
    if(!isnextstep){
      this.skipOnlyRequiredValidation();
    }
    jQuery('#frm-app-details').parsley().validate();
    isValidForm = jQuery('#frm-app-details').parsley().isValid();
    return isValidForm;
  }

  public isProcessorMode(){
    return this.auth.isProcessorMode();
  }

  public clearFields(){
  }

  public dblv(val: number) {
    if (isNil(val)) {
      return 0.0;
    }
    return Number(val);
  }

}
