import { AfterViewInit, Component, EventEmitter, Input, OnChanges,
   OnDestroy, OnInit, Output, SimpleChange, ViewEncapsulation } from "@angular/core";
import { LocalStorageService } from 'ngx-webstorage';
import { ApplicantService } from '../../bom/services/applicant.service';
import { AuthService } from '../../login/services/auth.service';
import { BOMSheet } from "../models/bom/bom-sheet.model";

declare let jQuery: any;
@Component({
  // tslint:disable-next-line: component-selector
  selector: "bom-prod-details",
  styleUrls: ['./create-bom.style.scss'],
  templateUrl: "./bom-prod-details.template.html",
  encapsulation: ViewEncapsulation.None
})
export class BOMProductDetailsTabPageComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {
  @Input() formdata: BOMSheet;
  @Output() validate = new EventEmitter<boolean>();

  showErrorFlag: boolean;
  piienabled: boolean;
  phienabled: boolean;
  hippaenabled: boolean;
  irsenabled: boolean;

  alerts: Array<Object>;

  constructor(
    private storage: LocalStorageService,
    private applicantService: ApplicantService,
    private auth: AuthService
    ){
    this.showErrorFlag = false;
    this.alerts = [{ type: 'warning',  msg: '' }];
  }

  ngOnInit(): void {
    // this.formdata.profileid = this.storage.retrieve("LOGGEDUSERID");
  }

  ngOnDestroy(): void {}

  ngAfterViewInit(): void {
    jQuery('#frm-prod-details').parsley();
  }

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    for (let propName in changes) {
      if (propName === "formdata") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.formdata = chg.currentValue;
          this.piienabled = (this.formdata.pii === "Y" ? true : false);
          this.phienabled = (this.formdata.phi === "Y" ? true : false);
          this.hippaenabled = (this.formdata.hippa === "Y" ? true : false);
          this.irsenabled = (this.formdata.irs === "Y" ? true : false);
        }
      }
    }
  }

  public skipOnlyRequiredValidation(){
    jQuery('#frm-prod-details .rq').each(function() {
      jQuery(this).removeAttr('required');
    });
  }

  /**
   *
   * @param isnextstep : false - Save and exit - skip validation
   */
  public validateForm(isnextstep): boolean{
    let isValidForm: boolean = true;
    if(!isnextstep){
      this.skipOnlyRequiredValidation();
    }
    jQuery('#frm-prod-details').parsley().validate();
    isValidForm = jQuery('#frm-prod-details').parsley().isValid();

    this.formdata.pii = (this.piienabled === true ? "Y" : "N");
    this.formdata.phi = (this.phienabled === true ? "Y" : "N");
    this.formdata.hippa = (this.hippaenabled === true ? "Y" : "N");
    this.formdata.irs = (this.irsenabled === true ? "Y" : "N");

    return isValidForm;
  }

  public clearFields(){
  }

}
