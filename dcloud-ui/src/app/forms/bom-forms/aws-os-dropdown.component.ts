import { ICellEditorAngularComp } from 'ag-grid-angular';
import { AfterViewInit, ElementRef, ViewChild, Component, OnDestroy } from '@angular/core';
import * as _ from 'lodash';
import { LookupService } from '../../admin/services/lookup.service';
import { Subscription } from 'rxjs';

@Component({
  template: `
  <select [(ngModel)]="selectedValue" (change)="selectionChanged()" #dropdown style="width:100%">
    <option *ngFor="let item of items" [ngValue]="item.refValue">{{item.refValue}}</option>
  </select>
  `
})
export class AWSOperatingSystemsDropdownEditorComponent implements ICellEditorAngularComp, AfterViewInit, OnDestroy {

  private params: any;
  public items: any[];
  public selectedValue: any;
  private selectControl: ElementRef;
  public busyLoadLookups: Subscription;

  @ViewChild('dropdown', {static: true}) dropdown: ElementRef;

  constructor(private lookupService: LookupService) {
      this.items = [];
  }
  ngAfterViewInit() {
      this.selectControl = this.dropdown;
  }

  ngOnDestroy(): void {
    if(this.busyLoadLookups){
      this.busyLoadLookups.unsubscribe();
    }
  }

  agInit(params: any): void {
    if(this.items.length === 0) {
      this.params = params;
      this.busyLoadLookups = this.lookupService.getAllByRefTypeId(3)
          .subscribe(result => {
              this.items = result.refValues;
              this.selectedValue = _.find(this.items, item => item.refValue === params.value);
          });
    }
  }

  getValue(): any {
      return this.selectedValue;
  }
  isPopup(): boolean {
      return false;
  }
  setValue(value: any): any {
      this.selectedValue = value;
  }
  selectionChanged(): void {
      // whatever you want to do
  }
}
