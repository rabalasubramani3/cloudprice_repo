import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';
import { isNil } from 'ramda';
import { Subscription } from 'rxjs';
import { environment } from '../../../environments/environment';
import { ApplicantService } from '../../bom/services/applicant.service';
import { OnComponentErrorHandler } from '../../core/coreerror.handler';
import { BOMReviewSubmitTabPage } from './bom-review-submit.component';
import { BOMSheet } from '../models/bom/bom-sheet.model';
import { BOMFormsService } from '../services/bom-forms.service';

declare var jQuery: any;

@Component({
  selector: 'view-bomform',
  templateUrl: './view-bomform.template.html',
  styleUrls: ['./view-bomform.style.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [BOMFormsService, ApplicantService]
})

export class ViewBOMFormPageComponent implements OnInit, OnDestroy, OnComponentErrorHandler {

  @ViewChild(BOMReviewSubmitTabPage, {static: true}) reviewSubmitPage: BOMReviewSubmitTabPage;

  router: Router;
  showErrorFlag: boolean;
  alerts: Array<Object>;
  busy: Subscription;
  model: BOMSheet;
  errorMessage: string;
  isCompleted: boolean;
  sub: any;
  applicationId: string;
  applmodel: BOMSheet;

  constructor(router: Router,
    private route: ActivatedRoute,
    private applicantService: ApplicantService,
    private storage: LocalStorageService,
  ) {
    this.router = router;

    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span>'
      }
    ];

    this.showErrorFlag = false;
  }

  // createSampleData() {
  //   this.model.name.firstName = "Will";
  //   this.model.name.middleInitial = "M.";
  //   this.model.name.lastName = "Ryan";
  //   this.model.SSN = "123-45-6789";
  //   this.model.DOB = "01/01/1970";
  //   this.model.startlivingDate = "01/01/1970";
  //   // this.model.rentresidencetype = "Apartment Building";
  //   this.model.daytimePhone = "(212) 234-2345";
  //   this.model.address.streetName = "123 Main Street";
  //   this.model.address.aptNum = "2";
  //   this.model.address.cityTown = "City";
  //   this.model.address.zipOptional = "";
  //   this.model.address.countyName = "5";
  //   this.model.address.zipCode = "32746";
  //   // this.model.prevaddress.streetName = "123 Cross Street";
  //   // this.model.prevaddress.aptNum = "2";
  //   // this.model.prevaddress.cityTown = "Cross City";
  //   // this.model.prevaddress.zipOptional = "";
  //   // this.model.prevaddress.countyName = "5";
  //   // this.model.prevaddress.zipCode = "32746";
  //   this.model.mailaddress.streetName = "456 Side Street";
  //   this.model.mailaddress.aptNum = "2";
  //   this.model.mailaddress.cityTown = "Side City";
  //   this.model.mailaddress.zipOptional = "";
  //   this.model.mailaddress.countyName = "5";
  //   this.model.mailaddress.zipCode = "32746";
  //   // this.model.publicHousing = "Yes";
  //   // this.model.isDisabled = "Yes";
  //   // this.model.maritalStatus = "Married";
  //   // this.model.residentialType = 1;
  //   // this.model.applicantStatus.over60 = true;
  //   // this.model.applicantStatus.disabled = true;
  //   // this.model.applicantStatus.survivingspouse = true;
  //   // this.model.applicantStatus.under60withchild = true;

  //   this.model.ownRealEstate = "Yes";
  //   this.model.rentalRecipient = "A Relative";
  //   this.model.rentalRecipientRelation = "Sister";
  //   this.model.rentalRecipientStartDate = "01/01/1970";
  //   this.model.rentRecipientName.firstName = "Tony";
  //   this.model.rentRecipientName.middleInitial = "M.";
  //   this.model.rentRecipientName.lastName = "Ryan";
  //   this.model.rentalRecipientPhoneNumber = "(212) 234-2345";
  //   this.model.rentalRecipientMgmtCompany = "ABC Rental Company";
  //   this.model.rentalRecipientAddress.streetName = "456 Cross Street";
  //   this.model.rentalRecipientAddress.aptNum = "2";
  //   this.model.rentalRecipientAddress.cityTown = "City";
  //   this.model.rentalRecipientAddress.zipOptional = "";
  //   this.model.rentalRecipientAddress.countyName = "5";
  //   this.model.rentalRecipientAddress.zipCode = "32746";

  //   this.model.isrelativerental = true;
  //   this.model.spousename.firstName = "Janet";
  //   this.model.spousename.middleInitial = "F.";
  //   this.model.spousename.lastName = "Ryan";
  //   this.model.spouseDOB = "01/01/1970";
  //   this.model.spouseSSN = "333-44-5555";
  //   // this.model.isSpouseDisabled = "Yes";

  //   this.model.isCoOwnerPresent = "Yes";
  //   let cot1 = new TPLApplicantCoOwnerType();
  //   cot1.memberId = 1;
  //   cot1.memtype = 1;
  //   cot1.name.firstName = "Jack";
  //   cot1.name.lastName = "Ryan"
  //   cot1.name.middleInitial = "M";
  //   cot1.DOB = "01/01/1970";
  //   cot1.SSN = "333-44-5555";
  //   // cot1.isDisabled = "No";
  //   cot1.relationship = "Child";
  //   cot1.grossIncome = 10000.00;
  //   cot1.isIncomeLastYear = "Yes";
  //   cot1.boardContribution = 50;
  //   this.model.coOwners.push(cot1);

  //   this.model.isHouseholdResidentPresent = "Yes";
  //   let hhold1 = new TPLApplicantCoOwnerType();
  //   hhold1.memberId = 1;
  //   hhold1.memtype = 2;
  //   hhold1.name.firstName = "Jill";
  //   hhold1.name.lastName = "Ryan";
  //   hhold1.name.middleInitial = "X";
  //   hhold1.DOB = "01/01/1970";
  //   hhold1.SSN = "333-44-5559";
  //   // hhold1.isDisabled = "No";
  //   hhold1.relationship = "Child";
  //   hhold1.grossIncome = 10000.00;
  //   hhold1.isIncomeLastYear = "Yes";
  //   hhold1.boardContribution = 50;
  //   this.model.householdResidents.push(hhold1);

  //   this.model.income.wages.applicant = 300.00;
  //   this.model.income.interestdividends.spouse = 400.00;

  //   console.dir(JSON.stringify(this.model));
  // }

  ngOnInit(): void {
    // this.model = new BOMSheet();
    // this.applmodel = new TPLProduct();
    // this.model.email = this.storage.retrieve("LOGGEDUSEREMAIL");
    if(!environment.production){
      // this.createSampleData();
    }
    // this.loadApplicationInfo();
    this.showErrorFlag = false;
  }

  // Load tpl Application info
  // loadApplicationInfo() {
  //   let handler = this;
  //   this.sub = this.route.params.subscribe(params => {
  //     if (params["id"]) {
  //       this.applicationId = params["id"];
  //       this.busy = this.applicantService
  //         .getTPLApplication(this.applicationId)
  //         .subscribe(
  //           data => {
  //             if (data) {
  //               handler.onShowBodyError(data);
  //               handler.applmodel = data;
  //               handler.model.fromDB(data);
  //             }
  //           },
  //           error => {
  //             this.onShowError(error);
  //           }
  //         );
  //     }
  //   });
  // }

  public ngOnDestroy(): void {
    if (this.busy) {
      this.busy.unsubscribe();
    }
  }

  onResetError(): void {
    this.showErrorFlag = false;
    this.alerts = [];
  }

  onShowError(error: any): void {
    this.showErrorFlag = true;
    this.alerts = [];
    this.alerts.push({ type: "warning", msg: "Error: " + error });
    // jQuery.scrollTo(jQuery("#pg-tbar"), 1000);
  }

  onShowBodyError(body: any): void {
    if (!isNil(body) && !isNil(body.errorMessage)) {
      this.showErrorFlag = true;
      this.alerts = [];
      this.alerts.push({ type: "warning", msg: "Error: " + body.errorMessage });
      // jQuery.scrollTo(jQuery("#pg-tbar"), 1000);
    }
  }

  isDebugAllowed(){
    return(!environment.production);
  }
}
