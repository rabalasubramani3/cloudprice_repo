import { AfterViewInit, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChange, ViewEncapsulation } from "@angular/core";
import * as moment from 'moment';
import { FileSystemDirectoryEntry, FileSystemFileEntry, UploadEvent, UploadFile } from 'ngx-file-drop';
import { Lightbox } from 'ngx-lightbox';
import { LocalStorageService } from 'ngx-webstorage';
import { isNil } from 'ramda';
import { Subscription } from 'rxjs';
import { OnComponentErrorHandler } from "../../core/coreerror.handler";
import { AuthService } from '../../login/services/auth.service';
import { BOMDocumentResponse } from '../models/bom/bom-doc-response.model';
import { BOMSheet } from "../models/bom/bom-sheet.model";
import { UploadChecker } from '../models/upload-checker.model';
import { UploadTracker } from '../models/upload-tracker.model';
import { BOMFormsService } from '../services/bom-forms.service';

declare var jQuery: any;

@Component({
  selector: "bom-doc-upload",
  templateUrl: "./bom-doc-upload.template.html",
  encapsulation: ViewEncapsulation.None
})
export class BOMDocumentUploadPage implements OnInit, OnDestroy, OnComponentErrorHandler, AfterViewInit, OnChanges {
  @Input() formdata: BOMSheet;
  showErrorFlag: boolean;
  busyUploadFile: Subscription;
  listUploadedFiles: BOMDocumentResponse[] = [];
  uploadtracker: UploadTracker[] = [];
  alerts: Array<Object>;
  // verify user has uploaded selected files
  checklist: Array<UploadChecker> = [];
  warned: boolean = true;

  // message to display
  msgDisplay: string;

  // title to display
  titleDisplay: string;
  dynamic: number;
  documentKey: string;
  filesToUpload: Array<File> = [];
  docFileName: string;
  public droppedfiles: UploadFile[] = [];

  constructor(public auth: AuthService,
    private _lightbox: Lightbox,
    private storage: LocalStorageService,
    private formsService: BOMFormsService){
    this.showErrorFlag = false;
    this.alerts = [{ type: 'warning',  msg: '' }];
    this.listUploadedFiles = [];
    this.uploadtracker = [];
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if(this.busyUploadFile){
      this.busyUploadFile.unsubscribe();
    }
  }

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    for (let propName in changes) {
      if (propName === "formdata") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.formdata = chg.currentValue;
        }
      }
    }
  }
  public uploadAllDroppedFiles(){
    let that = this;
    this.auth.getAuthenticatedUser()
      .subscribe(
      data => {
        if (!isNil(data)) {
          let index = 0;
          for (const droppedFile of this.droppedfiles) {
            if (droppedFile.fileEntry.isFile) {
              const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
              fileEntry.file((file: File) => {
                // that.documentKey = that.formdata.establishmentid + "/uploads/" + that.formdata.documents[0].productname + "/" + file.name;
                // tslint:disable-next-line: max-line-length
                // that.uploadDroppedFileToPrivateFolder(that.documentKey, index++, file, that.formdata.establishmentid, that.formdata.documents[0].productname);
              });
            }
          }
          if(!isNil(data.errorMessage)){
            that.onShowBodyError(data);
          }
        }
      },
      error => {
        this.onShowError(error);
      }
    );

  }
  public dropped(event: UploadEvent) {
    this.droppedfiles = event.files;
    this.uploadtracker = [];
    let that = this;
    let index = 0;
    for (const droppedFile of event.files) {

      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {

          let trk = new UploadTracker();
          trk.id = index++;
          trk.name = droppedFile.relativePath;
          trk.size = file.size;
          trk.modificationTime = new Date(file.lastModified);
          trk.progress = 0;
          that.uploadtracker.push(trk);

          // Here you can access the real file
          // console.log(droppedFile.relativePath, file);

          /**
          // You could upload it like this:
          const formData = new FormData()
          formData.append('logo', file, relativePath)

          // Headers
          const headers = new HttpHeaders({
            'security-token': 'mytoken'
          })

          this.http.post('https://mybackend.com/api/upload/sanitize-and-save-logo', formData, { headers: headers, responseType: 'blob' })
          .subscribe(data => {
            // Sanitized logo returned from backend
          })
          **/

        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  public fileOver(event){
    console.log(event);
  }

  public fileLeave(event){
    console.log(event);
  }

  public skipOnlyRequiredValidation(){
    jQuery('#frm-rent-details .rq').each(function() {
      jQuery(this).removeAttr('required');
    });
  }

  public validateForm(isnextstep): boolean{
    if(!(this.uploadedFiles() || this.warned)) {
      this.onShowError('Please click the blue “Upload” button for each of your selected files to complete the upload process.');
      this.warned = true;
      return false;
    }
    else {
      this.onResetError();
    }
    let isValidForm: boolean = true;
    if(!isnextstep){
      this.skipOnlyRequiredValidation();
    }
    jQuery('#frm-rent-details').parsley().validate();
    isValidForm = jQuery('#frm-rent-details').parsley().isValid();
    return isValidForm;
  }

  ngAfterViewInit(): void {
    this.getListOfDocumentsUploaded();
  }

  public clearFields(){

  }
  onResetError(): void {
    this.showErrorFlag = false;
    this.alerts = [];
  }

  onShowError(error: any): void {
    this.showErrorFlag = true;
    this.alerts = [];
    this.alerts.push({ type: "warning", msg: "Error: " + error });
    // jQuery.scrollTo(jQuery("#pg-tbar"), 1000);
  }

  onShowBodyError(body: any): void {
    if (!isNil(body) && !isNil(body.errorMessage)) {
      this.showErrorFlag = true;
      this.alerts = [];
      this.alerts.push({ type: "warning", msg: "Error: " + body.errorMessage });
      // jQuery.scrollTo(jQuery("#pg-tbar"), 1000);
    }
  }

  fileChangeEvent(event, index) {
    this.selected(index);
    this.filesToUpload[index] = (<Array<File>>event.target.files)[0];
  }
  public isProcessorMode(){
    return this.auth.isProcessorMode();
  }

  public getListOfDocumentsUploaded(){
    // this.formsService.getListOfUploadedDocuments(this.formdata.establishmentid, this.formdata.tpnumber).subscribe(data => {
    //   this.listUploadedFiles = data;
    //   this.onShowBodyError(data);
    // }, error => {
    //   this.onShowError(error);
    // });
  }

  public updateDocumentUploadStatus(){
    this.formsService.updateDocumentUploadStatus(this.formdata).subscribe(data => {
      this.onShowBodyError(data);
    }, error => {
      this.onShowError(error);
    });
  }

  public uploadDroppedFileToPrivateFolder(objKey, index, file: File, estb_id: string, product_id: string){
    let that = this;
    this.uploadtracker[index].starttime = new Date();

    this.busyUploadFile = this.auth.upload(objKey, file, {
      level: 'public',
      contentType: file.type,
      metadata: {"estb-id": estb_id, "product-id": product_id  },
      progressCallback(progress) {
        if(progress.total > 0){
          that.uploadtracker[index].progress = (progress.loaded/progress.total) * 100.0;
        }
        console.log(`Uploaded: ${progress.loaded}/${progress.total}`);
      },
    })
    .subscribe(
      data => {
        if (!isNil(data) && !isNil(data.errorMessage)) {
          that.displayUploadStatusMessage("Upload Status", "Failed to Upload Document!" );
          this.onShowBodyError(data);
        } else {
          this.uploadtracker[index].endtime = new Date();
          // tslint:disable-next-line: max-line-length
          let duration = moment.duration(moment(this.uploadtracker[index].endtime).diff(moment(this.uploadtracker[index].starttime)));
          this.uploadtracker[index].uploadtotalduration = duration.asSeconds();
          // that.displayUploadStatusMessage("Upload Status", "Document Uploaded Successfully!" );
          // let rowIndex = _.findIndex(this.formdata.documents, function (o) { return (o.name && o.name.toString() === docName)});
          // if(rowIndex !== -1) {
          //     let selRow = this.formdata.documents[rowIndex];
          //     selRow.fileName = file.name;
          //     selRow.status = 'Y';
          //     this.uploaded(index);
          // }
          // that.updateDocumentUploadStatus();
          that.getListOfDocumentsUploaded();
        }
      },
      error => {
        this.onShowError(error);
      }
    );
  }

  public uploadFileToPrivateFolder(objKey, index, docName){
    let that = this;
    this.dynamic = 0.0;
    this.auth.upload(objKey, this.filesToUpload[index], {
      level: 'public',
      contentType: this.filesToUpload[index].type,
      progressCallback(progress) {
        if(progress.total > 0){
          that.dynamic = (progress.loaded/progress.total) * 100.0;
        }
        console.log(`Uploaded: ${progress.loaded}/${progress.total}`);
      },
    })
    .subscribe(
      data => {
        if (!isNil(data) && !isNil(data.errorMessage)) {
          that.displayUploadStatusMessage("Upload Status", "Failed to Upload Document!" );
          this.onShowBodyError(data);
        } else {
          // that.displayUploadStatusMessage("Upload Status", "Document Uploaded Successfully!" );
          // let rowIndex = _.findIndex(this.formdata.documents, function (o) { return (o.name && o.name.toString() === docName)});
          // if(rowIndex !== -1) {
          //     let selRow = this.formdata.documents[rowIndex];
          //     selRow.fileName = this.filesToUpload[index].name;
          //     selRow.status = 'Y';
          //     this.uploaded(index);
          // }
          // that.updateDocumentUploadStatus();
          // that.getListOfDocumentsUploaded();
        }
      },
      error => {
        this.onShowError(error);
      }
    );
  }

  public uploadFile(index, docName, productname){
    if (!isNil(this.filesToUpload[index]) && this.filesToUpload[index].name.length > 0) {
      let that = this;
      this.auth.getAuthenticatedUser()
        .subscribe(
        data => {
          if (!isNil(data)) {
            // that.documentKey = "products/" + this.formdata.establishmentid + "/" + this.filesToUpload[index].name;
            // that.uploadFileToPrivateFolder(that.documentKey, index, docName);
            // if(!isNil(data.errorMessage)){
            //   that.onShowBodyError(data);
            // }
          }
        },
        error => {
          this.onShowError(error);
        }
      );
    }
  }


    // reset flag and empty title and message
    public onCloseInfoPopup(){
      this.msgDisplay = "";
      this.titleDisplay = "";
      jQuery('#info-popup').modal('hide');
  }

  // Display upload success message
  protected displayUploadStatusMessage(title:string, msg:string){
      this.msgDisplay = msg;
      this.titleDisplay = title;
      jQuery('#info-popup').modal('show');
  }

  // local checklist to verify that upload buttons have been pushed
  private selected(index: string) {
    this.warned = false;
    let entry = new UploadChecker();
    entry.index = index;
    entry.uploaded = "N";
    this.checklist.push(entry);
  }

  private uploaded(index: string) {
    let entry:UploadChecker = this.findUpload(index);
    entry.uploaded = 'Y';
    if(this.uploadedFiles()) {
      this.onResetError();
    }
  }

  private findUpload(index:string):UploadChecker {
    for(let i=0;i<this.checklist.length;i++) {
      if(this.checklist[i].index === index) {
        return this.checklist[i];
      }
    }
  }

  public uploadedFiles():boolean {
    for(let i=0;i<this.checklist.length;i++) {
      if(this.checklist[i].uploaded !== 'Y') {
        return false;
      }
    }
    return true;
  }

  public downloadFile(doc:BOMDocumentResponse){
      // let productname = this.formdata.documents[0].productname;
      // this.documentKey = this.formdata.establishmentid + "/uploads/" + productname + "/" + doc.filename;
      // // this.documentKey = formId + "/uploads/" + doc.filename;
      // this.auth.download(this.documentKey, {
      //   level: 'public'
      // })
      // .subscribe(
      //   data => {
      //     if (!isNil(data) && !isNil(data.errorMessage)) {
      //       this.onShowBodyError(data);
      //     } else {
      //       let xhr = new XMLHttpRequest();
      //       xhr.open('GET', data);
      //       xhr.responseType = 'blob';
      //       xhr.onload = function () {
      //           saveAs(this.response, doc.filename);
      //       };
      //       xhr.send();
      //     }
      //   },
      //   error => {
      //     this.onShowError(error);
      //   }
      // );
  }

  public viewFile(doc:BOMDocumentResponse){
    // let productname = this.formdata.documents[0].productname;
    // this.documentKey = this.formdata.establishmentid + "/uploads/" + productname + "/" + doc.filename;
    // // this.documentKey = formId + "/uploads/" + doc.filename;
    // this.auth.download(this.documentKey, {
    //   level: 'public'
    // })
    // .subscribe(
    //   data => {
    //     if (!isNil(data) && !isNil(data.errorMessage)) {
    //       this.onShowBodyError(data);
    //     } else {
    //       this.albums = [];
    //       const album = {
    //         src: data,
    //         caption: doc.filename,
    //         thumb: ""
    //       };
    //       this.albums.push(album);
    //       this._lightbox.open(this.albums, 0);
    //       // let xhr = new XMLHttpRequest();
    //       // xhr.open('GET', data);
    //       // xhr.responseType = 'blob';
    //       // xhr.onload = function () {
    //       //     saveAs(this.response, doc.filename);
    //       // };
    //       // xhr.send();
    //     }
    //   },
    //   error => {
    //     this.onShowError(error);
    //   }
    // );
  }

}
