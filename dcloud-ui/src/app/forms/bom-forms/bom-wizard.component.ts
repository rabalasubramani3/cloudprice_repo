import { Component, Output, EventEmitter, ContentChildren, QueryList, AfterContentInit } from '@angular/core';
import { BOMWizardStepComponent } from './bom-wizard-step.component';
import { AuthService } from '../../login/services/auth.service';

@Component({
  selector: 'bom-form-wizard',
  template:
    `<div class="card">
    <div class="card-header">
      <ul class="nav nav-justified">
        <li class="nav-item" *ngFor="let step of steps" [ngClass]="{'active': step.isActive, 'enabled': !step.isDisabled, 'disabled': step.isDisabled, 'completed': isCompleted}">
          <a (click)="goToStep(step)">{{step.title}}</a>
        </li>
      </ul>
    </div>
    <div class="card-block">
      <ng-content></ng-content>
    </div>
    <div class="card-footer" [hidden]="isCompleted">
        <button type="button" class="btn btn-primary float-right" (click)="next()" [disabled]="!activeStep.isValid" [hidden]="!hasNextStep || !activeStep.showNext">Save and Continue</button>
        <button type="button" class="txt-gap btn btn-primary float-right" (click)="exitstep()" [disabled]="!activeStep.isValid" [hidden]="!hasNextStep || !activeStep.showNext">Save and Exit</button>
        <button type="button" class="txt-gap btn btn-primary float-right" (click)="complete()" [disabled]="!activeStep.isValid" [hidden]="!hasNextStep">Save BOM</button>
        <button type="button" class="txt-gap btn btn-primary float-right" (click)="previous()" [hidden]="!hasPrevStep || !activeStep.showPrev">Back</button>
    </div>
  </div>`
  ,
  styles: [
    '.card { height: 100%; }',
    '.card-header { background-color: #fff; padding: 0; font-size: 1.25rem; }',
    '.card-block { overflow-y: auto; }',
    '.card-footer { background-color: #fff; border-top: 0 none; }',
    '.nav-item { padding: 1rem 0rem; border-bottom: 0.5rem solid #ccc; }',
    '.active { font-weight: bold; color: black; border-bottom-color: red !important; }',
    '.enabled { cursor: pointer; border-bottom-color: rgb(88, 162, 234); }',
    '.disabled { color: #ccc; }',
    '.completed { cursor: default; }'
  ]
})
export class BOMWizardComponent implements AfterContentInit {
  @ContentChildren(BOMWizardStepComponent) wizardSteps: QueryList<BOMWizardStepComponent>;

  private _steps: Array<BOMWizardStepComponent> = [];
  private _isCompleted: boolean = false;
  private auth: AuthService;

  @Output() onStepChanged: EventEmitter<BOMWizardStepComponent> = new EventEmitter<BOMWizardStepComponent>();

  constructor(authService: AuthService) {
    this.auth = authService;
  }

  ngAfterContentInit() {
    this.wizardSteps.forEach(step => this._steps.push(step));
    this.steps[0].isActive = true;
  }

  get steps(): Array<BOMWizardStepComponent> {
    return this._steps.filter(step => !step.hidden);
  }

  get isCompleted(): boolean {
    return this._isCompleted;
  }

  get activeStep(): BOMWizardStepComponent {
    return this.steps.find(step => step.isActive);
  }

  set activeStep(step: BOMWizardStepComponent) {
    if (step !== this.activeStep && !step.isDisabled) {
      this.activeStep.isActive = false;
      step.isActive = true;
      this.onStepChanged.emit(step);
    }
  }

  public get activeStepIndex(): number {
    return this.steps.indexOf(this.activeStep);
  }

  get hasNextStep(): boolean {
    return this.activeStepIndex < this.steps.length - 1;
  }

  get hasPrevStep(): boolean {
    return this.activeStepIndex > 0;
  }

  public goToStep(step: BOMWizardStepComponent): void {
    if (!this.isCompleted) {
      this.activeStep = step;
    }
  }

  public next(): void {
    if (this.hasNextStep) {
      this.activeStep.onNext.emit(this.activeStep);
    }
  }

  public proceednext(): void {
    if (this.activeStep.isValidated) {
      let nextStep: BOMWizardStepComponent = this.steps[this.activeStepIndex + 1];
      nextStep.isDisabled = false;
      this.activeStep = nextStep;
    }
  }

  public exitstep(): void {
    this.activeStep.onStepSaveAndExit.emit(this.activeStep);
  }

  public previous(): void {
    if (this.hasPrevStep) {
      let prevStep: BOMWizardStepComponent = this.steps[this.activeStepIndex - 1];
      this.activeStep.onPrev.emit();
      prevStep.isDisabled = false;
      this.activeStep = prevStep;
    }
  }

  public proceedComplete() {
    if (this.activeStep.isAgreed) {
      this.activeStep.onComplete.emit(this.activeStep);
      this._isCompleted = true;
    }
  }

  public complete(): void {
    this.auth.getAuthenticatedUser().subscribe(awsUser => {
      if (awsUser.attributes['email_verified'] === false) {
        this.activeStep.onSubmitValidate.emit(this.activeStep);
      }
      else {
        this.activeStep.onSubmitAgree.emit(this.activeStep);
      }
    })
  }

  public clear(): void {
    this.activeStep.onClear.emit(this.activeStep);
  }

}
