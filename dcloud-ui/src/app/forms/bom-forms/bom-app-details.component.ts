import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChange,
  ViewEncapsulation, AfterViewInit, OnChanges } from "@angular/core";
import { LocalStorageService } from 'ngx-webstorage';
import { isNil } from 'ramda';
import { ApplicantService } from '../../bom/services/applicant.service';
import { AuthService } from '../../login/services/auth.service';
import { BOMSheet } from "../models/bom/bom-sheet.model";

declare var jQuery: any;

@Component({
  // tslint:disable-next-line: component-selector
  selector: "bom-app-details",
  styleUrls: ['./create-bom.style.scss'],
  templateUrl: "./bom-app-details.template.html",
  encapsulation: ViewEncapsulation.None
})
export class BOMApplicantDetailsTabPageComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {
  @Input() formdata: BOMSheet;
  @Output() validate = new EventEmitter<boolean>();

  showErrorFlag: boolean;
  alerts: Array<Object>;

  constructor(
    private storage: LocalStorageService,
    private applicantService: ApplicantService,
    private auth: AuthService
  ){
    this.showErrorFlag = false;
    this.alerts = [{ type: 'warning',  msg: '' }];
  }

  ngOnInit(): void {
    // this.formdata.profileid = this.storage.retrieve("LOGGEDUSERID");
  }

  ngOnDestroy(): void {}

  ngAfterViewInit(): void {
    jQuery('#frm-app-details').parsley();
  }

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    for (let propName in changes) {
      if (propName === "formdata") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.formdata = chg.currentValue;
        }
      }
    }
  }

  public skipOnlyRequiredValidation(){
    jQuery('#frm-app-details .rq').each(function() {
      jQuery(this).removeAttr('required');
    });
  }

  /**
   *
   * @param isnextstep : false - Save and exit - skip validation
   */
  public validateForm(isnextstep): boolean{
    let isValidForm: boolean = true;
    if(!isnextstep){
      this.skipOnlyRequiredValidation();
    }
    jQuery('#frm-app-details').parsley().validate();
    isValidForm = jQuery('#frm-app-details').parsley().isValid();
    return isValidForm;
  }

  public clearFields(){
  }

}
