import { Component, OnDestroy, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { Subscription } from 'rxjs';
import { LookupService } from '../../../admin/services/lookup.service';
import { ProductGroupService } from '../../services/productgroup.service';
import * as _ from 'lodash';

declare var jQuery: any;

@Component({
  selector: 'app-add-cots-prodgroup',
  templateUrl: './add-cots-prodgroup.template.html',
  styleUrls: ['./add-cots-prodgroup.style.css']
})
export class AddCOTSProductGroupComponent implements OnInit, OnDestroy {
  public environments: any[];
  public productgroups: any[];
  public selProductGroupInfo: any;
  public selEnvironment: string;
  public selProductGroupId: number;
  public addProductGroupFlag: boolean;

  public showErrorFlag: boolean;

  public alerts: any[];

  public busyLoadProductGroups: Subscription;
  public busyLoadBuildEnvironments: Subscription;

  constructor(
      public bsModalRef: BsModalRef,
      private lookupService: LookupService,
      private prodgroupService: ProductGroupService
    ) {
      this.showErrorFlag = false;
      this.alerts = [];
      this.addProductGroupFlag = false;
    }

  ngOnInit() {
    this.loadEnvironments();
    this.loadProductGroups();
  }

  ngOnDestroy(): void {
    if(this.busyLoadBuildEnvironments){
      this.busyLoadBuildEnvironments.unsubscribe();
    }
    if(this.busyLoadProductGroups){
      this.busyLoadProductGroups.unsubscribe();
    }
  }

  public loadEnvironments(){
    let that = this;
    this.busyLoadBuildEnvironments = this.lookupService.getAllByRefTypeName("BUILD_ENVIRONMENTS").subscribe(data => {
      if (data) {
        that.environments = data.refValues;
      }
    }, error => {
      that.showErrorFlag = true;
      that.alerts = [];
      that.alerts.push({ type: 'warning', msg: error });
    });
  }

  public loadProductGroups(){
    let that = this;
    this.busyLoadProductGroups = this.prodgroupService.getAllProductGroups().subscribe(data => {
      if (data) {
        that.productgroups = data.prodgroups;
      }
    }, error => {
      that.showErrorFlag = true;
      that.alerts = [];
      that.alerts.push({ type: 'warning', msg: error });
    });
  }

  public onAddProductGroup(){
    jQuery('#addproductsgroupform').parsley().validate();

    // Close only if validation passes
    if (jQuery('#addproductsgroupform').parsley().isValid()) {
      this.addProductGroupFlag = true;
      this.selProductGroupInfo = _.find(this.productgroups, item => item.productGroupId === this.selProductGroupId);
      this.bsModalRef.hide();
    }
  }

  reset(flag){
    this.addProductGroupFlag = false;
    this.bsModalRef.hide();
  }
}
