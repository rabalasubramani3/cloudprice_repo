import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChange, ViewEncapsulation } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular";
import * as _ from "lodash";
import { BsModalService } from 'ngx-bootstrap';
import { isNil } from 'ramda';
import { Subscription, Observable, forkJoin } from 'rxjs';
import { LookupService } from '../../admin/services/lookup.service';
import { AuthService } from '../../login/services/auth.service';
import { BOMAWS, BOMSheet } from "../models/bom/bom-sheet.model";
import { AWSComponentSizesDropdownEditorComponent } from './aws-compsizes-dropdown.component';
import { AWSOperatingSystemsDropdownEditorComponent } from './aws-os-dropdown.component';
import { AWSServicesDropdownEditorComponent } from './aws-services-dropdown.component';

declare let jQuery: any;

@Component({
  template: `<i *ngIf="params.node.group===false" class="fa fa-edit" (click)="invokeParentMethod()"
  data-toggle="modal" data-keyboard="false" data-backdrop="static" data-target="#edit-tdl" title="Edit AWS Row"
  ></i>`
})
export class EditAWSItemFromGridComponent implements ICellRendererAngularComp {
  params: any;

  agInit(params: any): void {
      this.params = params;
  }

  public invokeParentMethod() {
      this.params.context.componentParent.editAWSItemRow(this.params.data);
  }

  refresh(): boolean {
      return false;
  }
}


@Component({
  selector: 'delete-aws-row',
  template: `<i *ngIf="params.node.group===false" class="fa fa-trash txt-gap" (click)="invokeParentMethod()"
   data-toggle="modal" title = "Delete Row"
  data-keyboard="false" data-backdrop="static" data-target="#confirm-popup"></i>`
})
export class DeleteAWSRowFromGridComponent implements ICellRendererAngularComp {
  public params: any;

  agInit(params: any): void {
      this.params = params;
  }

  public invokeParentMethod() {
      this.params.context.componentParent.selectDeleteAWSItem(this.params.data);
  }

  refresh(): boolean {
      return false;
  }
}

@Component({
  // tslint:disable-next-line: component-selector
  selector: "bom-aws-details",
  styleUrls: ['./create-bom.style.scss'],
  templateUrl: "./bom-aws-details.template.html",
  encapsulation: ViewEncapsulation.None
})
export class BOMAWSDetailsTabPageComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {
  @Input() formdata: BOMSheet;
  @Output() validate = new EventEmitter<boolean>();

  public lookupitems: Map<string, any> = new Map<string, any>();
  public lookupcategories: any;
  public lookupOperatingSystems: any;
  public lookupSizes: any;
  public lookupLicensing: any;

  public gridApi;
  public gridColumnApi;

  public columnDefs;
  public rowData;
  public rowSelection;
  public defaultColDef;
  public editType;
  public busyLoadOperatingSystems: Subscription;
  public busyLoadSizes: Subscription;
  public busyLoadLicensingTerms: Subscription;

  public busyLoadLookupCategoriesAndItems: Subscription;
  public busyLoadAWSBOMItems: Subscription;

  showErrorFlag: boolean;
  alerts: Array<Object>;
  public categorymap = new Map<string, string>();

  constructor(
    private lookupService: LookupService,
    private modalService: BsModalService,
    private auth: AuthService
  ){
    let that = this;
    this.showErrorFlag = false;
    this.alerts = [{ type: 'warning',  msg: '' }];

    this.lookupLicensing = [];
    this.lookupOperatingSystems = [];
    this.lookupSizes = [];

    this.categorymap.set("COMPUTE", "AWS_COMPUTE_COMPONENTS");
    this.categorymap.set("STORAGE", "AWS_STORAGE_COMPONENTS");
    this.categorymap.set("SOFTWARE", "COTS_SOFTWARE");
    this.categorymap.set("SOFTWARE-MAINT", "COTS_SOFTWARE_MAINTENANCE");
    this.categorymap.set("AWS-NATIVE", "AWS_NATIVE_COMPONENTS");

    this.columnDefs = [
      {
        headerName: "Environment",
        field: "environment",
        editable: false,
        rowGroup: true,
        hide: true
      },
      {
        headerName: "Category",
        field: "category",
        width: 150,
        cellEditor: "agRichSelectCellEditor",
        cellEditorParams: function(params) {
          return {
            values: that.lookupcategories
          };
        }
      },
      {
        headerName: "Components",
        field: "component",
        width: 150,
        cellEditor: "agRichSelectCellEditor",
        cellEditorParams: function(params) {
            return {
              values: that.lookupitems.get(params.data.category)
            };
        }
      },
      {
        headerName: "Licensing",
        field: "licensing",
        cellEditor: "agRichSelectCellEditor",
        cellEditorParams: function(params) {
          return {
            values: that.lookupLicensing
          };
        }
      },
      {
        headerName: "Size",
        field: "size",
        cellEditor: "agRichSelectCellEditor",
        cellEditorParams: function(params) {
          return {
            values: that.lookupSizes
          };
        }
      },
      {
        headerName: "Operating System/DB",
        field: "osdb",
        cellEditor: "agRichSelectCellEditor",
        cellEditorParams: function(params) {
          return {
            values: that.lookupOperatingSystems
          };
        }
      },
      {
        headerName: "Qty",
        field: "qty"
      },
      {
        headerName: "Comments",
        field: "comments",
        width: 200,
        cellEditor: "agLargeTextCellEditor"
      },
      {
        headerName: "List Price",
        field: "listprice",
        valueFormatter: currencyFormatter,
        cellStyle: { textAlign: "right" },
        valueGetter: function (params) {
          if (!isNil(params) && !isNil(params.data) && !isNil(params.data.listprice)) {
            return (params.data.listprice);
          } else {
            return 0.0;
          }
        }
      },
      {
        headerName: "Discount",
        field: "discount"
      },
      {
        headerName: "Discounted Price",
        field: "discountprice",
        valueFormatter: currencyFormatter,
        aggFunc: "sum",
        cellStyle: { textAlign: "right" },
        valueGetter: function (params) {
          if (!isNil(params) && !isNil(params.data) && !isNil(params.data.listprice) && !isNil(params.data.discount)) {
            return (params.data.listprice - (params.data.listprice * (params.data.discount / 100.0)));
          } else {
            return 0.0;
          }
        }
      },
      {
        headerName: "Util %",
        field: "utilpercent"
      },
      {
        headerName: "Term",
        field: "term"
      },
      {
        headerName: "Yearly Cost",
        field: "annualcost",
        valueFormatter: currencyFormatter,
        cellStyle: { textAlign: "right" },
        aggFunc: "sum",
        valueGetter: function (params) {
          if (!isNil(params) && !isNil(params.data) && !isNil(params.data.listprice) && !isNil(params.data.discount)
                && !isNil(params.data.qty)) {
            return (params.data.listprice - (params.data.listprice * (params.data.discount / 100.0))) * params.data.qty;
          } else {
            return 0.0;
          }
        }
      }
      // {
      //   headerName: "Save",
      //   field: "value",
      //   cellRendererFramework: DeleteTDLFromGridComponent,
      //   colId: "delete",
      //   width: 75,
      //   filter: false,
      // },
      // {
      //   headerName: "Delete",
      //   field: "value",
      //   cellRendererFramework: DeleteAWSRowFromGridComponent,
      //   colId: "delete",
      //   width: 75,
      //   filter: false,
      // }
    ];

    this.rowSelection = "multiple";
    this.editType = "fullRow";
    this.defaultColDef = {
      editable: true,
      resizable: true
    };
  }

  getRowData() {
    let rowData = [];
    this.gridApi.forEachNode(function(node) {
      rowData.push(node.data);
    });
    console.log("Row Data:");
    console.log(rowData);
  }

  clearData() {
    if(!isNil(this.gridApi)){
      this.gridApi.setRowData([]);
    }
  }

  onAddRow() {
    let newItem = createNewRowData();
    let res = this.gridApi.updateRowData({ add: [newItem] });
  }

  addItems() {
    let newItems = [createNewRowData(), createNewRowData(), createNewRowData()];
    let res = this.gridApi.updateRowData({ add: newItems });
  }

  updateItems() {
    let itemsToUpdate = [];
    this.gridApi.forEachNodeAfterFilterAndSort(function(rowNode, index) {
      if (index >= 5) {
        return;
      }
      let data = rowNode.data;
      data.price = Math.floor(Math.random() * 20000 + 20000);
      itemsToUpdate.push(data);
    });
    let res = this.gridApi.updateRowData({ update: itemsToUpdate });
  }

  onRemoveSelected() {
    let selectedData = this.gridApi.getSelectedRows();
    let res = this.gridApi.updateRowData({ remove: selectedData });
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  ngOnInit(): void {
    this.loadOperatingSystems();
    this.loadLicensing();
    this.loadSizes();
    if(this.busyLoadLookupCategoriesAndItems){
      this.busyLoadLookupCategoriesAndItems.unsubscribe();
    }
  }

  ngOnDestroy(): void {
    if(this.busyLoadAWSBOMItems){
      this.busyLoadAWSBOMItems.unsubscribe();
    }
    if(this.busyLoadOperatingSystems){
      this.busyLoadOperatingSystems.unsubscribe();
    }
    if(this.busyLoadSizes){
      this.busyLoadSizes.unsubscribe();
    }
    if(this.busyLoadLicensingTerms){
      this.busyLoadLicensingTerms.unsubscribe();
    }
    if(this.busyLoadLookupCategoriesAndItems){
      this.busyLoadLookupCategoriesAndItems.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
  }

  public loadOperatingSystems(){
    let that = this;
    this.busyLoadOperatingSystems = this.lookupService.getAllByRefTypeName("AWS_OPERATING_SYSTEMS").subscribe(data => {
      if (data) {
        that.lookupOperatingSystems = _.toArray(_.map(data.refValues, 'refValue'));
      }
    }, error => {
      that.showErrorFlag = true;
      that.alerts = [];
      that.alerts.push({ type: 'warning', msg: error });
    });
  }

  public loadSizes(){
    let that = this;
    this.busyLoadSizes = this.lookupService.getAllByRefTypeName("AWS_COMPONENT_SIZE").subscribe(data => {
      if (data) {
        that.lookupSizes = _.toArray(_.map(data.refValues, 'refValue'));
      }
    }, error => {
      that.showErrorFlag = true;
      that.alerts = [];
      that.alerts.push({ type: 'warning', msg: error });
    });
  }

  public loadLicensing(){
    let that = this;
    this.busyLoadLicensingTerms = this.lookupService.getAllByRefTypeName("COTS_LICENSING").subscribe(data => {
      if (data) {
        that.lookupLicensing = _.toArray(_.map(data.refValues, 'refValue'));
      }
    }, error => {
      that.showErrorFlag = true;
      that.alerts = [];
      that.alerts.push({ type: 'warning', msg: error });
    });
  }

  public loadLookupCategoriesAndItems(){
    let that = this;
    this.lookupcategories = [];
    this.busyLoadLookupCategoriesAndItems = this.lookupService.getAllByRefTypeName("INFRASTRUCTURE_CATEGORY").subscribe(data => {
      if (data) {
        that.lookupcategories = _.toArray(_.map(data.refValues, 'refValue'));
        let apiitems:Observable<any>[] = [];
        _.forEach(that.lookupcategories, function(category){
          apiitems.push(that.lookupService.getAllByRefTypeName(that.categorymap.get(category)));
        });
        forkJoin(apiitems).subscribe(results => {
          let kdx = 0;
          _.forEach(that.lookupcategories, function(category){
            if(!isNil(results[kdx])){
              if((results[kdx].success === false) && (!isNil(results[kdx].error))){
                console.log("error:" + category + ":" + results[kdx].error);
              }else{
                if(results[kdx].refValues.length > 0){
                  that.lookupitems.set(category, _.toArray(_.map(results[kdx].refValues, 'refValue')));
                }else{
                  that.lookupitems.set(category, []);
                }
              }
            }
            kdx++;
          });
        });

      }
    }, error => {
      that.showErrorFlag = true;
      that.alerts = [];
      that.alerts.push({ type: 'warning', msg: error });
    });
  }
  public loadSavedAWSBomItems(){
    this.rowData = [];

    let that = this;
    _.forEach(this.formdata.awsitems, function(awsitem){
      let rowItem: BOMAWS = new BOMAWS();
      rowItem.component = awsitem.component;
      rowItem.category = awsitem.category;
      rowItem.licensing = awsitem.licensing;
      rowItem.size = awsitem.size;
      rowItem.osdb = awsitem.osdb;
      rowItem.qty = awsitem.qty;
      rowItem.listprice = awsitem.listprice;
      rowItem.discount = awsitem.discount;
      rowItem.discountedPrice = awsitem.discountedPrice;
      rowItem.comments = awsitem.comments;
      that.rowData.push(rowItem);
    });

    if(!isNil(this.gridApi)){
      this.gridApi.setRowData(this.rowData);
      this.sizeToFit();
    }

  }

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    for (let propName in changes) {
      if (propName === "formdata") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.formdata = chg.currentValue;
          this.clearData();
          this.loadSavedAWSBomItems();
        }
      }
    }
  }

  onFirstDataRendered(params){
    this.sizeToFit();
  }

  public sizeToFit() {
    if(!isNil(this.gridApi)){
      this.gridApi.sizeColumnsToFit();
    }
  }

  public  autoSizeAll() {
    let allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(function(column) {
      allColumnIds.push(column.colId);
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);
  }

  onCellValueChanged(params) {
    console.log('cv changed');
    const cell = params.api.getFocusedCell();
    if (params.oldValue !== params.newValue) {
      cell.column.colDef.cellStyle = {color: 'red',  fontWeight: '550'};
    }
    params.api.refreshCells(cell);
  }

  onRowValueChanged(params){
    console.log('rv changed');
  }

  public skipOnlyRequiredValidation(){
    jQuery('#frm-aws-details .rq').each(function() {
      jQuery(this).removeAttr('required');
    });
  }

  /**
   *
   * @param isnextstep : false - Save and exit - skip validation
   */
  public validateForm(isnextstep): boolean{
    let isValidForm: boolean = true;
    if(!isnextstep){
      this.skipOnlyRequiredValidation();
    }
    // jQuery('#frm-aws-details').parsley().validate();
    // isValidForm = jQuery('#frm-aws-details').parsley().isValid();

    this.formdata.awsitems = [];
    let that = this;
    this.gridApi.forEachNode(function(node) {
      let rowItem: BOMAWS = new BOMAWS();
      // rowItem.environment = curEnvironment;
      rowItem.bomId = that.formdata.bomId;
      rowItem.category = node.data.category;
      rowItem.component = node.data.component;
      rowItem.licensing = node.data.licensing;
      rowItem.size = node.data.size;
      rowItem.osdb = node.data.osdb;
      rowItem.qty = node.data.qty;
      rowItem.comments = node.data.comments;
      rowItem.listprice = node.data.listprice;
      rowItem.discount = node.data.discount;
      rowItem.discountedPrice = node.data.discountedPrice;
      that.formdata.awsitems.push(rowItem);
    });
    // console.log("Row Data:");
    // console.log(this.formdata.awsitems);

    return isValidForm;
  }
  public clearFields(){
  }

}


let newCount = 1;
function createNewRowData() {
  let newData: BOMAWS = new BOMAWS();
  newData.category = "COMPUTE";
  newData.component = "EC2 " + newCount;
  newData.size = "m5.xlarge " + newCount;
  newData.osdb = "Linux";
  newData.qty = 10;
  newData.listprice = 35000 + newCount * 17;
  newData.discount = 0.0;
  newData.comments = "US GovCloud (East) | 1 yr Upfront RI (priced per year)";
  newCount++;
  return newData;
}


function currencyFormatter(params) {
  let usdFormate = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2
  });
  return usdFormate.format(params.value);
}
function formatNumber(number) {
  return Math.floor(number)
    .toString()
    .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}
