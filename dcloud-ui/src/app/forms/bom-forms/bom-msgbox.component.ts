
import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

/* This is a component which we pass in modal*/

@Component({
  selector: 'bom-msg-box',
  template: `
    <div class="modal-header" style="background-color:#4175ab; color: #ffff">
      <h4 class="modal-title pull-left" >{{title}}</h4>
      <button type="button" class="close pull-right" aria-label="Close" (click)="bsModalRef.hide()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>{{message}}</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" (click)="bsModalRef.hide()">{{closeBtnName}}</button>
    </div>
  `
})

export class BOMMessageBox implements OnInit {
  title: string;
  closeBtnName: string;
  message: string;

  constructor(public bsModalRef: BsModalRef) {}

  ngOnInit() {
  }
}
