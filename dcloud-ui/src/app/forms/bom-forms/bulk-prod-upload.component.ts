import { AfterViewInit, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChange, ViewEncapsulation } from "@angular/core";
import { saveAs } from 'file-saver';
import * as _ from 'lodash';
import * as moment from 'moment';
import { FileSystemDirectoryEntry, FileSystemFileEntry, UploadEvent, UploadFile } from 'ngx-file-drop';
import { Papa } from "ngx-papaparse";
import { WebWorkerService } from 'ngx-web-worker';
import { LocalStorageService } from 'ngx-webstorage';
import { isNil } from 'ramda';
import { Subscription } from 'rxjs';
import { OnComponentErrorHandler } from "../../core/coreerror.handler";
import { AuthService } from '../../login/services/auth.service';
import { BOMDocumentResponse } from '../models/bom/bom-doc-response.model';
import { BOMSheet } from "../models/bom/bom-sheet.model";
import { UploadChecker } from '../models/upload-checker.model';
import { UploadTracker } from '../models/upload-tracker.model';
import { BOMFormsService } from '../services/bom-forms.service';
import { RunUpload } from '../models/runupload.model';

declare var jQuery: any;

@Component({
  selector: "bulk-prod-upload",
  templateUrl: "./bulk-prod-upload.template.html",
  encapsulation: ViewEncapsulation.None,
  providers: [BOMFormsService, WebWorkerService]
})
export class BulkProductUploadPage implements OnInit, OnDestroy, OnComponentErrorHandler, AfterViewInit, OnChanges {
  @Input() formdata: BOMSheet;
  showErrorFlag: boolean;
  busyUploadFile: Subscription;
  listUploadedFiles: BOMDocumentResponse[] = [];
  uploadtracker: UploadTracker[] = [];
  alerts: Array<Object>;
  prodmapalerts: Array<Object>;
  // verify user has uploaded selected files
  checklist: Array<UploadChecker> = [];
  warned: boolean = true;
  public fileuploadprogress: number;
  public fileuploadcount: number;
  public fileuploaded: number;
  public uploadProgressMessage = "Uploading File(s)...";

  public allvalidfiles: boolean;

  // message to display
  msgDisplay: string;

  // title to display
  titleDisplay: string;
  dynamic: number;
  documentKey: string;
  filesToUpload: Array<File> = [];
  docFileName: string;
  public droppedfiles: UploadFile[] = [];

  constructor(
    public auth: AuthService,
    private storage: LocalStorageService,
    private papa: Papa,
    private _webWorkerService: WebWorkerService,
    private formsService: BOMFormsService){
    this.showErrorFlag = false;
    this.alerts = [{ type: 'warning',  msg: '' }];
    this.prodmapalerts =  [{ type: 'warning',  msg: '<strong>Note:</strong> Only *.csv files are allowed' }];
    this.listUploadedFiles = [];
    this.uploadtracker = [];
    this.allvalidfiles = false;
    this.fileuploadprogress = 0;
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if(this.busyUploadFile){
      this.busyUploadFile.unsubscribe();
    }
  }

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    for (let propName in changes) {
      if (propName === "formdata") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.formdata = chg.currentValue;
        }
      }
    }
  }

  private getFileSizeinMB(fileSize: number){
    let FileSize = fileSize / 1024 / 1024; // in MB
    return FileSize;
  }

  private getFileExtension(fileName: string){
    let re = /(?:\.([^.]+))?$/;
    let ext = re.exec(fileName)[1];   // "txt"
    return ext;
  }


  public uploadAllDroppedFiles(){
    let that = this;

    const estb_id = this.storage.retrieve("ESTABLISHMENTID");

    this.uploadProgressMessage = "Authenticating User...";
    this.auth.getAuthenticatedUser()
      .subscribe(
      data => {
        if (!isNil(data)) {
          let index = 0;
          that.fileuploadcount = that.uploadtracker.length;
          that.fileuploaded = 0;
          for (const droppedFile of that.droppedfiles) {
            if (droppedFile.fileEntry.isFile) {
              const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
              fileEntry.file((file: File) => {
                  that.documentKey = "products/" + estb_id + "/" + file.name;
                  // tslint:disable-next-line: max-line-length
                  that.uploadDroppedFileToPrivateFolder(that.documentKey, index++, file, estb_id);
              });
            }
          }
          if(!isNil(data.errorMessage)){
            that.onShowBodyError(data);
          }
        }
      },
      error => {
        this.onShowError(error);
      }
    );
  }

  private validateFile(file:File){
    let retVal: string = "OK";

    if(this.getFileExtension(file.name) !== "csv"){
      retVal = "File " + file.name + " does not have *.csv extension. Please try again!";
    }

    if(this.getFileSizeinMB(file.size) > 250){
      retVal = "File " + file.name + " exceeds 250 MB limit. Please try again!";
    }

    return retVal;
  }

  public dropped(event: UploadEvent) {
    this.droppedfiles = event.files;
    this.uploadtracker = [];
    let that = this;
    let index = 0;
    this.allvalidfiles = true;
    this.alerts = [];
    for (const droppedFile of event.files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
            let filevalidmsg = that.validateFile(file);
            if(filevalidmsg === "OK"){
              let trk = new UploadTracker();
              trk.id = index++;
              trk.name = droppedFile.relativePath;
              trk.size = file.size;
              trk.modificationTime = new Date(file.lastModified);
              trk.progress = 0;
              that.uploadtracker.push(trk);
            } else {
              that.allvalidfiles = false;
              this.onShowError(filevalidmsg);
            }
        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  public fileOver(event){
    console.log(event);
  }

  public fileLeave(event){
    console.log(event);
  }

  public skipOnlyRequiredValidation(){
    jQuery('#frm-rent-details .rq').each(function() {
      jQuery(this).removeAttr('required');
    });
  }

  public validateForm(isnextstep): boolean{
    if(!(this.uploadedFiles() || this.warned)) {
      this.onShowError('Please click the blue “Upload” button for each of your selected files to complete the upload process.');
      this.warned = true;
      return false;
    }
    else {
      this.onResetError();
    }
    let isValidForm: boolean = true;
    if(!isnextstep){
      this.skipOnlyRequiredValidation();
    }
    jQuery('#frm-rent-details').parsley().validate();
    isValidForm = jQuery('#frm-rent-details').parsley().isValid();
    return isValidForm;
  }

  ngAfterViewInit(): void {
    // this.getListOfDocumentsUploaded();
  }

  public clearFields(){

  }
  onResetError(): void {
    this.showErrorFlag = false;
    this.alerts = [];
  }

  onShowError(error: any): void {
    this.showErrorFlag = true;
    this.alerts = [];
    this.alerts.push({ type: "warning", msg: "Error: " + error });
    // jQuery.scrollTo(jQuery("#pg-tbar"), 1000);
  }

  onShowBodyError(body: any): void {
    if (!isNil(body) && !isNil(body.errorMessage)) {
      this.showErrorFlag = true;
      this.alerts = [];
      this.alerts.push({ type: "warning", msg: "Error: " + body.errorMessage });
      // jQuery.scrollTo(jQuery("#pg-tbar"), 1000);
    }
  }

  fileChangeEvent(event, index) {
    this.selected(index);
    this.filesToUpload[index] = (<Array<File>>event.target.files)[0];
  }
  public isProcessorMode(){
    return this.auth.isProcessorMode();
  }

  public getListOfDocumentsUploaded(){
    // this.formsService.getListOfUploadedDocuments(this.formdata.establishmentid, this.formdata.tpnumber).subscribe(data => {
    //   this.listUploadedFiles = data;
    //   this.onShowBodyError(data);
    // }, error => {
    //   this.onShowError(error);
    // });
  }

  public updateDocumentUploadStatus(){
    this.formsService.updateDocumentUploadStatus(this.formdata).subscribe(data => {
      this.onShowBodyError(data);
    }, error => {
      this.onShowError(error);
    });
  }



  public uploadMappingFileToMapsFolder(estb_id: string, file){
    let that = this;
    let objKey = "maps/" + estb_id + "/" + file.name;
    this.busyUploadFile = this.auth.upload(objKey, file, {
      level: 'public',
      contentType: file.type,
      metadata: {
        "estb_id": estb_id
      },
      progressCallback(progress) {
        // if(progress.total > 0){
        //   that.uploadtracker[index].progress = (progress.loaded/progress.total) * 100.0;
        // }
        // console.log(`Uploaded: ${progress.loaded}/${progress.total}`);
      },
    })
    .subscribe(
      data => {
        if (!isNil(data) && !isNil(data.errorMessage)) {
          this.onShowBodyError(data);
        } else {
        }
      },
      error => {
        this.onShowError(error);
      }
    );
  }

  // public uploadFunctionOfFileToS3Folder(rundata: RunUpload){
  //     let auth: AuthService = new AuthService();
  //     auth.upload_p(rundata.objKey, rundata.file, {
  //       level: 'public',
  //       contentType: rundata.file.type,
  //       metadata: {
  //           "estb_id": rundata.estb_id
  //       },
  //       progressCallback(progress) {
  //         // that.uploadProgressMessage = "Uploading In Progress...";
  //         if(rundata.uploadtracker[rundata.index].started  === false){
  //           rundata.uploadtracker[rundata.index].started  = true;
  //           rundata.uploadtracker[rundata.index].uploadstarttime =  new Date();
  //           // tslint:disable-next-line: max-line-length
  //           let startduration = moment.duration(moment(rundata.uploadtracker[rundata.index].uploadstarttime).diff(moment(rundata.uploadtracker[rundata.index].starttime)));
  //           rundata.uploadtracker[rundata.index].uploadstartduration = startduration.asSeconds();
  //         }
  //         if(progress.total > 0){
  //           rundata.uploadtracker[rundata.index].progress = (progress.loaded/progress.total) * 100.0;
  //         }
  //         // console.log(`Uploaded: ${progress.loaded}/${progress.total}`);
  //       },
  //     });

  //     return rundata;
  // }

  public uploadDroppedFileToPrivateFolder(objKey, index, file: File, estb_id: string){
    let that = this;

    this.uploadtracker[index].started = false;
    this.uploadtracker[index].starttime = new Date();

    this.uploadProgressMessage = "Uploading Started...";


    let rundata: RunUpload = new RunUpload();
    // rundata.auth = this.auth;
    rundata.estb_id = estb_id;
    rundata.file = file;
    rundata.objKey = objKey;
    rundata.index = index;

    // const runpromise = this._webWorkerService.run(this.uploadFunctionOfFileToS3Folder, rundata );

    this.busyUploadFile = this.auth.upload(objKey, file, {
      level: 'public',
      contentType: file.type,
      metadata: {
          "estb_id": estb_id
      },
      progressCallback(progress) {
        that.uploadProgressMessage = "Uploading In Progress...";
        if(that.uploadtracker[index].started  === false){
          that.uploadtracker[index].started  = true;
          that.uploadtracker[index].uploadstarttime =  new Date();
          // tslint:disable-next-line: max-line-length
          let startduration = moment.duration(moment(that.uploadtracker[index].uploadstarttime).diff(moment(that.uploadtracker[index].starttime)));
          that.uploadtracker[index].uploadstartduration = startduration.asSeconds();
        }
        if(progress.total > 0){
          that.uploadtracker[index].progress = (progress.loaded/progress.total) * 100.0;
        }
        // console.log(`Uploaded: ${progress.loaded}/${progress.total}`);
      },
    })
    .subscribe(
      data => {
        if (!isNil(data) && !isNil(data.errorMessage)) {
          that.displayUploadStatusMessage("Upload Status", "Failed to Upload Document!" );
          that.onShowBodyError(data);
        } else {
          that.uploadtracker[index].endtime = new Date();
          // tslint:disable-next-line: max-line-length
          let totalduration = moment.duration(moment(that.uploadtracker[index].endtime).diff(moment(that.uploadtracker[index].starttime)));
          that.uploadtracker[index].uploadtotalduration = totalduration.asSeconds();
          that.fileuploaded++;
          that.fileuploadprogress = ((that.fileuploaded)/that.fileuploadcount)*100;
        }
      },
      error => {
        that.onShowError(error);
      }
    );
  }

  public uploadFileToPrivateFolder(objKey, index, docName){
    let that = this;
    this.dynamic = 0.0;
    this.auth.upload(objKey, this.filesToUpload[index], {
      level: 'public',
      contentType: this.filesToUpload[index].type,
      progressCallback(progress) {
        if(progress.total > 0){
          that.dynamic = (progress.loaded/progress.total) * 100.0;
        }
        console.log(`Uploaded: ${progress.loaded}/${progress.total}`);
      },
    })
    .subscribe(
      data => {
        // if (!isNil(data) && !isNil(data.errorMessage)) {
        //   that.displayUploadStatusMessage("Upload Status", "Failed to Upload Document!" );
        //   this.onShowBodyError(data);
        // } else {
        //   that.displayUploadStatusMessage("Upload Status", "Document Uploaded Successfully!" );
        //   let rowIndex = _.findIndex(this.formdata.documents, function (o) { return (o.name && o.name.toString() === docName)});
        //   if(rowIndex !== -1) {
        //       let selRow = this.formdata.documents[rowIndex];
        //       selRow.fileName = this.filesToUpload[index].name;
        //       selRow.status = 'Y';
        //       this.uploaded(index);
        //   }
        //   that.updateDocumentUploadStatus();
        //   that.getListOfDocumentsUploaded();
        // }
      },
      error => {
        this.onShowError(error);
      }
    );
  }

  public uploadFile(index, docName, productname){
    if (!isNil(this.filesToUpload[index]) && this.filesToUpload[index].name.length > 0) {
      let that = this;
      this.auth.getAuthenticatedUser()
        .subscribe(
        data => {
          // if (!isNil(data)) {
          //   that.documentKey = this.formdata.establishmentid + "/uploads/" + productname + "/" + this.filesToUpload[index].name;
          //   that.uploadFileToPrivateFolder(that.documentKey, index, docName);
          //   if(!isNil(data.errorMessage)){
          //     that.onShowBodyError(data);
          //   }
          // }
        },
        error => {
          this.onShowError(error);
        }
      );
    }
  }


    // reset flag and empty title and message
    public onCloseInfoPopup(){
      this.msgDisplay = "";
      this.titleDisplay = "";
      jQuery('#info-popup').modal('hide');
  }

  // Display upload success message
  protected displayUploadStatusMessage(title:string, msg:string){
      this.msgDisplay = msg;
      this.titleDisplay = title;
      jQuery('#info-popup').modal('show');
  }

  // local checklist to verify that upload buttons have been pushed
  private selected(index: string) {
    this.warned = false;
    let entry = new UploadChecker();
    entry.index = index;
    entry.uploaded = "N";
    this.checklist.push(entry);
  }

  private uploaded(index: string) {
    let entry:UploadChecker = this.findUpload(index);
    entry.uploaded = 'Y';
    if(this.uploadedFiles()) {
      this.onResetError();
    }
  }

  private findUpload(index:string):UploadChecker {
    for(let i=0;i<this.checklist.length;i++) {
      if(this.checklist[i].index === index) {
        return this.checklist[i];
      }
    }
  }

  public uploadedFiles():boolean {
    for(let i=0;i<this.checklist.length;i++) {
      if(this.checklist[i].uploaded !== 'Y') {
        return false;
      }
    }
    return true;
  }

  public downloadFile(doc:BOMDocumentResponse){
      // let productname = this.formdata.documents[0].productname;
      // this.documentKey = this.formdata.establishmentid + "/uploads/" + productname + "/" + doc.filename;
      // // this.documentKey = formId + "/uploads/" + doc.filename;
      // this.auth.download(this.documentKey, {
      //   level: 'public'
      // })
      // .subscribe(
      //   data => {
      //     if (!isNil(data) && !isNil(data.errorMessage)) {
      //       this.onShowBodyError(data);
      //     } else {
      //       let xhr = new XMLHttpRequest();
      //       xhr.open('GET', data);
      //       xhr.responseType = 'blob';
      //       xhr.onload = function () {
      //           saveAs(this.response, doc.filename);
      //       };
      //       xhr.send();
      //     }
      //   },
      //   error => {
      //     this.onShowError(error);
      //   }
      // );
  }

}
