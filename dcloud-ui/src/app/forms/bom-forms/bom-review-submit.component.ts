import { Component, Input, OnDestroy, OnInit, AfterViewInit, SimpleChange, ViewEncapsulation, OnChanges } from "@angular/core";
import html2pdf from "html2pdf.js";
import { isNil } from 'ramda';
import { LookupService } from '../../core/lookup.service';
import { AuthService } from "../../login/services/auth.service";
import { BOMSheet } from "../models/bom/bom-sheet.model";

declare let jQuery: any;

@Component({
  selector: "bom-review-submit",
  templateUrl: "./bom-review-submit.template.html",
  styleUrls: ['./bom-review-submit.style.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [LookupService]
})
export class BOMReviewSubmitTabPage implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  @Input() formdata: BOMSheet;

  showErrorFlag: boolean;
  alerts: Array<Object>;

  constructor(
    private auth: AuthService
  ){
    this.showErrorFlag = false;
    this.alerts = [{ type: 'warning',  msg: '' }];
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {}

  ngAfterViewInit(): void {
    jQuery('#frm-app-details').parsley();
  }

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    for (let propName in changes) {
      if (propName === "formdata") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.formdata = chg.currentValue;
        }
      }
    }
  }

  public captureScreen()
  {
    let element = document.getElementById('view-pdf-content');
    let useWidth = jQuery( '#view-pdf-content' ).prop( 'scrollWidth' );
    let useHeight = jQuery( '#view-pdf-content' ).prop( 'scrollHeight' );

    jQuery('.pdf-field').css({"font-family":"'Helvetica', Helvetica, monospace", "font-size": "8px"});
    jQuery('.pdf-header-field').css({"font-family":"'Helvetica Neue', Helvetica, monospace", "font-size": "12px"});
    jQuery('.fc').css({"font-size":"8px", "font-weight": "500"});

    let opt = {
      filename:     'bom-application.pdf',
      image:        { type: 'jpeg', quality: 0.98 },
      html2canvas:  { scale: 3 },
      jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' },
      "footer": {
        "height": "28mm",
        "contents": {
          default: '<h1 class="applicant-copy">APPLICANT COPY</h1>', // fallback value
        }
      }
    };

    // New Promise-based usage:
    html2pdf().from(element).set(opt).save();

    jQuery('.pdf-field').css({"font-family":"inherit", "font-size": "inherit"});
    jQuery('.pdf-header-field').css({"font-family":"inherit"});
    jQuery('.fc').css({"font-size":"14px", "font-weight": "500"});
  }

  public isProcessorMode(){
    return this.auth.isProcessorMode();
  }
  public validateForm(): boolean{
    let isValidForm: boolean = true;
    jQuery('#frm-app-details').parsley().validate();
    isValidForm = jQuery('#frm-app-details').parsley().isValid();
    return isValidForm;
  }

  public sv(val: string) {
    if (isNil(val)) {
      return "";
    }
    return val;
  }


    // returns true if activity code is not equal to "T"
    public isSubmittedApplication(app:BOMSheet){
      // if(!isNil(app.productStatus) && app.productStatus !== 'T'){
      //   return true;
      // }
      return false;
    }


    // returns valid applicaiton reference number, otherwise returns "-"
    public getApplicationRefNumber(app:BOMSheet){
      // if(!isNil(app.tpnumber)){
      //   return app.tpnumber;
      // }
      return "-";
    }
}
