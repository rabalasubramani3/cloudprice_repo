import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { isNil } from 'ramda';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { AuthService } from '../../login/services/auth.service';
import { MessageService } from '../../_services';
import { BOMSheet } from '../models/bom/bom-sheet.model';
import { DocumentProofCheck } from '../models/doc-proof-req.model';
import { DocumentProofUpdate } from '../models/doc-proof-update.model';
import { SendMailRequest } from '../models/sendfile.model';
import { BOMDocumentResponse } from '../models/bom/bom-doc-response.model';
import { BOMDocumentQuery } from '../models/bom/bom-doc-query.model';


const httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" })
  };



@Injectable()
export class BOMFormsService {

    private apiemailURL = environment.V1URL + '/rtc/bom/sendemail';
    private formStepsURL = environment.APIURL + '/api/v1/bom';
    private apiProofCheckURL = environment.V1URL + '/tpl/bom/proofcheck';
    private apiProofUpdateURL = environment.V1URL + '/tpl/bom/proofupdate';
    private apiGetDocumentsURL = environment.V2URL + '/lists3files';

    constructor(private http: HttpClient,private authService: AuthService, private messageService: MessageService) {}

    public updateStep(app: BOMSheet, step: number) {
      app.step = step;
      console.log(app);
      return this.http.put(this.formStepsURL + "/" + app.bomId, app, httpOptions)
      .pipe(
          tap(_ => this.log('createStep')),
          catchError(this.handleError<any>('createStep'))
      );
    }

    public createStep(app: BOMSheet, step: number) {
      if(app.bomId > 0){
        return this.updateStep(app, step);
      }

      app.step = step;
      console.log(app);
      return this.http.post(this.formStepsURL, app, httpOptions)
      .pipe(
          tap(_ => this.log('createStep')),
          catchError(this.handleError<any>('createStep'))
      );
    }

    public updateAWSStep(app: BOMSheet, step: number) {
      app.step = step;
      console.log(app);
      return this.http.put(this.formStepsURL + "/aws/" + app.bomId, app.awsitems, httpOptions)
      .pipe(
          tap(_ => this.log('updateAWSStep')),
          catchError(this.handleError<any>('updateAWSStep'))
      );
    }

    public createAWSStep(app: BOMSheet, step: number) {
       app.step = step;
      console.log(app);

      return this.http.post(this.formStepsURL + '/aws', app.awsitems, httpOptions)
      .pipe(
          tap(_ => this.log('createAWSStep')),
          catchError(this.handleError<any>('createAWSStep'))
      );
    }

    public updateCOTSStep(app: BOMSheet, step: number) {
      app.step = step;
      console.log(app);
      return this.http.put(this.formStepsURL + "/cots/" + app.bomId, app.cotsitems, httpOptions)
      .pipe(
          tap(_ => this.log('updateCOTSStep')),
          catchError(this.handleError<any>('updateCOTSStep'))
      );
    }

    public createCOTSStep(app: BOMSheet, step: number) {
      app.step = step;
     console.log(app);

     return this.http.post(this.formStepsURL + '/cots', app.cotsitems, httpOptions)
     .pipe(
         tap(_ => this.log('createCOTSStep')),
         catchError(this.handleError<any>('createCOTSStep'))
     );
   }

    public getAllBOMs() {
      return this.http.get(this.formStepsURL, httpOptions)
      .pipe(
          tap(_ => this.log('getAllBOMs')),
          catchError(this.handleError<any>('getAllBOMs'))
      );
    }

    public getListOfUploadedDocuments(username: string, productname: string):Observable<BOMDocumentResponse[]> {
      let dq: BOMDocumentQuery = new BOMDocumentQuery();
      dq.user = username;
      dq.productname = productname;
      return this.http.post<BOMDocumentResponse[]>(this.apiGetDocumentsURL, dq, httpOptions)
      .pipe(
          tap(_ => this.log('getListOfUploadedDocuments')),
          catchError(this.handleError<any>('getListOfUploadedDocuments'))
      );
    }

    public sendSubmitEmail(applicationRefNum: string, emailAddress: string, formYear: number){
        let emailReq: SendMailRequest = new SendMailRequest();
        emailReq.ApplicationRefNum = applicationRefNum;
        emailReq.EmailAddress = emailAddress;
        emailReq.Year = !isNil(formYear) ? formYear.toString() : "2018";
        return this.http.post(this.apiemailURL, emailReq, httpOptions)
        .pipe(
            tap(_ => this.log('createResource id= ${resource.resourceId}')),
            catchError(this.handleError<any>("createResource"))
        );
    }

    public checkDocumentRequirements(uuid:string){
        let req: DocumentProofCheck = new DocumentProofCheck();
        req.AppUUID = uuid;
        return this.http.post(this.apiProofCheckURL, req, httpOptions)
        .pipe(
            tap(_ => this.log('checkDocumentRequirements')),
            catchError(this.handleError<any>("checkDocumentRequirements"))
        );
    }

    public updateDocumentUploadStatus(application: BOMSheet){
        let proofStatuses:DocumentProofUpdate[] = [];
        // if(!isNil(application.documents)){
        //     for(let doc of application.documents){
        //         if(!isNil(doc.fileName)){
        //             let newdocstatus:DocumentProofUpdate = new DocumentProofUpdate();
        //             newdocstatus.FileName = doc.fileName;
        //             newdocstatus.Description = doc.name;
        //             newdocstatus.UploadStatus = doc.status;
        //             newdocstatus.UUID = application.uuid;
        //             proofStatuses.push(newdocstatus);
        //         }
        //     }
        // }
        let statusjson = {"Details" : proofStatuses};
        console.log(statusjson);

        return this.http.post(this.apiProofUpdateURL, statusjson, httpOptions)
        .pipe(
            tap(_ => this.log('updateDocumentUploadStatus')),
            catchError(this.handleError<any>("updateDocumentUploadStatus"))
        );
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = "operation", result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log('${operation} failed: ${error.message}');

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a Attribute Service message with the MessageService */
    private log(message: string) {
        this.messageService.add('Forms Service: ${message}');
    }


}
