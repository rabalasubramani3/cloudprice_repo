import { AuthService } from './login/services/auth.service';
import { Subscription } from 'rxjs';
import { Component, ViewEncapsulation, AfterViewInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";

declare var jQuery: any;

let skipAuthorization: boolean = false;

@Component({
  selector: "home",
  styleUrls: ["./home.style.scss"],
  templateUrl: "./home.template.html",
  encapsulation: ViewEncapsulation.None,
  providers:[],
  host: {
    class: "home-page app"
  }
})
export class HomeComponent implements AfterViewInit {
  // model = new Credentials("", "");
  showErrorFlag: boolean;
  alerts: Array<Object>;
  currentYear: string;
  showConfirmPopup: boolean = false;
  loginData: any;
  subscription: Subscription;
  public loggedIn: boolean;

  constructor(
    private router: Router,
    private auth:AuthService
  ) {
    
    this.alerts = [
      {
        type: "warning",
        msg: '<span class="fw-semi-bold">Warning:</span> Error Logging in'
      }
    ];

    this.showErrorFlag = false;
    this.currentYear = new Date().getFullYear().toString();
  }

  ngOnInit(): void {
    this.showErrorFlag = false;
    this.subscription = this.auth.isAuthenticated()
    .subscribe(result => {
    });
  }

  
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


  ngAfterViewInit() {
    jQuery("#uname").focus();
  }

  clearLocalStorageContent() {}

  public isProcessorMode(){
    return this.auth.isProcessorMode();
  }

  login() {
  }

  logout() {
    this.router.navigate(["login/logout"]);
  }
}
