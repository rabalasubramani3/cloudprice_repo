import { Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { LicenseManager } from "ag-grid-enterprise";
import * as moment from 'moment';
import { LocalStorageService } from 'ngx-webstorage';
import { isNil } from 'ramda';
import { Subscription } from 'rxjs';
import { LoginState } from './login/model/login-state.model';
import { AuthService } from './login/services/auth.service';


declare var window: any;
declare var jQuery: any;

@Component({
  selector: "app-root",
  templateUrl: "./app.template.html",
  styleUrls: ["./app.style.scss"],
  providers: [LocalStorageService]
})
export class AppComponent implements OnInit, OnDestroy {
  public subscription: Subscription;
  public msg: string =
    "Your session is about to expire.  Click close to continue.";
  public title: string = "Session Timeout";
  public loggedUsername: string = "Logged User";

  constructor(
    public auth: AuthService,
    private storage: LocalStorageService,
    private titleService: Title,
    private router: Router
  ) {
    LicenseManager.setLicenseKey(
      "[TRIAL]_10_August_2019_[v2]_MTU2NTM5NTIwMDAwMA==53f1292ef78d2de19c8266ac2435ead8"
    );
  }

  ngOnInit() {
    this.subscription = this.auth.isAuthenticated().subscribe(result => {});
    this.auth.expiring.subscribe((x: boolean) => {
      // session is about to time out, display warning popup
      if (x) {
        jQuery("#app-popup").modal("show");
      } else {
        jQuery("#app-popup").modal("hide");
      }
    });
    this.setTitle("DCPricing");

    let context = this;
    window.addEventListener("beforeunload", function(e) {
      context.subscription.unsubscribe();
      // context.auth.expiring.unsubscribe();
      context.onClickLogout();
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    // this.auth.expiring.unsubscribe();
    this.onClickLogout();
  }

  public clearLocalStorageContent(modPrefix) {
    // 9 items to clear for each application type
    this.storage.clear(modPrefix + "RNF");
    this.storage.clear(modPrefix + "NMF");
    this.storage.clear(modPrefix + "SSNF");
    this.storage.clear(modPrefix + "CYF");
    this.storage.clear(modPrefix + "CODEF");
    this.storage.clear(modPrefix + "FLAGF");
    this.storage.clear(modPrefix + "COUNTYF");
    this.storage.clear(modPrefix + "STATUSF");
    this.storage.clear(modPrefix + "ADDRF");
  }

  public onClickLogout() {
    this.storage.clear("LOGGEDUSERID");
    this.storage.clear("LOGGEDUSEREMAIL");
    this.auth.signOut();
  }

  public onClose() {
    this.auth.resetSessionTimeout();
  }

  public canEditProfile() {
    return this.isLoggedIn() && !this.isProcessorMode();
  }

  public isLoggedIn() {
    if (this.auth.loggedIn.value) {
      return true;
    }
    return false;
  }

  public isProcessorMode() {
    return this.auth.isProcessorMode();
  }

  public isMyProfileShown() {
    let status: LoginState = this.auth.getLoginState();
    if (this.isLoggedIn() && !this.isProcessorMode() && status.isLoggedIn()) {
      this.loggedUsername = this.storage.retrieve("LOGGEDUSERID");
      return true;
    }
    return false;
  }

  public isMyHomePageShown() {
    let status: LoginState = this.auth.getLoginState();
    return this.isLoggedIn() && status.isLoggedIn();
  }

  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

  public closeAnnouncement() {
    jQuery("#announcement").hide();
  }
}
