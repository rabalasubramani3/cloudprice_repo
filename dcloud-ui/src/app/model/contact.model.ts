/*
Class to hold contact info
*/

/**
 * 
 * 
 * @export
 * @class Contact
 */
export class Contact {
  contactId: number;
  companyId: number;
  lastNm: string;
  firstNm: string;
  phoneNum: number;
  phoneExt: number;
  emailAddress: string;
  countryCd: string;
  cntryDialCd: string;
  faxNum: number;
  cntryFaxDialCd: string;
}
