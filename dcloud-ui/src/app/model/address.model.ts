/*
@author : Deloitte
Class to hold address info
*/

/**
 * 
 * 
 * @export
 * @class Address
 */
export class Address {
    companyId: string;
    addressTypeCd: string;
    attention: string;
    streetAddress: string;
    suite: string;
    city: string;
    state: string;
    province: string;
    postalCd: number;
    faxNum: number;
    countryCd: string;
    countryNm: string;
}
