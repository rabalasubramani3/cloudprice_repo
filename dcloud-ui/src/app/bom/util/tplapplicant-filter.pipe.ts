import { isNil } from 'ramda';
import { ApplicantFilterData } from '../models/applicant-filter.model';
import * as _ from "lodash";
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "tplapplicantfilterpipe"
})
export class TPLApplicantDataFilterPipe implements PipeTransform {

    getCompleteAddress(item:any){
        return  ( item.HT_ADDR1 + ", " + item.HT_ADDR2 + ", " + item.HT_CITY_TOWN_PO +
                    ", MD " + item.HT_ZIP1);
    }

    transform(data: any[], filter: ApplicantFilterData): any {
        let results = data;

        let helper = this;

        if (data) {
            if (filter) {
                // filter by flag
                if(!isNil(filter.flagFilter) && filter.flagFilter.length > 0) {
                    results = _.filter(results, function(item) {
                        let val1:string = item.HT_APPLICATION_FLAG;
                        let val2:string = filter.flagFilter;
                        return val1 === val2;
                    });
                }
                // filter by ref number
                if (!isNil(filter.refnumFilter) && (filter.refnumFilter.length > 0)){
                    results =  _.filter(results, function(item) {
                         let val1:string = !isNil(item.HT_APPLICATION_REF) ? item.HT_APPLICATION_REF.toString() : "";
                         let val2:string = filter.refnumFilter;
                         return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                     });
                 }
                // filter by name
                if (!isNil(filter.nameFilter) && (filter.nameFilter.length > 0)){
                   results =  _.filter(results, function(item) {
                        let val1:string = item.HT_LAST_NAME + ", " + item.HT_FIRST_NAME;
                        let val2:string = filter.nameFilter;
                        return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                    });
                }
                // filter by ssn
                if (!isNil(filter.ssnFilter) && (filter.ssnFilter.length > 0)){
                   results =  _.filter(results, function(item) {
                    let val1:string = item.HT_SSN_NO;
                    let val2:string = filter.ssnFilter;
                    return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                    });
                }
                if (!isNil(filter.countyFilter) && (filter.countyFilter.length > 0)){
                    results =  _.filter(results, function(item) {
                     let val1:number = Number(item.HT_COUNTY);
                     let val2:number = Number(filter.countyFilter);
                     return val1 === val2;
                     });
                 }
                // filter by year
                if (!isNil(filter.yearFilter) && (filter.yearFilter.length > 0)){
                   results =  _.filter(results, function(item) {
                        let val1:string = Number(item.HT_YEAR).toString();
                        let val2:string = filter.yearFilter;
                        return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                    });
                }
                // filter by code
                if (!isNil(filter.codeFilter) && (filter.codeFilter.length > 0)){
                   results =  _.filter(results, function(item) {
                        let val1:string = item.HT_APP_STATUS?item.HT_APP_STATUS:"";
                        let val2:string = filter.codeFilter?filter.codeFilter:"";
                        return (val1.toUpperCase() === val2.toUpperCase());
                    });
                }
                // filter by status
                if (!isNil(filter.statusFilter) && (filter.statusFilter.length > 0)){
                    results =  _.filter(results, function(item) {
                         let val1:string = item.TP_PRODUCT_STATUS?item.TP_PRODUCT_STATUS:"";
                         let val2:string = filter.statusFilter?filter.statusFilter:"";
                         return (val1.toUpperCase() === val2.toUpperCase());
                     });
                 }
                // filter by address
                if (!isNil(filter.addressFilter) && (filter.addressFilter.length > 0)){
                    results =  _.filter(results, function(item) {
                         let val1:string = helper.getCompleteAddress(item);
                         let val2:string = filter.addressFilter?filter.addressFilter:"";
                         return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                     });
                 }

            }
        }
        return results;
    }
// tslint:disable-next-line:eofline
}