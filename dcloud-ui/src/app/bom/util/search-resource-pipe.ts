import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'SearchApplicantPipe'
})

export class SearchApplicantPipe implements PipeTransform {

  transform(value, args?): Array<any> {
    let searchText = new RegExp(args, 'ig');
    if (value) {
      return value.filter(applicant => {
        if (applicant.resourceName) {
          return applicant.resourceName.search(searchText) !== -1;
        }
      });
    }
  }
}
