export class ContactEmailInfo{
    From: string;
    To: string;
    CC: string;
    Subject: string;
    Message: string;
}