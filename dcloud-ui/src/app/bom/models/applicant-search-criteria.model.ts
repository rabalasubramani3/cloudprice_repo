import { Applicant } from './applicant.model';

export class ApplicantSearchCriteria {
    email: string;
    year: string;
}