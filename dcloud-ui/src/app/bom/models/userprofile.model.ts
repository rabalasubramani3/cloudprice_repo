export class UserProfile{
    Address1: string;
    Address2: string;
    BirthDate: string;
    City: string;
    FirstName: string;
    LastName: string;
    SSN: string;
    State: string;
    Zip1: string;
    Zip2: string;
}