export class Applicant {
    resourceId: number;
    firstName: string;
    lastName: string;
    deloitteLevel: string;
    location: string;
    office: string;
    phoneNumber: string;
    altPhoneNumber: string;
    clientName: string;
    projectName: string;
    aacLevel: string;
    yearAccepted: string;
    goals: string;
    createdDt: Date;
    createdBy: String;
}
