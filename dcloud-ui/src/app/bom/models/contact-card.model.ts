export class ContactCardInfo{
    Name: string;
    Role: string;
    SSN: string;
    DOB: string;
    Phone: string;
    Email: string;
    LivingAddress: string;
    PrevLivingAddress: string;
    MailingAddress: string;
    isDisabled: boolean;

    constructor(){
        this.isDisabled = false;
    }
}