export class NameValue{
    Name: string;
    Value: string;

    constructor(name,value){
        this.Name = name;
        this.Value = value;
    }
}

export class Column4NameValues{
    col1: NameValue;
    col2: NameValue;
    col3: NameValue;
    col4: NameValue;

    constructor(n1, v1, n2, v2, n3, v3, n4, v4){
        this.col1 = new NameValue(n1, v1);
        this.col2 = new NameValue(n2, v2);
        this.col3 = new NameValue(n3, v3);
        this.col4 = new NameValue(n4, v4);
    }
}


export class Column3NameValues{
    col1: NameValue;
    col2: NameValue;
    col3: NameValue;

    constructor(n1, v1, n2, v2, n3, v3){
        this.col1 = new NameValue(n1, v1);
        this.col2 = new NameValue(n2, v2);
        this.col3 = new NameValue(n3, v3);
    }    
}