export class LetterTypeInfo{
    AccountNum: string;
    Year: string;
    MailDate: string;
    Address1: string;
    Address2: string;
    Address3: string;
    Address4: string;
    Zip1: string;
    Zip2: string;
    ApplYear: string;
    EmpName: string;
    LetterCategory: string;
    LetterType: string[];
    LetterText: string;
    AppRefNumber: string;
    FirstName: string;
    LastName: string;
    SSN: string;
    County: string;
    Processor: string;
    timestamp: string;
}