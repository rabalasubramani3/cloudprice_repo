import { Applicant } from './applicant.model';

export class ApplicantFilter {
    nameFilter: string;
    ssnFilter: string;
    yearFilter: string;
    codeFilter: string;
    flagFilter: string;
    countyFilter: string;
    refnumFilter: string;
    propertyFilter: string;
    statusFilter: string;
    addressFilter: string;
    processorFilter: string;
    submittedDateFilter: string;
    results: Applicant[];

    clear(){
        this.nameFilter = "";
        this.ssnFilter = "";
        this.yearFilter = "";
        this.codeFilter = "";
        this.flagFilter = "";
        this.countyFilter = "";
        this.refnumFilter = "";
        this.propertyFilter = "";
        this.statusFilter = "";
        this.addressFilter = "";
        this.processorFilter = "";
        this.submittedDateFilter = "";
    }
}

export class ApplicantFilterData {
    nameFilter: string;
    ssnFilter: string;
    yearFilter: string;
    codeFilter: string;
    flagFilter: string;
    countyFilter: string;  
    statusFilter: string;  
    addressFilter: string;
    refnumFilter: string;
    propertyFilter: string;
    processorFilter: string;
    submittedDateFilter: string;

    clear(){
        this.nameFilter = "";
        this.ssnFilter = "";
        this.yearFilter = "";
        this.codeFilter = "";
        this.flagFilter = "";
        this.countyFilter = "";
        this.refnumFilter = "";
        this.propertyFilter = "";
        this.statusFilter = "";
        this.addressFilter = "";
        this.processorFilter = "";
        this.submittedDateFilter = "";
    }    
 }
