export class UserSaveFilter {
	userFilterID: number;
	userName: string;
	filterCategory: string;
	filterName: string;
	filterJson: string;
}
