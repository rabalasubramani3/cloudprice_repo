export class Square9Request{
    Year: string;
    Type: string;
    Name: string;
    Address: string;
    Apt: string;
    City: string;
    County: string;
    Zip: string;
    AppRefNumber: string;
}