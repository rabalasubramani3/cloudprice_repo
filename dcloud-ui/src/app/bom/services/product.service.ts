import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cacheable } from 'ngx-cacheable';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { fetchprodquery } from '../../forms/models/fetchprodquery.model';
import { AuthService } from '../../login/services/auth.service';
import { MessageService } from '../../_services';
import { UserSaveFilter } from '../models/user-save-filter.model';
import { BOMSheet } from '../../forms/models/bom/bom-sheet.model';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

/**
 *
 *
 * @export
 * @class ProductService
 */
@Injectable()
export class ProductService {

    private apiProductsURL = environment.PRODUCTSURL;


    constructor(private http: HttpClient, private authService: AuthService, private messageService: MessageService) {}

    @Cacheable()
    public getAllProducts(pq: fetchprodquery): Observable<BOMSheet[]>  {
        return this.http.post<BOMSheet[]>(this.apiProductsURL + '/products', pq, httpOptions)
        .pipe(
            tap(_ => this.log('getAllProducts')),
            catchError(this.handleError<any>('getAllProducts'))
        );
      }

      // @Cacheable()
      // public getAllMaterials(pq: fetchprodquery): Observable<TPLMaterial[]>  {
      //     return this.http.post<TPLMaterial[]>(this.apiProductsURL + '/materials', pq, httpOptions)
      //     .pipe(
      //         tap(_ => this.log('getAllMaterials')),
      //         catchError(this.handleError<any>('getAllMaterials'))
      //     );
      //   }

      // @Cacheable()
      // public getAllProductsBySearchKeyword(pq: fetchprodquery): Observable<TPLProduct[]>  {
      //   return this.http.post<TPLProduct[]>(this.apiProductsURL + '/products/search', pq, httpOptions)
      //   .pipe(
      //       tap(_ => this.log('getAllProducts')),
      //       catchError(this.handleError<any>('getAllProducts'))
      //   );
      // }

      // @Cacheable()
      // public getAllMaterialsBySearchKeyword(pq: fetchprodquery): Observable<TPLMaterial[]>  {
      //   return this.http.post<TPLMaterial[]>(this.apiProductsURL + '/materials/search', pq, httpOptions)
      //   .pipe(
      //       tap(_ => this.log('getAllMaterialsBySearchKeyword')),
      //       catchError(this.handleError<any>('getAllMaterialsBySearchKeyword'))
      //   );
      // }

      public getAllProductsCount(): Observable<any>  {
        return this.http.get<any>(this.apiProductsURL + '/products/count', httpOptions)
        .pipe(
            tap(_ => this.log('getAllProducts')),
            catchError(this.handleError<any>('getAllProducts'))
        );
      }

      public getAllMaterialsCount(): Observable<any>  {
        return this.http.get<any>(this.apiProductsURL + '/materials/count', httpOptions)
        .pipe(
            tap(_ => this.log('getAllMaterialsCount')),
            catchError(this.handleError<any>('getAllMaterialsCount'))
        );
      }

      public resetAllProducts(){
        return this.http.delete(this.apiProductsURL, httpOptions)
        .pipe(
            tap(_ => this.log('resetAllProducts')),
            catchError(this.handleError<any>('resetAllProducts'))
        );
      }

    // public createProduct(prod: TPLProduct) {
    //     return this.http.post(this.apiProductsURL, prod, httpOptions)
    //     .pipe(
    //         tap(_ => this.log('createProduct')),
    //         catchError(this.handleError<any>('createProduct'))
    //     );
    // }

    public getUserFilters(ufq: UserSaveFilter): Observable<UserSaveFilter[]> {
      return this.http.post(this.apiProductsURL +  "/user/filter/list", ufq, httpOptions)
      .pipe(
          tap(_ => this.log('getUserFilters')),
          catchError(this.handleError<any>('getUserFilters'))
      );
    }

    public saveUserFilter(ufe: UserSaveFilter): Observable<UserSaveFilter> {
      return this.http.post(this.apiProductsURL +  "/user/filter", ufe, httpOptions)
      .pipe(
          tap(_ => this.log('saveUserFilter')),
          catchError(this.handleError<any>('saveUserFilter'))
      );
    }

    public getPredefinedS3Url(){
      return this.http.post(this.apiProductsURL + "/uploadfile", "", httpOptions)
      .pipe(
          tap(_ => this.log('getPredefinedS3Url')),
          catchError(this.handleError<any>('getPredefinedS3Url'))
      );
    }

    public uploadfileAWSS3(fileuploadurl: string, contenttype: string, file: any): Observable<any>{
       // this will be used to upload all csv files to AWS S3
       const headers = new HttpHeaders({'Content-Type': contenttype, 'x-amz-acl': 'public-read' });
       const req = new HttpRequest(
       'PUT',
       fileuploadurl,
       file,
       {
         headers: headers,
         reportProgress: true, //This is required for track upload process
       });
       return this.http.request(req);
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error('Error0001', error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log('${operation} failed: ${error.message}');

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a Attribute Service message with the MessageService */
    private log(message: string) {
        this.messageService.add('Attribute Service: ${message}');
    }

}
