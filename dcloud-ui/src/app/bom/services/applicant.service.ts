import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { AuthService } from '../../login/services/auth.service';
import { MessageService } from '../../_services';
import { Applicant } from '../models/applicant.model';
import { ContactEmailInfo } from '../models/contactemail.model';
import { LetterTypeInfo } from '../models/letter-type.model';
import { DocumentProofCheck } from './../../forms/models/doc-proof-req.model';
import { BOMSheet } from '../../forms/models/bom/bom-sheet.model';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
/**
 *
 *
 * @export
 * @class ApplicantService
 */
@Injectable()
export class ApplicantService {


    private apiBOMURL = environment.APIURL + '/api/v1/bom';

    constructor(private http: HttpClient, private authService: AuthService, private messageService: MessageService) {}

    public getAllBOMs() {
      return this.http.get(this.apiBOMURL, httpOptions)
      .pipe(
          tap(_ => this.log('getAllBOMs')),
          catchError(this.handleError<any>('getAllBOMs'))
      );
    }

    public getAllAWSBOMItems(bomId: number) {
      return this.http.get(this.apiBOMURL + '/aws/' + bomId, httpOptions)
      .pipe(
          tap(_ => this.log('getAllAWSBOMItemss')),
          catchError(this.handleError<any>('getAllAWSBOMItemss'))
      );
    }

    public getAllCOTSBOMItems(bomId: number) {
      return this.http.get(this.apiBOMURL + '/cots/' + bomId, httpOptions)
      .pipe(
          tap(_ => this.log('getAllCOTSBOMItems')),
          catchError(this.handleError<any>('getAllCOTSBOMItems'))
      );
    }

    public getBOM(bomId: number) {
      return this.http.get(this.apiBOMURL + '/' + bomId, httpOptions)
      .pipe(
          tap(_ => this.log('getBOM')),
          catchError(this.handleError<any>('getBOM'))
      );
    }

    public deleteBOM(bomId: number) {
      return this.http.delete(this.apiBOMURL + '/' + bomId, httpOptions)
      .pipe(
          tap(_ => this.log('getBOM')),
          catchError(this.handleError<any>('getBOM'))
      );
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error('Error0001', error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log('${operation} failed: ${error.message}');

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a Attribute Service message with the MessageService */
    private log(message: string) {
        this.messageService.add('Attribute Service: ${message}');
    }

}
