import { DocToUpload } from './../../forms/models/doc-to-upload.model';
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { saveAs } from 'file-saver';
import * as _ from 'lodash';
import * as moment from 'moment';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { isNil } from 'ramda';
import { Subscription } from 'rxjs';
import * as XLSX from 'xlsx';
import { ShareDataService } from '../../core/sharedata.service';
import { Applicant } from '../models/applicant.model';
import { ApplicantService } from '../services/applicant.service';
import { environment } from './../../../environments/environment.prod';
import { OnComponentErrorHandler } from './../../core/coreerror.handler';
import { AuthService } from './../../login/services/auth.service';
import { ContactCardInfo } from './../models/contact-card.model';
import { ContactEmailInfo } from './../models/contactemail.model';
import { LetterTypeInfo } from './../models/letter-type.model';
import { Column4NameValues } from './../models/namevalue.model';
import { Square9Request } from './../models/square9-request.model';
import { DataTableSortInfo } from '../../shared/datatable/DataTableSortInfo.model';
import { LocalStorageService } from 'ngx-webstorage';


declare var jQuery: any;

/**
 *
 *
 * @export
 * @class ApplicantDetailsPage
 * @implements {OnInit}
 * @implements {OnDestroy}
 */
@Component({
  selector: "[applicant-details]",
  templateUrl: "./applicant-details.template.html",
  styleUrls: ["./applicant-details.style.scss"],
  providers: [ShareDataService, BsModalService],
  encapsulation: ViewEncapsulation.None,
})
export class ApplicantDetailsPage implements OnInit, OnDestroy, OnComponentErrorHandler {
  // paginator required
  public sortInfo: DataTableSortInfo = new DataTableSortInfo("timestamp", "asc", "asc", 1);

  // message to display
  msgDisplay: string;

  // title to display
  titleDisplay: string;

  // busy cursor to subscriptions
  busySendEmail: Subscription;
  busyGenerateLetter: Subscription;
  busyAddNote: Subscription;
  busy: Subscription;
  sub: any;

  // manage error alerts with email
  showSendEmailFlag: boolean;
  emailalerts: Array<Object>;

  // manage error alters with letters
  showGenerateLetterFlag: boolean;
  letteralerts: Array<Object>;

  // array of files to upload
  filesToUpload: Array<File> = [];
  docFileName: string;

  // notes entered
  notestoadd: string;

  // flag to store if notes was entered
  notesFound: boolean = false;

  // manage page error
  showErrorFlag: boolean;
  alerts: Array<Object>;

  contacts: ContactCardInfo[]=[];
  profilesummaries: Column4NameValues[]=[];

  applicant: Applicant;
  applicationStatus: any;
  error: any;

  applicationId: string;

  documentKey: string;
  documentURL: string;

  shareData: ShareDataService;

  bsModalRef: BsModalRef;

  // manage email popup
  emailsender: string;
  emailsendercc: string;
  emailsubject: string;
  emailbody: string;

  selLetterType = [];
  selLetterCategory: string = "";

  receivedDate: string;
  manualCreditDate: string;

  correspondenceNote: string;

  dropdownList = [];
  dropdownList2 = [];
  selectedItems = [];
  dropdownSettings = {};


  dateMask = {
    mask: [/[0-1]/, /[0-9]/, "/", /[0-3]/, /[0-9]/, "/", /[1-2]/, /[0-9]/, /[0-9]/, /[0-9]/]
  };

  lettercategories = [
    { value: "INFO", name: "Info Letter" },
    { value: "DENIAL", name: "Denial Letter" }
  ];

  validDate="1";
  maxDate = new Date();

  applicationstatustypes = [
    { value: "", name: "" },
    { value: "T", name: "Draft" }
  ];

  applicationflags = [];

  infolettertypes = [
    "2 - Report SSN"
  ];

  denylettertypes = [
    "1 - Ineligible Gross Income"
  ];

  temp = {
    "RE_FREETEXT": ["FreeForm", "", "FreeForm", "FreeForm"]
  };

  constructor(
    private applicantService: ApplicantService,
    private router: Router,
    private route: ActivatedRoute,
    private _shareData: ShareDataService,
    private storage: LocalStorageService,
    private auth: AuthService,
    private modalService: BsModalService
  ) {
    this.shareData = _shareData;

    this.showErrorFlag = false;
    this.alerts = [{ type: "success", msg: "" }];

    this.showSendEmailFlag = false;
    this.emailalerts = [{ type: "success", msg: "" }];

    this.showGenerateLetterFlag = false;
    this.letteralerts = [{ type: "success", msg: "" }];
  }

  ngOnDestroy() {
    if (this.busy) {
      this.busy.unsubscribe();
    }
    if(this.busySendEmail){
      this.busySendEmail.unsubscribe();
    }
    if(this.busyGenerateLetter){
      this.busyGenerateLetter.unsubscribe();
    }
    if(this.busyAddNote){
      this.busyAddNote.unsubscribe();
    }
  }

  ngOnInit(): void {

    this.infolettertypes.forEach(
      (val, index) => {
        this.dropdownList.push(
          val
        );
      }
    );

    this.denylettertypes.forEach(
      (val, index) => {
        this.dropdownList2.push(
          val
        );
      }
    );
    // this.selectedItems = [];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      itemsShowLimit: 10,
      enableCheckAll: false
    };
    let searchInput = jQuery("#table-search-input, #search-permits");
    searchInput
      .focus(e => {
        jQuery(e.target)
          .closest(".input-group")
          .addClass("focus");
      })
      .focusout(e => {
        jQuery(e.target)
          .closest(".input-group")
          .removeClass("focus");
      });
    // this.loadApplicationInfo();
  }

  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  clearMultiSelection() {
    this.selLetterType = [];
  }
  /**
   * Used to populate contacts rolodex on this page
   */

  expandAll() {
    jQuery(".widget").widgster("expand");
  }

  collapseAll() {
    jQuery(".widget").widgster("collapse");
  }

  gotoEditNotes() {
    jQuery.scrollTo(jQuery("#sec_notes_info"), 1000);
  }

  gotoUploadDocuments() {
    jQuery.scrollTo(jQuery("#sec_doc_info"), 1000);
  }

  gotoActionBar() {
    jQuery.scrollTo(jQuery("#sec_tool_action"), 1000);
  }

  // public downloadFile(doc:DocToUpload){
  //   if(doc.status === 'Y'){
  //     this.documentKey = this.model.rtcformId + "/uploads/" + doc.fileName;
  //     let handler = this;
  //     this.auth.download(this.documentKey, {
  //       level: 'public'
  //     })
  //     .subscribe(
  //       data => {
  //         if (!isNil(data) && !isNil(data.errorMessage)) {
  //           this.onShowBodyError(data);
  //         } else {
  //           var xhr = new XMLHttpRequest();
  //           xhr.open("GET", data);
  //           xhr.responseType = "blob";

  //           xhr.onload = function () {
  //               saveAs(this.response, doc.fileName); // saveAs is a part of FileSaver.js
  //           };
  //           xhr.send();

  //           // var blob = new Blob(data);
  //           // doc.url = data;
  //           // saveAs(data, "dummy.pdf");
  //           // this.onShowUploadDocumentSuccess(data.key);
  //         }
  //       },
  //       error => {
  //         this.onShowError(error);
  //       }
  //     );

  //   }
  // }

  // public updateDocumentUploadStatus(selDoc:DocToUpload){
  //   this.rtcformService.updateDocumentUploadStatus(this.model).subscribe(data => {
  //     this.onShowBodyError(data);
  //   }, error => {
  //     this.onShowError(error);
  //   });
  // }

  // reset flag and empty title and message
  public onCloseInfoPopup(){
      this.msgDisplay = "";
      this.titleDisplay = "";
      jQuery('#info-popup').modal('hide');
  }

  // Display upload success message
  protected displayInfoPopup(title:string, msg:string){
      this.msgDisplay = msg;
      this.titleDisplay = title;
      jQuery('#info-popup').modal('show');
  }

  // public uploadFileToPrivateFolder(objKey, index, docName){
  //   let handler = this;
  //   this.auth.upload(objKey, this.filesToUpload[index], {
  //     level: 'public',
  //     contentType: this.filesToUpload[index].type
  //   })
  //   .subscribe(
  //     data => {
  //       if (!isNil(data) && !isNil(data.errorMessage)) {
  //         handler.displayInfoPopup("Upload Status", "Failed to Upload Document!" );
  //       } else {
  //         handler.displayInfoPopup("Upload Status", "Document Uploaded Successfully!" );
  //         let rowIndex = _.findIndex(this.model.documents, function (o) { return (o.name && o.name.toString() === docName)});
  //         if(rowIndex !== -1) {
  //             let selDoc = this.model.documents[rowIndex];
  //             selDoc.fileName = this.filesToUpload[index].name;
  //             selDoc.status = 'Y';
  //             handler.updateDocumentUploadStatus(selDoc);
  //         }
  //       }
  //     },
  //     error => {
  //       handler.displayInfoPopup("Upload Status", "Failed to Upload Document!" );
  //     }
  //   );
  // }

  fileChangeEvent(event, index) {
    this.filesToUpload[index] = (<Array<File>>event.target.files)[0];
  }

  // public uploadFile(index, docName){
  //   let handler = this;
  //   if (!isNil(this.filesToUpload[index]) && this.filesToUpload[index].name.length > 0) {
  //     let handler = this;
  //     this.auth.getAuthenticatedUser()
  //       .subscribe(
  //       data => {
  //         if (!isNil(data)) {
  //           handler.documentKey = this.model.rtcformId + "/uploads/" + this.filesToUpload[index].name;
  //           handler.uploadFileToPrivateFolder(handler.documentKey, index, docName);
  //           if(!isNil(data.errorMessage)){
  //             this.onShowBodyError(data);
  //           }
  //         }
  //       },
  //       error => {
  //         this.onShowError(error);
  //       }
  //     );
  //   }
  // }

  private getTimeStamp(){
    return " (timestamp: " + new Date().toLocaleTimeString() + ")";
  }

  public onShowGenerateLetterMessage(body) {
    if (!isNil(body)){
      if(!isNil(body.errorMessage)) {
        this.showGenerateLetterFlag = true;
        this.letteralerts = [];
        this.letteralerts.push({ type: "warning", msg: "Error: " + body.errorMessage + this.getTimeStamp() });
      } else {
        this.showGenerateLetterFlag = true;
        this.letteralerts = [];
        this.letteralerts.push({ type: "info", msg: "Success: " + body + this.getTimeStamp() });
      }
    }
  }

  onResetError(): void {
    this.showErrorFlag = false;
    this.alerts = [];
  }

  public onShowError(error) {
    this.showErrorFlag = true;
    this.alerts = [];
    this.alerts.push({ type: "warning", msg: "Error: " + error + this.getTimeStamp() });
    jQuery.scrollTo(jQuery("#pg-tbar"), 1000);
  }

  public onShowBodyError(body) {
    if (!isNil(body) && !isNil(body.errorMessage)) {
      this.showErrorFlag = true;
      this.alerts = [];
      this.alerts.push({ type: "warning", msg: "Error: " + body.errorMessage + this.getTimeStamp() });
      jQuery.scrollTo(jQuery("#pg-tbar"), 1000);
    }
  }

  public nv(val: number) {
    if (isNil(val)) {
      return "-";
    }
    return val.toString();
  }

  public dblv(val: number) {
    if (isNil(val)) {
      return 0.0;
    }
    return Number(val);
  }

  public iv(val: number) {
    if (isNil(val)) {
      return 0;
    }
    return val;
  }

  public sv(val: string) {
    if (isNil(val)) {
      return "-";
    }
    return val;
  }

  public sve(val: string) {
    if (isNil(val)) {
      return "";
    }
    return val;
  }

  public dv(val: string) {
    if (isNil(val) || val == '') {
      return "-";
    }
    return moment(val).format("MMM DD, YYYY");
  }

  // public ddv(val: Date) {
  //   if (isNil(val)) {
  //     return "-";
  //   }
  //   return moment(val).format("MMM DD, YYYY");
  // }

  private onShowEmailError(body) {
      this.showSendEmailFlag = true;
      this.emailalerts = [];
      this.emailalerts.push({ type: "warning", msg: "Error: " + body.errorMessage + this.getTimeStamp() });
  }

  // Show email sent alert at the bottom of the email box
  private onShowEmailSuccessMessage(body) {
      if(body.result.includes('Success')){
        this.showSendEmailFlag = true;
        this.emailalerts = [];
        this.emailalerts.push({ type: "info", msg: "Success: Email Sent" + this.getTimeStamp() });
      }
  }

  public discardEmail(){
    this.showSendEmailFlag = false;
    this.emailsender = null;
    this.emailsendercc = null;
    this.emailsubject = null;
    this.emailbody = null;
    this.emailalerts = [];
  }

  /**
   * Sends email
   */
  // public sendComposedEmail() {
  //   jQuery('#email-compose').parsley().validate();
  //   if (jQuery('#email-compose').parsley().isValid()) {
  //     this.auth.getAuthenticatedUser().subscribe(
  //       data => {
  //         if (!isNil(data) && !isNil(data.attributes.email)) {
  //           let emailInfo = new ContactEmailInfo();
  //           emailInfo.From = data.attributes.email;
  //           emailInfo.To = this.emailsender;
  //           if(!isNil(this.emailsendercc) && this.emailsendercc.length > 0){
  //             emailInfo.CC = this.emailsendercc;
  //           }
  //           emailInfo.Subject = this.emailsubject;
  //           emailInfo.Message = this.emailbody;
  //           this.busySendEmail = this.applicantService.sendComposedEmail(emailInfo).subscribe(
  //             data => {
  //               this.showSendEmailFlag = false;
  //               this.emailalerts = [];
  //               if (!isNil(data)) {
  //                 if (!isNil(data.errorMessage)) {
  //                   this.onShowEmailError(data);
  //                 } else {
  //                   let msg = JSON.parse(data);
  //                   this.onShowEmailSuccessMessage(msg);
  //                 }
  //               }
  //             },
  //             error => {
  //               this.onShowEmailError(error);
  //             }
  //           );
  //         }
  //       },
  //       error => {
  //         this.onShowEmailError(error);
  //       }
  //     );
  //   }
  // }

  log(event: number) {
    console.log('Slide has been switched: ${event}');
  }

  hideJSONViewer(){

  }

  isDebugAllowed(){
    if(!environment.production){
      return true;
    }
  }
}
