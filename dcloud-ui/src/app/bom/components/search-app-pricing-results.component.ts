import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewEncapsulation, OnDestroy } from "@angular/core";
import { Router } from '@angular/router';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { GridOptions } from 'ag-grid-community';
import 'ag-grid-enterprise';
import { LocalStorageService } from 'ngx-webstorage';
import { isNil } from 'ramda';
import { ShareDataService } from '../../core/sharedata.service';
import { BOMSheet } from '../../forms/models/bom/bom-sheet.model';
import { ApplicationUploadInfo } from '../models/app-upload.model';
import { RefreshSearch } from '../models/refresh-search.model';
import { ApplicantService } from '../services/applicant.service';
import { ProductService } from '../services/product.service';
import { Subscription } from 'rxjs';
import { OnComponentErrorHandler } from '../../core/coreerror.handler';

declare var jQuery: any;

@Component({
  template: `<i class="fa fa-edit" (click)="invokeParentMethod()"></i>`
})
export class EditBOMRendererComponent implements ICellRendererAngularComp {
  params: any;

  agInit(params: any): void {
      this.params = params;
  }

  public invokeParentMethod() {
    this.params.context.componentParent.editBOMSheet(this.params.data);
  }

  refresh(): boolean {
      return false;
  }
}

@Component({
  template: `<i class="fa fa-trash-o" (click)="invokeParentMethod()"></i>`
})
export class DeleteBOMRendererComponent implements ICellRendererAngularComp {
  params: any;

  agInit(params: any): void {
      this.params = params;
  }

  public invokeParentMethod() {
    this.params.context.componentParent.deleteBOMSheet(this.params.data);
  }

  refresh(): boolean {
      return false;
  }
}
@Component({
    // tslint:disable-next-line: component-selector
    selector: '[search-app-pricing-results]',
    templateUrl: './search-app-pricing-results.template.html',
    styleUrls: ['./search-pricing.style.scss'],
    providers: [ShareDataService],
    encapsulation: ViewEncapsulation.None
})


/**
 *
 *
 * @export
 * @class SearchApplicantPricingResultsComponent
 * @implements {OnInit}
 */
export class SearchApplicantPricingResultsComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {

    public router: Router;
    public data: any[];
    public showConfirmPopup: boolean = false;
    public shareData: ShareDataService;
    public gridOptions: GridOptions;
    public columnDefs: any;
    public autoGroupColumnDef: any;
    public defaultColDef: any;
    public rowSelection: any;
    public rowGroupPanelShow: any;
    public pivotPanelShow: any;
    public components;
    public rowModelType;
    public cacheOverflowSize;
    public sideBar;
    public maxConcurrentDatasourceRequests;
    public infiniteInitialRowCount;
    public paginationNumberFormatter: any;
    public maxBlocksInCache;
    public paginationPageSize;
    public cacheBlockSize;
    public getRowNodeId;
    public gridApi;
    public gridColumnApi;
    public overlayLoadingTemplate;
    public busyDeleteBOM: Subscription;

    public showEmail: boolean = false;

    @Input() searchresults: any;
    @Input() ShowLoadingProgressBar: boolean;

    @Output() refreshSearch = new EventEmitter<RefreshSearch>();
    // @Output() showEmailPopup = new EventEmitter<boolean>();
    @Output() showEmailPopup = new EventEmitter<any>();
    @Output() showUploadDocPopup = new EventEmitter<ApplicationUploadInfo>();

    constructor(
        private applicantService: ApplicantService,
        private productService: ProductService,
        private storage: LocalStorageService,
        router: Router,
        private _shareData: ShareDataService
    ) {
        this.shareData = _shareData;
        this.shareData.reset();
        this.router = router;
        this.paginationPageSize = 20;

        this.defaultColDef = {
          sortable: true,
          resizable: true
        };

        this.columnDefs = [
          {
            headerName: "",
            field: "value",
            cellRendererFramework: EditBOMRendererComponent,
            colId: "params",
            width: 50,
            filter: false
          },
          {headerName: 'Project', field: 'project', sortable: true, filter: true },
          {headerName: 'Client', field: 'client', sortable: true, filter: true },
          {headerName: 'Sponsor', field: 'sponsor', sortable: true, filter: true },
          {headerName: 'WBS Code', field: 'wbsCode', sortable: true, filter: true},
          {headerName: 'Offering', field: 'package', sortable: true, filter: true},
          {headerName: 'Total Cost', field: 'total_cost', sortable: true, filter: true},
          {headerName: 'Last Modified', field: 'modified_dt', sortable: true, filter: true},
          {
            headerName: "",
            field: "value",
            cellRendererFramework: DeleteBOMRendererComponent,
            colId: "params",
            width: 50,
            filter: false
          },
      ];
      this.sideBar = {
        toolPanels: [
          {
            id: "columns",
            labelDefault: "Columns",
            labelKey: "columns",
            iconKey: "columns",
            toolPanel: "agColumnsToolPanel"
          },
          {
            id: "filters",
            labelDefault: "Filters",
            labelKey: "filters",
            iconKey: "filter",
            toolPanel: "agFiltersToolPanel"
          }
        ],
        defaultToolPanel: ""
      };

      this.gridOptions = <GridOptions>{
        context: {
            componentParent: this
        },
        enableColResize: true,
      };
    }

    ngOnInit() {
        // this.data = this.searchresults;
        this.loadPreferences();
        this.clearLocalStorageContent("PROCHTC");
        this.clearLocalStorageContent("PROCRTC");
    }

    ngAfterViewInit() {
    }

    ngOnDestroy(): void {
      if(this.busyDeleteBOM){
        this.busyDeleteBOM.unsubscribe();
      }
    }

    onGridReady(params) {
      this.gridApi = params.api;
      this.gridColumnApi = params.columnApi;
      let that = this;

      // let pq: fetchprodquery = new fetchprodquery();
      // pq.rec_count = "1000";
      // pq.start_rec = "10";
      // pq.estb_id = "";

      // this.applicantService.getAllBOMs().subscribe(boms => {
      //     that.data = boms.boms;
      //     // let dataSource = {
      //     //   rowCount: null,
      //     //   getRows: function(fparams: any) {
      //     //     console.log("asking for " + fparams.startRow + " to " + fparams.endRow);
      //     //     setTimeout(function() {
      //     //       let dataAfterSortingAndFiltering = sortAndFilter(products, fparams.sortModel, fparams.filterModel);
      //     //       let rowsThisPage = dataAfterSortingAndFiltering.slice(fparams.startRow, fparams.endRow);
      //     //       let lastRow = -1;
      //     //       if (dataAfterSortingAndFiltering.length <= fparams.endRow) {
      //     //         lastRow = dataAfterSortingAndFiltering.length;
      //     //       }
      //     //       fparams.successCallback(rowsThisPage, lastRow);
      //     //     }, 500);
      //     //   }
      //     // };
      //     params.api.setDatasource(that.data);
      //   });
    }

    cosmeticFix(elementID: any){
        // The code below for cosmetic fix - Blue box now surrounds the icon on the search company text box
        let filterInput = jQuery(elementID);
        filterInput
            .focus((e) => {
                jQuery(e.target).closest('.input-group').addClass('focus');
            })
            .focusout((e) => {
                jQuery(e.target).closest('.input-group').removeClass('focus');
            });
    }

    selectEditProduct(prod: any){

    }

    loadPreferences(){
        // this.sortInfo.searchBy = (this.storage.retrieve('SearchApplicantResultsComponentBy') != null) ?
        //         this.storage.retrieve('SearchApplicantResultsComponentBy') : this.sortInfo.searchBy;
        // this.sortInfo.sortBy = (this.storage.retrieve('sortApplicantResultsBy') != null) ?
        //         this.storage.retrieve('sortApplicantResultsBy') : this.sortInfo.sortBy;
        // this.sortInfo.sortOrder = (this.storage.retrieve('sortApplicantResultsOrder') != null) ?
        //         this.storage.retrieve('sortApplicantResultsOrder') : this.sortInfo.sortOrder;
    }

    savePreferences() {
        // this.storage.store('SearchApplicantResultsComponentBy', this.sortInfo.searchBy);
        // this.storage.store('sortApplicantResultsBy', this.sortInfo.sortBy );
        // this.storage.store('sortApplicantResultsOrder', this.sortInfo.sortOrder );
    }

    clearLocalStorageContent(modPrefix) {
        // 9 items to clear for each application type
        this.storage.clear(modPrefix + 'RNF');
        this.storage.clear(modPrefix + 'NMF');
        this.storage.clear(modPrefix + 'SSNF');
        this.storage.clear(modPrefix + 'CYF');
        this.storage.clear(modPrefix + 'CODEF');
        this.storage.clear(modPrefix + 'FLAGF');
        this.storage.clear(modPrefix + 'COUNTYF');
        this.storage.clear(modPrefix + 'STATUSF');
        this.storage.clear(modPrefix + 'ADDRF');
      }


    ngOnChanges(changes: SimpleChanges){
       // tslint:disable-next-line: forin
        for (let propName in changes) {
            if (propName === "searchresults") {
                if(!isNil(changes[propName].currentValue)){
                    this.data = changes[propName].currentValue;
                }
            }
            if(propName === "ShowLoadingProgressBar"){
              if(!isNil(changes[propName].currentValue)){
                if(changes[propName].currentValue){
                  this.gridApi.showLoadingOverlay();
                }else {
                  this.gridApi.hideOverlay();
                }
              }
            }
        }
    }

    showUploadPopup(appl:BOMSheet){
        let upm:ApplicationUploadInfo = new ApplicationUploadInfo();
        upm.type = "tpl";
        // upm.uuid = appl.TP_PRODUCT_UUID;
        this.showUploadDocPopup.emit(upm);
    }

    isValidString(str) {
        return ( !isNil(str) && (str.length >= 0));
    }

    getApplicationFlag(appl:BOMSheet){
        // if(!isNil(appl.TP_STEP_FLAG)){
        //     return appl.TP_STEP_FLAG;
        // }
        return "";
    }

    isApplicationinProgress(appl:BOMSheet){
        // if(!isNil(appl.TP_PRODUCT_STATUS)){
        //     if(appl.TP_PRODUCT_STATUS === 'T'){
        //         return true;
        //     }
        // }
        return false;
    }

    public editBOMSheet(bom:BOMSheet){
      this.router.navigate(['/app/forms/edit/', bom.bomId]);
    }

    public deleteBOMSheet(bom:BOMSheet){
      let that = this;
      this.busyDeleteBOM = this.applicantService.deleteBOM(bom.bomId).subscribe( data => {
          if (!isNil(data)) {
          }else {
          }
        },
        error => {
        });
    }
    public ExportAllProductstoExcel(){
      let params = {
        skipHeader: false,
        columnGroups: false,
        skipFooters: false,
        skipGroups: true,
        skipPinnedTop: false,
        skipPinnedBottom: false,
        allColumns: true,
        onlySelected: false,
        suppressQuotes: false,
        fileName: 'export-data.csv',
        columnseparator: ","
      };
      // if (getBooleanValue("#skipGroupR")) {
      //   params.shouldRowBeSkipped = function(params) {
      //     return params.node.data.country.charAt(0) === "R";
      //   };
      // }
      // if (getBooleanValue("#useCellCallback")) {
      //   params.processCellCallback = function(params) {
      //     if (params.value && params.value.toUpperCase) {
      //       return params.value.toUpperCase();
      //     } else {
      //       return params.value;
      //     }
      //   };
      // }
      // if (getBooleanValue("#useSpecificColumns")) {
      //   params.columnKeys = ["country", "bronze"];
      // }
      // if (getBooleanValue("#processHeaders")) {
      //   params.processHeaderCallback = function(params) {
      //     return params.column.getColDef().headerName.toUpperCase();
      //   };
      // }
      // if (getBooleanValue("#appendHeader")) {
      //   params.customHeader = [
      //     [],
      //     [
      //       {
      //         data: {
      //           type: "String",
      //           value: "Summary"
      //         }
      //       }
      //     ],
      //     [
      //       {
      //         data: {
      //           type: "String",
      //           value: "Sales"
      //         },
      //         mergeAcross: 2
      //       },
      //       {
      //         data: {
      //           type: "Number",
      //           value: "3695.36"
      //         }
      //       }
      //     ],
      //     []
      //   ];
      // }
      // if (getBooleanValue("#appendFooter")) {
      //   params.customFooter = [
      //     [],
      //     [
      //       {
      //         data: {
      //           type: "String",
      //           value: "Footer"
      //         }
      //       }
      //     ],
      //     [
      //       {
      //         data: {
      //           type: "String",
      //           value: "Purchases"
      //         },
      //         mergeAcross: 2
      //       },
      //       {
      //         data: {
      //           type: "Number",
      //           value: "7896.35"
      //         }
      //       }
      //     ],
      //     []
      //   ];
      // }
      this.gridApi.exportDataAsCsv(params);
    }

    public refreshHTCSearchByYear(){
        // let rfs:RefreshSearch = new RefreshSearch();
        // rfs.year = this.searchHTCYear;
        // this.refreshSearch.emit(rfs);
    }
}


function sortAndFilter(allOfTheData, sortModel, filterModel) {
  return sortData(sortModel, filterData(filterModel, allOfTheData));
}
function sortData(sortModel, data) {
  var sortPresent = sortModel && sortModel.length > 0;
  if (!sortPresent) {
    return data;
  }
  var resultOfSort = data.slice();
  resultOfSort.sort(function(a, b) {
    for (var k = 0; k < sortModel.length; k++) {
      var sortColModel = sortModel[k];
      var valueA = a[sortColModel.colId];
      var valueB = b[sortColModel.colId];
      if (valueA == valueB) {
        continue;
      }
      var sortDirection = sortColModel.sort === "asc" ? 1 : -1;
      if (valueA > valueB) {
        return sortDirection;
      } else {
        return sortDirection * -1;
      }
    }
    return 0;
  });
  return resultOfSort;
}
function filterData(filterModel, data) {
  var filterPresent = filterModel && Object.keys(filterModel).length > 0;
  if (!filterPresent) {
    return data;
  }
  var resultOfFilter = [];
  for (var i = 0; i < data.length; i++) {
    var item = data[i];
    if (filterModel.age) {
      var age = item.age;
      var allowedAge = parseInt(filterModel.age.filter);
      if (filterModel.age.type == "equals") {
        if (age !== allowedAge) {
          continue;
        }
      } else if (filterModel.age.type == "lessThan") {
        if (age >= allowedAge) {
          continue;
        }
      } else {
        if (age <= allowedAge) {
          continue;
        }
      }
    }
    if (filterModel.year) {
      if (filterModel.year.indexOf(item.year.toString()) < 0) {
        continue;
      }
    }
    if (filterModel.country) {
      if (filterModel.country.indexOf(item.country) < 0) {
        continue;
      }
    }
    resultOfFilter.push(item);
  }
  return resultOfFilter;
}
