import { Component, ElementRef, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { QueryBuilderConfig } from 'angular2-query-builder';
import { LocalStorageService } from 'ngx-webstorage';
import { isNil } from 'ramda';
import { Subscription } from 'rxjs';
import { UserSaveFilter } from '../models/user-save-filter.model';
import { ProductService } from '../services/product.service';

declare var jQuery: any;

@Component({
	selector: "[prod-query-popup]",
	templateUrl: "./prod-query-popup.template.html",
	styleUrls: ["./prod-query-popup.style.scss"],
	encapsulation: ViewEncapsulation.None,
	providers: []
})
export class ProductQueryPopupModal implements OnInit {
	public filesToUpload: Array<File> = [];
	public busy: Subscription;
	public importSheetMode: number;

	public saveQueryName: string;
	public selEditUserFilter: UserSaveFilter;

	@Input() editUserFilter: UserSaveFilter;
	@Input() sheetMode: number;

	@Output() refreshSearch = new EventEmitter<any>();
	@Output() refreshFilter = new EventEmitter<any>();

	private _elementRef: ElementRef;
	private storage: LocalStorageService;

	public showErrorFlag: boolean;
	public alerts: any[] = [];

	query = {
		condition: "and",
		rules: [
			{
				field: "product_name",
				operator: "contains",
				type: "string",
				value: ""
			}
		]
	};

	myOperatorMap = {
		string: [
			"equal",
			"not_equal",
			"begins_with",
			"not_begins_with",
			"contains",
			"not_contains",
			"ends_with",
			"not_ends_with"
		],
		number: [
			"equal",
			"not_equal",
			"greater",
			"greater_or_equal",
			"between",
			"less",
			"less_or_equal",
			"begins_with",
			"not_begins_with",
			"contains",
			"not_contains",
			"ends_with",
			"not_ends_with"
		],
		time: [
			"equal",
			"not_equal",
			"greater",
			"greater_or_equal",
			"between",
			"less",
			"less_or_equal",
			"begins_with",
			"not_begins_with",
			"contains",
			"not_contains",
			"ends_with",
			"not_ends_with"
		],
		date: [
			"equal",
			"not_equal",
			"greater",
			"greater_or_equal",
			"between",
			"less",
			"less_or_equal"
		],
		category: ["equal", "not_equal", "in", "not_in"],
		boolean: ["equal", "not_equal"],
		multiselect: ["in", "not_in"]
	};

	// EQUAL("="),
	// NOT_EQUAL("!="),
	// IN("in"),
	// NOT_IN("not_in"),
	// LESS("<"),
	// LESS_OR_EQUAL("<="),
	// GREATER(">"),
	// GREATER_OR_EQUAL(">="),
	// BETWEEN("between"),
	// NOT_BETWEEN("not_between"),
	// BEGINS_WITH("begins_with"),
	// NOT_BEGINS_WITH("not_begins_with"),
	// CONTAINS("contains"),
	// NOT_CONTAINS("not_contains"),
	// ENDS_WITH("ends_with"),
	// NOT_ENDS_WITH("not_ends_with"),
	// IS_EMPTY("is_empty"),
	// IS_NOT_EMPTY("is_not_empty"),
	// IS_NULL("is_null"),
	// IS_NOT_NULL("is_not_null");


	config: QueryBuilderConfig = {
		fields: {
			product_name: {
				name: "Product Name",
				type: "string",
				operators: ["contains", "=", "!=", "is_empty", "is_not_empty", "in", "not_in", "is_null", "is_not_null", "begins_with", "ends_with", "ends_with", "not_ends_with"]
			},
			product_type: {
				name: "Product Type",
				type: "string",
				operators: ["contains", "=", "!=", "is_empty", "is_not_empty", "in", "not_in", "is_null", "is_not_null", "begins_with", "ends_with", "ends_with", "not_ends_with"]
			},
			product_id: {
				name: "Project ID",
				type: "string",
				operators: ["contains", "=", "!=", "is_empty", "is_not_empty", "in", "not_in", "is_null", "is_not_null", "begins_with", "ends_with", "ends_with", "not_ends_with"]
			},
			product_use: {
				name: "Product Use",
				type: "string",
				operators: ["contains", "=", "!=", "is_empty", "is_not_empty", "in", "not_in", "is_null", "is_not_null", "begins_with", "ends_with", "ends_with", "not_ends_with"]
			},
			product_category: {
				name: "Product Category",
				type: "string",
				operators: ["contains", "=", "!=", "is_empty", "is_not_empty", "in", "not_in", "is_null", "is_not_null", "begins_with", "ends_with", "ends_with", "not_ends_with"]
			},
			product_sub_category: {
				name: "Product Subcategory",
				type: "string",
				operators: ["contains", "=", "!=", "is_empty", "is_not_empty", "in", "not_in", "is_null", "is_not_null", "begins_with", "ends_with", "ends_with", "not_ends_with"]
			},
			product_other_category: {
				name: "Product Other Category",
				type: "string",
				operators: ["contains", "=", "!=", "is_empty", "is_not_empty", "in", "not_in", "is_null", "is_not_null", "begins_with", "ends_with", "ends_with", "not_ends_with"]
			},
			product_flavor: {
				name: "Product Flavor",
				type: "string",
				operators: ["contains", "=", "!=", "is_empty", "is_not_empty", "in", "not_in", "is_null", "is_not_null", "begins_with", "ends_with", "ends_with", "not_ends_with"]
			},
			product_other_flavor: {
				name: "Product Other Flavor",
				type: "string",
				operators: ["contains", "=", "!=", "is_empty", "is_not_empty", "in", "not_in", "is_null", "is_not_null", "begins_with", "ends_with", "ends_with", "not_ends_with"]
			},
			product_launch_date: {
				name: "Product Launch Date",
				type: "string",
				operators: ["contains", "=", "!=", "is_empty", "is_not_empty", "in", "not_in", "is_null", "is_not_null", "begins_with", "ends_with", "ends_with", "not_ends_with"]
      },
			product_estb_id: {
				name: "Establishment ID",
				type: "string",
				operators: ["contains", "=", "!=", "is_empty", "is_not_empty", "in", "not_in", "is_null", "is_not_null", "begins_with", "ends_with", "ends_with", "not_ends_with"]
			}
		}
	};

	// classNames: QueryBuilderClassNames = {
	// 	removeIcon: "fa fa-minus",
	// 	addIcon: "fa fa-plus",
	// 	arrowIcon: "fa fa-chevron-right px-2",
	// 	button: "btn",
	// 	buttonGroup: "btn-group",
	// 	rightAlign: "order-12 ml-auto",
	// 	switchRow: "d-flex px-2",
	// 	switchGroup: "d-flex align-items-center",
	// 	switchRadio: "custom-control-input",
	// 	switchLabel: "custom-control-label",
	// 	switchControl: "custom-control custom-radio custom-control-inline",
	// 	row: "row p-2 m-1",
	// 	rule: "border",
	// 	ruleSet: "border",
	// 	invalidRuleSet: "alert alert-danger",
	// 	emptyWarning: "text-danger mx-auto",
	// 	operatorControl: "form-control",
	// 	operatorControlSize: "col-auto pr-0",
	// 	fieldControl: "form-control",
	// 	fieldControlSize: "col-auto pr-0",
	// 	entityControl: "form-control",
	// 	entityControlSize: "col-auto pr-0",
	// 	inputControl: "form-control",
	// 	inputControlSize: "col-auto"
	// };

	constructor(
		elementRef: ElementRef,
		private prodService: ProductService,
		_storage: LocalStorageService
	) {
		this._elementRef = elementRef;
		this.saveQueryName = "";
		this.storage = _storage;

		jQuery(this._elementRef.nativeElement).on("shown.bs.modal", () => {
			this.initializeModal();
		});

		jQuery(this._elementRef.nativeElement).on("hidden.bs.modal", () => {
			this.cleanupModal();
		});
	}

	ngOnInit() {
		this.resetAlerts();
		this.showErrorFlag = false;
	}

	public initializeModal() {}

	// Free up memory and stop any event listeners/hubs
	public cleanupModal() {
		this.resetAlerts();
		this.showErrorFlag = false;
		jQuery("#frmSaveQuery")
			.parsley()
			.reset();
	}

	public resetAlerts() {
		this.alerts = [];
	}

	reset() {
		jQuery("#frmSaveQuery")
			.parsley()
			.reset();
		jQuery("#proj-query-popup").modal("hide");
	}

	public cancel() {
		this.reset();
	}

	public applyFilter() {
		// console.log(this.query);
		jQuery("#proj-query-popup").modal("toggle");
		this.refreshSearch.emit(this.query);
	}

	public saveFilter() {
		let uf: UserSaveFilter = new UserSaveFilter();
		uf.filterCategory = "KANB";
		uf.userName = (this.storage.retrieve('LoggedUser') != null) ? this.storage.retrieve('LoggedUser') : "NA";
		uf.filterName = this.saveQueryName;
		uf.filterJson = JSON.stringify(this.query);
		this.prodService.saveUserFilter(uf).subscribe(
			data => {
				jQuery("#proj-query-popup").modal("toggle");
				this.refreshFilter.emit(true);
			},
			error => {}
		);
	}

	ngOnChanges(changes: SimpleChanges) {
		for (let propName in changes) {
			if (propName === "editUserFilter") {
				this.selEditUserFilter = changes[propName].currentValue;
				if (!isNil(this.selEditUserFilter)) {
					if (!isNil(this.selEditUserFilter.filterJson)) {
						this.query = JSON.parse(this.selEditUserFilter.filterJson);
						this.saveQueryName = this.selEditUserFilter.filterName;
					}
				}
			}
			if (propName === "sheetMode") {
				let chg = changes[propName];
				if (chg.currentValue) {
					this.importSheetMode = chg.currentValue;
				}
			}
		}
	}
}
