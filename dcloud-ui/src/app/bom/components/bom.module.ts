import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { CarouselModule, ModalModule } from 'ngx-bootstrap';
import { AuthGuard } from '../../login/services/auth-guard.service';
import { SharedModule } from '../../shared/shared.module';
import { ApplicantService } from '../services/applicant.service';
import { ProductService } from '../services/product.service';
import { TPLApplicantDataFilterPipe } from '../util/tplapplicant-filter.pipe';
import { SearchApplicantPipe } from '../util/search-resource-pipe';
import { ApplicantDetailsPage } from './applicant-details.component';
import { ApplicantMessageBox } from './applicant-msgbox.component';
import { CommentsPanelComponent } from './comments-panel.component';
import { ContactCardComponent } from './contact-card.component';
import { ImportProductCSVComponent } from './imp-prod-csv.component';
import { MyLinkRendererComponent } from './my-link-renderer.component';
import { ProductQueryPopupModal } from './prod-query-popup.component';
import { SearchApplicantPricingResultsComponent, EditBOMRendererComponent,
   DeleteBOMRendererComponent } from './search-app-pricing-results.component';
import { SearchPricingPageComponent } from './search-pricing.component';
import { UploadDocumentComponent } from './upload-doc.component';

export const routes = [
  { path: '', component: SearchPricingPageComponent, pathMatch: 'full' },
  { path: 'search', component: SearchPricingPageComponent},
  { path: 'details/:id', component: ApplicantDetailsPage},
  { path: 'details', component: ApplicantDetailsPage},
];

@NgModule({
    imports: [
      CommonModule, SharedModule, RouterModule.forChild(routes), ModalModule, CarouselModule,
      NgMultiSelectDropDownModule.forRoot()
    ],
    declarations: [ SearchPricingPageComponent, SearchApplicantPipe, ApplicantDetailsPage, CommentsPanelComponent, UploadDocumentComponent,
      SearchApplicantPricingResultsComponent, MyLinkRendererComponent, ImportProductCSVComponent, ProductQueryPopupModal,
      EditBOMRendererComponent, DeleteBOMRendererComponent,
      ApplicantMessageBox, TPLApplicantDataFilterPipe, ContactCardComponent],
    providers: [ApplicantService, AuthGuard, ProductService],
    entryComponents: [
      ApplicantMessageBox, MyLinkRendererComponent, EditBOMRendererComponent, DeleteBOMRendererComponent
    ]
})

export class BOMModule {
  static routes = routes;
}
