import { Component, ElementRef, OnInit, ViewEncapsulation, EventEmitter, Output, Input, SimpleChanges } from "@angular/core";
import { AuthService } from '../../login/services/auth.service';
import * as _ from 'lodash';
import { isNil } from 'ramda';
import { OnComponentErrorHandler } from '../../core/coreerror.handler';
import { LocalStorageService } from 'ngx-webstorage';
import { ProductService } from '../services/product.service';

declare var jQuery: any;

@Component({
	selector: "[imp-prod-csv]",
	templateUrl: "./imp-prod-csv.template.html",
	styleUrls: ["./imp-prod-csv.style.scss"],
	encapsulation: ViewEncapsulation.None,
	providers: []
})
export class ImportProductCSVComponent implements OnInit, OnComponentErrorHandler {
  @Input() UploadMethod;
  uploadmethod: number;
  showErrorFlag: boolean;
  filesToUpload: Array<File> = [];
  alerts: any[] = [];
  docFileName: string;
  dynamic: number;
  documentKey: string;
  profileid: string;
  msgDisplay: string;

  // title to display
  titleDisplay: string;
  private _elementRef: ElementRef;

  @Output() refreshProducts= new EventEmitter<boolean>();

	constructor(
    public auth: AuthService,
    public prodService: ProductService,
    private storage: LocalStorageService,
		elementRef: ElementRef,
	) {
    this._elementRef = elementRef;

    this.profileid = this.storage.retrieve("LOGGEDUSERID");

		jQuery(this._elementRef.nativeElement).on("shown.bs.modal", () => {
			this.initializeModal();
		});

		jQuery(this._elementRef.nativeElement).on("hidden.bs.modal", () => {
			this.cleanupModal();
		});
  }

  ngOnInit(): void {
    this.uploadmethod = 1;
  }

  public initializeModal() {}

  public cleanupModal() {
		this.resetAlerts();
		this.showErrorFlag = false;
		jQuery("#fileImport").val(null);
		jQuery("#import-prod-csv")
			.find("input[type=text]")
			.val("");
		jQuery("#importfileform")
			.parsley()
			.reset();
  }

  public resetAlerts() {
		this.alerts = [];
	}

  reset() {
		jQuery("#importfileform")
			.parsley()
      .reset();
    this.dynamic =  0;
    this.refreshProducts.emit(true);
    jQuery("#import-prod-csv").modal("hide");
  }

  public uploadtoS3usingPredefinedUrl(predefurl: string, file: any){
    this.prodService.uploadfileAWSS3(predefurl, 'text/csv', file).subscribe(
      data => {
        if (!isNil(data) && !isNil(data.errorMessage)) {
          this.onShowBodyError(data);
        } else {
        }
      },
      error => {
        this.onShowError(error);
      }
    );
  }

  public importFileUsingApi(file: any){
    let that = this;
    this.prodService.getPredefinedS3Url().subscribe(
      data => {
        if (!isNil(data) && !isNil(data.errorMessage)) {
          that.onShowBodyError(data);
        } else {
          if(!isNil(data.body)){
            let predefurl: string = data.body;
            that.uploadtoS3usingPredefinedUrl(predefurl, file);
          }
        }
      },
      error => {
        that.onShowError(error);
      }
    );

  }
  public importFile() {
		jQuery("#importfileform")
			.parsley()
			.validate();

  		if (
  			!jQuery("#importfileform")
  				.parsley()
  				.isValid()
  		) {
        return;
      }


      if (!isNil(this.filesToUpload[0]) && this.filesToUpload[0].name.length > 0) {

        if(this.uploadmethod === 2){
          return this.importFileUsingApi(this.filesToUpload[0]);
        }

        const estb_id = this.storage.retrieve("ESTABLISHMENTID");

        let docName = this.filesToUpload[0].name;
        let that = this;
        this.auth.getAuthenticatedUser()
          .subscribe(
          data => {
            if (!isNil(data)) {
              that.documentKey = "products/" + estb_id + "/" + this.filesToUpload[0].name;
              that.uploadFileToPrivateFolder(that.documentKey, 0, docName);
              if(!isNil(data.errorMessage)){
                that.onShowBodyError(data);
              }
            }
          },
          error => {
            this.onShowError(error);
          }
        );
      }
    }

  public getFilePathExtension(path) {
		const filename = path
			.split("\\")
			.pop()
			.split("/")
			.pop();
		return filename.substr(
			(Math.max(0, filename.lastIndexOf(".")) || Infinity) + 1
		);
  }

  public onResetError(): void {
    this.showErrorFlag = false;
    this.alerts = [];
  }

  public onShowError(error: any): void {
    this.showErrorFlag = true;
    this.alerts = [];
    this.alerts.push({ type: "warning", msg: "Error: " + error });
    // jQuery.scrollTo(jQuery("#pg-tbar"), 1000);
  }

  public onShowBodyError(body: any): void {
    if (!isNil(body) && !isNil(body.errorMessage)) {
      this.showErrorFlag = true;
      this.alerts = [];
      this.alerts.push({ type: "warning", msg: "Error: " + body.errorMessage });
      // jQuery.scrollTo(jQuery("#pg-tbar"), 1000);
    }
  }

  protected displayUploadStatusMessage(title:string, msg:string){
    this.msgDisplay = msg;
    this.titleDisplay = title;
    jQuery('#info-popup').modal('show');
}

  public uploadFileToPrivateFolder(objKey, index, docName){
    let that = this;
    this.dynamic = 0.0;
    this.auth.upload(objKey, this.filesToUpload[index], {
      level: 'public',
      contentType: this.filesToUpload[index].type,
      progressCallback(progress) {
        if(progress.total > 0){
          that.dynamic = (progress.loaded/progress.total) * 100.0;
        }
        console.log(`Uploaded: ${progress.loaded}/${progress.total}`);
      },
    })
    .subscribe(
      data => {
        if (!isNil(data) && !isNil(data.errorMessage)) {
          // that.displayUploadStatusMessage("Upload Status", "Failed to Upload Document!" );
          this.onShowBodyError(data);
        } else {
          that.reset();
        }
      },
      error => {
        this.onShowError(error);
      }
    );
  }

  public fileChangeEvent(event) {
		this.showErrorFlag = false;
		this.resetAlerts();

		this.filesToUpload = <Array<File>>event.target.files;
		if (!this.filesToUpload[0]) {
      return;
    }

		this.docFileName = this.filesToUpload[0].name;
		const ext = this.getFilePathExtension(this.docFileName);
		jQuery("#fileImport")
			.parsley()
			.removeError("Invalid-Ext");
		switch (ext.toLowerCase()) {
			case "csv":
				break;

			default:
				const invalidexterror = "Error: Invalid file format";
				jQuery("#fileImport")
					.parsley()
					.addError("Invalid-Ext", { message: invalidexterror });
				break;
		}
	}

  public onCloseInfoPopup(){
    this.msgDisplay = "";
    this.titleDisplay = "";
    jQuery('#info-popup').modal('hide');
    this.reset();
}

  ngOnChanges(changes: SimpleChanges) {
    // tslint:disable-next-line:forin
    for (let propName in changes) {
      if (propName === "UploadMethod") {
        let chng = changes[propName];
        if (chng.currentValue) {
          this.uploadmethod = chng.currentValue;
        }
      }
    }
  }
}
