import { Component, ElementRef, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewEncapsulation, OnChanges, OnDestroy } from '@angular/core';
import { isNil } from 'ramda';
import { Subscription } from 'rxjs';
import { OnComponentErrorHandler } from '../../core/coreerror.handler';
import { BOMSheet } from '../../forms/models/bom/bom-sheet.model';
import { BOMFormsService } from '../../forms/services/bom-forms.service';
import { DocToUpload } from './../../forms/models/doc-to-upload.model';
import { AuthService } from './../../login/services/auth.service';
import { ApplicationUploadInfo } from './../models/app-upload.model';
import { ApplicantService } from './../services/applicant.service';

declare var jQuery: any;

@Component({
    selector: '[upload-doc]',
    templateUrl: './upload-doc.template.html',
    styleUrls: ['./upload-doc.style.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [BOMFormsService]
})

export class UploadDocumentComponent implements OnInit, OnComponentErrorHandler, OnDestroy, OnChanges {

    // array of files
    filesToUpload: Array<File> = [];

    // current documentname
    docFileName: string;

    // doc key to be uploaded
    documentKey: string;

    // handle subscription
    busy: Subscription;

    // flag to handle displaying error messages
    showErrorFlag: boolean = false;

    // array of alert messages
    alerts: any[] = [];

    // message to display
    msgDisplay: string;

    // title to display
    titleDisplay: string;

    tplmodel: BOMSheet = new BOMSheet();

    @Input() uploadmodel: ApplicationUploadInfo;
    @Output() hideUploadDocPopup = new EventEmitter<boolean>();

    private _elementRef: ElementRef;

    constructor(
        private applicantService: ApplicantService,
        private tplformService: BOMFormsService,
        private auth: AuthService,
        elementRef: ElementRef,
    ) {
        this._elementRef = elementRef;

        // This is the bootstrap on shown event where i call the initialize method
        jQuery(this._elementRef.nativeElement).on("shown.bs.modal", () => {
            this.initializeModal();
        });

         jQuery(this._elementRef.nativeElement).on("hidden.bs.modal", () => {
             this.cleanupModal();
         });
    }


    ngOnInit() {
        this.showErrorFlag = false;
    }

    public initializeModal() {
    }

    // Free up memory and stop any event listeners/hubs
    public cleanupModal() {
        this.showErrorFlag = false;
        jQuery('#uploadDocForm').parsley().reset();
    }

    onResetError(): void {
        this.showErrorFlag = false;
        this.alerts = [];
      }

    onShowError(error: any): void {
    }

    onShowBodyError(error: any): void {
    }

    public ngOnDestroy() {
    }

    // This gets called when user closes the upload document modal i.e. this modal window
    public hideMe(){
        this.hideUploadDocPopup.emit(true);
    }

    public loadTPLUploadedDocumentSummary(){
        // this.tplformService.checkDocumentRequirements(this.tplmodel.uuid)
        //   .subscribe(
        //     data => {
        //       if(!isNil(data)){
        //         this.tplmodel.documents = [];
        //         for(let item of data){
        //           let newdoc = new DocToUpload();
        //           newdoc.name = item.Description;
        //           newdoc.fileName =  item.FileName;
        //           newdoc.status = item.UploadStatus;
        //           this.tplmodel.documents.push(newdoc);
        //         }
        //       }
        //    }, error => {
        //     this.onShowError(error);
        //   }
        // );
    }


      //  public loadHTCApplicationInfo() {
      //   let handler = this;
      //   this.busy = this.applicantService
      //       .getTPLApplication(this.uploadmodel.uuid)
      //       .subscribe(
      //       data => {
      //           handler.onShowBodyError(data);
      //           if (!isNil(data)) {
      //               handler.tplmodel.fromDB(data);
      //               handler.loadTPLUploadedDocumentSummary();
      //           }else {
      //               this.onShowError("Retrieving Application Form Data for ID:" + this.uploadmodel.uuid);
      //           }
      //       },
      //       error => {
      //           this.onShowError(error);
      //       }
      //       );
      //  }

    ngOnChanges(changes: SimpleChanges) {
        for (let propName in changes) {
          if (propName === "uploadmodel") {
            this.uploadmodel = changes[propName].currentValue;
            // if(!isNil(this.uploadmodel)){
            //     if(this.uploadmodel.type === 'HTC'){
            //         this.loadHTCApplicationInfo();
            //     }
            // }
          }
        }
      }

    reset() {
        jQuery('#uploadDocForm').parsley().reset();
        jQuery('#upload-doc').modal('hide');
    }

    /**
     *
     *
     * @param {DocToUpload} doc
     * @memberof UploadDocumentComponent
     */
    public downloadFile(doc:DocToUpload, type: string){
        // let formId: string = "";
        // switch(type){
        //     case 'HTC':
        //         formId = this.tplmodel.tpnumber;
        //         break;
        //     default:
        //         break;
        // }

        // if(doc.status === 'Y'){
        //   console.log("FORMID:", formId);
        //   this.documentKey = formId + "/uploads/" + doc.fileName;
        //   let handler = this;
        //   this.auth.download(this.documentKey, {
        //     level: 'public'
        //   })
        //   .subscribe(
        //     data => {
        //       if (!isNil(data) && !isNil(data.errorMessage)) {
        //         this.onShowBodyError(data);
        //       } else {
        //         var xhr = new XMLHttpRequest();
        //         xhr.open("GET", data);
        //         xhr.responseType = "blob";

        //         xhr.onload = function () {
        //             saveAs(this.response, doc.fileName);
        //         };
        //         xhr.send();
        //       }
        //     },
        //     error => {
        //       this.onShowError(error);
        //     }
        //   );

        // }
    }

    /**
     *
     *
     * @param {DocToUpload} selDoc
     * @memberof UploadDocumentComponent
     */
    public updateDocumentUploadStatus(selDoc:DocToUpload){
        if(this.uploadmodel.type === 'HTC'){
            this.tplformService.updateDocumentUploadStatus(this.tplmodel).subscribe(data => {
                this.onShowBodyError(data);
            }, error => {
                this.onShowError(error);
            });
        }
    }

    // reset flag and empty title and message
    public onCloseInfoPopup(){
        this.msgDisplay = "";
        this.titleDisplay = "";
        jQuery('#info-popup').modal('hide');
    }

    // Display upload success message
    protected displayUploadStatusMessage(title:string, msg:string){
        this.msgDisplay = msg;
        this.titleDisplay = title;
        jQuery('#info-popup').modal('show');
    }

    /**
     *
     *
     * @param {*} objKey
     * @param {*} index
     * @param {*} docName
     * @memberof UploadDocumentComponent
     */
    protected uploadFileToPrivateFolder(objKey, index, docName){
        // let handler = this;

        // this.auth.upload(objKey, this.filesToUpload[index], {
        //     level: 'public',
        //     contentType: this.filesToUpload[index].type
        // })
        // .subscribe(
        //     data => {
        //         if (!isNil(data) && !isNil(data.errorMessage)) {
        //             handler.displayUploadStatusMessage("Upload Status", "Failed to Upload Document!" );
        //         } else {
        //             if(handler.uploadmodel.type === 'HTC') {
        //                 handler.displayUploadStatusMessage("Upload Status", "Document Uploaded Successfully!" );
        //                 let rowIndex = _.findIndex(this.tplmodel.documents, function (o) { return (o.name && o.name.toString() === docName)});
        //                 if(rowIndex !== -1) {
        //                     let selDoc = this.tplmodel.documents[rowIndex];
        //                     selDoc.fileName = this.filesToUpload[index].name;
        //                     selDoc.status = 'Y';
        //                     handler.updateDocumentUploadStatus(selDoc);
        //                 }
        //             }
        //         }
        //     },
        //     error => {
        //         handler.displayUploadStatusMessage("Upload Status", "Failed to Upload Document!" );
        //     }
        // );
    }

    /**
     *
     *
     * @param {*} event
     * @param {*} index
     * @memberof UploadDocumentComponent
     */
    public fileChangeEvent(event, index) {
        console.log("SELECT:", this.filesToUpload);
        this.filesToUpload[index] = (<Array<File>>event.target.files)[0];
        console.log("SELECT:", (<Array<File>>event.target.files)[0]);
    }

    /**
     *
     *
     * @param {*} index
     * @param {*} docName
     * @memberof UploadDocumentComponent
     */
    public uploadFile(index, docName){
        // let handler = this;
        // if (!isNil(this.filesToUpload[index]) && this.filesToUpload[index].name.length > 0) {
        //     this.auth.getAuthenticatedUser()
        //     .subscribe(
        //         data => {
        //             if (!isNil(data)) {
        //                 if(handler.uploadmodel.type === 'HTC'){
        //                     handler.documentKey = this.tplmodel.profileid + "/uploads/" + this.tplmodel.tpnumber
        //                         + "/" + this.filesToUpload[index].name;
        //                     handler.uploadFileToPrivateFolder(handler.documentKey, index, docName);
        //                 }
        //                 if(!isNil(data.errorMessage)){
        //                     this.onShowBodyError(data);
        //                 }
        //             }
        //         },
        //         error => {
        //             this.onShowError(error);
        //         }
        //     );
        // }
    }
}
