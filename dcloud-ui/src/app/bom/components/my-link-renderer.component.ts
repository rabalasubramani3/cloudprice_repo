import { Component } from '@angular/core';
import { AgRendererComponent, ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  template: `<i class="fa fa-edit" (click)="invokeParentMethod()"></i>`
})
export class MyLinkRendererComponent implements ICellRendererAngularComp {
  params: any;

  agInit(params: any): void {
      this.params = params;
  }

  public invokeParentMethod() {
    this.params.context.componentParent.viewProduct(this.params.data);
  }

  refresh(): boolean {
      return false;
  }
}
