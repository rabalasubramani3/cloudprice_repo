import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { LocalStorageService } from 'ngx-webstorage';
import { isNil } from 'ramda';
import { Subscription } from 'rxjs';
import { OnComponentErrorHandler } from '../../core/coreerror.handler';
import { ShareDataService } from '../../core/sharedata.service';
import { BOMSheet } from '../../forms/models/bom/bom-sheet.model';
import { fetchprodquery } from '../../forms/models/fetchprodquery.model';
import { AuthService } from '../../login/services/auth.service';
import { ProfileService } from '../../login/services/profile.service';
import { ApplicationUploadInfo } from '../models/app-upload.model';
import { ApplicantSearchCriteria } from '../models/applicant-search-criteria.model';
import { UserSaveFilter } from '../models/user-save-filter.model';
import { ApplicantService } from '../services/applicant.service';
import { ProductService } from '../services/product.service';
import { ApplicantMessageBox } from './applicant-msgbox.component';
import { SearchApplicantPricingResultsComponent } from './search-app-pricing-results.component';

declare var jQuery: any;
export enum SearchPageMode {
    DAILY_OPERATIONS = 1,
    NAVIGATED_BACK,
    NEW_SEARCH
}

export enum LandingPageMode {
    APPLICANT_VIEW = 1,
    PROCESSOR_VIEW = 2,
}

@Component({
    // tslint:disable-next-line: component-selector
    selector: '[search-pricing]',
    templateUrl: './search-pricing.template.html',
    styleUrls: ['./search-pricing.style.scss'],
    providers: [ ApplicantService, ShareDataService, BsModalService, ProductService ],
    encapsulation: ViewEncapsulation.None
})

export class SearchPricingPageComponent implements OnInit, OnDestroy, OnComponentErrorHandler, AfterViewInit {
    public delfiltertitle: string = "Delete Filter";
    public delfiltermessage: string =
      "Are you sure you want to delete this filter?";
    public route: ActivatedRoute;
    public dbRecordsCount: number = 0;
    public showLoadingProgressBar: boolean = false;
    public router: Router;
    public userFilters: UserSaveFilter[] = [];
    public editUserFilter: UserSaveFilter;
    public selFilterName: string = "";
    public limitRows: string = "1000";
    public uploadappmodel: ApplicationUploadInfo = new ApplicationUploadInfo();
    public uploadmethod: number;
    public applicantData: any[];
    public keywordSearch: string;
    public resmodel: ApplicantSearchCriteria = new ApplicantSearchCriteria();
    public tplproducts: BOMSheet[]=[];
    public busyLoadBOMs: Subscription;
    public busyLoadTPLProductsByKeywords: Subscription;
    public busyResetTPLProducts: Subscription;
    public busyGetDBRecordsCount: Subscription;
    public busySendEmail: Subscription;
    public emailSubjectRef: string;
    public showSendEmailFlag: boolean;
    public emailalerts: Array<Object>;
    public emailsender: string;
    public emailsendercc: string;
    public emailsubject: string;
    public emailbody: string;
    public loggedUserName: string;
    public loggedUserAddress: string;
    public loggedUserPhoneNumber: string;
    public loggedUserEmail: string;
    public showReportFilter: boolean;
    public showErrorFlag: boolean;
    public alerts: Array<Object>;

    @ViewChild(SearchApplicantPricingResultsComponent, {static: true}) _grid: SearchApplicantPricingResultsComponent;
    bsModalRef: BsModalRef;

    constructor(private applicantService: ApplicantService,
                private storage: LocalStorageService,
                private profile: ProfileService,
                private _route: ActivatedRoute,
                private auth: AuthService,
                private shareData: ShareDataService,
                private modalService: BsModalService,
                private productService: ProductService,
                router: Router,
                ) {
        this.route = _route;
        this.router = router;
        this.showErrorFlag = false;
        this.uploadmethod = 1; // 1 - amplify.s3    2 - http api method
        // this.loadSampleData();
    }

    ngOnInit(): void {

        this.showErrorFlag = false;

        this.showSendEmailFlag = false;
        this.emailalerts = [{ type: 'success', msg: '' }];
        this.emailsender = '';

    }

    public cosmeticFix(elementID: any) {
      // The code below for cosmetic fix - Blue box now surrounds the icon on the search tdl text box
      let searchInput = jQuery(elementID);
      searchInput
        .focus(e => {
          jQuery(e.target)
            .closest(".input-group")
            .addClass("focus");
        })
        .focusout(e => {
          jQuery(e.target)
            .closest(".input-group")
            .removeClass("focus");
        });
    }

    ngAfterViewInit() {
      this.cosmeticFix("#keyword-search-input");
      this.fetchApplicantSearchResults();
      jQuery("#search-widget").widgster("collapse");
    }

    ngOnDestroy() {
        if (this.busyLoadBOMs) {
            this.busyLoadBOMs.unsubscribe();
        }
        if(this.busyLoadTPLProductsByKeywords){
          this.busyLoadTPLProductsByKeywords.unsubscribe();
        }
        if (this.busySendEmail) {
            this.busySendEmail.unsubscribe();
          }
        if(this.busyResetTPLProducts){
          this.busyResetTPLProducts.unsubscribe();
        }
        if(this.busyGetDBRecordsCount){
          this.busyGetDBRecordsCount.unsubscribe();
        }
    }

    executeSearch() {
        this.fetchSearchResults();
    }

    public onClickSearch(ks: string){
      this.loadProductListForCustomerBySearchKeyword(ks);
    }

    // refreshTPLSearch(rfs: RefreshSearch) {
    //     if (!isNil(rfs.year)) {
    //         this.loadProductListForReviewer();
    //     }
    // }
    protected sv(val) {
        if (isNil(val)) {
            return '-';
        }
        return val;
    }

    // public getUserProfile() {
    //     const handler = this;
    //     this.auth.getAuthenticatedUser().subscribe(user => {
    //         const localUser: RegistrationUser = new RegistrationUser();
    //         localUser.populate(user);
    //         handler.profile.getUserProfileData(localUser.username).subscribe( profile => {
    //             if (!isNil(profile)) {
    //                 if (profile.length > 0) {
    //                     handler.loggedUserName = (isNil(profile[0].Prefix) ? '' : this.sv(profile[0].Prefix) + ' ') +
    //                         this.sv(profile[0].FirstName) + ' ' + this.sv(profile[0].LastName) + (isNil(profile[0].Suffix) ? '' : ' ' + this.sv(profile[0].Suffix)) ;
    //                     const addressArray = [];
    //                     if (this.sv(profile[0].Address1) !== 'undefined' ) {
    //                         addressArray.push(this.sv(profile[0].Address1));
    //                     }
    //                     if (this.sv(profile[0].Address2) !== 'undefined' && this.sv(profile[0].Address2).length > 0) {
    //                         addressArray.push(this.sv(profile[0].Address2));
    //                     }
    //                     if (this.sv(profile[0].City) !== 'undefined') {
    //                         addressArray.push(this.sv(profile[0].City));
    //                     }
    //                     if (this.sv(profile[0].State) !== 'undefined') {
    //                         addressArray.push(this.sv(profile[0].State));
    //                     }
    //                     if (this.sv(profile[0].Zip1) !== 'undefined') {
    //                         addressArray.push(this.sv(profile[0].Zip1));
    //                     }
    //                     handler.loggedUserAddress =   addressArray.join(', ') +  ((this.sv(profile[0].Zip2) === 'unde') ? '' : '-' + this.sv(profile[0].Zip2)) ;
    //                     handler.loggedUserPhoneNumber = this.sv(profile[0].PhoneNumber);
    //                     handler.loggedUserEmail = this.sv(profile[0].EmailAddress);
    //                 }
    //             }
    //         }, error => {
    //         });
    //       });
    // }

    public clearResourcePreferencesFromLocalStorage() {
        this.storage.clear('searchApplicantResultsBy');
        this.storage.clear('sortApplicantResultsOrder');
        this.storage.clear('sortApplicantResultsBy');
    }

    /**
     * For Processor - show all tpl applications for current year
     */
    // public loadProductListForReviewer() {
    //     let that = this;

    //     this.busyLoadBOMs = this.applicantService.getTPLProcessorListByYear().subscribe(data => {
    //         if (!isNil(data)) {
    //             this.onShowBodyError(data);
    //             if (!isNil(data.errorMessage)) {
    //               that.onShowError('Error Loading TPL Application List - Info:' + data.errorMessage);
    //             } else if (!isNil(data.message)) {
    //                 if (data.message === 'no records found') {
    //                   that.tplproducts = [];
    //                   // that.loadSampleData();
    //                 }
    //             } else {
    //               that.tplproducts = data;
    //             }
    //         }
    //     },
    //     error => {
    //       that.onShowError(error);
    //     });
    // }

    /**
     * For Applicant - show tpl applications that match by email address
     */
    public loadPricingSheetList() {

        const that = this;
        // const profileid: string = this.storage.retrieve('LOGGEDUSERID');

        // let pq: fetchprodquery = new fetchprodquery();
        // pq.search_text = "";
        // pq.rec_count = this.limitRows;
        // pq.start_rec = "0";
        // pq.estb_id = "";

        this.busyLoadBOMs = this.applicantService.getAllBOMs().subscribe(data => {
            if (!isNil(data)) {
                that.onShowBodyError(data);
                that.tplproducts = data.boms;
                // that.getDBRecordsCount();
            }else {
              that.tplproducts = [];
              // that.getDBRecordsCount();
            }
        },
        error => {
            that.onShowError(error);
        });
    }

    // public getDBRecordsCount(){
    //   let that = this;
    //   this.busyGetDBRecordsCount = this.productService.getAllProductsCount().subscribe(data => {
    //       if (!isNil(data)) {
    //         that.dbRecordsCount =  data.tot_rec_count;
    //       }else {
    //         that.dbRecordsCount =  0;
    //       }
    //     },
    //     error => {
    //       that.onShowError(error);
    //   });
    // }

    public loadProductListForCustomerBySearchKeyword(skw: string) {

      const that = this;
      const profileid: string = this.storage.retrieve('LOGGEDUSERID');

      let pq: fetchprodquery = new fetchprodquery();
      pq.search_text = isNil(skw) ? "": skw;
      pq.rec_count = this.limitRows;
      pq.start_rec = "0";
      pq.estb_id = "";

      this.tplproducts = [];

      this.showLoadingProgressBar = true;
      // this.busyLoadBOMs = this.productService.getAllProductsBySearchKeyword(pq).subscribe(data => {
      //     if (!isNil(data)) {
      //         that.onShowBodyError(data);
      //         that.tplproducts = data;
      //         that.showLoadingProgressBar = false;
      //         that.getDBRecordsCount();
      //     }else {
      //       that.tplproducts = [];
      //       that.showLoadingProgressBar = false;
      //     }
      // },
      // error => {
      //     that.onShowError(error);
      //     that.showLoadingProgressBar = false;
      // });
  }

    onResetError(): void {
        this.showErrorFlag = false;
        this.alerts = [];
      }

    public onShowError(error: any): void {
        this.showErrorFlag = true;
        this.alerts = [];
        this.alerts.push({type: 'warning', msg: error});
    }

    public onShowBodyError(body: any): void {
        if (!isNil(body.errorMessage)) {
            this.showErrorFlag = true;
            this.alerts = [];
            this.alerts.push({type: 'warning', msg: body.errorMessage});
        }
    }
    public fetchApplicantSearchResults() {
      // this.getUserProfile();
      this.loadPricingSheetList();
    }

    public fetchSearchResults() {

        this.showErrorFlag = false;

        // reset the table first
        this.applicantData = null;

        this.clearResourcePreferencesFromLocalStorage();
        this.fetchApplicantSearchResults();
    }

    public showUploadDocument(uploadappinfo: ApplicationUploadInfo) {
        this.uploadappmodel = uploadappinfo;
        jQuery('#upload-doc').modal('show');
    }

    public hideUploadDocPopup(event) {
        jQuery('#upload-doc').modal('hide');
    }

    private onShowEmailError(body) {
        this.showSendEmailFlag = true;
        this.emailalerts = [];
        this.emailalerts.push({ type: 'warning', msg: 'Error: ' + body.errorMessage + this.getTimeStamp() });
    }

    private onShowEmailSuccessMessage(body) {
        if (body.result === 'Success.') {
            this.showSendEmailFlag = true;
            this.emailalerts = [];
            this.emailalerts.push({ type: 'info', msg: 'Success: Email Sent' + this.getTimeStamp() });
        }
    }

    public showEmail($email, $event) {
        // jQuery('#compose-email-modal').show();
        this.emailsubject = 'Application Reference #: ' + $event;
        $email.show();
    }
    public closeComposeEmail(composemailModal) {
        this.discardEmail();
        composemailModal.hide();
    }

    public discardEmail() {
        this.showSendEmailFlag = false;
        // this.emailsender = null;
        // this.emailsendercc = null;
        // this.emailsubject = null;
        this.emailbody = null;
        this.emailalerts = [];
      }

      private getTimeStamp() {
        return ' (timestamp: ' + new Date().toLocaleTimeString() + ')';
      }

      public showSuccess() {
          jQuery('#emailSuccess').show();
      }

      public sendComposedEmail() {}
      //   jQuery('#email-compose').parsley().validate();
      //   if (jQuery('#email-compose').parsley().isValid()) {
      //     this.auth.getAuthenticatedUser().subscribe(
      //       data => {
      //         if (!isNil(data) && !isNil(data.attributes.email)) {
      //           const emailInfo = new ContactEmailInfo();
      //           if (this.isProcessorMode()) {
      //               emailInfo.From = '';
      //           } else {
      //               emailInfo.From = '';
      //           }
      //           emailInfo.To = this.emailsender;
      //           if (!isNil(this.loggedUserEmail) && this.loggedUserEmail.length > 0) {
      //             emailInfo.CC = this.loggedUserEmail;
      //           }
      //           emailInfo.Subject = this.emailsubject;
      //           emailInfo.Message = this.emailbody;
      //           this.busySendEmail = this.applicantService.sendComposedEmail(emailInfo).subscribe(
      //             data => {
      //               this.showSendEmailFlag = false;
      //               this.emailalerts = [];
      //               if (!isNil(data)) {
      //                 if (!isNil(data.errorMessage)) {
      //                   this.onShowEmailError(data);
      //                 } else {
      //                   jQuery('#compose-email-modal').hide();
      //                   this.showSuccess();
      //                   this.discardEmail();
      //                   // let msg = JSON.parse(data);
      //                   // this.onShowEmailSuccessMessage(msg + ', We will contact you within 2 business days.');
      //                 }
      //               }
      //             },
      //             error => {
      //               this.onShowEmailError(error);
      //             }
      //           );
      //         }
      //       },
      //       error => {
      //         this.onShowEmailError(error);
      //       }
      //     );
      //   }
      // }

      public isHTCApplicantVisible() {
        if (!isNil(this.tplproducts) && (this.tplproducts.length > 0)) {
            return true;
        }
        return false;
    }
    public displayConfirmationMessage(selTitle: string, selMessage: string) {
        const initialState = {
          message: selMessage,
          title: selTitle
        };
        this.bsModalRef = this.modalService.show(ApplicantMessageBox, { initialState });
        this.bsModalRef.content.closeBtnName = 'Close';
      }

      public AddNewProduct() {
        this.router.navigate(['/app/forms/tplcreate']);
      }

      public AddProductFromFile(prodid: number) {
        this.router.navigate(['/app/forms/tpledit']);
      }

    public onEditApplicationProfile() {
        this.router.navigate(['/app/forms/profile']);
    }

    public isProcessorMode() {
        return this.auth.isProcessorMode();
    }

    // public ResetAllProducts(){
    //   let that = this;
    //   this.busyResetTPLProducts = this.productService.resetAllProducts().subscribe( data => {
    //       that.loadProductListForCustomer();
    //     },
    //     error => {
    //       this.onShowEmailError(error);
    //     }
    //   );
    // }

    public refreshPricingList(flag: boolean){
      this.loadPricingSheetList();
    }

    public isValidFilterSelected() {
      if (!isNil(this.selFilterName) && this.selFilterName.length > 0) {
          return true;
      }
      return false;
    }

    public changeFilterName(){

    }

    public setEditMode(mode: boolean){

    }

    public clearFilter(){
    }

    public refreshUserQuerySearch(event){
    }

    public refreshFilter(event){

    }

  public setUploadMethod(upm: number){
    this.uploadmethod = upm;
  }

    public ExportAllProductstoExcel(){
      this._grid.ExportAllProductstoExcel();
    }

    public bulkUpload(){
      this.router.navigate(['/app/forms/bulkprodupload']);
    }

    public createNewBOM(){
      this.router.navigate(['/app/forms']);
    }

  }
