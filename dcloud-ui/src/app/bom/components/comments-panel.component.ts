import { ApplicantService } from '../services/applicant.service';
import {
  Component,
  Input,
  OnInit,
  ViewEncapsulation,
  SimpleChanges,
  Output,
  EventEmitter,
  OnDestroy
} from "@angular/core";
import { Subscription } from "rxjs";

declare var jQuery: any;

@Component({
  selector: "[comments-panel]",
  templateUrl: "./comments-panel.template.html",
  styles: [String(require("./comments-panel.style.scss"))],
  encapsulation: ViewEncapsulation.None
})
export class CommentsPanelComponent implements OnInit, OnDestroy {
  @Input() objectId;
  @Input() comment;
  @Input() objectType;
  @Input() contextInfo;

  @Output() onCommentEdit = new EventEmitter<boolean>();

  showCommentsErrorFlag: boolean;
  alertsComments: Array<Object>;
  busy: Subscription;
  context: any;

  constructor(private applicantService: ApplicantService) {
    this.alertsComments = [
      {
        type: "warning",
        msg: '<span class="fw-semi-bold">Warning:</span> Error: Saving Comments'
      }
    ];
    this.showCommentsErrorFlag = false;
  }

  ngOnChanges(changes: SimpleChanges) {
    // tslint:disable-next-line:forin
    for (let propName in changes) {
      if (propName === "comment") {
        let chng = changes[propName];
        if (chng.currentValue) {
          this.comment = chng.currentValue;
          jQuery(".summernote")
            .eq(0)
            .summernote("code", this.comment);
        }
      }
      if (propName === "contextInfo") {
        let chng = changes[propName];
        if (chng.currentValue) {
          this.context = chng.currentValue;
        }
      }
    }
  }

  ngOnInit() {
    jQuery(".summernote").summernote({
      height: 250, // set editor height
      minHeight: null, // set minimum height of editor
      maxHeight: null, // set maximum height of editor
      focus: true, // set focus to editable area after initializing summernote
      callbacks: {
        onInit: function() {
          jQuery("body > .note-popover").hide();
        }
      },
      dialogsInBody: true,
      popover: {
        image: [],
        link: [],
        air: []
      },
      toolbar: [
        // [groupName, [list of button]]
        ["style", ["bold", "italic", "underline"]],
        ["fontsize", ["fontsize"]],
        ["color", ["color"]],
        ["para", ["ul", "ol", "paragraph"]],
        ["height", ["height"]]
      ]
    });
  }

  ngOnDestroy() {
    if (this.busy) {
      this.busy.unsubscribe();
    }
    jQuery(".summernote").summernote("destroy");
  }

  updateComment() {
    this.comment = jQuery(".summernote")
      .eq(0)
      .summernote("code");
    if (this.comment != null) {
      // this.busy = this.commentsService
      //   .updateComment(this.objectId, this.comment, this.objectType)
      //   .subscribe(
      //     data => {
      //       if (data) {
      //         this.comment = data;
      //         jQuery(".summernote")
      //           .eq(0)
      //           .summernote("code", this.comment);
      //         this.onCommentEdit.emit(true);
      //       }
      //     },
      //     error => {
      //       this.showCommentsErrorFlag = true;
      //       this.alertsComments = [];
      //       this.alertsComments.push({ type: "warning", msg: error });
      //     }
      //   );
    }
  }
}
