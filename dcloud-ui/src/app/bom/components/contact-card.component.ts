import { isNil } from 'ramda';
import { ContactCardInfo } from "./../models/contact-card.model";
import {
  OnInit,
  OnDestroy,
  Component,
  ViewEncapsulation,
  Input
} from "@angular/core";
import * as moment from 'moment';

@Component({
  selector: "[contact-card]",
  templateUrl: "./contact-card.template.html",
  providers: [],
  encapsulation: ViewEncapsulation.None
})
export class ContactCardComponent implements OnInit, OnDestroy {
  @Input()
  contactmodel: ContactCardInfo;

  ngOnInit(): void {}

  ngOnDestroy(): void {}

  public dv(val: string) {
    if (isNil(val)) {
      return "-";
    }
    return moment(val)
      .format("MMM DD, YYYY");
  }
}
