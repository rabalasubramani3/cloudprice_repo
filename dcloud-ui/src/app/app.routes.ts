import { HomeComponent } from './home.component';
import { AuthGuard } from './login/services/auth-guard.service';
import { Routes } from '@angular/router';
import { ErrorComponent } from './error/error.component';

  // {
  //  path: '', redirectTo: 'home', pathMatch: 'full'
  // },

export const ROUTES: Routes = [

  {
    path: '',    loadChildren: './login/login.module#LoginModule'
  },
  {
    path: 'app',   loadChildren: './layout/layout.module#LayoutModule'
  },
  {
    path: 'login', loadChildren: './login/login.module#LoginModule'
  },
  {
    path: 'error', component: ErrorComponent
  },
  {
    path: '**',    component: ErrorComponent
  }
];
