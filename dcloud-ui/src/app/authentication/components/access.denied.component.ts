import { Component } from '@angular/core';
import { Router }      from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'login',
  templateUrl: 'access.denied.html'
})

export class AccessDeniedComponent {
    constructor() {}
}
