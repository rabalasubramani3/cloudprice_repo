"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var JwtHelper = (function () {
    function JwtHelper() {
    }
    JwtHelper.prototype.createJWTAuthorizationHeader = function (config) {
        var token = this.getToken();
        if (token) {
            config.headers.Authorization = 'Bearer ' + token;
        }
        return config;
    };
    JwtHelper.prototype.parseJwt = function (token) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(atob(base64));
    };
    JwtHelper.prototype.saveToken = function (token) {
        localStorage['jwtToken'] = token;
    };
    JwtHelper.prototype.getToken = function () {
        return localStorage['jwtToken'];
    };
    JwtHelper.prototype.removeToken = function () {
        localStorage.removeItem('jwtToken');
    };
    JwtHelper = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], JwtHelper);
    return JwtHelper;
}());
exports.JwtHelper = JwtHelper;
//# sourceMappingURL=jwthelper.util.js.map