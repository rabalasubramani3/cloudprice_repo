
import {throwError as observableThrowError,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Http, Headers, ResponseContentType, RequestOptions } from '@angular/http';
import { AuthService } from '../../login/services/auth.service';
import { AuthorizationHelper } from './authorization.helper.util';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';
// Statics

// Operators







@Injectable()
export class HttpClientX {

  constructor(
      private _http: HttpClient,
      private authService: AuthService,
      private router: Router
   ) {}

//   get(url) {
//     let token = this.authService.getAccessToken();
//     const headers = new HttpHeaders()
//         .append('Content-Type' , 'application/json')
//         .append('X-Requested-With', 'XMLHttpRequest')
//         .append('Cache-control', 'no-cache')
//         .append('Cache-control', 'no-store')
//         .append('Pragma', 'no-cache')
//         .append('Expires', '0')
//         .append('X-Authorization', 'Bearer ' + token);
//     return this._http.get(url, { headers: headers })
//         .pipe(
//             catchError(this.handleError('getError', result))
//         );
//         //   .catch(result => this.handleError(this, result));
//   }

//   getPromise(url){
//     let token = this.authService.getAccessToken();
//     const headers = new HttpHeaders()
//         .append('Content-Type' , 'application/json')
//         .append('X-Requested-With', 'XMLHttpRequest')
//         .append('Cache-control', 'no-cache')
//         .append('Cache-control', 'no-store')
//         .append('Pragma', 'no-cache')
//         .append('Expires', '0')
//         .append('X-Authorization', 'Bearer ' + token);    
//     return this._http.get(url, { headers: headers }).toPromise();    
//   }

//   getBlob(url) {
//     let token = this.authService.getAccessToken();
//     const headers = new HttpHeaders()
//         .append('Content-Type' , 'application/json')
//         .append('X-Requested-With', 'XMLHttpRequest')
//         .append('Cache-control', 'no-cache')
//         .append('Cache-control', 'no-store')
//         .append('Pragma', 'no-cache')
//         .append('Expires', '0')
//         .append('X-Authorization', 'Bearer ' + token);
//     return this._http.get(url, { headers: headers, responseType: 'arraybuffer' })
//           .catch(result => this.handleError(this, result));
//     }

//   post(url, body) {
//     let token = this.authService.getAccessToken();
//     const headers = new HttpHeaders()
//         .append('Content-Type' , 'application/json')
//         .append('X-Requested-With', 'XMLHttpRequest')
//         .append('Cache-control', 'no-cache')
//         .append('Cache-control', 'no-store')
//         .append('Pragma', 'no-cache')
//         .append('Expires', '0')
//         .append('X-Authorization', 'Bearer ' + token);
//     return this._http.post(url, body, { headers: headers })
//         .catch(result => this.handleError(this, result));
//   }

//   put(url, body) {
//     let token = this.authService.getAccessToken();
//     const headers = new HttpHeaders()
//         .append('Content-Type' , 'application/json')
//         .append('X-Requested-With', 'XMLHttpRequest')
//         .append('Cache-control', 'no-cache')
//         .append('Cache-control', 'no-store')
//         .append('Pragma', 'no-cache')
//         .append('Expires', '0')
//         .append('X-Authorization', 'Bearer ' + token);
//     return this._http.put(url, body, { headers: headers })
//         .catch(result => this.handleError(this, result));
//   }

//   getFile(url): Observable<Object[]> {
//         return Observable.create(observer => {
//             let xhr = new XMLHttpRequest();
//             xhr.open('GET', url, true);
//             xhr.setRequestHeader('Content-type', 'application/json');
//             let token = this.authService.getAccessToken();
//             xhr.setRequestHeader('X-Authorization', 'Bearer ' + token);
//             xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');            
//             xhr.responseType='arraybuffer';

//             xhr.onreadystatechange = function () {
//                 if (xhr.readyState === 4) {
//                     if (xhr.status === 200) {
//                         var contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
//                         var blob = new Blob([xhr.response], { type: contentType });
//                         observer.next(blob);
//                         observer.complete();
//                     } else {
//                         observer.error(xhr.response);
//                     }
//                 }
//             }
//             xhr.send();

//         });
//     } 

//     makeFileRequest(httpMethod: string, url: string, formData: FormData) {
//         return new Promise((resolve, reject) => {

//             let xhr = new XMLHttpRequest();
//             xhr.open("POST", url, true);

//             xhr.onreadystatechange = function () {
//                 if (xhr.readyState === 4) {
//                     if (xhr.status === 200) {
//                         resolve(xhr.response);
//                     } else {
//                         reject(xhr.response);
//                     }
//                 }
//             }

//             // Append JWT token


//             let token = this.authService.getAccessToken();
//             xhr.setRequestHeader('X-Authorization', 'Bearer ' + token);
//             xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
//             xhr.send(formData);
//         });
//     }

//   delete(url) {
//     let token = this.authService.getAccessToken();
//     const headers = new HttpHeaders()
//         .append('Content-Type' , 'application/json')
//         .append('X-Requested-With', 'XMLHttpRequest')
//         .append('Cache-control', 'no-cache')
//         .append('Cache-control', 'no-store')
//         .append('Pragma', 'no-cache')
//         .append('Expires', '0')
//         .append('X-Authorization', 'Bearer ' + token);
//     return this._http.delete(url, { headers: headers })
//     .pipe(
//         catchError(this.handleError('deleteError'))
//     );
//   }

//   private handleError (_httpClient: any, error: any) {
//     let errStatus = error.status;
//     if(errStatus === 401) {
//         _httpClient.router.navigate(['login']);
//     }
//     let errMsg = error.json().ex;
//     console.log(errMsg); // log to console instead
//     return observableThrowError(errMsg);
//   }

}
