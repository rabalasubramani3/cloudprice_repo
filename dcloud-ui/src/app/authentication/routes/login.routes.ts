
import { environment } from '../../../environments/environment';
import { LoginComponent } from '../../login/components/login.component';
import { RegisterComponent } from '../../login/components/register/registration.component';

export const loginRoutes = [
    { path: '', component: LoginComponent, children: [
        { path: 'register', component: RegisterComponent}
    ]}    
];
