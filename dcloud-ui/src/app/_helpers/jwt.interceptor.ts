import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../login/services/auth.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    sessionToken:string;
    constructor(public auth: AuthService) {
    }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.auth.session().subscribe( 
            result => {
                this.sessionToken = result.idToken.jwtToken;
                let headers = request.headers
                    .set('Content-Type', 'application/json')
                    .set('X-Requested-With', 'XMLHttpRequest')                    
                    .set('Cache-control', 'no-cache')                                        
                    .set('Cache-control', 'no-store')                                                            
                    .set('Pragma', 'no-cache')                                                                                
                    .set('Expires', '0')                                                                                                    
                    .set('Authorization', `Bearer ${this.sessionToken}`);

                request = request.clone({ headers });    
                return next.handle(request);
            });
        return next.handle(request);
    }
}