import { AuthService } from './../../login/services/auth.service';
import { Component, OnInit, ElementRef, Output } from '@angular/core';
import { AppConfig } from '../../app.config';
import { Router } from '@angular/router';
declare let jQuery: any;

@Component({
  selector: 'app-navbar',
  styleUrls: [ './navbar.style.scss' ],
  templateUrl: './navbar.template.html'
})
export class NavbarComponent implements OnInit {
  $el: any;
  config: any;

  constructor(el: ElementRef, private router: Router, config: AppConfig,
    private auth:AuthService) {
    this.$el = jQuery(el.nativeElement);
    this.config = config.getConfig();
  }

  ngOnInit(): void {
  }

  public setNavItemActive(settings){

  }
  public logout() {
    this.auth.signOut();
    this.router.navigate(['/login']);
  }
}
