import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
  { path: '', component: LayoutComponent, children: [
    { path: 'dashboard', loadChildren: '../dashboard/dashboard.module#DashboardModule'},
    { path: 'forms', loadChildren: '../forms/forms.module#BOMFormsModule'},
    { path: 'bom', loadChildren: '../bom/components/bom.module#BOMModule'},
    { path: 'admin', loadChildren: '../admin/components/admin.module#AdminModule'}
  ]}
];

export const ROUTES = RouterModule.forChild(routes);
