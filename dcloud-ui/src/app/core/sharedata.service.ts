import { Injectable, Optional  } from '@angular/core';

export class ShareDataServiceConfig {
  userName = 'Philip Marlowe';
}

let nextId = 1;

export enum AppRunMode {
    APPLICANT_VIEW = 1,
    PROCESSOR_VIEW = 2,
};


@Injectable()
export class ShareDataService {
    id = nextId++;

    assessmentId: number;
    idArray: number[];

    previousRoute: string;
    breadcrumb: string;
    acceptanceFlag: string;

    qatabfrom: string;

    submitted: string;
    createdDate: string;

    phase: string;

    runmode: AppRunMode;

    private _userName = '';

    constructor(@Optional() config: ShareDataServiceConfig) {
        if (config) {
            this._userName = config.userName;
        }
        this.reset();

    }

    get userName() {
        // Demo: add a suffix if this service has been created more than once
        const suffix = this.id > 1 ? ` times ${this.id}` : '';
        return this._userName + suffix;
    }

    reset() {
        this.acceptanceFlag = '';

        this.assessmentId = -1;

        this.previousRoute = '';
        this.breadcrumb = '';
        this.qatabfrom = '';

        this.submitted = '';
        this.createdDate = '';
        // this.idArray = [];
    }
}
