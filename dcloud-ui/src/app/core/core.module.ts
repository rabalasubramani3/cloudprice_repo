
/* tslint:disable:member-ordering no-unused-variable */
import {
  ModuleWithProviders, NgModule,
  Optional, SkipSelf }       from '@angular/core';

import { CommonModule }      from '@angular/common';

import { ShareDataService }       from './sharedata.service';
import { ShareDataServiceConfig } from './sharedata.service';

@NgModule({
  imports:      [ CommonModule ],
  providers:    [ ShareDataService ]
})
export class CoreModule {

  constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(config: ShareDataServiceConfig): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        {provide: ShareDataServiceConfig, useValue: config }
      ]
    };
  }
}

