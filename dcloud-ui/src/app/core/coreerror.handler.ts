export interface OnComponentErrorHandler {
    onResetError(): void;
    onShowError(error): void;
    onShowBodyError(error:any): void;
}