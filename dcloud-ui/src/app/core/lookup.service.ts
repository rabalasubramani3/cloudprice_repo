import { isNil } from 'ramda';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import * as _ from 'lodash';

@Injectable()
export class LookupService {
    constructor() { }

    static residenceTypes = [
        {value:"", name: ""},    
        {value: "1", name: "Apartment Building"},
        {value: "2", name: "Single Family Home"},
        {value: "3", name: "Mobile Home"},
        {value: "4", name: "Other"},
      ];

    static CountyNames = [
        {value:"", name: ""},    
        {value: "01", name: "Allegany"},
        {value: "02", name: "Anne Arundel"},
        {value: "03", name: "Baltimore City"},
        {value: "04", name: "Baltimore County"},
        {value: "05", name: "Calvert"},
        
        {value: "06", name: "Caroline"},
        {value: "07", name: "Carroll"},
        {value: "08", name: "Cecil"},
        {value: "09", name: "Charles"},
        {value: "10", name: "Dorchester"},
        
        {value: "11", name: "Frederick"},
        {value: "12", name: "Garrett"},
        {value: "13", name: "Harford"},
        {value: "14", name: "Howard"},
        {value: "15", name: "Kent"},
        
        {value: "16", name: "Montgomery"},
        {value: "17", name: "Prince George's"},
        {value: "18", name: "Queen Anne's"},
        {value: "19", name: "St. Mary's"},
        {value: "20", name: "Somerset"},
        
        {value: "21", name: "Talbot"},
        {value: "22", name: "Washington"},    
        {value: "23", name: "Wicomico"},
        {value: "24", name: "Worcester"},        
    ];
    
    static getCountyNameLookup(countyNum:any){
        let selCounty: string;
        if(!isNil(countyNum)){
            selCounty = countyNum.toString();
            let rowIndex = _.findIndex(this.CountyNames, function (o) { return (o.value && o.value.toString() === selCounty)});
            if(rowIndex !== -1) {
                let selRow = this.CountyNames[rowIndex];
                return selRow.name;
            }
            return "";
        }
    }

}