export class AccountView{
    accountId: number;
    name: string;
    industryId: number;
    sectorId: number;
    industryCode: string;
    industryName: string;
    sectorCode: string;
    sectorName: string;
    enabled: string;
}