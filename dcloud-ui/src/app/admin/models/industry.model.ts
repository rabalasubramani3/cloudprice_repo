export class Industry{
    industryId: number;
    code: string;
    name: string;
}