export class UserFilter {
    firstNameFilter: String;
    lastNameFilter: String;
}

export class UserFilterData {
    private _firstNameFilter: String;
    private _lastNameFilter: String;

    get firstNameFilter(): any {
        return this._firstNameFilter;
    }

    set firstNameFilter(value: any) {
        this._firstNameFilter = value;
    }

    get lastNameFilter(): any {
        return this._lastNameFilter;
    }

    set lastNameFilter(value: any) {
        this._lastNameFilter = value;
    }
 }
