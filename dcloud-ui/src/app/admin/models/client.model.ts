export class Client{
    clientId: number;
    name: string;
    accountId: number;
    enabled: string;

    constructor(){
        this.enabled = 'Y';
    }
}