
export class LookupFilter {
    refCodeFilter: String;
    refValueFilter: String;
}

export class LookupFilterData {
    private _refCodeFilter: String;
    private _refValueFilter: String;

    get refCodeFilter(): any {
        return this._refCodeFilter;
    }

    set refCodeFilter(value: any) {
        this._refCodeFilter = value;
    }

    get refValueFilter(): any {
        return this._refValueFilter;
    }

    set refValueFilter(value: any) {
        this._refValueFilter = value;
    }
 }
