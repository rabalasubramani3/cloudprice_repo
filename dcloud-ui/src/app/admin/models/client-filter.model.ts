
export class ClientFilter {
    codeFilter: String;
}

export class ClientFilterData {
    private _codeFilter: String;

    get codeFilter(): any {
        return this._codeFilter;
    }

    set codeFilter(value: any) {
        this._codeFilter = value;
    }
 }
