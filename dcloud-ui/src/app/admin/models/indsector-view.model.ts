export class IndustrySectorView{
    indsectId: number;
    industryId: number;
    sectorId: number;
    industryCode: string;
    industryName: string;
    sectorCode: string;
    sectorName: string;
}