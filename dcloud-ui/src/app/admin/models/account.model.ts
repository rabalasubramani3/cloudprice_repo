export class Account{
    accountId: number;
    name: string;
    industryId: number;
    sectorId: number;
    enabled: string;

    constructor(){
        this.enabled = 'Y';
    }
}

