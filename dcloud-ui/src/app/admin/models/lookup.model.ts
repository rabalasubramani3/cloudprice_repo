
export class Lookup {
    lookupId: number;
    refTypeId: number;
    refCode: string;
    refValue: string;
    bronze: string;
    silver: string;
    gold: string;
    platinum: string;
}
