
export class User {
    userId: number;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    role: string;
    employeeId: string;
    lastLoginDt: string;
    endDt: string;
    enabled: string;
}
