export class Sector{
    clientId: number;
    name: string;
    industryId: number;
    sectorId: number;
    enabled: string;
}