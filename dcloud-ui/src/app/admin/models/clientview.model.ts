export class ClientView{
    clientId: number;
    name: string;
    accountId: number;
    account: string;
    industry: string;
    sector: string;
    enabled: string;
}