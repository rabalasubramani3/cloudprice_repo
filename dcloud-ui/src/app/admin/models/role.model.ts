
export class Role {
    roleId: number;
    name: string;
    roleDescription: string;
}
