
import * as _ from 'lodash';
import { Pipe, PipeTransform } from '@angular/core';
import { ClientFilterData } from '../models/client-filter.model';

@Pipe({
    name: "clientfilterpipe"
})
export class ClientFilterPipe implements PipeTransform {
    transform(data: any[], filter: ClientFilterData): any {
        let results = data;

        if (data) {
            if (filter) {
                // filter by tdl name
                if (typeof filter.codeFilter !== 'undefined' && filter.codeFilter.length > 0) {
                    results =  _.filter(results, function(item) {
                        let val1:string = item.name;
                        let val2:string = filter.codeFilter;
                        return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                    });
                                                        
                }
            }
        }
        return results;
    }
}