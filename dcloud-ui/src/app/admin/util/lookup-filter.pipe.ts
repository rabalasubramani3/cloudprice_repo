
import * as _ from 'lodash';
import { Pipe, PipeTransform } from '@angular/core';
import { LookupFilterData } from '../models/lookup-filter.model';

@Pipe({
    name: "lookupfilterpipe"
})
export class LookupFilterPipe implements PipeTransform {
    transform(data: any[], filter: LookupFilterData): any {
        let results = data;

        if (data) {
            if (filter) {
                let handler = this;
                // filter by refType
                if (typeof filter.refCodeFilter !== 'undefined' && filter.refCodeFilter.length > 0) {
                    results =  _.filter(results, function(item) {
                        let val1:string = item.refCode;
                        let val2:string = filter.refCodeFilter;
                        return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                    });

                }
                if (typeof filter.refValueFilter !== 'undefined' && filter.refValueFilter.length > 0) {
                    results =  _.filter(results, function(item) {
                        let val1:string = item.refValue;
                        let val2:string = filter.refValueFilter;
                        return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                    });

                }
            }
        }
        return results;
    }
// tslint:disable-next-line:eofline
}