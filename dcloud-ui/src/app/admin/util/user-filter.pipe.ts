
import * as _ from 'lodash';
import { Pipe, PipeTransform } from '@angular/core';
import { UserFilterData } from '../models/user-filter.model';

@Pipe({
    name: "userfilterpipe"
})
export class UserFilterPipe implements PipeTransform {
    transform(data: any[], filter: UserFilterData): any {
        let results = data;

        if (data) {
            if (filter) {
                let handler = this;
                // filter by tdl name
                if (typeof filter.firstNameFilter !== 'undefined' && filter.firstNameFilter.length > 0) {
                    results =  _.filter(results, function(item) {
                        let val1:string = item.firstName;
                        let val2:string = filter.firstNameFilter;
                        return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                    });
                                                        
                }
                if (typeof filter.lastNameFilter !== 'undefined' && filter.lastNameFilter.length > 0) {
                    results =  _.filter(results, function(item) {
                        let val1:string = item.lastName;
                        let val2:string = filter.lastNameFilter;
                        return val1.toUpperCase().indexOf(val2.toUpperCase()) > -1;
                    });
                                                        
                }                
            }
        }
        return results;
    }
// tslint:disable-next-line:eofline
}