import { Component, OnDestroy, OnInit, ViewEncapsulation, AfterViewInit } from "@angular/core";
import { Router } from "@angular/router";
import * as _ from "lodash";
import { Subscription } from "rxjs";
import { Lookup } from '../../models/lookup.model';
import { LookupService } from '../../services/lookup.service';
import { isNil } from 'ramda';
import { ReferenceType } from '../../models/refertype.model';
import { ICellRendererAngularComp } from "ag-grid-angular";
import { GridOptions } from "ag-grid-community";

declare var jQuery: any;


@Component({
  selector: 'edit-cell',
  template: `<i class="fa fa-edit" (click)="invokeParentMethod()" data-toggle="modal" title="Edit Lookup"
                   data-keyboard="false" data-backdrop="static" data-target="#edit-lookup"></i>`
  // styles: [
  //     `.btn {
  //         line-height: 0.5
  //     }`
  // ]
})
export class EditLookupComponent implements ICellRendererAngularComp {
  public params: any;

  agInit(params: any): void {
      this.params = params;
  }

  public invokeParentMethod() {
      this.params.context.componentParent.selectEditLookup(this.params.data);
  }

  refresh(): boolean {
      return false;
  }
}

@Component({
  selector: 'delete-cell',
  template: `<i class="fa fa-trash-o txt-gap" (click)="invokeParentMethod()" data-toggle="modal"  title="Delete Lookup"
  data-keyboard="false" data-backdrop="static" data-target="#confirm-popup"></i>`
  // styles: [
  //     `.btn {
  //         line-height: 0.5
  //     }`
  // ]
})
export class DeleteLookupComponent implements ICellRendererAngularComp {
  public params: any;

  agInit(params: any): void {
      this.params = params;
  }

  public invokeParentMethod() {
      this.params.context.componentParent.deleteLookup(this.params.data);
  }

  refresh(): boolean {
      return false;
  }
}


@Component({
  selector: "lookup-dashboard",
  templateUrl: "./lookup-dashboard.template.html",
  styleUrls: ["./lookup-dashboard.styles.scss"],
  providers: [LookupService],
  encapsulation: ViewEncapsulation.None
})
export class LookupDashboardPage implements OnInit, OnDestroy, AfterViewInit {
  public router: Router;
  public data: any[];
  public refTypes: ReferenceType[];
  public adminCount: number;

  public busyLoadLookups: Subscription;
  public busyDeleteLookup: Subscription;
  public busyLoadRefereneTypes: Subscription;

  public selRefTypeId: number;

  public dropdownData = [];
  public showErrorFlag: boolean;
  public alerts: Array<Object>;
  public effectiveDate: Date;
  public selLookup: Lookup;
  public showCreateLookup: any;
  public showEditLookup: any;

  public dellookuptitle: string = "Delete Lookup";
  public dellookupmessage: string = "Are you sure you want to delete lookup?";
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;

  public gridOptions: GridOptions;

  public gridApi;
  public gridColumnApi;

  public defaultColDef = {
    sortable: true,
    filter: true,
    headerClass: 'fw-semi-bold'
  };

  public columnDefs = [
    {
      headerName: "",
      field: "value",
      cellRendererFramework: EditLookupComponent,
      colId: "params",
      width: 50,
      filter: false
    },
    {
      headerName: 'Code',
      field: 'refCode',
    },
    {
      headerName: 'Value',
      field: 'refValue'
    },
    {
      headerName: 'Bronze',
      field: 'bronze',
      editable: false,
      cellRenderer: params => {
        return `<input onclick="return false;" type='checkbox' ${params.value === 'Y' ? 'checked' : ''} />`;
      }
    },
    {
      headerName: 'Silver',
      field: 'silver',
      editable: false,
      cellRenderer: params => {
        return `<input onclick="return false;" type='checkbox' ${params.value === 'Y' ? 'checked' : ''} />`;
      }
    },
    {
      headerName: 'Gold',
      field: 'gold',
      editable: false,
      cellRenderer: params => {
        return `<input onclick="return false;" type='checkbox' ${params.value === 'Y' ? 'checked' : ''} />`;
      }
    },
    {
      headerName: 'Platinum',
      field: 'platinum',
      editable: false,
      cellRenderer: params => {
        return `<input onclick="return false;" type='checkbox' ${params.value === 'Y' ? 'checked' : ''} />`;
      }
    },
    {
      headerName: "",
      field: "value",
      cellRendererFramework: DeleteLookupComponent,
      colId: "params",
      width: 50,
      filter: false
    }
  ];

  constructor(private lookupService: LookupService, router: Router) {
    this.alerts = [
      {
        type: "success",
        msg:
          '<i class="fa fa-circle text-success"></i><span class="alert-text"></span>'
      }
    ];

    this.router = router;
    this.showCreateLookup = false;
    this.showEditLookup = false;
    this.selRefTypeId = 1;

    this.gridOptions = <GridOptions>{
      context: {
          componentParent: this
      },
      enableColResize: true,
      rowHeight: 30
    };

  }

  ngOnInit() {
    let searchInput = jQuery("#table-search-input");
    searchInput
      .focus(e => {
        jQuery(e.target)
          .closest(".input-group")
          .addClass("focus");
      })
      .focusout(e => {
        jQuery(e.target)
          .closest(".input-group")
          .removeClass("focus");
      });

    this.loadReferenceTypes();
  }

  ngOnDestroy() {
    if (this.busyLoadLookups) {
      this.busyLoadLookups.unsubscribe();
    }
    if (this.busyDeleteLookup) {
      this.busyDeleteLookup.unsubscribe();
    }
    if(this.busyLoadRefereneTypes){
      this.busyLoadRefereneTypes.unsubscribe();
    }
  }

  ngAfterViewInit() {
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }



  public cosmeticFix(elementID: any){
    // The code below for cosmetic fix - Blue box now surrounds the icon on the search company text box
    let filterInput = jQuery(elementID);
    filterInput
    .focus((e) => {
        jQuery(e.target).closest('.input-group').addClass('focus');
    })
    .focusout((e) => {
        jQuery(e.target).closest('.input-group').removeClass('focus');
    });
  }

  private loadReferenceTypes() {
    let that = this;
    this.busyLoadRefereneTypes = this.lookupService.getAllRefTypes().subscribe(rt => {
      if (rt) {
        that.refTypes = rt.refTypes;
        if(isNil(that.refTypes)){
          that.selRefTypeId = rt[0].referenceTypeId;
        }
        that.loadLookups(that.selRefTypeId);
      }
    }, error => {
      that.showErrorFlag = true;
      that.alerts = [];
      that.alerts.push({ type: 'warning', msg: error });
    });
  }

  private loadLookups(rtid:number) {
    let that = this;
    this.busyLoadLookups = this.lookupService.getAllByRefTypeId(rtid).subscribe(lv => {
      if (lv) {
        that.data = lv.refValues;
      }
    }, error => {
      that.showErrorFlag = true;
      that.alerts = [];
      that.alerts.push({ type: 'warning', msg: error });
    });
  }

  public reloadLookups(reload: boolean) {
    this.showCreateLookup = false;
    this.showEditLookup = false;
    if (reload) {
      this.loadLookups(this.selRefTypeId);
    } else {
      this.selLookup = new Lookup();
    }
  }

  public deleteLookup(lookup: Lookup) {
    if(!isNil(lookup)){
      this.busyDeleteLookup = this.lookupService.deleteLookupById(lookup.lookupId).subscribe(data => {
        if (data) {
          this.reloadLookups(true);
        }
      }, error => {
        this.showErrorFlag = true;
        this.alerts = [];
        this.alerts.push({ type: 'warning', msg: error });
      });
    }

  }

  public addLookup() {
    this.showCreateLookup = true;
  }

  public selectEditLookup(lookupValue) {
    this.showEditLookup = true;
    this.selLookup = lookupValue;
  }

  public onReferenceTypeChanged(newRefTypeId: number){
    this.selRefTypeId = newRefTypeId;
    this.reloadLookups(true);
  }

  public onClickBackButton(){
		this.router.navigate(["/app/bom/search"]);
  }
}
