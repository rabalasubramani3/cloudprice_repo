import { Component, EventEmitter, Input, OnDestroy, OnInit, Output,
    OnChanges, ViewEncapsulation, SimpleChanges } from "@angular/core";
import { Router } from "@angular/router";
import { isNil } from 'ramda';
import { Subscription } from "rxjs";
import { Lookup } from '../../models/lookup.model';
import { LookupService } from '../../services/lookup.service';
declare var jQuery: any;
@Component({
	// tslint:disable-next-line: component-selector
	selector: "[edit-lookup-popup]",
	templateUrl: "./edit-lookup-popup.template.html",
	styleUrls: ["./edit-lookup-popup.styles.scss"],
	encapsulation: ViewEncapsulation.None,
	providers: []
})

/**
 *
 *
 * @export
 * @class EditLookupPopup
 * @implements {OnInit}
 */
export class EditLookupPopup implements OnInit, OnDestroy, OnChanges {
	@Input() model: Lookup;
	@Output() reloadLookups = new EventEmitter<boolean>();

	public router: Router;
	public showErrorFlag: boolean;
	public alerts: Array<Object>;
	public editLookup = new Lookup();
	public busyEditLookup: Subscription;
	public bronzeenabled: boolean;
	public silverenabled: boolean;
	public goldenabled: boolean;
	public platinumenabled: boolean;

	ngOnInit(): void {
		this.showErrorFlag = false;
	}

	ngOnDestroy() {
		if (this.busyEditLookup) {
			this.busyEditLookup.unsubscribe();
		}
	}

	constructor(
		router: Router,	private lookupService: LookupService
	) {
		this.router = router;

		this.alerts = [
			{
				type: "warning",
				msg:
					'<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages'
			}
		];

		this.showErrorFlag = false;
	}

	// Update lookup
	public updateLookup(lv: Lookup) {
		jQuery("#editlookupform")
			.parsley()
			.validate();

		// Toggle to edit mode or navigate to the next screen if validation passes
		if (
			jQuery("#editlookupform")
				.parsley()
				.isValid()
		) {
			this.editLookup.refCode = isNil(lv.refCode) ? "" : lv.refCode.toUpperCase();
			this.editLookup.refValue = lv.refValue;
			this.editLookup.bronze = this.bronzeenabled ? 'Y' : 'N';
			this.editLookup.silver = this.silverenabled ? 'Y' : 'N';
			this.editLookup.gold = this.goldenabled ? 'Y' : 'N';
      this.editLookup.platinum = this.platinumenabled ? 'Y' : 'N';

			this.busyEditLookup = this.lookupService.updateLookup(this.editLookup).subscribe(
				data => {
					if (data) {
						this.reset(true);
					}
				},
				error => {
					if (!isNil(error) && error.indexOf("USER_EMAIL_UK") > -1) {
						error = "Lookup already exists";
					}
					this.showErrorFlag = true;
					this.alerts = [];
					this.alerts.push({
						type: "warning",
						msg: isNil(error) ? "Error: unable to save resource" : error
					});
				}
			);
		}
	}

	public reset(flag) {
		jQuery("#edit-lookup")
			.on("hidden")
			.find("#ref-code")
			.val("");
		jQuery("#edit-lookup")
			.on("hidden")
			.find("#ref-val")
			.val("");
		jQuery("#editlookupform")
			.parsley()
			.reset();
		jQuery("#edit-lookup").modal("hide");
		this.reloadLookups.emit(flag);
		this.showErrorFlag = false;
	}

	ngOnChanges(changes: SimpleChanges) {
		for (let propName in changes) {
			if (propName === "model") {
				let chg = changes[propName];
				if (chg.currentValue) {
          this.editLookup = Object.assign({}, chg.currentValue);
          this.bronzeenabled = this.editLookup.bronze === 'Y' ? true : false;
          this.silverenabled = this.editLookup.silver === 'Y' ? true : false;
          this.goldenabled = this.editLookup.gold === 'Y' ? true : false;
          this.platinumenabled = this.editLookup.platinum === 'Y' ? true : false;
				}
			}
		}
	}
}
