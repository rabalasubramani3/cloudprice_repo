import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { isNil } from 'ramda';
import { Subscription } from "rxjs";
import { Lookup } from '../../models/lookup.model';
import { LookupService } from '../../services/lookup.service';
declare var jQuery: any;

@Component({
	selector: "[create-lookup-popup]",
	templateUrl: "./create-lookup-popup.template.html",
	styleUrls: ["./create-lookup-popup.styles.scss"],
	encapsulation: ViewEncapsulation.None,
	providers: []
})

/**
 *
 *
 * @export
 * @class CreateLookupPopup
 * @implements {OnInit}
 */
export class CreateLookupPopup implements OnInit, OnDestroy {
	@Input() ReferTypeId;
	@Output() reloadLookups = new EventEmitter<boolean>();

	public router: Router;
	public showErrorFlag: boolean;
	public alerts: Array<Object>;
	public lookup = new Lookup();
	public busyCreateLookup: Subscription;
	public bronzeenabled: boolean;
	public silverenabled: boolean;
	public goldenabled: boolean;
	public platinumenabled: boolean;

	constructor(
		router: Router,
		private lookupService: LookupService
	) {
		this.router = router;

		this.alerts = [
			{
				type: "warning",
				msg:
					'<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages'
			}
		];

		this.showErrorFlag = false;

	}

	ngOnInit(): void {
		this.showErrorFlag = false;
	}

	ngOnDestroy() {
		if (this.busyCreateLookup) {
			this.busyCreateLookup.unsubscribe();
		}
	}
	public createLookup(lv: Lookup) {
		jQuery("#addlookupform")
			.parsley()
			.validate();

		// Toggle to edit mode or navigate to the next screen if validation passes
		if (
			jQuery("#addlookupform")
				.parsley()
				.isValid()
		) {

			this.lookup.refTypeId = this.ReferTypeId;
			this.lookup.refCode = isNil(lv.refCode) ? "" : lv.refCode.toUpperCase();
			this.lookup.refValue = lv.refValue;
			this.lookup.bronze = this.bronzeenabled ? 'Y' : 'N';
			this.lookup.silver = this.silverenabled ? 'Y' : 'N';
			this.lookup.gold = this.goldenabled ? 'Y' : 'N';
			this.lookup.platinum = this.platinumenabled ? 'Y' : 'N';

			this.busyCreateLookup = this.lookupService.createLookup(this.lookup).subscribe(
				data => {
					if (data) {
						this.reset(true);
						this.reloadLookups.emit(true);
					}
				},
				error => {
					this.showErrorFlag = true;
					this.alerts = [];
					this.alerts.push({ type: "warning", msg: error });
				}
			);
		}
	}

	public reset(flag) {
		jQuery("#create-lookup")
			.on("hidden")
			.find("#ref-code")
			.val("");
		jQuery("#create-lookup")
			.on("hidden")
			.find("#ref-val")
			.val("");
		jQuery("#addlookupform")
			.parsley()
			.reset();
		jQuery("#create-lookup").modal("hide");
		this.showErrorFlag = false;
	}
}
