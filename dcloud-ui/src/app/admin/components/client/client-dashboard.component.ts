import { Component, OnDestroy, OnInit, ViewEncapsulation, AfterViewInit } from "@angular/core";
import { Router } from "@angular/router";
import * as _ from "lodash";
import { Subscription } from "rxjs";
import { ClientService } from '../../services/client.service';
import { Client } from '../../models/client.model';
import { ClientView } from '../../models/clientview.model';
import { ICellRendererAngularComp } from "ag-grid-angular";
import { GridOptions } from "ag-grid-community";

declare var jQuery: any;

@Component({
  selector: 'edit-cell',
  template: `<i class="glyphicon glyphicon-pencil" (click)="invokeParentMethod()" data-toggle="modal"  title="Edit Client"
                   data-keyboard="false" data-backdrop="static" data-target="#edit-client"></i>`
  // styles: [
  //     `.btn {
  //         line-height: 0.5
  //     }`
  // ]
})
export class EditClientComponent implements ICellRendererAngularComp {
  public params: any;

  agInit(params: any): void {
      this.params = params;
  }

  public invokeParentMethod() {
      this.params.context.componentParent.selectEditClient(this.params.data);
  }

  refresh(): boolean {
      return false;
  }
}

@Component({
  selector: 'delete-cell',
  template: `<i class="glyphicon glyphicon-trash txt-gap" (click)="invokeParentMethod()" data-toggle="modal"  title="Delete Client"
  data-keyboard="false" data-backdrop="static" data-target="#confirm-popup"></i>`
  // styles: [
  //     `.btn {
  //         line-height: 0.5
  //     }`
  // ]
})
export class DeleteClientComponent implements ICellRendererAngularComp {
  public params: any;

  agInit(params: any): void {
      this.params = params;
  }

  public invokeParentMethod() {
      this.params.context.componentParent.selectDeleteClient(this.params.data);
  }

  refresh(): boolean {
      return false;
  }
}

// con
@Component({
  selector: "client-dashboard",
  templateUrl: "./client-dashboard.template.html",
  styleUrls: ["./client-dashboard.styles.scss"],
  providers: [ClientService],
  encapsulation: ViewEncapsulation.None
})
export class ClientDashboardPage implements OnInit, OnDestroy, AfterViewInit {
  public router: Router;
  public data: any[];

  public lstClients: ClientView[];

  public adminCount: number;
  public selClientId: number;

  public busyLoadClients: Subscription;
  public busyDeleteClient: Subscription;

  public paginationPageSize = 20;

  public dropdownData = [];
  public showErrorFlag: boolean;
  public alerts: Array<Object>;
  public effectiveDate: Date;
  public selClient: Client;

  public showCreateClient: boolean;
  public showEditClient: boolean;
  public showDeleteClient: boolean;

  public delClientTitle: string = "Delete Client";
  public delClientMessage: string = "Are you sure you want to delete Client?";
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;

  public gridOptions: GridOptions;

  public gridApi;
  public gridColumnApi;

  public defaultColDef = {
    sortable: true,
    filter: true,
    headerClass: 'fw-semi-bold'
  };

  public columnDefs = [
    {
      headerName: "",
      field: "value",
      cellRendererFramework: EditClientComponent,
      colId: "params",
      width: 50,
      filter: false
    },
    {
      headerName: 'Name',
      field: 'name',
      cellClass: function(params) {
        return params.data.enabled !== 'Y' ? "fw-semi-bold inactive" : "fw-semi-bold ";
      }
    },
    {
      headerName: 'Account',
      field: 'account',
      cellClass: function(params) {
        return params.data.enabled !== 'Y' ? "inactive" : "";
      }
    },
    {
      headerName: 'Industry',
      field: 'industry',
      cellClass: function(params) {
        return params.data.enabled !== 'Y' ? "inactive" : "";
      }
    },
    {
      headerName: 'Sector',
      field: 'sector',
      cellClass: function(params) {
        return params.data.enabled !== 'Y' ? "inactive" : "";
      }
    },
    {
      headerName: "",
      field: "value",
      cellRendererFramework: DeleteClientComponent,
      colId: "params",
      width: 50,
      filter: false
    }
];

  constructor(
    private clientService: ClientService,
    router: Router) {
    this.alerts = [
      {
        type: "success",
        msg:
          '<i class="fa fa-circle text-success"></i><span class="alert-text">Successfully generated 12 monthly reports</span>'
      }
    ];

    this.router = router;

    this.showCreateClient = false;
    this.showEditClient = false;
    this.showDeleteClient = false;

    this.selClientId = 1;
    this.gridOptions = <GridOptions>{
      context: {
          componentParent: this
      },
      enableColResize: true,
      rowHeight: 30
  };
  }

  ngOnInit() {
    let searchInput = jQuery("#table-search-input, #search-countries");
    searchInput
      .focus(e => {
        jQuery(e.target)
          .closest(".input-group")
          .addClass("focus");
      })
      .focusout(e => {
        jQuery(e.target)
          .closest(".input-group")
          .removeClass("focus");
      });

      this.loadAllClients();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  ngOnDestroy() {
    if (this.busyLoadClients) {
      this.busyLoadClients.unsubscribe();
    }
    if (this.busyDeleteClient) {
      this.busyDeleteClient.unsubscribe();
    }
 }

  ngAfterViewInit() {
  }

  public cosmeticFix(elementID: any){
    // The code below for cosmetic fix - Blue box now surrounds the icon on the search company text box
    let filterInput = jQuery(elementID);
    filterInput
    .focus((e) => {
        jQuery(e.target).closest('.input-group').addClass('focus');
    })
    .focusout((e) => {
        jQuery(e.target).closest('.input-group').removeClass('focus');
    });
  }

  private loadAllClients() {
    let that = this;
    this.busyLoadClients = this.clientService.getAll().subscribe(clientdata => {
      if (clientdata) {
        that.data = clientdata;
        that.gridApi.sizeColumnsToFit();
      }
    }, error => {
      this.showErrorFlag = true;
      this.alerts = [];
      this.alerts.push({ type: 'warning', msg: error });
    });
  }

  public reloadClients(reload: boolean) {
    this.showCreateClient = false;
    this.showEditClient = false;
    if (reload) {
      this.loadAllClients();
    } else {
      this.selClient = new Client();
    }
  }

  public deleteClient(id) {
    this.busyDeleteClient = this.clientService.deleteClientById(id).subscribe(data => {
      if (data) {
        this.reloadClients(true);
        this.showDeleteClient = false;
        jQuery('#confirm-popup').modal('hide');
      }
    }, error => {
      this.showErrorFlag = true;
      this.alerts = [];
      this.alerts.push({ type: 'warning', msg: error });
    });
  }

  public addClient() {
    this.showCreateClient = true;
  }

  public selectEditClient(client) {
    this.showEditClient = true;
    this.selClient = client;
  }

  public selectDeleteClient(client){
    this.showDeleteClient = true;
    this.selClient = client;
  }
  public onClickBackButton(){
		this.router.navigate(["/app/bom/search"]);
  }
}
