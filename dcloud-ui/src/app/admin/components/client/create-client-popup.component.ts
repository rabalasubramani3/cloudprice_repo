/*
@author : Deloitte
this is Component for creating a labor category
*/
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AccountView } from '../../models/accountview.model';
import { Client } from '../../models/client.model';
import { AccountService } from '../../services/account.service';
import { ClientService } from '../../services/client.service';

declare var jQuery: any;

@Component({
  selector: '[create-client-popup]',
  templateUrl: './create-client-popup.template.html',
  styleUrls: ['./create-client-popup.styles.scss'],
  providers: [AccountService, ClientService],
  encapsulation: ViewEncapsulation.None
})

/**
 *
 *
 * @export
 * @class CreateClientPopup
 * @implements {OnInit}
 */

export class CreateClientPopup implements OnInit, OnDestroy{
  @Output() reloadClients = new EventEmitter<boolean>();
  @Input() ClientId;

  public router: Router;
  public showErrorFlag: boolean;
  public alerts: Array<Object>;


  public client = new Client();
  public lstAccounts: AccountView[] = [];

  public busySaveClient:Subscription;
  public busyLoadAccounts: Subscription;


  public clientenabled: boolean = true;

  constructor(router: Router,
    private clientService: ClientService,
    private accountService: AccountService
  ) {
    this.router = router;

    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages'
      }
    ];

    this.showErrorFlag = false;
  }

  ngOnInit(): void {
    this.showErrorFlag = false;
    this.loadAllAccounts();
  }

  ngOnDestroy() {
    if(this.busySaveClient){
      this.busySaveClient.unsubscribe();
    }
    if(this.busyLoadAccounts){
      this.busyLoadAccounts.unsubscribe();
    }
  }

  private loadAllAccounts() {
    let that = this;
    this.busyLoadAccounts = this.accountService.getAll().subscribe(accountdata => {
      if (accountdata) {
        that.lstAccounts = accountdata;
      }
    }, error => {
      this.showErrorFlag = true;
      this.alerts = [];
      this.alerts.push({ type: 'warning', msg: error });
    });
  }

  public createClient(newClient: Client) {
    jQuery('#addclientform').parsley().validate();

    // Toggle to edit mode or navigate to the next screen if validation passes
    if (jQuery('#addclientform').parsley().isValid()) {

      newClient.enabled = (this.clientenabled) ? 'Y' : 'N';
      this.busySaveClient = this.clientService.saveClient(newClient).subscribe(data => {
        if (data) {
          this.reset(true);
          this.reloadClients.emit(true);
        }
      }, error => {
          this.showErrorFlag = true;
          this.alerts = [];
          this.alerts.push({ type: 'warning', msg: error });
      });
    }
  }





  public reset(flag) {
    jQuery('#create-client').on('hidden').find('#name').val('');
    jQuery('#addclientform').parsley().reset();
    jQuery('#create-client').modal('hide');
    this.showErrorFlag = false;
  }



}
