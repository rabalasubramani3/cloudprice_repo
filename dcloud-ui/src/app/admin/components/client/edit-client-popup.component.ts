/*
@author : Deloitte
this is Componentfor adding contact as a popup.
*/
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChanges, ViewEncapsulation, OnChanges } from '@angular/core';
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { Client } from '../../models/client.model';
import { AccountService } from '../../services/account.service';
import { ClientService } from '../../services/client.service';
import { AccountView } from '../../models/accountview.model';
declare var jQuery: any;

@Component({
  selector: "[edit-client-popup]",
  templateUrl: "./edit-client-popup.template.html",
  styleUrls: ["./edit-client-popup.styles.scss"],
  providers: [AccountService, ClientService],
  encapsulation: ViewEncapsulation.None
})

/**
 *
 *
 * @export
 * @class EditClientPopup
 * @implements {OnInit}
 */
export class EditClientPopup implements OnInit, OnDestroy, OnChanges {
  @Input() model;
  @Output() reloadClients = new EventEmitter<boolean>();

  public router: Router;
  public showErrorFlag: boolean;
  public alerts: Array<Object>;
  public editClient: Client;
  public lstAccounts: AccountView[] = [];
  public busySaveClient:Subscription;
  public busyLoadAccounts: Subscription;
  public clientenabled: boolean = true;
  ngOnInit(): void {
    this.showErrorFlag = false;

  }
  ngOnDestroy() {
    if(this.busySaveClient){
      this.busySaveClient.unsubscribe();
    }
    if(this.busyLoadAccounts){
      this.busyLoadAccounts.unsubscribe();
    }
  }

  constructor(
    router: Router,
    private clientService: ClientService,
    private accountService: AccountService
  ) {
    this.router = router;

    this.alerts = [
      {
        type: "warning",
        msg:
          '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages'
      }
    ];

    this.showErrorFlag = false;
    this.loadAllAccounts();
  }

  // Update labor cat
  public updateClient(client: Client) {
    jQuery("#editclientform").parsley().validate();
    client.enabled = (this.clientenabled) ? 'Y' : 'N';
    // Toggle to edit mode or navigate to the next screen if validation passes
    if ( jQuery("#editclientform").parsley().isValid() ) {
      this.busySaveClient = this.clientService.saveClient(client).subscribe(
        data => {
          if (data) {
            this.reset(true);
          }
        },
        error => {
          this.showErrorFlag = true;
          this.alerts = [];
          this.alerts.push({ type: "warning", msg: error });
        }
      );
    }
  }

  public reset(flag) {
    jQuery("#edit-client").on("hidden").find("#name").val("");
    jQuery("#editclientform").parsley().reset();
    jQuery("#edit-client").modal("hide");
    this.reloadClients.emit(flag);
    this.showErrorFlag = false;
  }


  private loadAllAccounts() {
    let that = this;
    this.busyLoadAccounts = this.accountService.getAll().subscribe(accountdata => {
      if (accountdata) {
        that.lstAccounts = accountdata;
      }
    }, error => {
      this.showErrorFlag = true;
      this.alerts = [];
      this.alerts.push({ type: 'warning', msg: error });
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "model") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.editClient = Object.assign({}, chg.currentValue);
          this.clientenabled = (this.editClient.enabled === 'Y') ? true : false;

        }
      }
    }
  }
}
