/*
@author : Deloitte
this is Component for creating a labor category
*/
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Account } from '../../models/account.model';
import { IndustrySectorView } from '../../models/indsector-view.model';
import { Industry } from '../../models/industry.model';
import { AccountService } from '../../services/account.service';
import { ClientService } from '../../services/client.service';

declare var jQuery: any;

@Component({
  selector: '[create-account-popup]',
  templateUrl: './create-account-popup.template.html',
  styleUrls: ['./create-account-popup.styles.scss'],
  providers: [AccountService, ClientService],
  encapsulation: ViewEncapsulation.None
})

/**
 *
 *
 * @export
 * @class CreateAccountPopup
 * @implements {OnInit}
 */

export class CreateAccountPopup implements OnInit, OnDestroy {
  @Output() reloadAccounts = new EventEmitter<boolean>();
  @Input() AccountId;

  public router: Router;
  public showErrorFlag: boolean;
  public alerts: Array<Object>;
  public account = new Account();

  public busySaveAccount:Subscription;
  public busyLoadIndustries:Subscription;
  public busyLoadIndustrySectors: Subscription;
  public accountenabled: boolean = true;

  public lstIndustries: Industry[]=[];
  public lstSectors: IndustrySectorView[]=[];

  constructor(router: Router,
    private accountService: AccountService,
    private clientService: ClientService
  ) {
    this.router = router;

    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages'
      }
    ];

    this.showErrorFlag = false;
  }

  ngOnInit(): void {
    this.showErrorFlag = false;
    this.loadIndustries();
  }

  ngOnDestroy() {
    if(this.busySaveAccount){
      this.busySaveAccount.unsubscribe();
    }
    if(this.busyLoadIndustries){
      this.busyLoadIndustries.unsubscribe();
    }
    if(this.busyLoadIndustrySectors){
      this.busyLoadIndustrySectors.unsubscribe();
    }
  }

  public loadIndustries(){
    this.busyLoadIndustries = this.accountService.loadIndustries().subscribe(data => {
      if (data) {
        this.lstIndustries = data;
      }
    }, error => {
        this.showErrorFlag = true;
        this.alerts = [];
        this.alerts.push({ type: 'warning', msg: error });
    });
  }

  public loadSectorsForIndustry(selIndustryId: number){
    let that = this;
    this.busyLoadIndustrySectors = this.accountService.loadSectorsForIndustry(selIndustryId).subscribe(data => {
      if (data) {
        that.lstSectors = data;
      }
    }, error => {
        this.showErrorFlag = true;
        this.alerts = [];
        this.alerts.push({ type: 'warning', msg: error });
    });
  }

  public createAccount(newAccount: Account) {
    jQuery('#addaccountform').parsley().validate();

    // Toggle to edit mode or navigate to the next screen if validation passes
    if (jQuery('#addaccountform').parsley().isValid()) {

      newAccount.enabled = (this.accountenabled) ? 'Y' : 'N';
      this.busySaveAccount = this.accountService.saveAccount(newAccount).subscribe(data => {
        if (data) {
          this.reset(true);
          this.reloadAccounts.emit(true);
        }
      }, error => {
          this.showErrorFlag = true;
          this.alerts = [];
          this.alerts.push({ type: 'warning', msg: error });
      });
    }
  }

  public OnIndustryChanged(event){
    this.loadSectorsForIndustry(this.account.industryId);
 }

  public reset(flag) {
    jQuery('#create-account').on('hidden').find('#name').val('');
    jQuery('#addaccountform').parsley().reset();
    jQuery('#create-account').modal('hide');
    this.showErrorFlag = false;
  }

}
