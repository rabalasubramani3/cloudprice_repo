import { AfterViewInit, Component, OnDestroy, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { ICellRendererAngularComp } from "ag-grid-angular";
import { GridOptions } from "ag-grid-community";
import { saveAs } from 'file-saver';
import { Subscription } from "rxjs";
import * as XLSX from 'xlsx';
import { Account } from '../../models/account.model';
import { AccountView } from '../../models/accountview.model';
import { AccountService } from '../../services/account.service';

declare var jQuery: any;

@Component({
  selector: 'edit-cell',
  template: `<i class="glyphicon glyphicon-pencil" (click)="invokeParentMethod()" data-toggle="modal"  title="Edit Account"
                   data-keyboard="false" data-backdrop="static" data-target="#edit-account"></i>`
  // styles: [
  //     `.btn {
  //         line-height: 0.5
  //     }`
  // ]
})
export class EditAccountComponent implements ICellRendererAngularComp {
  public params: any;

  agInit(params: any): void {
      this.params = params;
  }

  public invokeParentMethod() {
      this.params.context.componentParent.selectEditAccount(this.params.data);
  }

  refresh(): boolean {
      return false;
  }
}

@Component({
  selector: 'delete-cell',
  template: `<i class="glyphicon glyphicon-trash txt-gap" (click)="invokeParentMethod()" data-toggle="modal"  title="Delete Account"
  data-keyboard="false" data-backdrop="static" data-target="#confirm-popup"></i>`
  // styles: [
  //     `.btn {
  //         line-height: 0.5
  //     }`
  // ]
})
export class DeleteAccountComponent implements ICellRendererAngularComp {
  public params: any;

  agInit(params: any): void {
      this.params = params;
  }

  public invokeParentMethod() {
      this.params.context.componentParent.selectDeleteAccount(this.params.data);
  }

  refresh(): boolean {
      return false;
  }
}

// con
@Component({
  selector: "account-dashboard",
  templateUrl: "./account-dashboard.template.html",
  styleUrls: ["./account-dashboard.styles.scss"],
  providers: [AccountService],
  encapsulation: ViewEncapsulation.None
})
export class AccountDashboardPage implements OnInit, OnDestroy, AfterViewInit {
  public router: Router;
  public data: any[];

  public lstAccounts: AccountView[];

  public adminCount: number;
  public selAccountId: number;

  public busyLoadAccounts: Subscription;
  public busyDeleteAccount: Subscription;

  public paginationPageSize = 20;

  public dropdownData = [];
  public showErrorFlag: boolean;
  public alerts: Array<Object>;
  public effectiveDate: Date;
  public selAccount: Account;

  public showCreateAccount: boolean;
  public showEditAccount: boolean;
  public showDeleteAccount: boolean;

  public delAccountTitle: string = "Delete Account";
  public delAccountMessage: string = "Are you sure you want to delete Account?";
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;

  public gridOptions: GridOptions;

  public gridApi;
  public gridColumnApi;

  public defaultColDef = {
    sortable: true,
    filter: true,
    headerClass: 'fw-semi-bold'
  };

  public columnDefs = [
    {
      headerName: "",
      field: "value",
      cellRendererFramework: EditAccountComponent,
      colId: "params",
      width: 50,
      filter: false
    },
    {
      headerName: 'Name',
      field: 'name',
      cellClass: function(params) {
        return params.data.enabled !== 'Y' ? "fw-semi-bold inactive" : "fw-semi-bold ";
      }
    },
    {
      headerName: 'Industry',
      field: 'industryName',
      cellClass: function(params) {
        return params.data.enabled !== 'Y' ? "inactive" : "";
      }
    },
    {
      headerName: 'Sector',
      field: 'sectorName',
      cellClass: function(params) {
        return params.data.enabled !== 'Y' ? "inactive" : "";
      }
    },
    {
      headerName: "",
      field: "value",
      cellRendererFramework: DeleteAccountComponent,
      colId: "params",
      width: 50,
      filter: false
    }
];

  constructor(
    private accountService: AccountService,
    router: Router) {
    this.alerts = [
      {
        type: "success",
        msg:
          '<i class="fa fa-circle text-success"></i><span class="alert-text">Successfully generated 12 monthly reports</span>'
      }
    ];

    this.router = router;

    this.showCreateAccount = false;
    this.showEditAccount = false;
    this.showDeleteAccount = false;

    this.selAccountId = 1;
    this.gridOptions = <GridOptions>{
      context: {
          componentParent: this
      },
      enableColResize: true,
      rowHeight: 30
    };
  }

  ngOnInit() {
    let searchInput = jQuery("#table-search-input, #search-countries");
    searchInput
      .focus(e => {
        jQuery(e.target)
          .closest(".input-group")
          .addClass("focus");
      })
      .focusout(e => {
        jQuery(e.target)
          .closest(".input-group")
          .removeClass("focus");
      });

      this.loadAllAccounts();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  ngOnDestroy() {
    if (this.busyLoadAccounts) {
      this.busyLoadAccounts.unsubscribe();
    }
    if (this.busyDeleteAccount) {
      this.busyDeleteAccount.unsubscribe();
    }
 }

  ngAfterViewInit() {
  }

  public cosmeticFix(elementID: any){
    // The code below for cosmetic fix - Blue box now surrounds the icon on the search company text box
    let filterInput = jQuery(elementID);
    filterInput
    .focus((e) => {
        jQuery(e.target).closest('.input-group').addClass('focus');
    })
    .focusout((e) => {
        jQuery(e.target).closest('.input-group').removeClass('focus');
    });
  }

  private loadAllAccounts() {
    let that = this;
    this.busyLoadAccounts = this.accountService.getAll().subscribe(accountdata => {
      if (accountdata) {
        that.data = accountdata;
        that.gridApi.sizeColumnsToFit();
      }
    }, error => {
      this.showErrorFlag = true;
      this.alerts = [];
      this.alerts.push({ type: 'warning', msg: error });
  });
  }

  public reloadAccounts(reload: boolean) {
    this.showCreateAccount = false;
    this.showEditAccount = false;
    if (reload) {
      this.loadAllAccounts();
    } else {
      this.selAccount = new Account();
    }
  }

  public deleteAccount(id) {
    this.busyDeleteAccount = this.accountService.deleteAccountById(id).subscribe(data => {
      if (data) {
        this.reloadAccounts(true);
        this.showDeleteAccount = false;
        jQuery('#confirm-popup').modal('hide');
      }
    }, error => {
      this.showErrorFlag = true;
      this.alerts = [];
      this.alerts.push({ type: 'warning', msg: error });
  });
  }

  private s2ab(s) {
    let buf = new ArrayBuffer(s.length);
    let view = new Uint8Array(buf);
    for (let i=0; i !== s.length; ++i){
      // tslint:disable-next-line: no-bitwise
      view[i] = s.charCodeAt(i) & 0xFF;
    }
    return buf;
  }

  public exportAccountsReport(data:any, filename, sheetName){
    let ws:XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
    let wb:XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, sheetName);

    /* save to file */
    let wbout: string = XLSX.write(wb, { bookType: 'xlsx', type: 'binary' });
    saveAs(new Blob([this.s2ab(wbout)]), filename);
  }

  public downloadLaborCategories(){
    this.exportAccountsReport(this.data, 'laborcat-report.xlsx', 'Clients');
  }

  public addAccount() {
    this.showCreateAccount = true;
  }

  public selectEditAccount(account) {
    this.showEditAccount = true;
    this.selAccount = account;
  }

  public selectDeleteAccount(account){
    this.showDeleteAccount = true;
    this.selAccount = account;
  }
  public onClickBackButton(){
		this.router.navigate(["/app/bom/search"]);
  }
}

