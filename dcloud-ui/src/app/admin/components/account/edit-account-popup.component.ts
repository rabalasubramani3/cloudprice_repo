/*
@author : Deloitte
this is Componentfor adding contact as a popup.
*/
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { Account } from '../../models/account.model';
import { IndustrySectorView } from '../../models/indsector-view.model';
import { Industry } from '../../models/industry.model';
import { AccountService } from '../../services/account.service';
import { ClientService } from '../../services/client.service';

declare var jQuery: any;

@Component({
  selector: "[edit-account-popup]",
  templateUrl: "./edit-account-popup.template.html",
  styleUrls: ["./edit-account-popup.styles.scss"],
  providers: [AccountService, ClientService],
  encapsulation: ViewEncapsulation.None
})

/**
 *
 *
 * @export
 * @class EditAccountPopup
 * @implements {OnInit}
 */
export class EditAccountPopup implements OnInit, OnDestroy, OnChanges {
  @Input() model;
  @Output() reloadAccounts = new EventEmitter<boolean>();

  public router: Router;
  public showErrorFlag: boolean;
  public alerts: Array<Object>;
  public editAccount: Account;

  public busySaveAccount:Subscription;
  public busyLoadIndustries:Subscription;
  public busyLoadIndustrySectors: Subscription;

  public accountenabled: boolean = true;
  public lstIndustries: Industry[]=[];
  public lstSectors: IndustrySectorView[]=[];

  ngOnInit(): void {
    this.showErrorFlag = false;
    this.loadIndustries();
  }

  ngOnDestroy() {
    if(this.busySaveAccount){
      this.busySaveAccount.unsubscribe();
    }
    if(this.busyLoadIndustries){
      this.busyLoadIndustries.unsubscribe();
    }
    if(this.busyLoadIndustrySectors){
      this.busyLoadIndustrySectors.unsubscribe();
    }
  }

  constructor(
    router: Router,
    private accountService: AccountService,
    private clientService: ClientService
  ) {
    this.router = router;

    this.alerts = [
      {
        type: "warning",
        msg:
          '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages'
      }
    ];

    this.showErrorFlag = false;
  }

  public loadIndustries(){
    this.busyLoadIndustries = this.accountService.loadIndustries().subscribe(data => {
      if (data) {
        this.lstIndustries = data;
      }
    }, error => {
        this.showErrorFlag = true;
        this.alerts = [];
        this.alerts.push({ type: 'warning', msg: error });
    });
  }

  public loadSectorsForIndustry(selIndustryId: number){
    let that = this;
    this.busyLoadIndustrySectors = this.accountService.loadSectorsForIndustry(selIndustryId).subscribe(data => {
      if (data) {
        that.lstSectors = data;
      }
    }, error => {
        this.showErrorFlag = true;
        this.alerts = [];
        this.alerts.push({ type: 'warning', msg: error });
    });
  }

  public OnIndustryChanged(event){
     this.loadSectorsForIndustry(this.editAccount.industryId);
  }
  public updateAccount(account: Account) {
    jQuery("#editaccountform").parsley().validate();
    account.enabled = (this.accountenabled) ? 'Y' : 'N';
    // Toggle to edit mode or navigate to the next screen if validation passes
    if ( jQuery("#editaccountform").parsley().isValid() ) {
      this.busySaveAccount = this.accountService.saveAccount(account).subscribe(
        data => {
          if (data) {
            this.reset(true);
          }
        },
        error => {
          this.showErrorFlag = true;
          this.alerts = [];
          this.alerts.push({ type: "warning", msg: error });
        }
      );
    }
  }



  public reset(flag) {
    jQuery("#edit-account").on("hidden").find("#name").val("");
    jQuery("#editaccountform").parsley().reset();
    jQuery("#edit-account").modal("hide");
    this.reloadAccounts.emit(flag);
    this.showErrorFlag = false;
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "model") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.editAccount = Object.assign({}, chg.currentValue);
          this.loadSectorsForIndustry(this.editAccount.industryId);
          this.accountenabled = (this.editAccount.enabled === 'Y') ? true : false;
        }
      }
    }
  }
}
