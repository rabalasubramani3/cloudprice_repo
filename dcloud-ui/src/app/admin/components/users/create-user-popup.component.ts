/*
@author : Deloitte
this is Component for creating user
*/
import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Role } from '../../models/role.model';
import { User } from '../../models/user.model';
import { AdminService } from '../../services/admin.service';
import { RoleService } from '../../services/role.service';
declare var jQuery: any;

@Component({
  selector: '[create-user-popup]',
 templateUrl: 'create-user-popup.template.html',
  styleUrls: ['./create-user-popup.styles.scss'],
  encapsulation: ViewEncapsulation.None
})

/**
 *
 *
 * @export
 * @class CreateUserPopup
 * @implements {OnInit}
 */

export class CreateUserPopup implements OnInit, OnDestroy {
  @Output() reloadUsers = new EventEmitter<boolean>();

  public router: Router;
  public showErrorFlag: boolean;
  public alerts: Array<Object>;
  public user = new User();
  public userRoles: Role[] = [];
  public busy: Subscription;
  public userenabled: boolean;

  constructor(router: Router,
    private _adminService: AdminService,
    private _roleService: RoleService
  ) {
    this.router = router;

    this.alerts = [
      {
        type: 'warning',
        msg: '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages'
      }
    ];

    this.showErrorFlag = false;

    this.showUserRoles();

  }

  ngOnInit(): void {
    this.showErrorFlag = false;
  }

  ngOnDestroy() {
    if (this.busy) {
      this.busy.unsubscribe();
    }
  }

  // Create a new user
  public createUser(user: User) {
    jQuery('#adduserform').parsley().validate();

    // Toggle to edit mode or navigate to the next screen if validation passes
    if (jQuery('#adduserform').parsley().isValid()) {
      switch(user.role){
        case "Administrator":
          user.role = "ROLE_ADMIN";
          break;

        case "PMO":
          user.role = "ROLE_PMO";
          break;

        case "Project Manager":
          user.role = "ROLE_PM";
          break;

        default:
          user.role = "ROLE_USER";
          break;
      }
      user.email = user.email.toLowerCase();
      user.enabled = (this.userenabled === true ? 'Y' : 'N');

      this.busy = this._adminService.createUser(user).subscribe(data => {
        if (data) {
          this.reset(true);
          this.reloadUsers.emit(true);
        }
      }, error => {
          if (error.indexOf('USER_EMAIL_UK') > -1) {
            error = "User already exists";
          }
          this.showErrorFlag = true;
          this.alerts = [];
          this.alerts.push({ type: 'warning', msg: error });
      });
    }

  }

  public reset(flag) {
    jQuery('#create-user').on('hidden').find('#first-name').val('');
    jQuery('#create-user').on('hidden').find('#last-name').val('');
    jQuery('#create-user').on('hidden').find('#email-address').val('');
    jQuery('#create-user').on('hidden').find('#password').val('');
    jQuery('#create-user').on('hidden').find('#phone-number').val('');
    jQuery('#create-user').on('hidden').find('input[type="email"]').val('');
    jQuery('#create-user').on('hidden').find('input[type="checkbox"]').prop('checked', false);
    jQuery('#adduserform').parsley().reset();
    jQuery('#create-user').modal('hide');
    this.showErrorFlag = false;
  }

  public showUserRoles() {
    this._roleService.getRoles().subscribe(
      r => {
      this.userRoles = r;
      }, error => {
        this.showErrorFlag = true;
        this.alerts = [];
        this.alerts.push({ type: 'warning', msg: error });
    });
  }

}
