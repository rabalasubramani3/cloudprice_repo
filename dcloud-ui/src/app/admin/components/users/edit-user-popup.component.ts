/*
@author : Deloitte
this is Componentfor adding contact as a popup.
*/
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChanges, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { Role } from "../../models/role.model";
import { User } from "../../models/user.model";
import { AdminService } from "../../services/admin.service";
import { RoleService } from "../../services/role.service";
declare var jQuery: any;

@Component({
  selector: "[edit-user-popup]",
  templateUrl: 'edit-user-popup.template.html',
  styleUrls: ["./edit-user-popup.styles.scss"],
  encapsulation: ViewEncapsulation.None
})

/**
 *
 *
 * @export
 * @class EditUserPage
 * @implements {OnInit}
 */
export class EditUserPopup implements OnInit, OnDestroy {
  @Input() model;
  @Output() reloadUsers = new EventEmitter<boolean>();

  public router: Router;
  public showErrorFlag: boolean;
  public alerts: Array<Object>;
  public editUser = new User();
  public userRoles: Role[] = [];
  public busy: Subscription;
  public userenabled: boolean;

  ngOnInit(): void {
    this.showErrorFlag = false;
  }

  ngOnDestroy() {
    if (this.busy) {
      this.busy.unsubscribe();
    }
  }

  constructor(
    router: Router,
    private _adminService: AdminService,
    private _roleService: RoleService
  ) {
    this.router = router;

    this.alerts = [
      {
        type: "warning",
        msg:
          '<span class="fw-semi-bold">Warning:</span> Placeholder for Error Validation messages'
      }
    ];

    this.showErrorFlag = false;

    this.showUserRoles();
  }

  // Update user
  public updateUser(user: User) {
    jQuery("#edituserform").parsley().validate();

    // Toggle to edit mode or navigate to the next screen if validation passes
    if ( jQuery("#edituserform").parsley().isValid() ) {
      switch (user.role) {
        case "Administrator":
          user.role = "ROLE_ADMIN";
          break;

        case "PMO":
          user.role = "ROLE_PMO";
          break;

        case "Project Manager":
          user.role = "ROLE_PM";
          break;

        default:
          user.role = "ROLE_USER";
          break;
      }
      user.email = user.email.toLowerCase();
      user.enabled = (this.userenabled === true ? "Y" : "N");

      this.busy = this._adminService.updateUser(user).subscribe(
        data => {
          if (data) {
            this.reset(true);
          }
        },
        error => {
          if (error.indexOf("USER_EMAIL_UK") > -1) {
            error = "User already exists";
          }
          this.showErrorFlag = true;
          this.alerts = [];
          this.alerts.push({ type: "warning", msg: error });
        }
      );
    }
  }

  public reset(flag) {
    jQuery("#edit-user").on("hidden").find("#first-name").val("");
    jQuery("#edit-user").on("hidden").find("#last-name").val("");
    jQuery("#edit-user").on("hidden").find("#email-address").val("");
    jQuery("#edit-user").on("hidden").find("#password").val("");
    jQuery("#edit-user").on("hidden").find("#phone-number").val("");
    jQuery("#edit-user").on("hidden").find('input[type="email"]').val("");
    jQuery("#edit-user").on("hidden").find('input[type="checkbox"]').prop("checked", false);
    jQuery("#edituserform").parsley().reset();
    jQuery("#edit-user").modal("hide");
    this.reloadUsers.emit(flag);
    this.showErrorFlag = false;
  }

  public showUserRoles() {
    this._roleService.getRoles().subscribe(
      r => {
        // let roles = r.map(r => r.roleDescription);
        // console.log(`...edit-user-popup.component: roles===${JSON.stringify(roles)}`);

        // Initialize blank role first and concat the rolenames afterwards
        this.userRoles = r;
      }, error => {
        this.showErrorFlag = true;
        this.alerts = [];
        this.alerts.push({ type: 'warning', msg: error });
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === "model") {
        let chg = changes[propName];
        if (chg.currentValue) {
          this.editUser = Object.assign({}, chg.currentValue);
          this.userenabled = (this.editUser.enabled === "Y" ? true : false);
        }
      }
    }
  }
}
