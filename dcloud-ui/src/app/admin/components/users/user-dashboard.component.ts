import { AfterViewInit, Component, OnDestroy, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import * as _ from 'lodash';
import { Subscription } from "rxjs";
import { User } from '../../models/user.model';
import { AdminService } from '../../services/admin.service';
import { ICellRendererAngularComp } from "ag-grid-angular";
import { GridOptions } from "ag-grid-community";
import { isNil } from "ramda";

declare var jQuery: any;


@Component({
  selector: 'edit-cell',
  template: `<i class="glyphicon glyphicon-pencil" (click)="invokeParentMethod()" data-toggle="modal" title="Edit User"
                   data-keyboard="false" data-backdrop="static" data-target="#edit-user"></i>`
})
export class EditUserComponent implements ICellRendererAngularComp {
  public params: any;

  agInit(params: any): void {
      this.params = params;
  }

  public invokeParentMethod() {
      this.params.context.componentParent.selectEditUser(this.params.data);
  }

  refresh(): boolean {
      return false;
  }
}

@Component({
  selector: 'delete-cell',
  template: `<i class="glyphicon glyphicon-trash txt-gap" (click)="invokeParentMethod()" data-toggle="modal" title="Delete User"
  data-keyboard="false" data-backdrop="static" data-target="#confirm-popup"></i>`
})
export class DeleteUserComponent implements ICellRendererAngularComp {
  public params: any;

  agInit(params: any): void {
      this.params = params;
  }

  public invokeParentMethod() {
      this.params.context.componentParent.deleteUser(this.params.data);
  }

  refresh(): boolean {
      return false;
  }
}


// con
@Component({
  selector: "user-dashboard",
  templateUrl: "./user-dashboard.template.html",
  styleUrls: ["./user-dashboard.styles.scss"],
  providers: [AdminService],
  encapsulation: ViewEncapsulation.None
})
export class UserDashboardPage implements OnInit, OnDestroy, AfterViewInit {
  public router: Router;
  public data: any[];
  public adminCount: number;

  public busyLoadMissingUsers: Subscription;
  public busyLoadUsers: Subscription;
  public busyDeleteUser: Subscription;

  public dropdownData = [];
  public showErrorFlag: boolean;
  public alerts: Array<Object>;
  public effectiveDate: Date;
  public selUser: User;
  public showCreateUser: any;
  public showEditUser: any;

  public delusertitle: string = "Delete User";
  public delusermessage: string = "Are you sure you want to delete user?";
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;

  public gridOptions: GridOptions;

  public gridApi;
  public gridColumnApi;

  public defaultColDef = {
    sortable: true,
    filter: true,
    headerClass: 'fw-semi-bold'
  };

  public columnDefs = [
    {
      headerName: "",
      field: "value",
      cellRendererFramework: EditUserComponent,
      colId: "params",
      width: 50,
      filter: false
    },
    {
      headerName: 'First Name',
      field: 'firstName',
      cellClass: function(params) {
        return params.data.enabled !== 'Y' ? "fw-semi-bold inactive" : "fw-semi-bold ";
      }
    },
    {
      headerName: 'Last Name',
      field: 'lastName',
      cellClass: function(params) {
        return params.data.enabled !== 'Y' ? "inactive" : "";
      }
    },
    {
      headerName: 'Login Id',
      field: 'email',
      cellClass: function(params) {
        return params.data.enabled !== 'Y' ? "inactive" : "";
      }
    },
    {
      headerName: 'Role',
      field: 'role',
      cellClass: function(params) {
        return params.data.enabled !== 'Y' ? "inactive" : "";
      }
    },
    {
      headerName: 'Active',
      field: 'enabled',
      cellClass: function(params) {
        return params.data.enabled !== 'Y' ? "inactive" : "";
      }
    },
    {
      headerName: "",
      field: "value",
      cellRendererFramework: DeleteUserComponent,
      colId: "params",
      width: 50,
      filter: false
    }
];


  constructor(private _adminService: AdminService, router: Router) {
    this.alerts = [
      {
        type: "success",
        msg:
          '<i class="fa fa-circle text-success"></i><span class="alert-text">Successfully generated 12 monthly reports</span>'
      }
    ];

    this.router = router;
    this.showCreateUser = false;
    this.showEditUser = false;

    this.gridOptions = <GridOptions>{
      context: {
          componentParent: this
      },
      enableColResize: true,
      rowHeight: 30
    };
  }

  ngOnInit() {
    let searchInput = jQuery("#table-search-input, #search-countries");
    searchInput
      .focus(e => {
        jQuery(e.target)
          .closest(".input-group")
          .addClass("focus");
      })
      .focusout(e => {
        jQuery(e.target)
          .closest(".input-group")
          .removeClass("focus");
      });

    // load the users
    this.loadUsers();
  }

  ngOnDestroy() {
    if (this.busyLoadMissingUsers) {
      this.busyLoadMissingUsers.unsubscribe();
    }
    if (this.busyLoadUsers) {
      this.busyLoadUsers.unsubscribe();
    }
    if (this.busyDeleteUser) {
      this.busyDeleteUser.unsubscribe();
    }
  }

  ngAfterViewInit() {
  }



  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  public cosmeticFix(elementID: any){
    // The code below for cosmetic fix - Blue box now surrounds the icon on the search company text box
    let filterInput = jQuery(elementID);
    filterInput
    .focus((e) => {
        jQuery(e.target).closest('.input-group').addClass('focus');
    })
    .focusout((e) => {
        jQuery(e.target).closest('.input-group').removeClass('focus');
    });
  }

  public loadUsers() {
    this.busyLoadUsers = this._adminService.getUsers().subscribe(users => {
      if (users) {
        this.data = users;
      }
    }, error => {
      this.showErrorFlag = true;
      this.alerts = [];
      this.alerts.push({ type: 'warning', msg: error });
    });
  }

  public reloadUsers(reload: boolean) {
    this.showCreateUser = false;
    this.showEditUser = false;
    if (reload) {
      this.loadUsers();
    } else {
      this.selUser = new User();
    }
  }

  public deleteUser(user: User) {
    if(!isNil(user)){
      this.busyDeleteUser = this._adminService.deleteUserById(user.userId).subscribe(data => {
        if (data) {
          this.reloadUsers(true);
        }
      }, error => {
        this.showErrorFlag = true;
        this.alerts = [];
        this.alerts.push({ type: 'warning', msg: error });
      });
    }

  }

  public addUser() {
    this.showCreateUser = true;
  }

  public selectEditUser(user) {
    this.showEditUser = true;
    this.selUser = user;
  }

  public refreshResourceFilterObject() {

  }

  public onClickBackButton(){
		this.router.navigate(["/app/bom/search"]);
  }
}
