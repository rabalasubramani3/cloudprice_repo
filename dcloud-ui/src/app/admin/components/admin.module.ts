import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { AuthorizationHelper } from "../../authentication/util/authorization.helper.util";
import { AuthService } from "../../login/services/auth.service";
import { SharedModule } from "../../shared/shared.module";
import { AdminService } from "../services/admin.service";
import { LookupService } from '../services/lookup.service';
import { RoleService } from "../services/role.service";
import { ClientFilterPipe } from "../util/client-filter.pipe";
import { LookupFilterPipe } from "../util/lookup-filter.pipe";
import { UserFilterPipe } from '../util/user-filter.pipe';
import { AccountDashboardPage, DeleteAccountComponent, EditAccountComponent } from './account/account-dashboard.component';
import { CreateAccountPopup } from "./account/create-account-popup.component";
import { EditAccountPopup } from "./account/edit-account-popup.component";
import { ClientDashboardPage, DeleteClientComponent, EditClientComponent } from './client/client-dashboard.component';
import { CreateClientPopup } from './client/create-client-popup.component';
import { EditClientPopup } from './client/edit-client-popup.component';
import { CreateLookupPopup } from './lookup/create-lookup-popup.component';
import { EditLookupPopup } from './lookup/edit-lookup-popup.component';
import { DeleteLookupComponent, EditLookupComponent, LookupDashboardPage } from './lookup/lookup-dashboard.component';
import { CreateUserPopup } from "./users/create-user-popup.component";
import { EditUserPopup } from "./users/edit-user-popup.component";
import { DeleteUserComponent, EditUserComponent, UserDashboardPage } from "./users/user-dashboard.component";

export const routes = [
	{ path: "users", component: UserDashboardPage },
	{ path: "lookup", component: LookupDashboardPage },
	{ path: "clients", component: ClientDashboardPage },
	{ path: "accounts", component: AccountDashboardPage },
];

@NgModule({
	imports: [CommonModule, SharedModule, RouterModule.forChild(routes)],
	declarations: [
		UserDashboardPage,
		ClientDashboardPage,
		LookupDashboardPage,
		CreateUserPopup,
		EditUserPopup,
		CreateLookupPopup,
		EditLookupPopup,
		CreateClientPopup,
		EditClientPopup,
		EditClientComponent,
		DeleteClientComponent,
		AccountDashboardPage,
		CreateAccountPopup,
		EditAccountPopup,
		EditAccountComponent,
		DeleteAccountComponent,
		EditUserComponent,
		DeleteUserComponent,
		EditLookupComponent,
    DeleteLookupComponent,
    LookupFilterPipe,
    ClientFilterPipe,
    UserFilterPipe
	],
	providers: [
		AuthorizationHelper,
		AuthService,
		AdminService,
		RoleService,
		LookupService
	],
	entryComponents: [
		EditClientComponent,
		DeleteClientComponent,
		EditAccountComponent,
		DeleteAccountComponent,
		EditUserComponent,
		DeleteUserComponent,
		EditLookupComponent,
		DeleteLookupComponent,
	]
})
export class AdminModule {
	static routes = routes;
}
