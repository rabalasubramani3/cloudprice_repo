/*
@author : Deloitte
Service class to perform client related operations.
*/

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from "rxjs";
import { catchError, tap } from 'rxjs/operators';
import { Client } from '../models/client.model';
import { environment } from '../../../environments/environment';

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};
/**
 *
 *
 * @export
 * @class ClientService
 */
@Injectable()
export class ClientService {

    private getClientUrl = environment.APIURL + '/api/v1/clients';
    private getTDLUrl = environment.APIURL + '/api/v1/tdl';

    constructor(private http: HttpClient) { }


    public saveClient(newClient: Client): Observable<Client> {
        return this.http.post(this.getClientUrl, newClient, httpOptions).pipe(
			tap(_ => this.log("saveClient")),
			catchError(this.handleError<any>("saveClient"))
		);
    }


    public getClient(id: number) {
        return this.http.get(this.getClientUrl + '/' + id, httpOptions).pipe(
			tap(_ => this.log("getClient")),
			catchError(this.handleError<any>("getClient"))
		);
    }

    public deleteClientById(id: number){
        return this.http.delete(this.getClientUrl + '/' + id, httpOptions).pipe(
			tap(_ => this.log("deleteClientById")),
			catchError(this.handleError<any>("deleteClientById"))
		);
    }


    public getAll(){
        return this.http.get(this.getTDLUrl + '/client/list', httpOptions).pipe(
			tap(_ => this.log("getAll")),
			catchError(this.handleError<any>("getAll"))
		);
    }



  	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}

}
