/*
@author : Deloitte
Service class to perform resource related operations.
*/

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from "rxjs";
import { catchError, tap } from 'rxjs/operators';
import { Lookup } from '../models/lookup.model';
import { environment } from '../../../environments/environment';

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};

/**
 *
 *
 * @export
 * @class LaborCategoryService
 */
@Injectable()
export class LookupService {

    private getLookupurl = environment.APIURL + '/api/v1/lookup';

    constructor(private http: HttpClient) { }

    public getAll(){
        return this.http.get(this.getLookupurl, httpOptions).pipe(
    			tap(_ => this.log("getAll")),
    			catchError(this.handleError<any>("getAll"))
    		);
    }

    public getAllByRefTypeId(refTypeId: number){
        return this.http.get(this.getLookupurl+ '/reftype/' + refTypeId, httpOptions).pipe(
  			tap(_ => this.log("getAllByRefTypeId")),
  			catchError(this.handleError<any>("getAllByRefTypeId"))
  		);
    }

    public getAllByRefTypeName(refTypeName: string){
      return this.http.get(this.getLookupurl+ '/reftypename/' + refTypeName, httpOptions).pipe(
      tap(_ => this.log("getAllByRefTypeName")),
      catchError(this.handleError<any>("getAllByRefTypeName"))
    );
   }

    public async getAllByRefTypeIdAsync(refTypeId: number): Promise<any[]>{
      return await this.http.get<any[]>(this.getLookupurl+ '/reftype/' + refTypeId, httpOptions).toPromise();
   }


    public getAllRefTypes(){
        return this.http.get(this.getLookupurl+ '/reftype', httpOptions).pipe(
			tap(_ => this.log("getAllRefTypes")),
			catchError(this.handleError<any>("getAllRefTypes"))
		);
    }

    public createLookup(lookup: Lookup): Observable<Lookup> {
        return this.http.post(this.getLookupurl, lookup, httpOptions).pipe(
			tap(_ => this.log("createLookup")),
			catchError(this.handleError<any>("createLookup"))
		);
    }

    public updateLookup(lookup: Lookup): Observable<Lookup> {
        return this.http.put(this.getLookupurl+ '/' + lookup.lookupId, lookup, httpOptions).pipe(
			tap(_ => this.log("updateLookup")),
			catchError(this.handleError<any>("updateLookup"))
		);
    }


    public deleteLookupById(id: number){
        return this.http.delete(this.getLookupurl + '/' + id, httpOptions).pipe(
			tap(_ => this.log("deleteLookupById")),
			catchError(this.handleError<any>("deleteLookupById"))
		);
    }

  	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}

}
