/*
@author : Deloitte
Service class to perform Admin related operations.
*/
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { User } from "../models/user.model";
import { environment } from '../../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { HttpHeaders, HttpClient } from '@angular/common/http';

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
}

/**
 *
 *
 * @export
 * @class AdminService
 */
@Injectable()
export class AdminService {

  private useradminurl = environment.APIURL + "/api/useradmin";
  private pipelineurl = environment.APIURL + "/api/v1/tdl";
  private getLaborCaturl = environment.APIURL + '/api/v1/laborcats';

  constructor(private http: HttpClient) { }

  public getUsers() {
    return this.http.get(this.useradminurl + '/users', httpOptions).pipe(
			tap(_ => this.log("getRoles")),
			catchError(this.handleError<any>("getRoles"))
		);
  }

  public getImportedPipelineProjects() {
    return this.http.get(this.pipelineurl + '/pipeline/list', httpOptions).pipe(
			tap(_ => this.log("getRoles")),
			catchError(this.handleError<any>("getRoles"))
		);
  }


  public getMissingUsers() {
    return this.http.get(this.useradminurl + '/users/missing', httpOptions).pipe(
			tap(_ => this.log("getRoles")),
			catchError(this.handleError<any>("getRoles"))
		);
  }

  public getResourceTypes(){
    return this.http.get(this.useradminurl + '/restypes', httpOptions).pipe(
			tap(_ => this.log("getRoles")),
			catchError(this.handleError<any>("getRoles"))
		);
  }

  public getResourceTeams(){
    return this.http.get(this.useradminurl + '/resteams', httpOptions).pipe(
			tap(_ => this.log("getRoles")),
			catchError(this.handleError<any>("getRoles"))
		);
  }

  public getPositionNames(){
    return this.http.get(this.useradminurl + '/posnames', httpOptions).pipe(
			tap(_ => this.log("getRoles")),
			catchError(this.handleError<any>("getRoles"))
		);
  }

  /**
   * Gets user object based on email address
   *
   * @param {String} email
   * @returns
   * @memberof AdminService
   */
  getUserbyEmail(email: String) {
    return this.http.post(this.useradminurl + '/retrieve', email, httpOptions).pipe(
			tap(_ => this.log("getRoles")),
			catchError(this.handleError<any>("getRoles"))
		);
  }

  /**
   * @param  {User} user
   * @returns Observable
   */
  createUser(user: User): Observable<User> {
    return this.http.post( this.useradminurl + '/user',  user, httpOptions).pipe(
			tap(_ => this.log("getRoles")),
			catchError(this.handleError<any>("getRoles"))
		);
  }

  /**
   *
   *
   * @param {any} id
   * @returns
   *
   * @memberOf adminService
   */
  getUser(id) {
    return this.http.get(this.useradminurl + "/user" + "/" + id, httpOptions).pipe(
			tap(_ => this.log("getRoles")),
			catchError(this.handleError<any>("getRoles"))
		);
  }

  /**
   *
   *
   * @param {User} user
   * @returns {Observable<User>}
   *
   * @memberOf AdminService
   */
  updateUser(user: User): Observable<User> {
    return this.http.put( this.useradminurl + "/user" + "/" + user.userId, user, httpOptions).pipe(
			tap(_ => this.log("getRoles")),
			catchError(this.handleError<any>("getRoles"))
		);
  }

  /**
   *
   *
   * @param {any} id
   * @returns {Observable<User>}
   * @memberof AdminService
   */
  deleteUserById(id): Observable<User> {
    return this.http.delete(this.useradminurl + "/user"  + "/" + id, httpOptions).pipe(
			tap(_ => this.log("getRoles")),
			catchError(this.handleError<any>("getRoles"))
		);
  }
  	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}
}
