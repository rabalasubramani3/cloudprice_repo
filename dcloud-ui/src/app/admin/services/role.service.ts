/*
@author : Deloitte
Service class to perform Role related operations.
*/
import { Injectable } from '@angular/core';
import { Observable, of } from "rxjs";
import { Role } from '../models/role.model';
import { tap, catchError } from 'rxjs/operators';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
}
/**
 *
 *
 * @export
 * @class RoleService
 */
@Injectable()
export class RoleService {

    private getrolesurl = environment.APIURL + '/api/useradmin/roles';

    constructor(
        private http: HttpClient
    ) { }

    getRoles(): Observable<Role[]> {
        return this.http.get(this.getrolesurl, httpOptions).pipe(
			tap(_ => this.log("getRoles")),
			catchError(this.handleError<any>("getRoles"))
		);
    }

  	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}

}
