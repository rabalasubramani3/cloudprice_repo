/*
@author : Deloitte
Service class to perform account related operations.
*/

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from "rxjs";
import { catchError, tap } from 'rxjs/operators';
import { Account } from '../models/account.model';
import { environment } from '../../../environments/environment';

const httpOptions = {
	headers: new HttpHeaders({ "Content-Type": "application/json" })
};
/**
 *
 *
 * @export
 * @class AccountService
 */
@Injectable()
export class AccountService {

    private getAccountUrl = environment.APIURL + '/api/v1/accounts';
    private getTDLUrl = environment.APIURL + '/api/v1/tdl';

    constructor(private http: HttpClient) { }


    public saveAccount(newAccount: Account): Observable<Account> {
        return this.http.post(this.getAccountUrl, newAccount, httpOptions).pipe(
			tap(_ => this.log("saveAccount")),
			catchError(this.handleError<any>("saveAccount"))
		);
    }


    public getAccount(id: number) {
        return this.http.get(this.getAccountUrl + '/' + id, httpOptions).pipe(
			tap(_ => this.log("getAccount")),
			catchError(this.handleError<any>("getAccount"))
		);
    }

    public deleteAccountById(id: number){
        return this.http.delete(this.getAccountUrl + '/' + id, httpOptions).pipe(
			tap(_ => this.log("deleteAccountById")),
			catchError(this.handleError<any>("deleteAccountById"))
		);
    }

    public getAll(){
        return this.http.get(this.getTDLUrl + '/account/list', httpOptions).pipe(
			tap(_ => this.log("getAll")),
			catchError(this.handleError<any>("getAll"))
		);
	}
	public loadIndustries(){
        return this.http.get(this.getTDLUrl + '/industry/list', httpOptions).pipe(
			tap(_ => this.log("loadIndustries")),
			catchError(this.handleError<any>("loadIndustries"))
		);
    }

    public loadSectorsForIndustry(industryId: number){
        return this.http.get(this.getTDLUrl + '/sector/list/' + industryId, httpOptions).pipe(
			tap(_ => this.log("loadSectorsForIndustry")),
			catchError(this.handleError<any>("loadSectorsForIndustry"))
		);
    }

  	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T>(operation = "operation", result?: T) {
		return (error: any): Observable<T> => {
			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			this.log("${operation} failed: ${error.message}");

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a Attribute Service message with the MessageService */
	private log(message: string) {
		//this.messageService.add("Profile Service: ${message}");
	}

}
