export class CustomNumberValidator {
  private _regexNumber = /^\d+(,\d+)*$/;

    public validate(value) : boolean{
           return this._regexNumber.test(value);
    }

}
