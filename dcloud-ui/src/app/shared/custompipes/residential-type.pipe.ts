import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "residentialtype" })
export class ResidentialTypePipe implements PipeTransform {
  retstatus: string = "NA";
  transform(value: number): string {
    switch (value) {
      case 1:
        this.retstatus = "Apartment Building";
        break;

      case 2:
        this.retstatus = "Single Family";
        break;

      case 3:
        this.retstatus = "Mobile Homes";
        break;

      case 4:
        this.retstatus = "Other";
        break;

      default:
        return "NA";
    }
    return this.retstatus;
  }
}
