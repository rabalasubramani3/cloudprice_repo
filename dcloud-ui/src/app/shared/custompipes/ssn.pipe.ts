import { Pipe, PipeTransform } from "@angular/core";
/*
 * Format the SSN  as xxx-xx-xxxx
 * Takes permit number and formats it.
 * Usage:
 *   value | ssnPipe
 * Example:
 *   {{ xxxxxxxxxx |  ssnPipe}}
 *   formats to: xxx-xx-xxxx
*/
@Pipe({ name: "ssnPipe" })
export class SSNPipe implements PipeTransform {
  transform(value: string, countryCode: string): string {
    let formattedValue = "";
    if (!value) {
      return value;
    }
    let ssn = value.toString();
    let plainStr = ssn.replace(/-/g, "");
    if (plainStr.length === 9) {
      formattedValue = plainStr.substring(0, 3) + "-" +  plainStr.substring(3, 5) +  "-" +  plainStr.substring(5);
    } else {
      formattedValue = value;
    }
    return formattedValue;
  }
}
