import { isNil } from 'ramda';
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "homesteadStatusPipe" })
export class HomesteadStatusPipe implements PipeTransform {

  constructor() {
  }

  transform(value: string): string {
    let status:string = "Draft";
    if(!isNil(value)) {
        let firstChar = value.charAt(0);
        if(firstChar === "N") {
            status = "Draft";
        }
        else if (firstChar === "P") {
            status = "Pending";
        }
        else if (firstChar === "A") {
            status = "Approved";
        }
        else if (firstChar === "R") {
            status = "Rejected";
        }
    }
    return status;
  }
}
