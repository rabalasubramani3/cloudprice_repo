import { isNil } from 'ramda';
import { Pipe, PipeTransform } from "@angular/core";
import * as _ from 'lodash';

@Pipe({ name: "propaccnumPipe" })
export class PropertyAccountNumberPipe implements PipeTransform {
  test(){
    let str = "999999999"; // 99-9999999;
    let val = str.slice(0, 2) + '-' + str.slice(2 + Math.abs(0));
    console.log("str = " + str + " val = " + val + " result = " + val);
    console.log((val === "99-9999999") ? "OK" : "False");

    str = "9999999999"; // "99-99999999"
    val = str.slice(0, 2) + '-' + str.slice(2 + Math.abs(0));
    console.log("str = " + str + " val = " + val + " result = " + val);
    console.log((val === "99-99999999") ? "OK" : "False");      

    str = "999999999999"; // "99-9999999999"
    val = str.slice(0, 2) + '-' + str.slice(2 + Math.abs(0));
    console.log("str = " + str + " val = " + val + " result = " + val);
    console.log((val === "99-9999999999") ? "OK" : "False");   
    
    str = "9999999999999"; // "99-999-99999999"
    val = str.slice(0, 2) + '-' + str.slice(2,5) + '-' + str.slice(5);
    console.log("str = " + str + " val = " + val + " result = " + val);
    console.log((val === "99-999-99999999") ? "OK" : "False"); 

    str = "99999999X999X"; // "99-99-9999X-999X"
    val = str.slice(0, 2) + '-' + str.slice(2,4) + '-' + str.slice(4,9) + '-' + str.slice(9);
    console.log("str = " + str + " val = " + val + " result = " + val);
    console.log((val === "99-99-9999X-999X") ? "OK" : "False");   
  }

  transform(value: string, countyName: string): string {
    if(isNil(countyName) || isNil(value)){
      return value;
    }
    switch(countyName){
      case "01":
      case "05":
      case "06":
      case "07":
      case "08":                        
      case "09":                        
      case "10":                        
      case "11":                        
      case "12":                        
      case "13":                                                      
      case "14":                        
      case "15":                        
      case "18":                        
      case "19":                                                      
      case "20":                        
      case "21":                                                      
      case "22":                        
      case "23":                                                      
      case "24":  
        if(value.length === 8){
          let str = value;
          value = str.slice(0, 2) + '-' + str.slice(2 + Math.abs(0));
        }                                                                
        break;

      case "17": // 2-7
        if(value.length === 9){
          let str = value;
          value = str.slice(0, 2) + '-' + str.slice(2 + Math.abs(0));
        }
        break;

      case "16": // 2-8
        if(value.length === 10){
          let str = value;
          value = str.slice(0, 2) + '-' + str.slice(2 + Math.abs(0));
        }
        break;   
        
      case "04": // 2-10
        if(value.length === 12){
          let str = value;
          value = str.slice(0, 2) + '-' + str.slice(2 + Math.abs(0));
        }
        break; 
        
      case "02": // 2-3-8
        if(value.length === 13){
          let str = value;
          value = str.slice(0, 2) + '-' + str.slice(2,5) + '-' + str.slice(5);
        }
        break; 
        
      case "03": // 2-2-4[x]-3[x]
        if(value.length === 17){
          let str = value;
          value = str.slice(0, 2) + '-' + str.slice(2,4) + '-' + str.slice(4,9) + '-' + str.slice(9);
        }
        break;   
        
      default:
        break;
    }
    return value;
  }
}
