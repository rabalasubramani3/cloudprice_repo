import { Pipe, PipeTransform } from '@angular/core';
import { isNil } from 'ramda';

@Pipe({name: 'minuteSeconds', pure: false})
export class MinuteSecondsPipe implements PipeTransform {
    transform(value: number): string {
        if(isNil(value)){
            return "";
        }
        const minutes: number = Math.floor(value / 60);
        return minutes.toString().padStart(2, '0') + ':' + 
            (value - minutes * 60).toString().padStart(2, '0');
     }
}
