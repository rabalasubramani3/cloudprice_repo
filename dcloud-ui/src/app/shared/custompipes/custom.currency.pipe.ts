import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

/*
 * Changes input value to currency format
*/

//const _NUMBER_FORMAT_REGEXP = /^(\d+)?\.((\d+)(-(\d+))?)?$/;
const _NUMBER_FMT_REGEX = /^(?:\d*\.\d{1,2}|\d+)$/;
const _BAD_VALUE = /^[.,]{2,}$/;

@Pipe({name: 'asmatcurrency'})
export class AsmatCurrencyPipe implements PipeTransform {

    constructor(private currencyPipe:CurrencyPipe) {};
    
    transform(value: any,args:any[]): string {
        
        if(!value && value!==0){return " ";}

        let val= String(value).trim();
        if(value ==='.' || value ==="," || _BAD_VALUE.test(val))
            return value;
        if (typeof value === 'number' ||  _NUMBER_FMT_REGEX.test(val)) {
            return this.currencyPipe.transform(value, 'USD', true, '1.2-2');
        } else {
            return value;
        }        
    }
}