import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "resourceStatusPipe" })
export class ResourceStatusPipe implements PipeTransform {

  constructor() {
  }

  transform(value: string): string {
      if (value === "ACTV" || value === "Active") {
        return("Active");
      } else {
        return ("Inactive");
      }
  }
}
