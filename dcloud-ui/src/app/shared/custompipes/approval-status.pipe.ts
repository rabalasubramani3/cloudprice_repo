import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "approvalStatusPipe" })
export class ApprovalStatusPipe implements PipeTransform {

  constructor() {
  }

  transform(value: string): string {
      let status:string = "";
      if (value === "APRV") {
        status = "Approved";
      } else if (value === "UNAP") {
        status = "Not Approved";
      } 
      return status;
  }
}
