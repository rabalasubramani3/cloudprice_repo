import { isNil } from 'ramda';
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "applicantStatusPipe" })
export class ApplicantStatusPipe implements PipeTransform {

  constructor() {
  }

  transform(value: string): string {
      let status:string = "Draft";
      if(!isNil(value)){
        if (value.toUpperCase() === "S") {
          status = "Submitted";
        } else if (value.toUpperCase()  === "T") {
          status = "Draft";
        } else if (value.toUpperCase()  === "C") {
          status = "Approved";
        } else if (value.toUpperCase()  === "D") {
          status = "Denied";
        }  else if (value.toUpperCase()  === "L") {
          status = "Pending Review";
        } else if (value.toUpperCase()  === "E") {
          status = "Completed";
        } else if (value.toUpperCase()  === "F") {
          status = "Pending Review";
        } else if (value.toUpperCase()  === "M") {
          status = "Pending Review";
        } else if (value.toUpperCase()  === "O") {
          status = "Pending Review";
        } else if (value.toUpperCase()  === "U") {
          status = "Unable to Process";
        } else if (value.toUpperCase()  === "Z") {
          status = "Pending Review";
        } else if (value.toUpperCase()  === "A") {
          status = "Audit";
        }
      }
      return status;
  }
}
