import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "maritalstatus" })
export class MaritalStatusPipe implements PipeTransform {
  retstatus: string = "NA";
  transform(value: number): string {
    switch (value) {
      case 1:
        this.retstatus = "Single";
        break;

      case 2:
        this.retstatus = "Married";
        break;

      case 3:
        this.retstatus = "Divorced";
        break;

      case 4:
        this.retstatus = "Widowed";
        break;

      case 5:
        this.retstatus = "Separated";
        break;

      default:
        return "NA";
    }
    return this.retstatus;
  }
}
