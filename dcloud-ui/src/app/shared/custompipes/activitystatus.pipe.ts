import { isNil } from 'ramda';
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "activityStatusPipe" })
export class ActivityStatusPipe implements PipeTransform {

  constructor() {
  }

  transform(value: string): string {
      let status:string = "-";
      if(!isNil(value)){
        if (value.toUpperCase() === "T") {
          status = "Draft";
        } else if (value.toUpperCase()  === "C") {
          status = "Completed";
        } else if (value.toUpperCase()  === "D") {
          status = "Denied";
        }  else if (value.toUpperCase() === "L") {
          status = "Additional Details Needed";
        } else if (value.toUpperCase()  === "S") {
          status = "Submitted";
        } else if (value.toUpperCase()  === "R") {
          status = "Received";
        } else if (value.toUpperCase()  === "E") {
          status = "Extracted";
        } else if (value.toUpperCase()  === "F") {
          status = "Flagged";
        } else if (value.toUpperCase()  === "M") {
          status = "Manual";
        } else if (value.toUpperCase()  === "O") {
          status = "Overpayment";
        } else if (value.toUpperCase()  === "U") {
          status = "Unable to process";
        } else if (value.toUpperCase()  === "A") {
          status ="Audit";
        }
      }
      return status;
  }
}

