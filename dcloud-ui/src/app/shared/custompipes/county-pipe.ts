import { isNil } from 'ramda';
import { Pipe, PipeTransform } from "@angular/core";
import * as _ from 'lodash';

@Pipe({ name: "countyPipe" })
export class CountyPipe implements PipeTransform {
  countyNames = [
    { value: "", name: "" },
    { value: "01", name: "Allegany" },
    { value: "02", name: "Anne Arundel" },
    { value: "03", name: "Baltimore City" },
    { value: "04", name: "Baltimore County" },
    { value: "05", name: "Calvert" },

    { value: "06", name: "Caroline" },
    { value: "07", name: "Carroll" },
    { value: "08", name: "Cecil" },
    { value: "09", name: "Charles" },
    { value: "10", name: "Dorchester" },

    { value: "11", name: "Frederick" },
    { value: "12", name: "Garrett" },
    { value: "13", name: "Harford" },
    { value: "14", name: "Howard" },
    { value: "15", name: "Kent" },

    { value: "16", name: "Montgomery" },
    { value: "17", name: "Prince George's" },
    { value: "18", name: "Queen Anne's" },
    { value: "19", name: "St. Mary's" },
    { value: "20", name: "Somerset" },

    { value: "21", name: "Talbot" },
    { value: "22", name: "Washington" },
    { value: "23", name: "Wicomico" },
    { value: "24", name: "Worcester" }
  ];

  transform(value: string, args: string[]): string {
    let retCountyName = "";
    if (isNil(value)) {
      return value;
    }
    retCountyName = value;
    let rowIndex = _.findIndex(this.countyNames, function (o) { return (o.value && o.value === value)});
    if(rowIndex !== -1) {
        let selRow = this.countyNames[rowIndex];
        if(!isNil(selRow)){
            retCountyName = selRow.value + "-" + selRow.name;
        }
    }
    return retCountyName;
  }
}
