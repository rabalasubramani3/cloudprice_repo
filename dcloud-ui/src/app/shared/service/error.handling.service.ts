import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs';

/**
 *
 *
 * @export
 * @class ErrorHandlingService
 */
@Injectable()
export class ErrorHandlingService {
  // Observable string sources
  private form3852ChangedSource = new Subject<Event>();
  private formsChangedSource = new Subject<Event>();


  private impFormChangedSource = new Subject<Event>();

  // Observable string streams for mfg form
  // tslint:disable-next-line:member-ordering
  form3852Changed$ = this.form3852ChangedSource.asObservable();
  formsChanged$ =  this.formsChangedSource.asObservable();

 // Observable string streams for importer form
 impFormChanged$= this.impFormChangedSource.asObservable();

 // source for manufacturing form
 public form3852Changed(event) {
      this.form3852ChangedSource.next(event);
 }

  public formsChanged(event) {
      this.formsChangedSource.next(event);
  }

  /** source for importer form */
  public impFormChanged(event) {
        this.impFormChangedSource.next(event);
  }

}

