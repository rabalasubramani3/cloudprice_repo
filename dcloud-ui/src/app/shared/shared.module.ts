import { CommonModule, CurrencyPipe, TitleCasePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { QueryBuilderModule } from "angular2-query-builder";
import { TextMaskModule } from 'angular2-text-mask';
import { NgBusyModule } from 'ng-busy';
import { CustomFormsModule } from 'ng2-validation';
import { AccordionModule, BsDatepickerModule, ButtonsModule, ProgressbarModule, TabsModule, TooltipModule } from 'ngx-bootstrap';
import { AlertModule } from 'ngx-bootstrap/alert';
import { FileDropModule } from 'ngx-file-drop';
import { FileSizeModule } from 'ngx-filesize';
import { LightboxModule } from 'ngx-lightbox';
import { MomentModule } from 'ngx-moment';
import { PapaParseModule } from 'ngx-papaparse';
import { NgxSummernoteModule } from 'ngx-summernote';
import { Ng2Webstorage } from 'ngx-webstorage';
import { WidgetModule } from './../layout/widget/widget.module';
import { ConfirmPopupPh } from './confirmpopup/confirm-popup.ph.component';
import { InfoBoxPopup } from './confirmpopup/infobox.component';
import { AbsoluteValuePipe } from './custompipes/absolute-value.pipe';
import { ActivityStatusPipe } from './custompipes/activitystatus.pipe';
import { ApplicantStatusPipe } from './custompipes/applicant-status.pipe';
import { ApprovalStatusPipe } from './custompipes/approval-status.pipe';
import { CountyPipe } from './custompipes/county-pipe';
import { AsmatCurrencyPipe } from './custompipes/custom.currency.pipe';
import { DateAgoPipe } from './custompipes/date-ago.pipe';
import { DatePipe } from './custompipes/date.pipe';
import { HomesteadStatusPipe } from './custompipes/homestead-status.pipe';
import { EscapeHtmlPipe } from './custompipes/keep-html.pipe';
import { LoopIndexPipe } from './custompipes/loopindex.pipe';
import { MaritalStatusPipe } from './custompipes/marital-status.pipe';
import { MaskSSNPipe } from './custompipes/mask-ssn.pipe';
import { MinusToParensPipe } from './custompipes/minus.parentheses.pipe';
import { MinuteSecondsPipe } from './custompipes/minute-seconds.pipe';
import { PhoneNumberPipe } from './custompipes/phone-number.pipe';
import { PropertyAccountNumberPipe } from './custompipes/propaccnum.pipe';
import { RemoveHtmlPipe } from './custompipes/remove-html.pipe';
import { RemoveHyphen } from './custompipes/removehyphen.pipe';
import { ReportStatusPipe } from './custompipes/report-status.pipe';
import { ResidentialTypePipe } from './custompipes/residential-type.pipe';
import { ResourceStatusPipe } from './custompipes/resource-status.pipe';
import { SSNPipe } from './custompipes/ssn.pipe';
import { ThousandSuffixesPipe } from './custompipes/thousand-suffix.pipe';
import { TitleCase } from './custompipes/titlecase.pipe';
import { TruncatePipe } from './custompipes/truncate.pipe';
import { CustomValidatorDirective } from './customvalidators/custom.validator.directive';
import { NgTableFilteringDirective } from './datatable/datatable.filtering.directive';
import { DataTableModule } from './datatable/DataTableModule';
import { NKDatetimeModule } from './datetime/ng2-datetime-module';
import { CustomAnchorClickDirective } from './globalDirectives/anchorClick';
import { ChecklistDirective } from './globalDirectives/checklist';
import { ConfirmDirective } from './globalDirectives/confirm.directive';
import { CustomPercentDirective } from './globalDirectives/custom.percent';
import { FocusDirective } from './globalDirectives/focus.directive';
import { FocusOutDirective } from './globalDirectives/focusOut.directive';
import { CustomNumericsDirective } from './globalDirectives/numerics';
import { ErrorHandlingService } from './service/error.handling.service';
import { CustomIntegerDirective } from './util/custom-integer';
import { CustomCurrencyFormatterDirective } from './util/custom.currency.formatter';
import { CustomCurrencyPipe } from './util/custom.currency.pipe';
import { CustomDecimalFormatterDirective } from './util/custom.decimal.formatter';
import { CustomNumberFormatterDirective } from './util/custom.number.formatter';
import { CustomNumberPipe } from './util/custom.number.pipe';
import { CustomNumberVaidatorDirective } from './util/custom.number.validator';
import { MDWizardStepComponent } from './wizard/wizard-step.component';
import { MDWizardComponent } from './wizard/wizard.component';

@NgModule({
  imports: [ CommonModule, FormsModule, TabsModule, AccordionModule, ButtonsModule, BsDatepickerModule.forRoot(),
    NgxSummernoteModule, TooltipModule.forRoot(), TextMaskModule, CustomFormsModule, AlertModule.forRoot(),
    ProgressbarModule.forRoot(), AgGridModule.withComponents([]),	QueryBuilderModule, FileDropModule, MomentModule, PapaParseModule,
    LightboxModule,
    Ng2Webstorage, NgBusyModule, WidgetModule, NKDatetimeModule, FileSizeModule, ConfirmationPopoverModule.forRoot({
      focusButton: 'confirm'
    })],
  declarations: [ PhoneNumberPipe, ReportStatusPipe, ResourceStatusPipe, ApplicantStatusPipe, HomesteadStatusPipe,
            TitleCase, FocusDirective, DatePipe, CustomNumericsDirective, ConfirmPopupPh, CustomDecimalFormatterDirective,
            MaritalStatusPipe, ResidentialTypePipe, EscapeHtmlPipe, RemoveHtmlPipe, CustomValidatorDirective,
            CustomCurrencyPipe, CustomCurrencyFormatterDirective, ActivityStatusPipe, MaskSSNPipe,
            CustomNumberFormatterDirective, CustomNumberPipe, AbsoluteValuePipe, CustomNumberVaidatorDirective,
            CustomPercentDirective, CountyPipe, ThousandSuffixesPipe, DateAgoPipe,
            CustomIntegerDirective, RemoveHyphen, CustomAnchorClickDirective, FocusOutDirective, LoopIndexPipe,
            AsmatCurrencyPipe, MinusToParensPipe, MinuteSecondsPipe,
            NgTableFilteringDirective, InfoBoxPopup, ApprovalStatusPipe, ConfirmDirective, SSNPipe, PropertyAccountNumberPipe,
            TruncatePipe, ChecklistDirective, MDWizardComponent, MDWizardStepComponent  ],
  exports: [ CommonModule, DataTableModule, AlertModule, FormsModule, TabsModule, AccordionModule, NgxSummernoteModule, FileSizeModule,
      ButtonsModule, TooltipModule, NKDatetimeModule, TextMaskModule, CustomFormsModule, WidgetModule, QueryBuilderModule,
      BsDatepickerModule, SSNPipe, CountyPipe, ProgressbarModule, AgGridModule, FileDropModule, MomentModule, PapaParseModule,
      LightboxModule,
        ConfirmationPopoverModule, PhoneNumberPipe, NgBusyModule, ReportStatusPipe, ResourceStatusPipe, TitleCase, RemoveHyphen,
      MaritalStatusPipe, ResidentialTypePipe, ApplicantStatusPipe, ActivityStatusPipe, MaskSSNPipe, HomesteadStatusPipe,
      FocusDirective, DatePipe, AbsoluteValuePipe, CustomNumericsDirective, ConfirmPopupPh, CustomDecimalFormatterDirective,
      CustomNumberFormatterDirective,
      CustomNumberPipe, CustomNumberVaidatorDirective, CustomIntegerDirective, CustomPercentDirective, CustomAnchorClickDirective,
      FocusOutDirective, LoopIndexPipe, AsmatCurrencyPipe, MinusToParensPipe, NgTableFilteringDirective, InfoBoxPopup,
      PropertyAccountNumberPipe, ThousandSuffixesPipe, MinuteSecondsPipe, DateAgoPipe,
      ApprovalStatusPipe, TruncatePipe, ChecklistDirective, MDWizardComponent, MDWizardStepComponent ],
  providers: [ ErrorHandlingService, CurrencyPipe, TitleCasePipe ]
})

// https://angular.io/docs/ts/latest/guide/ngmodule.html#!#shared-module
export class SharedModule {
}
