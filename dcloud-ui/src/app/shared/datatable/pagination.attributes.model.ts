declare let jQuery: any;

export class PaginationAttributes {
    activePage: number;
    pageSize: number;
    itemsTotal: number;
    calculateTotalRows: boolean;
    sortBy: string;
    sortOrder: string;

    constructor(activePage, pageSize, totalRows?, calculateTotalRows?, sortBy?, sortOrder?) {
        this.activePage = activePage;
        this.pageSize = pageSize;
        this.itemsTotal = totalRows;
        this.calculateTotalRows = calculateTotalRows;
        this.sortBy = sortBy;
        this.sortOrder = sortOrder;
    }

    getQueryParamString() {

        let params = { "activePage": this.activePage, 
                        "pageSize": this.pageSize, 
                        //"itemsTotal": this.itemsTotal, 
                        //"calculateTotalRows": this.calculateTotalRows,
                        "sortBy": this.sortBy,
                        "sortOrder": this.sortOrder }

        return jQuery.param(params);
    }
}