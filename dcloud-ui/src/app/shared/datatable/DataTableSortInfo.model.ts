export class DataTableSortInfo{
    searchBy: string;
    sortOrder: string;
    sortBy: string;
    pageStart: number;

    constructor(_searchby: string, _sortorder:string, _sortby:string, _pagestart:number){
        this.searchBy = _searchby;
        this.sortOrder = _sortorder;
        this.sortBy = _sortby;
        this.pageStart = _pagestart;
    }
}