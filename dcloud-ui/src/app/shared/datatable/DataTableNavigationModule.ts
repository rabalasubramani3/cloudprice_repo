import { NgModule, OnInit } from "@angular/core";
// import { HotkeyModule, HotkeysService, Hotkey } from 'angular2-hotkeys';
import { Router, NavigationExtras } from '@angular/router';

declare var jQuery: any;

// @NgModule({
//     imports: [
//         // HotkeyModule
//     ]
// })
export class DataTableNavigationModule  {

    currTableIndex: number = 0;
    tables;

    constructor(private router: Router) {

        /** Register hot keys for navigating data grid **/

        // this._hotkeysService.add(new Hotkey('shift+t', (event: KeyboardEvent): boolean => {

        //     //check if any table cell is active if not set this.currTableIndex to 0
        //     let currFocused = jQuery(document.activeElement);
        //     let parent = (currFocused.prop('tagName') === 'TD')?currFocused:currFocused.closest('td');
        //     if (!parent && parent.length === 0) this.currTableIndex=0;

        //     //select all tables that are visible
        //     this.tables = jQuery('table:visible');
        //     let currentTable: JQuery;
        //     if (this.tables && this.tables.length > 0) {
        //         if (this.tables.get(this.currTableIndex)) {
        //             currentTable = this.tables.get(this.currTableIndex);
        //             ++this.currTableIndex;
        //         } else {
        //             currentTable = this.tables.get(0);
        //             ++this.currTableIndex
        //         }
        //     }

        //     if (!currentTable) return;

        //     let firstCell = jQuery(currentTable).find('tr:nth-child(1) td:first');

        //     if (firstCell && firstCell.length > 0) {
        //         firstCell.focus();
        //     }
        //     return false;
        // }));

        // this._hotkeysService.add(new Hotkey('ctrl+alt+right', (event: KeyboardEvent): boolean => {

        //     let currFocused = jQuery(document.activeElement);
        //     let parent;
        //     if (currFocused.prop('tagName') === 'TD')
        //         parent = currFocused;
        //     else
        //         parent = currFocused.closest('td');

        //     if (parent.prop('tagName') !== 'TD') return;
        //     //move focus to sibling on the right
        //     let c = parent.next();
        //     if (c.length > 0) {
        //         c.focus();
        //     }
        //     return false;
        // }));

        // this._hotkeysService.add(new Hotkey('ctrl+alt+left', (event: KeyboardEvent): boolean => {

        //     let currFocused = jQuery(document.activeElement);

        //     let parent;
        //     if (currFocused.prop('tagName') === 'TD')
        //         parent = currFocused;
        //     else
        //         parent = currFocused.closest('td');

        //     if (parent.prop('tagName') !== 'TD') return;
        //     //move focus to sibling on the left
        //     let c = parent.prev();
        //     if (c.length > 0) {
        //         c.focus();
        //     }

        //     return false; // Prevent bubbling
        // }));

        // this._hotkeysService.add(new Hotkey('ctrl+alt+up', (event: KeyboardEvent): boolean => {

        //     let currFocused = jQuery(document.activeElement);

        //     let parent;
        //     if (currFocused.prop('tagName') === 'TD')
        //         parent = currFocused;
        //     else
        //         parent = currFocused.closest('td');

        //     let c;
        //     if (parent.prop('tagName') !== 'TD') return;
        //     //move focus to cell above
        //     c = parent.closest('tr').prev().find('td:eq(' +
        //         parent.index() + ')');
        //     if (c.length > 0) 
        //         c.focus();

        //     return false; // Prevent bubbling
        // }));

        // this._hotkeysService.add(new Hotkey('ctrl+alt+down', (event: KeyboardEvent): boolean => {

        //     let currFocused = jQuery(document.activeElement);
        //     let parent;

        //     if (currFocused.prop('tagName') === 'TD')
        //         parent = currFocused;
        //     else
        //         parent = currFocused.closest('td');
        //     let c;
        //     if (parent.prop('tagName') !== 'TD') return;
        //     //move focus to cell below 
        //     c = parent.closest('tr').next().find('td:eq(' +
        //         parent.index() + ')');
        //     if (c.length > 0) 
        //         c.focus();

        //     return false; // Prevent bubbling
        // }));
    }


}

