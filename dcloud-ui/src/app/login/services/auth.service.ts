import { isNil } from 'ramda';
import { saveAs } from 'file-saver';
import { VerifyEmail } from './../model/verifyemail.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of, from, throwError } from 'rxjs';
import { Subject, Subscription, timer } from 'rxjs';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { map, tap, catchError } from 'rxjs/operators';
import Amplify, { Auth, Storage } from 'aws-amplify';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { RegistrationUser } from '../components/register/registration.model';
import { ProfileUser } from '../model/profile-user.model';
import { LoginState } from '../model/login-state.model';
import { VerifyUsername } from '../model/verifyusername.model';


const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

const textOptions = {
  headers: new HttpHeaders({ "Content-Type": "text" })
};

@Injectable()
export class AuthService {
  accessToken:string = null;
  emails:string = null;
  credentialPromise: Promise<any>;
  private loginState: LoginState = new LoginState();
  public email_verified: boolean = true;
  public loggedIn: BehaviorSubject<boolean>;
  private apiVerifyURL =  environment.V1URL + '/rtc/bom/verifyemail';
  private apiForgotUsername = environment.V1URL + '/rtc/bom/forgotuser';
  /* session timeout */
  public expiring: Subject<boolean> = new Subject<boolean>();
  private expiringSubscription: Subscription = null;
  private expiredSubscription: Subscription = null;
  private approvedProcessor:Observable<boolean> = null;

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    Amplify.configure(environment.amplify);
    this.loggedIn = new BehaviorSubject<boolean>(false);
  }

  /** signup */
  public signUp(newUser:RegistrationUser): Observable<any> {
    this.loginState.signUp(newUser);
    return from(Auth.signUp({
      'username': newUser.username, // was username
      'password': newUser.password,
      attributes: {
        'email': newUser.email,
        'custom:estb_id': newUser.estb_id
      }
    }));
  }

  public updateUser(user: ProfileUser, awsUser:any) : Observable<any> {
      return from(Auth.updateUserAttributes(awsUser, {
        'email': user.EMAIL_ADDRESS,
        'custom:estb_id': user.ESTB_ID
      }));
  }

  /* These two calls are for passing user information */
  /* During registration only */
  public getLoginState(): LoginState {
    return this.loginState;
  }

  /* Amplify / Cognito Session */
  public session() :Observable<any>{
    return from(Auth.currentSession());
  }

  /** confirm code */
  public confirmSignUp(username, code): Observable<any> {
    return from(Auth.confirmSignUp(username, code));
  }

  /** verify change in email with code */
  public confirmVerification(code: string) {
    return from(Auth.verifyCurrentUserAttributeSubmit('email', code));
  }

  public updateUsername(user, username) {
    return from(Auth.updateUserAttributes(user, {'preferred_username': username}));
  }

  public updateUserEmail(user, email) {
    return from(Auth.updateUserAttributes(user, {'email': email}));
  }

  /** forgot Password */
  public forgotPassword(email): Observable<any> {
    return from(Auth.forgotPassword(email));
  }

  /** forgot Password submit*/
  public forgotPasswordSubmit(email, code, new_password): Observable<any> {
    return from(Auth.forgotPasswordSubmit(email, code, new_password));
  }

  /** forgot username submit*/
  public forgotUsername(email): Observable<any> {
    let objVerify:VerifyUsername = new VerifyUsername(email);
    return this.http.post(this.apiForgotUsername, objVerify, httpOptions)
    .pipe(
        tap(_ => this.log('forgotusername id= ${email}')),
        catchError(this.handleError<any>("forgotusername"))
    );
  }

  public getAuthenticatedUser(){
    return from(Auth.currentAuthenticatedUser());
  }

  public resendVerificationCode(username): Observable<any> {
    throwError("NOPE"); // TODO
    // if(!this.registerUser) {
    //   this.registerUser = new RegistrationUser();
    //   this.registerUser.username = username;
    // }
    return from(Auth.resendSignUp(username));
  }

  // public async getAuthenticatedUserP(){
  //   try {
  //     const { favorite_flavor } = await Auth.currentUserInfo();
  //   } catch (err) {
  //     console.log('error fetching user info: ', err);
  //   }
  // }

  /** signin */
  public signIn(email, password): Observable<any> {
    this.resetSessionTimeout();
    return from(Auth.signIn(email, password))
      .pipe(
        tap(() => {
          this.loggedIn.next(true);
        })
      );
  }

  public verifyemail(email): Observable<any> {
    let objVerifyEmail: VerifyEmail = new VerifyEmail();
    objVerifyEmail.EmailAddress = email;
    return this.http.post(this.apiVerifyURL, objVerifyEmail, httpOptions)
    .pipe(
        tap(_ => this.log('verifyemail id= ${email}')),
        catchError(this.handleError<any>("verifyemail"))
    );
  }


  public upload(objKey, file, options): Observable<any> {
    return from(Storage.put(objKey, file, options));
  }

  public upload_p(objKey, file, options): Promise<any> {
    return Storage.put(objKey, file, options);
  }

  public download(objKey, options): Observable<any> {
    return from(Storage.get(objKey, options));
  }

  public delete(objKey,options) {
    Storage.remove(objKey, options);
  }

  /** get authenticat state */
  public isAuthenticated(): Observable<boolean> {
    return from(Auth.currentAuthenticatedUser())
      .pipe(
        map(result => {
          this.resetSessionTimeout();
          this.loggedIn.next(true);
          return true;
        }),
        catchError(error => {
          this.loggedIn.next(false);
          return of(false);
        })
      );
  }

  /** signout */
  public signOut(stayonPage?: boolean) {
    console.log("signing out");
    this.loginState.clear();
    this.loggedIn.next(false);
    this.removeTimers();
    from(Auth.signOut())
      .subscribe(
        result => {
          if(isNil(stayonPage)){
            this.router.navigate(['/']);
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  // session methods
  private removeTimers() {
    this.expiring.next(false);
    // delete timer by unsubscribing
    if (this.expiringSubscription) {
      this.expiringSubscription.unsubscribe();
    }
    if (this.expiredSubscription) {
      this.expiredSubscription.unsubscribe();
    }
  }

  public resetSessionTimeout() {
    this.removeTimers();
    // create new timers
    this.expiringSubscription = timer(25 * 60 * 1000).subscribe(x => {
      // push true to expiring subscribers
      this.expiring.next(true);
    });
    this.expiredSubscription = timer(30 * 60 * 1000).subscribe(x => {
      this.signOut();
    });
  }

  public isProcessorMode(){
    if(environment.mode === "PROCESSOR_MODE" ){
      return true;
    }
    return false;
  }

  // Only authorized approved email addresses will have Processor Access
  public isApprovedProcessor(email: string): Observable<boolean>{
    return of(false);
    // let handler = this;
    // return new Observable<boolean>(observer => {
    //   if(handler.emails) {
    //     observer.next(handler.emails.includes(email));
    //     observer.complete();
    //   }
    //   else {
    //     let filename = "SDAT_Processors.txt";
    //     let handler = this;
    //     this.download(filename, {
    //     level: 'public'
    //     })
    //     .subscribe(
    //       data => {
    //         if (!isNil(data) && !isNil(data.errorMessage)) {
    //           console.error("could not get processor file:", data);
    //           observer.next(false);
    //           observer.complete();
    //         } else {
    //           var xhr = new XMLHttpRequest();
    //           xhr.open("GET", data);
    //           xhr.responseType = "text";

    //           xhr.onload = function () {
    //               handler.emails = this.responseText;
    //               observer.next(handler.emails.includes(email));
    //               observer.complete();
    //           };
    //           xhr.send();
    //         }
    //       },
    //       error => {
    //         console.error("error getting processor list:", error);
    //         observer.next(false);
    //         observer.complete();
    //       });
    //   }
    // });
  }

  private handleError<T>(operation = "operation", result?: T) {
      return (error: any): Observable<T> => {
          // TODO: send the error to remote logging infrastructure
          console.error(error); // log to console instead

          // TODO: better job of transforming error for user consumption
          this.log('${operation} failed: ${error.message}');

          // Let the app keep running by returning an empty result.
          return of(result as T);
      };
  }

  /** Log a Attribute Service message with the MessageService */
  private log(message: string) {
      // this.messageService.add('Attribute Service: ${message}');
  }


}
