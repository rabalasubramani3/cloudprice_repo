import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { AuthService } from '../../login/services/auth.service';
import { MessageService } from '../../_services';
import { RegistrationUser } from '../components/register/registration.model';
import { ProfileUser } from '../model/profile-user.model';

const httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" })
  };

@Injectable()
export class ProfileService {

    private createProfileURL = environment.V1URL + '/rtc/bom/createprofile';
    private apiProfileDataUrl = environment.V1URL + '/rtc/bom/profiledata';
    private updateProfileURL = environment.V1URL + '/rtc/bom/updateprofile';

    constructor(private http: HttpClient,private authService: AuthService, private messageService: MessageService) {}

    public createProfile(user: RegistrationUser) {
        let profile: ProfileUser = new ProfileUser();
        profile.PROFILE_ID = user.username;
        profile.EMAIL_ADDRESS = user.email;
        profile.ESTB_ID = user.estb_id;
        return this.http.post(this.createProfileURL, profile, httpOptions)
         .pipe(
             tap(_ => this.log('createProfile')),
             catchError(this.handleError<any>('createProfile'))
        );
    }

        /**
     * Gets user profile data
     * @param profileId
     */
    getUserProfileData(username: string){
        let asc = Object.assign({}, {ProfileID: username});
        return this.http.post(this.apiProfileDataUrl, asc, httpOptions)
        .pipe(
            tap(_ => this.log('getUserProfileData')),
            catchError(this.handleError<any>("getUserProfileData"))
        );
    }

    /**
     * Updates user profile data
     * @param user: ProfileUser
     */
    updateUserProfile(profileid: string, user: ProfileUser) {
        let newUser: ProfileUser = new ProfileUser();
        newUser.ESTB_ID = user.ESTB_ID;
        newUser.EMAIL_ADDRESS = user.EMAIL_ADDRESS;
        newUser.PROFILE_ID = profileid;
        return this.http.post(this.updateProfileURL, newUser, httpOptions)
        .pipe(
             tap(_ => this.log('updateProfile')),
             catchError(this.handleError<any>('updateProfile'))
        );
    }



    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = "operation", result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log('${operation} failed: ${error.message}');

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a Attribute Service message with the MessageService */
    private log(message: string) {
        this.messageService.add('Profile Service: ${message}');
    }


}
