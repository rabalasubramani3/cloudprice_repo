import { Pipe, PipeTransform } from '@angular/core';

// simple pipe to transform phone number
// from our format (xxx-xxx-xxxx)
// to AWS format (+1xxxxxxxxxx)
@Pipe({name: 'cognito-phone'})
export class CognitoPhonePipe implements PipeTransform {
  transform(value: string): any {
    if (!value) return value;
    value = value.replace(new RegExp('-', 'g'), '');
    return '+1'+value;
  }
  parse(value: string) {
    if(!value) return value;
    if(value.substring(0, 2) !== '+1') return value;
    let newValue = value.substring(2, 5) + "-" + value.substring(5, 8) + "-" + value.substring(8,12);
    return newValue;
  }
}