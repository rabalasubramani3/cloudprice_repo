import { CommonModule, TitleCasePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ModalModule } from 'ngx-bootstrap';
import { AccessDeniedComponent } from '../authentication/components/access.denied.component';
import { SharedModule } from './../shared/shared.module';
import { ConfirmRegistrationComponent } from './components/confirm/confirmRegistration.component';
import { ForgotPassword2Component, ForgotPasswordStep1Component } from './components/forgot/forgotPassword.component';
import { LoginMessageBox } from './components/lg-msgbox.component';
import { LoginComponent } from './components/login.component';
import { LogoutComponent } from './components/logout.component';
import { MFAComponent } from './components/mfa/mfa.component';
import { NewPasswordComponent } from './components/newpassword/newpassword.component';
import { RegisterComponent } from './components/register/registration.component';
import { ResendCodeComponent } from './components/resend/resendCode.component';

export const routes = [
  { path: '', component: LoginComponent, pathMatch: 'full' },
  { path: 'register', component: RegisterComponent},
  { path: 'forgotPassword', component: ForgotPasswordStep1Component},
  { path: 'forgotPassword/:username', component: ForgotPassword2Component},  
  { path: 'resendCode', component: ResendCodeComponent},
  { path: 'confirmRegistration', component: ConfirmRegistrationComponent}        
];

@NgModule({
  declarations: [
    LoginComponent, LogoutComponent, RegisterComponent, ConfirmRegistrationComponent, ForgotPasswordStep1Component,
    MFAComponent, NewPasswordComponent, ResendCodeComponent, AccessDeniedComponent, ForgotPassword2Component,
    LoginMessageBox
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule, ModalModule, 
    RouterModule.forChild(routes)
  ],
  providers: [ TitleCasePipe  ],
  entryComponents: [
    LoginMessageBox
  ]
})
export class LoginModule {
  static routes = routes;
}
