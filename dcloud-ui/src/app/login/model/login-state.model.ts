import { RegistrationUser } from '../components/register/registration.model';

export class LoginState {
    private accountConfirmed: boolean = false;
    private emailVerified: boolean = false;
    private loggedIn: boolean = false;
    private user: RegistrationUser = null;
    private username: string = null;

    clear() {
        this.accountConfirmed = false;
        this.emailVerified = false;
        this.loggedIn = false;
        this.user = null;
        this.username = null;
    }

    getUser():RegistrationUser {
        return this.user;
    }

    getUsername():string {
        if(this.user) {
            return this.user.username;
        }
        else {
            return this.username;

        }
    }

    isAccountConfirmed(): boolean {
        return this.accountConfirmed;
    }

    isEmailVerified(): boolean {
        return this.emailVerified;
    }

    isLoggedIn(): boolean {
        return this.loggedIn;
    }

    signUp(user: RegistrationUser) {
        this.user = user;
        this.accountConfirmed = false;
        console.log("state: signUp=", this);
    }

    confirmRegistration() {
        this.user = null;
        console.log("state: confirmRegistration=", this);
    }

    emailUpdated(username: string) {
        this.loggedIn = false;
        this.emailVerified = false;
        this.username = username;
        console.log("state: emailUpdated=", this);
    }

    verifyEmail() {
        this.emailVerified = true;
        console.log("state: verifyEmail=", this);
    }

    signInFailedUnconfirmed(username: string) {
        this.username = username;
        this.accountConfirmed = false;
        console.log("state: signInFailedUnconfirmed=", this);
    }

    signedInButEmailUnverified() {
        this.accountConfirmed = true;
        this.emailVerified = false;
        console.log("state: signedInButEmailUnverified=", this);
    }

    successfulSignIn() {
        this.loggedIn = true;
        this.accountConfirmed = true;
        this.emailVerified = true;
        console.log("state: successfulSignIn=", this);
    }

    confirmSignup() {
        this.accountConfirmed = true;
        this.emailVerified = true;
        console.log("state: confirmSignup=", this);
    }

}