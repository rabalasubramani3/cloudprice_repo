import * as moment from 'moment';

export class ProfileUser
{
    constructor() {
    }
    PROFILE_ID: string;
    EMAIL_ADDRESS: string;
    ESTB_ID: string;


    fromDB(data: any) {
        this.EMAIL_ADDRESS = data[0].EmailAddress;
        this.ESTB_ID = data[0].estb_id;
    }
}
