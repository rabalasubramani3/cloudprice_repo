import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { isNil } from 'ramda';
import { AuthService } from "./../../services/auth.service";
import { LoginState } from '../../model/login-state.model';
import { LocalStorageService } from 'ngx-webstorage';

declare var jQuery: any;

@Component({
    selector: 'confirm-registration',
    templateUrl: './confirmRegistration.html',
    styleUrls: ["./confirmRegistration.style.scss"]
})

export class ConfirmRegistrationComponent {
    msg: string = "A confirmation code has been sent.  Please check your email and enter the code to continue.";
    title: string = "Resend Confirmation Code";

    router: Router;
    status: LoginState;
    errorMessage: string;
    confirmationCode: string;
    accountConfirmed = "N";

    newEmailAddressOnly: boolean = false;

    constructor(router: Router,
      private auth: AuthService,
      private storage: LocalStorageService) {
        this.router = router;
        this.onInit();
    }

    onInit() {
      this.status = this.auth.getLoginState();
      let username = this.status.getUsername();
      let loggedInUsername = this.storage.retrieve('LOGGEDUSERID');
      if(!(loggedInUsername || username)) {
        this.router.navigate(['/login']);
      }
      this.errorMessage = null;
      if(this.status.isAccountConfirmed()) this.accountConfirmed = "Y";
    }

    onConfirmRegistration() {
        let handler = this;
        let confirmationCode = this.confirmationCode ? this.confirmationCode.trim():'';
        if(!this.status.isAccountConfirmed()) {
          let username = this.status.getUsername();
          this.auth.confirmSignUp(username, confirmationCode).subscribe(
            result => {
                this.status.confirmSignup();
                this.router.navigate(["/login"]);
            },
            error => {
              if(!isNil(error.message)){
                handler.errorMessage = error.message;
              }
              console.log(error);
            }
          );
        }
        else {
          this.auth.confirmVerification(confirmationCode).subscribe(
            result => {
              this.status.successfulSignIn();
              this.router.navigate(["/app/bom/search"]);
          }, error => {
              if(!isNil(error.message)){
                handler.errorMessage = error.message;
              }
              console.error(error);
          });
        }
    }

    onResend() {
      let loggedInUsername = this.storage.retrieve('LOGGEDUSERID');
      if(!isNil(loggedInUsername)) {
          this.auth.getAuthenticatedUser().subscribe(awsUser => {
              let saveEmail = awsUser.attributes['email'];
              if(awsUser.attributes['email_verified'] === false) {
                    this.auth.updateUserEmail(awsUser, 'dev@null.xyz').subscribe(result => {
                      this.auth.updateUserEmail(awsUser, saveEmail).subscribe(result => {
                          jQuery('#app-popup').modal('show');
                      });
                    });
              }
              else {
                this.router.navigate(["/app/bom/search"]);
              }
          });
      }
      else {
        let username = this.status.getUsername();
        this.auth.resendVerificationCode(username).subscribe(result => {
          jQuery('#app-popup').modal('show');
        },
        error => {
            console.log(error);
            if(!isNil(error.message)){
                this.errorMessage = error.message;
            }
        });
      }
  }

  public onClose(){
    jQuery('#app-popup').modal('hide');
  }

}
