import { Component } from "@angular/core";
import { Router } from "@angular/router";

export class NewPasswordUser {
    username: string;
    existingPassword: string;
    password: string;
}
/**
 * This component is responsible for displaying and controlling
 * the registration of the user.
 */
@Component({
    selector: 'awscognito-angular2-app',
    templateUrl: './newpassword.html',
    styleUrls: ["./newpassword.style.scss"]
})
export class NewPasswordComponent  {
    registrationUser: NewPasswordUser;
    router: Router;
    errorMessage: string;

    onRegister() {
    }
}
