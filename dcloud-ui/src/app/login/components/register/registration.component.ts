import { isNil } from 'ramda';
import { AuthService } from "./../../services/auth.service";
import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { DatePipe } from "@angular/common";
import { CognitoPhonePipe } from '../../util/cognito-phone.pipe';
import { RegistrationUser } from './registration.model';
import { TitleCasePipe } from '@angular/common';
import { LoginState } from '../../model/login-state.model';

declare var jQuery: any;

/**
 * This component is responsible for displaying and controlling
 * the registration of the user.
 */
@Component({
  selector: "app-register-cognito",
  templateUrl: "./registration.template.html",
  styleUrls: ["./registration.style.scss"]
})
export class RegisterComponent {
  registrationUser: RegistrationUser;
  router: Router;
  errorMessage: string;
  datePipe: DatePipe;
  phonePipe: CognitoPhonePipe;
  maxDate = new Date();

  prefixes = [
    {value:"", name: ""},
    {value: "Mr.", name: "Mr."},
    {value: "Mrs.", name: "Mrs."},
    {value: "Ms.", name: "Ms."},
    {value: "Dr.", name: "Dr."}
  ];

  ssnMask = {
    mask: [
      /[0-9]/,
      /[0-9]/,
      /[0-9]/,
      /[0-9]/
    ]
  };

  dateMask = {
    mask: [/[0-1]/, /[0-9]/, "/", /[0-3]/, /[0-9]/, "/", /[1-2]/, /[0-9]/, /[0-9]/, /[0-9]/]
  };

  zipCodeMask = {
    mask: [
      /[0-9]/,
      /[0-9]/,
      /[0-9]/,
      /[0-9]/,
      /[0-9]/
    ]
  };

  phoneMask = {
    mask: [
      /[0-9]/,
      /[0-9]/,
      /[0-9]/,
      "-",
      /[0-9]/,
      /[0-9]/,
      /[0-9]/,
      "-",
      /[0-9]/,
      /[0-9]/,
      /[0-9]/,
      /[0-9]/
    ]
  };

  stateNames = [
    {value:"AL", name: "Alabama"},
    {value:"AK", name: "Alaska"},
    {value:"AZ", name: "Arizona"},
    {value:"AR", name: "Arkansas"},
    {value:"CA", name: "California"},
    {value:"CO", name: "Colorado"},
    {value:"CT", name: "Connecticut"},
    {value:"DE", name: "Delaware"},
    {value:"FL", name: "Florida"},
    {value:"GA", name: "Georgia"},
    {value:"HI", name: "Hawaii"},
    {value:"ID", name: "Idaho"},
    {value:"IL", name: "Illinois"},
    {value:"IN", name: "Indiana"},
    {value:"IA", name: "Iowa"},
    {value:"KS", name: "Kansas"},
    {value:"KY", name: "Kentucky"},
    {value:"LA", name: "Louisiana"},
    {value:"ME", name: "Maine"},
    {value:"MD", name: "Maryland"},
    {value:"MA", name: "Massachusetts"},
    {value:"MI", name: "Michigan"},
    {value:"MN", name: "Minnesota"},
    {value:"MS", name: "Mississippi"},
    {value:"MO", name: "Missouri"},
    {value:"MT", name: "Montana"},
    {value:"NE", name: "Nebraska"},
    {value:"NV", name: "Nevada"},
    {value:"NH", name: "New Hampshire"},
    {value:"NJ", name: "New Jersey"},
    {value:"NM", name: "New Mexico"},
    {value:"NY", name: "New York"},
    {value:"NC", name: "North Carolina"},
    {value:"ND", name: "North Dakota"},
    {value:"OH", name: "Ohio"},
    {value:"OK", name: "Oklahoma"},
    {value:"OR", name: "Oregon"},
    {value:"PA", name: "Pennsylvania"},
    {value:"RI", name: "Rhode Island"},
    {value:"SC", name: "South Carolina"},
    {value:"SD", name: "South Dakota"},
    {value:"TN", name: "Tennessee"},
    {value:"TX", name: "Texas"},
    {value:"UT", name: "Utah"},
    {value:"VT", name: "Vermont"},
    {value:"VA", name: "Virginia"},
    {value:"WA", name: "Washington"},
    {value:"WV", name: "West Virginia"},
    {value:"WI", name: "Wisconsin"},
    {value:"WY", name: "Wyoming"},
    {value:"-", name: "Other"},
  ];

  constructor(router: Router,
      private auth: AuthService,
      private titlecasePipe: TitleCasePipe
      ) {
    this.router = router;
    this.datePipe = new DatePipe('en-US');
    this.phonePipe = new CognitoPhonePipe();
    this.onInit();
  }

  onInit() {
    this.registrationUser = new RegistrationUser();
    this.errorMessage = null;
    jQuery(document).ready(function(){
      jQuery('#userName').focus();
    });
  }

  onRegister() {
    this.errorMessage = null;

    let transformUser = new RegistrationUser();
    transformUser.username = this.registrationUser.username;
    transformUser.email = this.registrationUser.email;
    transformUser.password = this.registrationUser.password;
    transformUser.estb_id = this.registrationUser.estb_id;

    let handler = this;
    jQuery("#frm-register")
      .parsley()
      .validate();
    if (
      jQuery("#frm-register")
        .parsley()
        .isValid()
    ) {
      this.auth.signUp(transformUser).subscribe(
        result => {
          let status = this.auth.getLoginState();
          status.signUp(transformUser);
          this.router.navigate(["/login/confirmRegistration"]);
          // this.onSubmitConfirmation(result);
        },
        error => {
          if(!isNil(error.message)){
            handler.errorMessage = error.message;
          }
          console.log(error);
        }
      );
    }
  }

  lowercaseEmail() {
    this.registrationUser.email = !isNil(this.registrationUser.email) ? this.registrationUser.email.toLowerCase() : "";
    this.registrationUser.confirmEmail = !isNil(this.registrationUser.confirmEmail) ? this.registrationUser.confirmEmail.toLowerCase() : "";
  }

  replaceNil(text:string) {
    if(isNil(text)) {
      return "";
    }
    return text;
  }

  onSubmitConfirmation(value: any) {}

  public isProcessorMode(){
    return this.auth.isProcessorMode();
  }
}
