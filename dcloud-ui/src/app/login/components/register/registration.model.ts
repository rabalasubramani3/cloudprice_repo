
export class RegistrationUser {
    constructor() {
    }
    username: string;
    estb_id: string;
    email: string;
    confirmEmail: string;
    password: string;
    confirmPassword: string;
    confirmationCode: string;
    emailVerified: boolean;

    public populate(user: any) {
      if(user) {
        this.username = user.username;
        let attributes = user.attributes;
        this.estb_id = attributes["custom:estb_id"];
        this.email = attributes['email'];
        this.emailVerified = attributes['email_verified'];
      }
    }
  }
