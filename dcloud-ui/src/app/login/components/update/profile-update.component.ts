import { DatePipe, TitleCasePipe } from "@angular/common";
import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { LocalStorageService } from 'ngx-webstorage';
import { isNil } from 'ramda';
import { ProfileUser } from '../../model/profile-user.model';
import { AuthService } from '../../services/auth.service';
import { ProfileService } from '../../services/profile.service';

declare var jQuery: any;

/**
 * This component is responsible for displaying and controlling
 * the registration of the user.
 */
@Component({
  selector: "app-profile-update",
  templateUrl: "./profile-update.template.html",
  styleUrls: ["./profile-update.style.scss"]
})
export class ProfileUpdateComponent {
  // popup settings for verification
  msg: string = "You have updated your email address.  You cannot submit an application until your email address has been validated.  Please check your email for confirmation instructions.";
  title: string = "Update Email Address";

  showErrorFlag: boolean;
  profileService: ProfileService;
  auth: AuthService;
  storage: LocalStorageService;
  profileid: string;
  user: ProfileUser = new ProfileUser();
  emailConfirm: string = "";
  saveEmail: string = "";
  establishmentId: string = "";
  router: Router;
  errorMessage: string;
  maxDate = new Date();
  validDate="1";
  constructor(router: Router, authService: AuthService, profileService: ProfileService,
        storage: LocalStorageService,
        private titlecasePipe: TitleCasePipe) {
    this.emailConfirm = "";
    this.profileService = profileService;
    this.auth = authService;
    this.storage = storage;
    this.router = router;
    this.onInit();
  }

  onInit() {
    this.profileid = this.storage.retrieve("LOGGEDUSERID");
    this.auth.getAuthenticatedUser().subscribe( awsUser => {
      if(!isNil(awsUser) && !isNil(awsUser.attributes["email"])){
        this.saveEmail = this.user.EMAIL_ADDRESS = awsUser.attributes["email"];
        this.emailConfirm = awsUser.attributes["email"];
        this.establishmentId = this.user.ESTB_ID = awsUser.attributes["custom:estb_id"];
      }
      else {
          this.showErrorFlag = true;
          this.errorMessage = "Failed to load email address.";
        }
    }, error => {
        this.showErrorFlag = true;
        this.errorMessage = "Failed to load profile.";
        console.error(error);
    });
    this.showErrorFlag = false;
    this.errorMessage = null;
  }

  // convert email to lowercase
  lowercaseEmail() {
    this.user.EMAIL_ADDRESS = !isNil(this.user.EMAIL_ADDRESS) ? this.user.EMAIL_ADDRESS.toLowerCase() : "";
    this.emailConfirm = !isNil(this.emailConfirm) ? this.emailConfirm.toLowerCase() : "";
  }

  updateProfile() {
    this.errorMessage = null;
    jQuery("#frmupdate").parsley().validate();
    if (jQuery("#frmupdate").parsley().isValid()) {
        this.auth.getAuthenticatedUser().subscribe( awsUser => {
          this.auth.updateUser(this.user, awsUser).subscribe( data => {
            if(this.saveEmail !== this.user.EMAIL_ADDRESS) {
              // pop up warning, let close button initiate navigation.
              jQuery('#app-popup').modal('show');
            }
            else {
              // navigate immediately.
              this.router.navigate(['/app/bom/search']);
            }
          }, error => {
              this.errorMessage = "Failed to update profile.";
              this.showErrorFlag = true;
              console.error("update user:",error);
            });
          }, error => {
            this.errorMessage = "Failed to update profile.";
            this.showErrorFlag = true;
            console.error("getAuthenticatedUser:", error);
          });
    }
  }

  public onClose(){
    jQuery('#app-popup').modal('hide');
    let status = this.auth.getLoginState();
    status.emailUpdated(this.user.PROFILE_ID);
    this.router.navigate(["/login/confirmRegistration"]);
  }

  preventNil(text:string) {
    if(isNil(text)) {
      return "";
    }
    return text;
  }
}
