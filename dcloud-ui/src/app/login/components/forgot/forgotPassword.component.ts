import { isNil } from 'ramda';
import { AuthService } from './../../services/auth.service';
import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

declare var jQuery: any;

@Component({
    selector: 'awscognito-angular2-app',
    templateUrl: './forgotPassword.html'
})
export class ForgotPasswordStep1Component {
    msg: string = "Please check your email for your user name and log in to continue.";
    title: string = "User Name Recovery";

    username: string;
    email: string;
    errorMessage: string;

    constructor(public router: Router,
                public auth: AuthService) {
        this.errorMessage = null;
    }

    onNext() {
        this.errorMessage = null;
        // forgot username
        if(this.email && this.email.trim().length>0) {
            this.auth.forgotUsername(this.email).subscribe( result => {
                jQuery('#app-popup').modal('show');
            }, error => {
                console.error(error);
                if(!isNil(error.message)) {
                    this.errorMessage = error.message;
                }
            });
        }
        else {
        // forgot password
            console.log("username:", this.username);
            this.auth.forgotPassword(this.username).subscribe( result => {
                this.router.navigate(['/login/forgotPassword', this.username]);
            }, error => {
                console.log(error);
                if(!isNil(error.message)){
                    this.errorMessage = error.message;
                }
            });
        }
    }

    onClose() {
        jQuery('#app-popup').modal('hide');
        this.router.navigate(['/login']);
    }
}


@Component({
    selector: 'awscognito-angular2-app',
    templateUrl: './forgotPasswordStep2.html'
})
export class ForgotPassword2Component implements OnInit, OnDestroy {

    verificationCode: string;
    username: string;
    password: string;
    errorMessage: string;
    private sub: any;

    constructor(public router: Router, public route: ActivatedRoute,
            public auth: AuthService) {
        console.log("username from the url: " + this.username);
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.username = params['username'];

        });
        this.errorMessage = null;
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    onNext() {
        this.errorMessage = null;
        this.auth.forgotPasswordSubmit(this.username, this.verificationCode, this.password)
        .subscribe(
            result => {
                this.router.navigate(['/login']);
            },
            error => {
                console.log(error);
                if(!isNil(error.message)){
                    this.errorMessage = error.message;
                }
            });  
    }
}