import { isNil } from 'ramda';
import { AuthService } from './../../services/auth.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from "ngx-webstorage";
import { ProfileUser } from '../../model/profile-user.model';

declare var jQuery: any;

@Component({
    selector: 'awscognito-angular2-app',
    templateUrl: './resendCode.html',
    styleUrls: ["./resendCode.style.scss"]
})

export class ResendCodeComponent {

    username: string;
    errorMessage: string;
    confirmName: string;

    constructor(public router: Router,
        private auth: AuthService,
        private storage: LocalStorageService) {
    }

    resendCode() {
        jQuery("#frm-resend")
        .parsley()
        .validate();
      if (
        jQuery("#frm-resend")
          .parsley()
          .isValid()
      ) {
        this.confirmName = this.storage.retrieve('LOGGEDUSERID');   
        if(!isNil(this.confirmName)) {
            this.auth.getAuthenticatedUser().subscribe(awsUser => {
                let saveEmail = awsUser.attributes['email'];
                if(awsUser.attributes['email_verified'] === false) {
//                    this.auth.updateUserEmail(awsUser, 'dev@null.xyz').subscribe(result => {
                        this.auth.updateUserEmail(awsUser, saveEmail).subscribe(result => {
                            console.log("updated");
                        });
//                    });
                }
                else {
                    this.errorMessage = "User is already confirmed.";
                }
            });
        }
        else {
            this.auth.resendVerificationCode(this.username).subscribe(
            result => {
                this.router.navigate(["/login/confirmRegistration"]);
            },
            error => {
                console.log(error);
                if(!isNil(error.message)){
                    this.errorMessage = error.message;
                }              
            });  
        }
      }        
    }

}