import { AfterViewInit, Component, TemplateRef, ViewEncapsulation, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { LocalStorageService } from "ngx-webstorage";
import { isNil } from "ramda";
import { Credentials } from "../model/credentials.model";
import { AuthService } from "../services/auth.service";
import { ProfileService } from '../services/profile.service';
import { environment } from './../../../environments/environment';
import { OnComponentErrorHandler } from './../../core/coreerror.handler';
import { ShareDataService } from './../../core/sharedata.service';
import { LoginMessageBox } from './lg-msgbox.component';
import { RegistrationUser } from './register/registration.model';
import { LoginState } from '../model/login-state.model';

declare var jQuery: any;

// Step 1: User logs in
// Step 2:
@Component({
  selector: "login",
  styleUrls: ["./login.style.scss"],
  templateUrl: "./login.template.html",
  encapsulation: ViewEncapsulation.None,
  providers:[ ShareDataService, BsModalService ],
  host: {
    class: "login-page app"
  }
})
export class LoginComponent implements OnInit, AfterViewInit, OnComponentErrorHandler {

  model = new Credentials("", "");
  showErrorFlag: boolean;
  alerts: Array<Object>;
  currentYear: string;
  loginData: any;
  loginTitle: string;
  isLoginButtonClicked: boolean;

  bsModalRefSplash: BsModalRef;
  bsModalRefMessageBox: BsModalRef;

  private modPrefix = "TXC";

  constructor(
    private storage: LocalStorageService,
    private auth: AuthService,
    private profile: ProfileService,
    private sharedata: ShareDataService,
    private modalService: BsModalService,
    private router: Router
  ) {
    this.alerts = [
      {
        type: "warning",
        msg: '<span class="fw-semi-bold">Warning:</span> Error Logging in'
      }
    ];

    this.loginTitle = "Customer Login";
    this.showErrorFlag = false;
    this.currentYear = new Date().getFullYear().toString();
  }

  ngOnInit(): void {
    this.showErrorFlag = false;
    this.isLoginButtonClicked = false;
    if(this.auth.isProcessorMode()){
      this.loginTitle = "FDA Reviewer Login";
    }
  }

  ngAfterViewInit() {
    jQuery("#uname").focus();
    this.clearLocalStorageContent();
    this.model.username = "raghubala";
    this.model.password = "!NN0vation!";
  }

  onResetError(): void {
    this.showErrorFlag = false;
    this.alerts = [];
  }

  onShowError(error: any): void {
    this.showErrorFlag = true;
    this.alerts.push({ type: "warning", msg: error });
  }

  onShowBodyError(error: any): void {
    this.showErrorFlag = true;
    this.alerts.push({ type: "warning", msg: error });
  }

  clearLocalStorageContent() {
    this.storage.clear(this.modPrefix + 'RNF');
    this.storage.clear(this.modPrefix + 'NMF');
    this.storage.clear(this.modPrefix + 'SSNF');
    this.storage.clear(this.modPrefix + 'CYF');
    this.storage.clear(this.modPrefix + 'CODEF');
    this.storage.clear(this.modPrefix + 'FLAGF');
    this.storage.clear(this.modPrefix + 'COUNTYF');
  }

  public displayConfirmationMessage(msgtoDisplay) {
    const initialState = {
      message: msgtoDisplay,
      title: 'Need Email Verification'
    };
    this.bsModalRefMessageBox = this.modalService.show(LoginMessageBox, { initialState });
    this.bsModalRefMessageBox.content.closeBtnName = 'Close';
  }

  public displaySplashDialog(template: TemplateRef<any>) {
    // tslint:disable-next-line: max-line-length
    this.bsModalRefSplash = this.modalService.show(template, { class: 'modal-lg modal-center', animated: true, keyboard: false, backdrop: true, ignoreBackdropClick: true });
  }

  public proceedtoLandingPage(){
      this.router.navigate(["/app/bom/search"]);
  }

  // Step 6: Send check for verification email
  public verifySendEmail(localUser:RegistrationUser){
    let handler = this;

    // Step 6a: checking for verification
    this.auth.verifyemail(localUser.email).subscribe(
      data => {

        if(!isNil(data.result)){
          if(data.result !== "Success"){

            // Step 6b: Prompt user for email verification message
            handler.displayConfirmationMessage(data.result);
          }

        }else {

          if(!isNil(data)){
            console.log(data);
          }

        }

        handler.proceedtoLandingPage();

      },
      error => {
        handler.onResetError();
      }
    );

  }

  // Step 5: Create user profile
  public createUserProfile() {
    let handler = this;
    // Step 5a: Get Cognito user name
    this.auth.getAuthenticatedUser().subscribe(user => {
      let localUser: RegistrationUser = new RegistrationUser();
      localUser.populate(user);

      // If processor mode - don't touch user profile - skip create profile for Processor
      if (this.isProcessorMode()) {
        console.log("HANDLER:",handler);
        // Check if user is authorized to be a processor
        handler.auth.isApprovedProcessor(localUser.email).subscribe(result =>{
          let approved: boolean = result;
          if(approved) {
            handler.verifySendEmail(localUser);
          }
          else {
            handler.onShowError("User not authorized!");
            handler.auth.signOut(true);
            return;
          }
        });
      } else {
        // Check if user is authorized to be a processor
        handler.auth.isApprovedProcessor(localUser.email).subscribe(result => {
          let approved: boolean = result;
          if(approved) {
            handler.onShowError("User not authorized!");
            handler.auth.signOut(true);
            return;
          }
          else {

            this.verifySendEmail(localUser);
            // // Step 5b: Get user profile data from DB
            // handler.profile.getUserProfileData(localUser.username).subscribe( profile => {

            //   // if profile not found
            //   if(isNil(profile) || (profile.length === 0) || ((profile.length > 0) && (isNil(profile[0].FirstName)))) {

            //     // Step: 5c Create user profile
            //     // handler.profile.createProfile(localUser).subscribe(
            //     //   data => {

            //     //     // Step 6: Send check for verification email
            //     //     this.verifySendEmail(localUser);

            //     //   }
            //     // );
            //   }else {
            //     // Step 6: Send check for verification email
            //     this.verifySendEmail(localUser);
            //   }
            // }, error => {

            //   console.log("Error in getUserProfileData");

            //   // Step: 5c Create user profile
            //   // handler.profile.createProfile(localUser).subscribe(
            //   //   data => {

            //   //     // Step 6: Send check for verification email
            //   //     this.verifySendEmail(localUser);
            //   //   }
            //   // );

            // });

          }
        });

      }
    });
  }

  // Step 1: User logins
  public login() {
    this.proceedtoLandingPage();
    // // MAR-208 - Prevent user from clicking the login button multiple times
    // if(this.isLoginButtonClicked){
    //   return true;
    // }
    // this.clearLocalStorageContent();
    // this.onResetError();

    // jQuery("#frm-login").parsley().validate();
    // if (jQuery("#frm-login").parsley().isValid()) {
    //     let handler = this;
    //     this.isLoginButtonClicked = true;
    //     let status: LoginState = this.auth.getLoginState();
    //     // Step 2: See if user is authorized to login
    //     this.auth.signIn(this.model.username, this.model.password).subscribe(
    //       result => {
    //         this.storage.store('LOGGEDUSERID', result.username);
    //         this.storage.store("ESTABLISHMENTID", result.attributes["custom:estb_id"]);
    //         this.auth.getAuthenticatedUser().subscribe( awsUser => {
    //           if(awsUser.attributes['email_verified']===false) {
    //             status.signedInButEmailUnverified();
    //             this.router.navigate(["/login/confirmRegistration"]);
    //           }
    //           else {
    //             // Step 3: If authorized, display WARNING splash dialog
    //             status.successfulSignIn();
    //             handler.createUserProfile();
    //             // handler.displaySplashDialog(template);
    //           }
    //         });
    //       },
    //       error => {
    //         console.error(error.code);
    //         if(error.code === "UserNotConfirmedException") {
    //           status.signInFailedUnconfirmed(this.model.username);
    //           this.router.navigate(["/login/confirmRegistration"]);
    //         }
    //         if (!isNil(error.code)) {
    //           this.onShowError(error.message);
    //         } else {
    //           this.onShowError(error);
    //         }
    //         this.isLoginButtonClicked = false;
    //       }
    //     );
    //   }
  }

  public logout() {
    this.router.navigate(["login"]);
  }

  // Step 3a: If user agrees or cancels the WARNING popup
  public closeSplashDialog(agreeflag:boolean) {
    this.bsModalRefSplash.hide();
    this.bsModalRefSplash = null;
    this.isLoginButtonClicked = false;
    // Step 4: If user agrees, create user profile
    if(agreeflag){

      // Step 5: Create user profile
      this.createUserProfile();

    }else {

      // Step 3b: logout
      this.logout();

    }
  }

  public isProcessorMode(){
    return this.auth.isProcessorMode();
  }
}
