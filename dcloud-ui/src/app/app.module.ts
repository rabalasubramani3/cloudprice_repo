import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PreloadAllModules, RouterModule } from '@angular/router';
import { CookieService } from 'angular2-cookie';
import { AppComponent } from './app.component';
import { AppConfig } from './app.config';
import { ROUTES } from './app.routes';
import { AuthorizationHelper } from './authentication/util/authorization.helper.util';
import { ErrorComponent } from './error/error.component';
import { HomeComponent } from './home.component';
import { AuthGuard } from './login/services/auth-guard.service';
import { AuthService } from './login/services/auth.service';
import { ProfileService } from './login/services/profile.service';
import { CognitoPhonePipe } from './login/util/cognito-phone.pipe';
import { JwtUtil } from './login/util/jwt.util';
import { JwtInterceptor } from './_helpers';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';


const APP_PROVIDERS = [
  AppConfig
];

@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    ErrorComponent,
    HomeComponent,
    CognitoPhonePipe,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgMultiSelectDropDownModule.forRoot(),
    RouterModule.forRoot(ROUTES, {
      useHash: false,
      preloadingStrategy: PreloadAllModules
    })
  ],
  providers: [
    APP_PROVIDERS,
    AuthGuard,
    AuthService,
    ProfileService,
    JwtUtil,
    AuthorizationHelper,
    CookieService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
  ]
})
export class AppModule {}
