export const environment = {
  production: true,
  mode: "PROCESSOR_MODE", // option : APPLICANT_MODE or PROCESSOR_MODE
  V1URL: 'https://ucwm3kn8kh.execute-api.us-east-1.amazonaws.com/dev',
  PRODUCTSURL: 'https://elpc8uddy4.execute-api.us-east-1.amazonaws.com/dev',
  amplify: {
    Auth: {
      identityPoolId: 'us-east-1:072912bc-2af2-4529-828f-16966b12cb50',
      region: 'us-east-1',
      userPoolId: 'us-east-1_mjN41ZFZs',
      userPoolWebClientId: '2p15maa902d3ki9ucasam8j36v'
    },
    Storage: {
        bucket: 'testrtc',
        region:  'us-east-1', //Specify the region your bucket was created in;
    }
  }
};
