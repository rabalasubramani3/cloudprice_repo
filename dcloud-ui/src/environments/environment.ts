export const environment = {
  production: false,
  mode: "APPLICANT_MODE", // option : APPLICANT_MODE or PROCESSOR_MODE
  // APIURL: 'http://18.205.5.13:8443',
  APIURL: 'http://localhost:8889',
  V1URL: 'https://ucwm3kn8kh.execute-api.us-east-1.amazonaws.com/dev',
  V2URL: 'https://elpc8uddy4.execute-api.us-east-1.amazonaws.com/dev',
  PRODUCTSURL: 'https://elpc8uddy4.execute-api.us-east-1.amazonaws.com/dev',
  amplify: {
    Auth: {
      identityPoolId: 'us-east-1:72cd09bf-d172-4f52-b53c-8f51b77a5bb3',
      region: 'us-east-1',
      userPoolId: 'us-east-1_t2KvD36Ot',
      userPoolWebClientId: '5478djc5724f83udotnhl77inf'
    },
    Storage: {
        bucket: 'fda-trlm-poc', //Your bucket ARN;
        region:  'us-east-1', //Specify the region your bucket was created in;
    }
  }
};
