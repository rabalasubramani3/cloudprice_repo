@ECHO OFF
IF %1== 1 GOTO :SUITE1
IF %1== 2 GOTO :SUITE2
IF %1== 3 GOTO :SUITE3
IF %1== 4 GOTO :SUITE4
IF %1== 5 GOTO :SUITE5

:SUITE1
ECHO "Running Suite1 Tests @ Port 3030"
ng e2e --suite=suite1 --port=3030 --webdriverUpdate=false
pause
EXIT

:SUITE2
ECHO "Running Suite2 Tests @ Port 3031"
ng e2e --suite=suite2 --port=3031 --webdriverUpdate=false
pause
EXIT

:SUITE3
ECHO "Running Suite3 Tests @ Port 3032"
ng e2e --suite=suite3 --port=3032 --webdriverUpdate=false
pause
EXIT

:SUITE4
ECHO "Running Suite4 Tests @ Port 3033"
ng e2e --suite=suite4 --port=3033 --webdriverUpdate=false
pause
EXIT

:SUITE5
ECHO "Running Suite5 Tests @ Port 3034"
ng e2e --suite=suite5 --port=3034 --webdriverUpdate=false
pause
EXIT
